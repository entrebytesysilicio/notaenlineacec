<?php require_once('Connections/MySQL.php'); ?><?php
if (!isset($_SESSION)) {
  session_start();
}
$MM_authorizedUsers = "Administrador,Profesor";
$MM_donotCheckaccess = "false";

// *** Restrict Access To Page: Grant or deny access to this page
function isAuthorized($strUsers, $strGroups, $UserName, $UserGroup) { 
  // For security, start by assuming the visitor is NOT authorized. 
  $isValid = False; 

  // When a visitor has logged into this site, the Session variable MM_Username set equal to their username. 
  // Therefore, we know that a user is NOT logged in if that Session variable is blank. 
  if (!empty($UserName)) { 
    // Besides being logged in, you may restrict access to only certain users based on an ID established when they login. 
    // Parse the strings into arrays. 
    $arrUsers = Explode(",", $strUsers); 
    $arrGroups = Explode(",", $strGroups); 
    if (in_array($UserName, $arrUsers)) { 
      $isValid = true; 
    } 
    // Or, you may restrict access to only certain users based on their username. 
    if (in_array($UserGroup, $arrGroups)) { 
      $isValid = true; 
    } 
    if (($strUsers == "") && false) { 
      $isValid = true; 
    } 
  } 
  return $isValid; 
}

$MM_restrictGoTo = "error.php";
if (!((isset($_SESSION['MM_Username'])) && (isAuthorized("",$MM_authorizedUsers, $_SESSION['MM_Username'], $_SESSION['MM_UserGroup'])))) {   
  $MM_qsChar = "?";
  $MM_referrer = $_SERVER['PHP_SELF'];
  if (strpos($MM_restrictGoTo, "?")) $MM_qsChar = "&";
  if (isset($QUERY_STRING) && strlen($QUERY_STRING) > 0) 
  $MM_referrer .= "?" . $QUERY_STRING;
  $MM_restrictGoTo = $MM_restrictGoTo. $MM_qsChar . "accesscheck=" . urlencode($MM_referrer);
  header("Location: ". $MM_restrictGoTo); 
  exit;
}
?>
<?php
function GetSQLValueString($theValue, $theType, $theDefinedValue = "", $theNotDefinedValue = "") 
{
  $theValue = (!get_magic_quotes_gpc()) ? addslashes($theValue) : $theValue;

  switch ($theType) {
    case "text":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;    
    case "long":
    case "int":
      $theValue = ($theValue != "") ? intval($theValue) : "NULL";
      break;
    case "double":
      $theValue = ($theValue != "") ? "'" . doubleval($theValue) . "'" : "NULL";
      break;
    case "date":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;
    case "defined":
      $theValue = ($theValue != "") ? $theDefinedValue : $theNotDefinedValue;
      break;
  }
  return $theValue;
}

$editFormAction = $_SERVER['PHP_SELF'];
if (isset($_SERVER['QUERY_STRING'])) {
  $editFormAction .= "?" . htmlentities($_SERVER['QUERY_STRING']);
}

if ((isset($_POST["MM_insert"])) && ($_POST["MM_insert"] == "form1")) {
  $insertSQL = sprintf("INSERT INTO desempeno (desempeno, notaMinima, notaMaxima, ano) VALUES (%s, %s, %s, %s)",
                       GetSQLValueString($_POST['desempeno'], "text"),
                       GetSQLValueString($_POST['notaMinima'], "double"),
                       GetSQLValueString($_POST['notaMaxima'], "double"),
                       GetSQLValueString($_POST['ano'], "int"));

  mysql_select_db($database_MySQL, $MySQL);
  $Result1 = mysql_query($insertSQL, $MySQL) or die(mysql_error());


  $insertGoTo = "DesempenoCrear.php";
  if (isset($_SERVER['QUERY_STRING'])) {
    $insertGoTo .= (strpos($insertGoTo, '?')) ? "&" : "?";
    $insertGoTo .= $_SERVER['QUERY_STRING'];
  }
  header(sprintf("Location: %s", $insertGoTo));
}

mysql_select_db($database_MySQL, $MySQL);
$query_rsDesempeno = "SELECT * FROM desempeno where ano = ".date("Y")." ORDER BY ano,notaMinima ASC";
$rsDesempeno = mysql_query($query_rsDesempeno, $MySQL) or die(mysql_error());
$row_rsDesempeno = mysql_fetch_assoc($rsDesempeno);
$totalRows_rsDesempeno = mysql_num_rows($rsDesempeno);
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title>:: Desempeno ::</title>
<? include "header.php";?>
<script src="SpryAssets/SpryCollapsiblePanel.js" type="text/javascript"></script>
<link href="SpryAssets/SpryCollapsiblePanel.css" rel="stylesheet" type="text/css" />
</head>

<body>
<? include "menu.php";?>

<div id="CollapsiblePanel1" class="CollapsiblePanel">
  <div class="CollapsiblePanelTab" tabindex="0">Crear desempe�o...</div>
  <div class="CollapsiblePanelContent">
    <form method="post" name="form1" action="<?php echo $editFormAction; ?>"><br />
    <table align="center">
    <thead><tr><td colspan="2">Crear desempe�o</td></tr></thead>
      <tr >
        <td >A�o:</td>
        <td><input type="text" name="ano" value="<? echo date('Y'); ?>" size="32"></td>
      </tr>	
      <tr >
        <td >Desempe&ntilde;o:</td>
        <td><input type="text" name="desempeno" value="" size="32"></td>
      </tr>
      <tr >
        <td >Nota M&iacute;nima:</td>
        <td><input type="text" name="notaMinima" value="" size="32"></td>
      </tr>
      <tr >
        <td >Nota M&aacute;xima:</td>
        <td><input type="text" name="notaMaxima" value="" size="32"></td>
      </tr>
		<thead>
      <tr>
        <td colspan="2"><input type="submit" value="Crear"></td>
      </tr></thead>
    </table>
    <input type="hidden" name="MM_insert" value="form1"><br />
  </form>
  </div>
</div>
<br />
  <table border="1" align="center">
  <thead>
    <tr>
      <td>No.</td>
      <td>A�o</td>	  
      <td>Desempe&ntilde;o</td>
      <td>Nota M&iacute;nima</td>
      <td>Nota M&aacute;xima</td>

	  <td>Acciones</td>
    </tr>
    </thead>
    <?php $i = 1; do { ?>
      <tr>
        <td align="center"><? echo $i; $i++; ?></td>
        <td><?php echo $row_rsDesempeno['ano']; ?></td>		
        <td><?php echo $row_rsDesempeno['desempeno']; ?></td>
        <td align="center"><?php echo $row_rsDesempeno['notaMinima']; ?></td>
        <td align="center"><?php echo $row_rsDesempeno['notaMaxima']; ?></td>

		<td><a href="DesempenoActualizar.php?idDesempeno=<?php echo $row_rsDesempeno['idDesempeno']; ?>">Actualizar</a> <a href="DesempenoEliminar.php?idDesempeno=<?php echo $row_rsDesempeno['idDesempeno']; ?>" onclick="return confirm('Desea eliminar el registro?.');">Eliminar</a></td>
      </tr>
      <?php } while ($row_rsDesempeno = mysql_fetch_assoc($rsDesempeno)); ?>
  </table>

<script type="text/javascript">
var CollapsiblePanel1 = new Spry.Widget.CollapsiblePanel("CollapsiblePanel1");
</script>
</body>
</html>
<?php
mysql_free_result($rsDesempeno);
?>
