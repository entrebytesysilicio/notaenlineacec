<?php require_once('Connections/MySQL.php'); ?>
<?php
if (!isset($_SESSION)) {
session_start();
}
$MM_authorizedUsers = "Administrador,Profesor";
$MM_donotCheckaccess = "false";
// *** Restrict Access To Page: Grant or deny access to this page
function isAuthorized($strUsers, $strGroups, $UserName, $UserGroup) {
// For security, start by assuming the visitor is NOT authorized.
$isValid = False;
// When a visitor has logged into this site, the Session variable MM_Username set equal to their username.
// Therefore, we know that a user is NOT logged in if that Session variable is blank.
if (!empty($UserName)) {
// Besides being logged in, you may restrict access to only certain users based on an ID established when they login.
// Parse the strings into arrays.
$arrUsers = Explode(",", $strUsers);
$arrGroups = Explode(",", $strGroups);
if (in_array($UserName, $arrUsers)) {
$isValid = true;
}
// Or, you may restrict access to only certain users based on their username.
if (in_array($UserGroup, $arrGroups)) {
$isValid = true;
}
if (($strUsers == "") && false) {
$isValid = true;
}
}
return $isValid;
}
$MM_restrictGoTo = "error.php";
if (!((isset($_SESSION['MM_Username'])) && (isAuthorized("",$MM_authorizedUsers, $_SESSION['MM_Username'], $_SESSION['MM_UserGroup'])))) {
$MM_qsChar = "?";
$MM_referrer = $_SERVER['PHP_SELF'];
if (strpos($MM_restrictGoTo, "?")) $MM_qsChar = "&";
if (isset($QUERY_STRING) && strlen($QUERY_STRING) > 0)
$MM_referrer .= "?" . $QUERY_STRING;
$MM_restrictGoTo = $MM_restrictGoTo. $MM_qsChar . "accesscheck=" . urlencode($MM_referrer);
header("Location: ". $MM_restrictGoTo);
exit;
}
?>
<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<?php include "header.php"; ?>
		<title>:: Imprimir matr&iacute;cula ::</title>
		<!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
		<!-- Bootstrap -->
		<link href="css/bootstrap.min.css" rel="stylesheet">
		<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
		<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
		<!--[if lt IE 9]>
		<script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
		<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
		<![endif]-->
                <style type="text/css">
            @page {
                margin-left: 30px;
                margin-right: 30px;
                margin-top: 30px;
                margin-bottom: 30px;
                size: letter;
                                size:portrait;
            }

            @media all {
                .page-break {
                    display: none;
                }
            }

            @media print {
                .page-break {
                    display: block;
                    page-break-before: always;
                }
            }
hr {
    margin: 5px;
}

li,td,legend {
padding: 5px 15px !important;
}
</style>

	</head>
	<body>
		<?
		if(isset($_GET['idMatricula']))
		{
		$sql = "select m.fecha,
		m.folio,
		m.matricula,
		m.idAlumno,
		m.idCurso,
		m.idMatricula,
		year(m.fechaMatricula) as matriculaAno,
		monthname(m.fechaMatricula) as matriculaMes,
		day(m.fechaMatricula) as matriculaDia ,
		c.curso,
		c.ano as cursoAno
		from matricula as m
		inner join curso as c on c.idCurso = m.idCurso
		where m.idMatricula=".$_GET['idMatricula'];

		if($resultado = mysql_query($sql,$MySQL))
		{
		$fila = mysql_fetch_array($resultado);
		?>
		<div class="container">
			<div class="row">
				<div class="col-xs-3 col-sm-3 col-md-3 col-lg-3">
					<img src="imagenes/logo.png">
				</div>
				<div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
					<!--Titulo-->
					<p class="text-center h5">
						V.J.E. <br>
						COLEGIO EUCAR&Iacute;STICO CAMPESTRE <br><br>
						HOJA DE MATR&Iacute;CULA
					</P>
				</div>
				<div class="col-xs-3 col-sm-3 col-md-3 col-lg-3 text-left">
					<!--Folio-->
					<table class="text-right table" align="right">
						<tr>
							<td class="h6 text-muted">Folio:</td>
							<td class="h6 text-info"><?php echo $fila['folio']; ?></td>
						</tr>
						<tr>
							<td class="h6 text-muted">Matr&iacute;cula:</td>
							<td class="h6 text-info"><?php echo $fila['matricula']; ?></td>
						</tr>
						<tr>
							<td class="h6 text-muted">C&oacute;digo:</td>
							<td class="h6 text-info"><?php echo $fila['idMatricula']; ?></td>
						</tr>
					</table>
				</div>
			</div>
			<?
			$alumnoSql = "select * from alumno where idAlumno = ".$fila['idAlumno'];
			if($alumnoResultado = mysql_query($alumnoSql,$MySQL))
			{
			$alumnoFila = mysql_fetch_array($alumnoResultado);
			}
			else
			{
			?>
			<div class="alert alert-danger">
				Error al consultar la informaci&oacute;n del alumno.
			</div>
			<?
			}
			?>
			<div class="row">
				<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
					<table class=" table"  style="width: 100%; ">
						<tr>
							<td class="h6 text-muted" >Lugar y fecha</td>
							<td class="h6 text-info">Subachoque, <?php echo $fila['matriculaMes']; ?>, <?php echo $fila['matriculaDia']; ?> de <?php echo $fila['matriculaAno']; ?></td> 
						<!--	<td class="h6 text-info">Subachoque, Diciembre 9 de 2017</td> -->
							<td class="h6 text-muted">RH</td>
							<td class="h6 text-info"><?php echo $alumnoFila['grupoSanguineo']; ?></td>
						</tr>
						<tr>
							<td class="h6 text-muted">Nombre del estudiante </td>
							<td class="h6 text-info"><?php echo $alumnoFila['nombres'].' '.$alumnoFila['apellidos']; ?></td>
							<td class="h6 text-muted">Curso</td>
							<td class="h6 text-info"><?php echo $fila['curso']." (".$fila['cursoAno'].")"; ?></td>
						</tr>
						<tr>
							<td class="h6 text-muted">Documento de identidad</td>
							<td class="h6 text-info"><?php echo $alumnoFila['identificacion']; ?></td>
							<td class="h6 text-muted">De</td>
							<td class="h6 text-info"><?php echo $alumnoFila['nacimientoLugar']; ?></td>
						</tr>
						<tr>
							<td class="h6 text-muted">Fecha de nacimiento</td>
							<td class="h6 text-info"><?php echo $alumnoFila['nacimientoFecha']; ?></td>
							<td class="h6 text-muted">Edad</td>
							<td class="h6 text-info"><?php echo date("Y-m-d")-$alumnoFila['nacimientoFecha']; ?> </td>
						</tr>
						<tr>
							<td class="h6 text-muted">Direcci&oacute;n</td>
							<td class="h6 text-info"><?php echo $alumnoFila['direccionResidencia']; ?></td>
							<td class="h6 text-muted">Tel&eacute;fono</td>
							<td class="h6 text-info"><?php echo $alumnoFila['telefono']; ?></td>
						</tr>
						<tr>
							<td class="h6 text-muted">Nombre del padre</td>
							<td class="h6 text-info"><?php echo $alumnoFila['padreNombre']; ?></td>
							<td class="h6 text-muted">Profesi&oacute;n</td>
							<td class="h6 text-info"><?php echo $alumnoFila['padreProfesion']; ?></td>
						</tr>
						<tr>
							<td class="h6 text-muted">Nombre de la madre</td>
							<td class="h6 text-info"><?php echo $alumnoFila['madreNombre']; ?></td>
							<td class="h6 text-muted">Profesi&oacute;n</td>
							<td class="h6 text-info"><?php echo $alumnoFila['madreProfesion']; ?></td>
						</tr>
					</table>
				</div>
			</div>
			<div class="row">
				<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
					<!--Record escolar-->
					<legend class="h6 text-center">Record escolar</legend>
					<?
					$cursosSql = "select
					c.curso,
					c.ano
					from alumnocurso as ac
					inner join curso as c on ac.idCurso = c.idCurso
					where ac.idAlumno = ".$fila['idAlumno']." order by c.orden asc";


$cursosSql = "select curso.ano, curso.curso, '' as institucion
		from alumnocurso 
		inner join curso 
			on alumnocurso.idCurso = curso.idCurso 
		where idAlumno = ".$fila['idAlumno']."
		union
		select ano, curso, institucion
		from cursootrasinstituciones
		where idAlumno = ".$fila['idAlumno']."
		order by 1 desc, 2 asc";

					if($cursosResultado = mysql_query($cursosSql))
					{
					if(mysql_num_rows($cursosResultado) > 0)
					{
					?>
					<div class="row">
						<div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
						<ul class="list-group">
							<?
							for($i = 1; $i<=7;$i++)
							{
                                                                        ?>
                                                                        <li class="list-group-item">
                                                                        <?

								if($cursosFila = mysql_fetch_array($cursosResultado))
								{
									echo $cursosFila['ano']; ?> - <? echo $cursosFila['curso']; ?> - <? echo $cursosFila['institucion']; 
								}
                                                                        ?>
                                                                        &nbsp;
                                                                        </li>
                                                                        <?

							}
							?>
							</ul>
						</div>
						<div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
						<ul class="list-group">
							<?
							for($i = 1; $i<=7;$i++)
							{
                                                                       ?>
                                                                        <li class="list-group-item">
                                                                        <?

								if($cursosFila = mysql_fetch_array($cursosResultado))
								{
									echo $cursosFila['ano']; ?> - <? echo $cursosFila['curso']; 
								}
                                                                        ?>
                                                                        &nbsp;
                                                                        </li>
                                                                        <?

							}
							?>
							</ul>
						</div>						
					</div>
					<?
					}
					else
					{
					?>
					Aún no ha estudiado en esta instituci&oacute;n.
					<?
					}
					}
					else
					{
					?>
					<div class="alert alert-danger">
						Error al consultar los cursos del alumno. <br><br><?php echo mysql_error(); ?>
					</div>
					<?
					}
					?>
				</div>
			</div>
			<div class="row">
			<legend class="h6 text-center">Acudiente</legend>
				<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
					<!--Acudientes-->			
			<?php 
			$acudientesSql ="select nombre, direccion, telefono, celular 
				from alumnoacudiente as aa
				inner join usuario as u on aa.idAcudiente = u.idUsuario
				where aa.idAlumno = ".$fila['idAlumno'];
			if($acudientesResultado = mysql_query($acudientesSql))
			{
				if(mysql_num_rows($acudientesResultado) > 0)
				{
					?>
					<table class="table">
							<tr>
								<td class="h6 text-muted">Acudiente</td>
								<td class="h6 text-muted">Direcci&oacute;n</td>
								<td class="h6 text-muted">Tel&eacute;fono</td>
							</tr>
						<?php 
						$acudientesFila = mysql_fetch_array($acudientesResultado);
						do {
							?>
							<tr>
								<td class="h6 text-info"><?php echo $acudientesFila['nombre']; ?></td>
								<td class="h6 text-info"><?php echo $acudientesFila['direccion']; ?></td>
								<td class="h6 text-info"><?php echo $acudientesFila['telefono']; ?><?php echo $acudientesFila['celular'];?> </td>
							</tr>
							<?
						} while ($acudientesFila = mysql_fetch_array($acudientesResultado));
						 ?>
					</table>
					<?
					
				}
				else 
				{
					?>
					<div class="alert alert-warning">
					El estudiante no tiene acudientes.
					</div>	
					<?
				}
			}
			else
			{
				?>
				<div class="alert alert-danger">
				Error al consultar la informaci&oacute;n de los acudientes acudientes.
				</div>
				<?
			}


			 ?>

				</div>
			</div>
			<div class="row">
			<legend class="h6 text-center">Observaciones</legend>
				<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
				<hr><hr>
				<p class="text-center text-info h5">Nos comprometemos a cumplir el MANUAL DE CONVIVENCIA Y REGLAMENTO ESCOLAR de la INSTITUCI&Iacute;N</p>
					
				</div>
			</div>

			<div class="row">
				<div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
					<br><br>
					<hr>
					<p class="h6 text-info text-center">Estudiante</p>
					<br><br>
					<hr>
					<p class="h6 text-info text-center">Rectora</p>					
				</div>
				<div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
					
				</div>
				<div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
					<br><br>
					<hr>
					<p class="h6 text-info text-center">Padre o acudiente</p>
					<br><br>
					<hr>
					<p class="h6 text-info text-center">Secreataria</p>						
				</div>								
			</div>

			<p class="text-muted text-center"><?php echo "Fecha de impresi&oacute;n: ".date("Y")."/".date("M")."/".date("d")." ".date("H:i"); ?></p>
		</div>
		<?
		}
		else
		{
		?>
		<div class="alert alert-danger">
			Error al consultar la informaci&oacute;n de las matr&iacute;culas. <br><?php echo mysql_error(); ?>
		</div>
		<?
		}
		}
		else
		{
		?>
		<div class="alert alert-danger">
			No a indicado el id de la matr&iacute;cula.
		</div>
		<?
		}
		?>

	</body>
</html>
