<?php
////Listado de alumnos perdiendo la materia
////Este listado puede llamarce (Recuperación Final)
////Filtros:
////Año (Debería ser solo el año actual)
////Materia (Si es profesor solo le muestra sus materias asignadas, si es administrador muestra todo)
////Curso (Muestra los cursos que tiene asignados el profesor con la materia seleccionada)
////
////Cada vez que se selecciona un filtro, se reduce la consulta, inicialmente se muestra todo el listado.
////Se debe colocar una entrada de datos, para colocar la nota de recuperación final.

if (!isset($_SESSION)) {
  session_start();
}
$MM_authorizedUsers = "Administrador,Profesor";
$MM_donotCheckaccess = "false";

// *** Restrict Access To Page: Grant or deny access to this page
function isAuthorized($strUsers, $strGroups, $UserName, $UserGroup) { 
  // For security, start by assuming the visitor is NOT authorized. 
  $isValid = False; 

  // When a visitor has logged into this site, the Session variable MM_Username set equal to their username. 
  // Therefore, we know that a user is NOT logged in if that Session variable is blank. 
  if (!empty($UserName)) { 
    // Besides being logged in, you may restrict access to only certain users based on an ID established when they login. 
    // Parse the strings into arrays. 
    $arrUsers = Explode(",", $strUsers); 
    $arrGroups = Explode(",", $strGroups); 
    if (in_array($UserName, $arrUsers)) { 
      $isValid = true; 
    } 
    // Or, you may restrict access to only certain users based on their username. 
    if (in_array($UserGroup, $arrGroups)) { 
      $isValid = true; 
    } 
    if (($strUsers == "") && false) { 
      $isValid = true; 
    } 
  } 
  return $isValid; 
}

$MM_restrictGoTo = "error.php";
if (!((isset($_SESSION['MM_Username'])) && (isAuthorized("",$MM_authorizedUsers, $_SESSION['MM_Username'], $_SESSION['MM_UserGroup'])))) {   
  $MM_qsChar = "?";
  $MM_referrer = $_SERVER['PHP_SELF'];
  if (strpos($MM_restrictGoTo, "?")) $MM_qsChar = "&";
  if (isset($QUERY_STRING) && strlen($QUERY_STRING) > 0) 
  $MM_referrer .= "?" . $QUERY_STRING;
  $MM_restrictGoTo = $MM_restrictGoTo. $MM_qsChar . "accesscheck=" . urlencode($MM_referrer);
  header("Location: ". $MM_restrictGoTo); 
  exit;
}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title>:: Reporte Resumen de Desempe&ntilde;o ::</title>
<? include "header.php"; ?>

</head>

<body>
<br>
<center>
<h3>Alumnos en cuarto periodo que tienen materias perdidas</h3>
</center>
<br>
<table border="1" align="center" cellspacing="0" cellpadding="3">
<tr>
<td>
A&ntilde;o:
</td>
<td>
<input type="text" name="ano" id="ano" readonly="readonly" value="<? echo date("Y"); ?>"/>
</td>
<td>
Materia:
</td>
<td>
<?
////Listado de materias que tiene asignados el profesor.
//$funcionJs = "fRecuperacionFinalFiltroMaterias(this.value)";
$funcionJs = "";
$utilidades = new utils();
$utilidades->listdoMateriaProfesor($_SESSION['MM_UserGroup'],$_SESSION['MM_Username'],date("Y"),$funcionJs);
?>
</td>
<td>
Curso:
</td>
<td>
<?
////Listado de materias que tiene asignados el profesor.
//$funcionJs = "fRecuperacionFinalFiltroCursos(this.value)";
$funcionJs = "";
//$utilidades = new utils();
$utilidades->listadoCursoProfesor($_SESSION['MM_UserGroup'],$_SESSION['MM_Username'],date("Y"),$funcionJs);
?>
</td>
<td>
  <input type="submit" name="bVerReporte" value="Ver" onclick="fRecuperacionFinalVer();">
</td>
</table>
<div id="divRecuperacionFinalReporte"> </div>
</body>
</html>
