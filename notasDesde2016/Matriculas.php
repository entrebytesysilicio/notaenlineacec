<?php require_once('Connections/MySQL.php'); ?>
<?php
if (!isset($_SESSION)) {
	session_start();
}
$MM_authorizedUsers = "Administrador";
$MM_donotCheckaccess = "false";
// *** Restrict Access To Page: Grant or deny access to this page
function isAuthorized($strUsers, $strGroups, $UserName, $UserGroup) {
	// For security, start by assuming the visitor is NOT authorized.
	$isValid = False;
	// When a visitor has logged into this site, the Session variable MM_Username set equal to their username.
	// Therefore, we know that a user is NOT logged in if that Session variable is blank.
	if (!empty($UserName)) {
		// Besides being logged in, you may restrict access to only certain users based on an ID established when they login.
		// Parse the strings into arrays.
		$arrUsers = Explode(",", $strUsers);
		$arrGroups = Explode(",", $strGroups);
		if (in_array($UserName, $arrUsers)) {
			$isValid = true;
		}
		// Or, you may restrict access to only certain users based on their username.
		if (in_array($UserGroup, $arrGroups)) {
			$isValid = true;
		}
		if (($strUsers == "") && false) {
			$isValid = true;
		}
	}
	return $isValid;
}
$MM_restrictGoTo = "error.php";
if (!((isset($_SESSION['MM_Username'])) && (isAuthorized("",$MM_authorizedUsers, $_SESSION['MM_Username'], $_SESSION['MM_UserGroup'])))) {
	$MM_qsChar = "?";
	$MM_referrer = $_SERVER['PHP_SELF'];
	if (strpos($MM_restrictGoTo, "?")) $MM_qsChar = "&";
	if (isset($QUERY_STRING) && strlen($QUERY_STRING) > 0)
		$MM_referrer .= "?" . $QUERY_STRING;
	$MM_restrictGoTo = $MM_restrictGoTo. $MM_qsChar . "accesscheck=" . urlencode($MM_referrer);
	header("Location: ". $MM_restrictGoTo);
	exit;
}
?>
<!DOCTYPE html>
<html lang="en">
    <head>
		<title>Definici&oacute;n de notas</title>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <? include "clases.php"; ?>
        <!--<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />-->
        <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
        <script language="javascript" type="text/javascript" src="js/jquery-1.10.2.min.js"></script>
        <script language="javascript" type="text/javascript" src="js/bootstrap.min.js"></script>
        <script language="javascript" type="text/javascript" src="ajax.js"></script>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
        <!-- Bootstrap -->
        <link href="css/bootstrap.min.css" rel="stylesheet">
        <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->
        <script type="text/javascript">
        	function fImprimirMatriculas() {
        		idCurso = document.getElementById('idCurso');
        		idCurso = idCurso.options[idCurso.selectedIndex].value;
        		if (idCurso > 0 && idCurso != '%') {
        			window.open('MatriculasImprimir.php?idCurso=' + idCurso, '_MatriculasImprimir', 'fullscreen=1,location=0,menubar=0,resizable=1,scrollbars=1,status=1,titlebar=1,toolbar=0,left=0px,top=0px');
        		}
        		else {
        			alert('Por favor seleccione todos los datos.');
        		}
        	}
    </script>
    </head>
    <body>
        <div class="container">
            <?php include "menu.php"; ?>
			<div class="row">
                <form action="<? echo $_SERVER['PHP_SELF']; ?>" method="post" enctype="application/x-www-form-urlencoded">

                    <div class="col-xs-3 col-sm-3 col-md-3 col-lg-3">
                        <div class="form-group">
                            <label class="control-label" for="anoSeleccionado">A&ntilde;o: </label>
                            <select class="form-control" id="anoSeleccionado" name="anoSeleccionado" onchange="this.form.submit();">
                                <option value="%">Todos</option>
								<?
								$anoSeleccionado = "%";
								if(isset($_POST['anoSeleccionado']))
								{
									$anoSeleccionado = $_POST['anoSeleccionado'];
								}

								$cursoSeleccionado ="%";
								if(isset($_POST['idCurso']))
								{
									$cursoSeleccionado = $_POST['idCurso'];
								}

										////Listado de a�os
										$sql = "select ano from curso group by ano order by ano desc";
										if($resultado = mysql_query($sql,$MySQL))
										{
											if(mysql_num_rows($resultado) > 0)
											{
												$fila = mysql_fetch_assoc($resultado);
												do{
												?>
												<option value="<?php echo $fila['ano']; ?>" <? if ($fila['ano'] == $anoSeleccionado) { ?> selected="selected" <?} ?> ><?php echo $fila['ano']; ?></option>
												<?
												} while($fila = mysql_fetch_assoc($resultado));
											}
											else
											{
                                        ?>
									<option value="%">No se encontraron datos.</option>
									<?
																	}
																}
																else
																{
                                    ?>
                                <option value="%">Error al consultar los a&ntilde;s.</option><?
																}
                                                                                             ?>

                            </select>
                        </div>                
				</div>

				<div class="col-xs-3 col-sm-3 col-md-3 col-lg-3">
					<div class="form-group">
						<label class="control-label" for="cursoSeleccionado">Curso: </label>
						<select class="form-control" id="idCurso" name="idCurso" onchange="this.form.submit();">
							<option value="%">Todos</option>
								<?
								////Listado de a�os
								$sql = "select idCurso, concat(ano,' - ',curso) curso from curso ";

								if( $anoSeleccionado != '%')
								{
									$sql = $sql." where ano like ".$anoSeleccionado;
								}

								$sql = $sql." order by ano desc, orden asc";
								if($resultado = mysql_query($sql,$MySQL))
								{
									if(mysql_num_rows($resultado) > 0)
									{
										$fila = mysql_fetch_assoc($resultado);
										do{
                                ?>
							<option value="<?php echo $fila['idCurso']; ?>"<? if ($fila['idCurso'] == $cursoSeleccionado) { ?> selected="selected" <?} ?> ><?php echo $fila['curso']; ?></option><?
										} while($fila = mysql_fetch_assoc($resultado));
									}
									else
									{
                                                                                                                                                                                                          ?>
									<option value="%">No se encontraron datos.</option>
									<?
									}
								}
								else
								{
                                    ?>
								<option value="%">Error al consultar los cursos.</option>
								<?
								}
                                ?>
							</select>
						</div>                
					</div>
					<div class="col-xs-3 col-sm-3 col-md-3 col-lg-3">
                        <div class="form-group">
                            <label class="control-label" for="bImprimir">&nbsp;</label>
                            <button type="button" name="bImprimir" onclick="fImprimirMatriculas();" id="bImprimir" class="btn btn-warning form-control">Imprimir matr&iacute;culas</button>
                        </div>
						</div>
				</form>
			</div>
     
        </div>  
    </body>
</html>