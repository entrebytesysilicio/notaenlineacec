<?php require_once('Connections/MySQL.php'); ?>
<?php
if (!isset($_SESSION)) {
  session_start();
}
$MM_authorizedUsers = "Administrador,Profesor";
$MM_donotCheckaccess = "false";

// *** Restrict Access To Page: Grant or deny access to this page
function isAuthorized($strUsers, $strGroups, $UserName, $UserGroup) { 
  // For security, start by assuming the visitor is NOT authorized. 
  $isValid = False; 

  // When a visitor has logged into this site, the Session variable MM_Username set equal to their username. 
  // Therefore, we know that a user is NOT logged in if that Session variable is blank. 
  if (!empty($UserName)) { 
    // Besides being logged in, you may restrict access to only certain users based on an ID established when they login. 
    // Parse the strings into arrays. 
    $arrUsers = Explode(",", $strUsers); 
    $arrGroups = Explode(",", $strGroups); 
    if (in_array($UserName, $arrUsers)) { 
      $isValid = true; 
    } 
    // Or, you may restrict access to only certain users based on their username. 
    if (in_array($UserGroup, $arrGroups)) { 
      $isValid = true; 
    } 
    if (($strUsers == "") && false) { 
      $isValid = true; 
    } 
  } 
  return $isValid; 
}

$MM_restrictGoTo = "error.php";
if (!((isset($_SESSION['MM_Username'])) && (isAuthorized("",$MM_authorizedUsers, $_SESSION['MM_Username'], $_SESSION['MM_UserGroup'])))) {   
  $MM_qsChar = "?";
  $MM_referrer = $_SERVER['PHP_SELF'];
  if (strpos($MM_restrictGoTo, "?")) $MM_qsChar = "&";
  if (isset($QUERY_STRING) && strlen($QUERY_STRING) > 0) 
  $MM_referrer .= "?" . $QUERY_STRING;
  $MM_restrictGoTo = $MM_restrictGoTo. $MM_qsChar . "accesscheck=" . urlencode($MM_referrer);
  header("Location: ". $MM_restrictGoTo); 
  exit;
}
?>
<?php
if (!function_exists("GetSQLValueString")) {
function GetSQLValueString($theValue, $theType, $theDefinedValue = "", $theNotDefinedValue = "") 
{
  if (PHP_VERSION < 6) {
    $theValue = get_magic_quotes_gpc() ? stripslashes($theValue) : $theValue;
  }

  $theValue = function_exists("mysql_real_escape_string") ? mysql_real_escape_string($theValue) : mysql_escape_string($theValue);

  switch ($theType) {
    case "text":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;    
    case "long":
    case "int":
      $theValue = ($theValue != "") ? intval($theValue) : "NULL";
      break;
    case "double":
      $theValue = ($theValue != "") ? doubleval($theValue) : "NULL";
      break;
    case "date":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;
    case "defined":
      $theValue = ($theValue != "") ? $theDefinedValue : $theNotDefinedValue;
      break;
  }
  return $theValue;
}
}

$editFormAction = $_SERVER['PHP_SELF'];
if (isset($_SERVER['QUERY_STRING'])) {
  $editFormAction .= "?" . htmlentities($_SERVER['QUERY_STRING']);
}

$updateSQL1 = "";
$updateSQL2 = "";
$updateSQL3 = "";

$sql = "select * from boletin where idAlumno = ".$_GET['idAlumno']." and idCurso = ".$_GET['idCurso']." and idMateria = ".$_GET['idMateria']." and bimestre = ".$_GET['bimestre'];
mysql_select_db($database_MySQL, $MySQL);
if($resultado = mysql_query($sql, $MySQL))
	{
	if(mysql_num_rows($resultado) > 0)
		{
			
		$updateSQL1 = "UPDATE boletin SET ".$_GET['nota']."=".$_GET['valor']."  where idAlumno = ".$_GET['idAlumno']." and idCurso = ".$_GET['idCurso']." and idMateria = ".$_GET['idMateria']." and bimestre = ".$_GET['bimestre'];	
					
			if ($updateSQL1 != "")
				{
					mysql_select_db($database_MySQL, $MySQL);
					if($Result1 = mysql_query($updateSQL1, $MySQL))
						{
						?>
						<center><img src="imagenes/bien.png"  height="32"  /></center>
						<?	
						}
						else
						{
						?><center><img src="imagenes/mal.png" height="32" onmouseover="alert('<? echo str_replace("'","\'",mysql_error()); ?>')" /></center><?
						}						
				}
	
		}
		else
		{//El dato no existe, toca insertar
		$insertSQL = sprintf("INSERT INTO boletin (idAlumno, idCurso, idMateria, bimestre, ".$_GET['nota'].") VALUES (%s, %s, %s, %s, %s)",
							   GetSQLValueString($_GET['idAlumno'], "int"),
							   GetSQLValueString($_GET['idCurso'], "int"),
							   GetSQLValueString($_GET['idMateria'], "int"),
							   GetSQLValueString($_GET['bimestre'], "int"),
							   GetSQLValueString($_GET['valor'], "double"));
		
		  mysql_select_db($database_MySQL, $MySQL);
		  if($Result1 = mysql_query($insertSQL, $MySQL))
			{
			?>
			<center><img src="imagenes/bien.png"  height="32"  /></center>
			<?	
			}
			else
			{
						?><center><img src="imagenes/mal.png" height="32" onmouseover="alert('<? echo str_replace("'","\'",mysql_error()); ?>')" /></center><?
			}	
		}
	}
	else
	{
	echo mysql_error();
	}
?>