<?php require_once('Connections/MySQL.php'); ?>
<?php
if (!isset($_SESSION)) {
  session_start();
}
$MM_authorizedUsers = "Administrador,Padre,Profesor";
$MM_donotCheckaccess = "false";

// *** Restrict Access To Page: Grant or deny access to this page
function isAuthorized($strUsers, $strGroups, $UserName, $UserGroup) { 
  // For security, start by assuming the visitor is NOT authorized. 
  $isValid = False; 

  // When a visitor has logged into this site, the Session variable MM_Username set equal to their username. 
  // Therefore, we know that a user is NOT logged in if that Session variable is blank. 
  if (!empty($UserName)) { 
    // Besides being logged in, you may restrict access to only certain users based on an ID established when they login. 
    // Parse the strings into arrays. 
    $arrUsers = Explode(",", $strUsers); 
    $arrGroups = Explode(",", $strGroups); 
    if (in_array($UserName, $arrUsers)) { 
      $isValid = true; 
    } 
    // Or, you may restrict access to only certain users based on their username. 
    if (in_array($UserGroup, $arrGroups)) { 
      $isValid = true; 
    } 
    if (($strUsers == "") && false) { 
      $isValid = true; 
    } 
  } 
  return $isValid; 
}

$MM_restrictGoTo = "error.php";
if (!((isset($_SESSION['MM_Username'])) && (isAuthorized("",$MM_authorizedUsers, $_SESSION['MM_Username'], $_SESSION['MM_UserGroup'])))) {   
  $MM_qsChar = "?";
  $MM_referrer = $_SERVER['PHP_SELF'];
  if (strpos($MM_restrictGoTo, "?")) $MM_qsChar = "&";
  if (isset($_SERVER['QUERY_STRING']) && strlen($_SERVER['QUERY_STRING']) > 0) 
  $MM_referrer .= "?" . $_SERVER['QUERY_STRING'];
  $MM_restrictGoTo = $MM_restrictGoTo. $MM_qsChar . "accesscheck=" . urlencode($MM_referrer);
  header("Location: ". $MM_restrictGoTo); 
  exit;
}
?>

<?php
if (!function_exists("GetSQLValueString")) {
function GetSQLValueString($theValue, $theType, $theDefinedValue = "", $theNotDefinedValue = "") 
{
  if (PHP_VERSION < 6) {
    $theValue = get_magic_quotes_gpc() ? stripslashes($theValue) : $theValue;
  }

  $theValue = function_exists("mysql_real_escape_string") ? mysql_real_escape_string($theValue) : mysql_escape_string($theValue);

  switch ($theType) {
    case "text":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;    
    case "long":
    case "int":
      $theValue = ($theValue != "") ? intval($theValue) : "NULL";
      break;
    case "double":
      $theValue = ($theValue != "") ? doubleval($theValue) : "NULL";
      break;
    case "date":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;
    case "defined":
      $theValue = ($theValue != "") ? $theDefinedValue : $theNotDefinedValue;
      break;
  }
  return $theValue;
}
}

mysql_select_db($database_MySQL, $MySQL);
$query_rsAnios = "select ano from curso group by ano order by ano asc";
$rsAnios = mysql_query($query_rsAnios, $MySQL) or die(mysql_error());
$row_rsAnios = mysql_fetch_assoc($rsAnios);
$totalRows_rsAnios = mysql_num_rows($rsAnios);
?>


<table>
  <tr>
    <td>Seleccione el a&ntilde;o</td>
    <td><select onchange="fReporteImpresionBoletinFinalMostrarCursos(this.value);" id="ano" name="ano">
        <option value="0"></option>
        <?php
			do {  
			?>
        <option value="<?php echo $row_rsAnios['ano']?>"><?php echo $row_rsAnios['ano']?></option>
        <?php
			} while ($row_rsAnios = mysql_fetch_assoc($rsAnios));
			  $rows = mysql_num_rows($rsAnios);
			  if($rows > 0) {
				  mysql_data_seek($rsAnios, 0);
				  $row_rsAnios = mysql_fetch_assoc($rsAnios);
			  }
			?>
      </select>
    </td>
    <td>Seleccione el curso:</td>

    <td>
      <div id="divListadoCursos">
        Seleccione el a&ntilde;o...
      </div>
    </td>
    <td>
      Libro final <input type="checkbox" id="libroFinal" name="libroFinal" />
    </td>
    <td><input type="button" name="Imprimir" value="Imprimir"
        onclick="javascript:fReporteImpresionBoletinFinalListarAreas();" /></td>
  </tr>
</table>
<?php
mysql_free_result($rsAnios);
?>