<?php
////Listado de alumnos perdiendo la materia
////Este listado puede llamarce (Recuperación Final)
////Filtros:
////Año (Debería ser solo el año actual)
////Materia (Si es profesor solo le muestra sus materias asignadas, si es administrador muestra todo)
////Curso (Muestra los cursos que tiene asignados el profesor con la materia seleccionada)
////
////Cada vez que se selecciona un filtro, se reduce la consulta, inicialmente se muestra todo el listado.
////Se debe colocar una entrada de datos, para colocar la nota de recuperación final.

if (!isset($_SESSION)) {
  session_start();
}
$MM_authorizedUsers = "Administrador,Profesor";
$MM_donotCheckaccess = "false";

// *** Restrict Access To Page: Grant or deny access to this page
function isAuthorized($strUsers, $strGroups, $UserName, $UserGroup) { 
  // For security, start by assuming the visitor is NOT authorized. 
  $isValid = False; 

  // When a visitor has logged into this site, the Session variable MM_Username set equal to their username. 
  // Therefore, we know that a user is NOT logged in if that Session variable is blank. 
  if (!empty($UserName)) { 
    // Besides being logged in, you may restrict access to only certain users based on an ID established when they login. 
    // Parse the strings into arrays. 
    $arrUsers = Explode(",", $strUsers); 
    $arrGroups = Explode(",", $strGroups); 
    if (in_array($UserName, $arrUsers)) { 
      $isValid = true; 
    } 
    // Or, you may restrict access to only certain users based on their username. 
    if (in_array($UserGroup, $arrGroups)) { 
      $isValid = true; 
    } 
    if (($strUsers == "") && false) { 
      $isValid = true; 
    } 
  } 
  return $isValid; 
}

$MM_restrictGoTo = "error.php";
if (!((isset($_SESSION['MM_Username'])) && (isAuthorized("",$MM_authorizedUsers, $_SESSION['MM_Username'], $_SESSION['MM_UserGroup'])))) {   
  $MM_qsChar = "?";
  $MM_referrer = $_SERVER['PHP_SELF'];
  if (strpos($MM_restrictGoTo, "?")) $MM_qsChar = "&";
  if (isset($QUERY_STRING) && strlen($QUERY_STRING) > 0) 
  $MM_referrer .= "?" . $QUERY_STRING;
  $MM_restrictGoTo = $MM_restrictGoTo. $MM_qsChar . "accesscheck=" . urlencode($MM_referrer);
  header("Location: ". $MM_restrictGoTo); 
  exit;
}

include "clases.php";
$sql =  "
        #identificar los alumnos con una materia perdida y que no supero la recuperacion
        select  b.idBoletin 
                    as idBoletin, 
                c.curso 
                    as Curso, 
                ar.area
                    as Area,
                m.materia
                    as Materia, 
                a.apellidos
                    as Apellidos, 
                a.nombres
                    as Nombres,
                b.recuperacionFinal
                    as recuperacionFinal
        from boletin as b
        inner join curso as c on b.idCurso = c.idCurso
        inner join alumno as a on b.idAlumno = a.idAlumno
        inner join materia as m on b.idMateria = m.idMateria
        inner join area as ar on ar.idArea = m.idArea
        where 
         case when (
        ifnull(((((nota1 +
        nota2 +
        nota3 +
        nota4 +
        nota5 +
        nota6 )/

        (
        case when nota1 > 0 then 1 else 0 end   
        +
        case when nota2 > 0 then 1 else 0 end   
        +
        case when nota3 > 0 then 1 else 0 end   
        +
        case when nota4 > 0 then 1 else 0 end   
        +
        case when nota5 > 0 then 1 else 0 end   
        +
        case when nota6 > 0 then 1 else 0 end   
        )
        ) * 40 )/100),0)
        +
        ifnull(((((nota7 +
        nota8 +
        nota9 +
        nota10 +
        nota11 +
        nota12 )/
        (
        case when nota7 > 0 then 1 else 0 end   
        +
        case when nota8 > 0 then 1 else 0 end   
        +
        case when nota9 > 0 then 1 else 0 end   
        +
        case when nota10 > 0 then 1 else 0 end  
        +
        case when nota11 > 0 then 1 else 0 end  
        +
        case when nota12 > 0 then 1 else 0 end  
        )           

        ) * 40 )/100),0)
        +
        ifnull(((((nota13 +
        nota14 )/
        (
        case when nota13 > 0 then 1 else 0 end  
        +
        case when nota14 > 0 then 1 else 0 end              
        )
        ) * 20 )/100),0) ) <
        recuperacion then recuperacion else 

        (
        ifnull(((((nota1 +
        nota2 +
        nota3 +
        nota4 +
        nota5 +
        nota6 )/

        (
        case when nota1 > 0 then 1 else 0 end   
        +
        case when nota2 > 0 then 1 else 0 end   
        +
        case when nota3 > 0 then 1 else 0 end   
        +
        case when nota4 > 0 then 1 else 0 end   
        +
        case when nota5 > 0 then 1 else 0 end   
        +
        case when nota6 > 0 then 1 else 0 end   
        )
        ) * 40 )/100),0)
        +
        ifnull(((((nota7 +
        nota8 +
        nota9 +
        nota10 +
        nota11 +
        nota12 )/
        (
        case when nota7 > 0 then 1 else 0 end   
        +
        case when nota8 > 0 then 1 else 0 end   
        +
        case when nota9 > 0 then 1 else 0 end   
        +
        case when nota10 > 0 then 1 else 0 end  
        +
        case when nota11 > 0 then 1 else 0 end  
        +
        case when nota12 > 0 then 1 else 0 end  
        )           

        ) * 40 )/100),0)
        +
        ifnull(((((nota13 +
        nota14 )/
        (
        case when nota13 > 0 then 1 else 0 end  
        +
        case when nota14 > 0 then 1 else 0 end              
        )
        ) * 20 )/100),0) )

        end < 3.5 
        and b.bimestre = 4 ";


$where = "";
if ($_GET['idCurso'] > 0)
    {
      $where .= " and b.idCurso = ".$_GET['idCurso'];
    }
if ($_GET['idMateria'] > 0)
    {
      $where .= " and b.idMateria = ".$_GET['idMateria'];
    }
if ($_GET['ano']>0)
    {
      $where .= " and c.ano = ".$_GET['ano'];
    }

$sql .= $where;

$sql .= "
        group by b.idAlumno, c.curso, ar.area,m.materia, a.apellidos, a.nombres
        order by c.orden, ar.orden, m.materia, a.apellidos, a.nombres        
        ";
?>
<br>
  <center>
Filtro: <b><? if($_GET['materia'] == '') { echo 'TODOS'; } else { echo $_GET['materia']; } ?></b>, 
Curso: <b><? if($_GET['curso'] == '') { echo 'TODOS'; } else { echo $_GET['curso']; } ?></b>
</center>
<br>
<?
$rs = mysql_query($sql, $MySQL) or die(mysql_error());
$filas = mysql_fetch_assoc($rs);
$totalFilas = mysql_num_rows($rs);  
if ($totalFilas > 0)
    {
        
        ?>
        <table align="center">
        <tr><td>Registros encontrados: <? echo mysql_num_rows($rs); ?></td></tr>
        <tr><td>
        <table align="center" border="1" cellpadding="3" cellspacing="0">
        <tr>
            <td>No.</td>
            <td>Curso</td>
            <td>Area</td>
            <td>Materia</td>
            <td>Apellidos</td>
            <td>Nombres</td>
            <td>Recuperaci&oacute;n Final</td>
            <td></td>
        </tr>
        <?


        $i = 1;
        
            do{
               ?>
                <tr id="tr_<? echo $i; ?>">
                    <td><? echo $i; ?></td>
                    <td><? echo $filas['Curso']; ?></td>
                    <td><? echo $filas['Area']; ?></td>
                    <td><? echo $filas['Materia']; ?></td>
                    <td><? echo $filas['Apellidos']; ?></td>
                    <td><? echo $filas['Nombres']; ?></td>
                    <td align="center"><input type="text"  onfocus="sombrearTr(<? echo $i; ?>)" onchange="actualizarRecuperacionFinal(<? echo $filas['idBoletin']?>,this.value,<? echo $i; ?>);" name="recuperacionFinal" value="<? echo $filas['recuperacionFinal']; ?>" id="recuperacionFinal<? echo $filas['idBoletin']; ?>" class="inputtextnota">  </td>
                    <td><div id="info_<? echo $i; ?>"></div></td>
                </tr>
               <?

                $i++;
            }while ($filas = mysql_fetch_assoc($rs))
        ?>
        </table>
        </td>
        </tr>
        </table>
        <?
    }   
    else
    {
        ?><center>No se encontraron datos.</center><?
    }   



 
?>
