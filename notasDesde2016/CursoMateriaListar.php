<?php require_once('Connections/MySQL.php'); ?>
<?php
if (!isset($_SESSION)) {
  session_start();
}
$MM_authorizedUsers = "Administrador,Profesor";
$MM_donotCheckaccess = "false";

// *** Restrict Access To Page: Grant or deny access to this page
function isAuthorized($strUsers, $strGroups, $UserName, $UserGroup) { 
  // For security, start by assuming the visitor is NOT authorized. 
  $isValid = False; 

  // When a visitor has logged into this site, the Session variable MM_Username set equal to their username. 
  // Therefore, we know that a user is NOT logged in if that Session variable is blank. 
  if (!empty($UserName)) { 
    // Besides being logged in, you may restrict access to only certain users based on an ID established when they login. 
    // Parse the strings into arrays. 
    $arrUsers = Explode(",", $strUsers); 
    $arrGroups = Explode(",", $strGroups); 
    if (in_array($UserName, $arrUsers)) { 
      $isValid = true; 
    } 
    // Or, you may restrict access to only certain users based on their username. 
    if (in_array($UserGroup, $arrGroups)) { 
      $isValid = true; 
    } 
    if (($strUsers == "") && false) { 
      $isValid = true; 
    } 
  } 
  return $isValid; 
}

$MM_restrictGoTo = "error.php";
if (!((isset($_SESSION['MM_Username'])) && (isAuthorized("",$MM_authorizedUsers, $_SESSION['MM_Username'], $_SESSION['MM_UserGroup'])))) {   
  $MM_qsChar = "?";
  $MM_referrer = $_SERVER['PHP_SELF'];
  if (strpos($MM_restrictGoTo, "?")) $MM_qsChar = "&";
  if (isset($QUERY_STRING) && strlen($QUERY_STRING) > 0) 
  $MM_referrer .= "?" . $QUERY_STRING;
  $MM_restrictGoTo = $MM_restrictGoTo. $MM_qsChar . "accesscheck=" . urlencode($MM_referrer);
  header("Location: ". $MM_restrictGoTo); 
  exit;
}
?>
<?php
if (!function_exists("GetSQLValueString")) {
function GetSQLValueString($theValue, $theType, $theDefinedValue = "", $theNotDefinedValue = "") 
{
  if (PHP_VERSION < 6) {
    $theValue = get_magic_quotes_gpc() ? stripslashes($theValue) : $theValue;
  }

  $theValue = function_exists("mysql_real_escape_string") ? mysql_real_escape_string($theValue) : mysql_escape_string($theValue);

  switch ($theType) {
    case "text":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;    
    case "long":
    case "int":
      $theValue = ($theValue != "") ? intval($theValue) : "NULL";
      break;
    case "double":
      $theValue = ($theValue != "") ? doubleval($theValue) : "NULL";
      break;
    case "date":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;
    case "defined":
      $theValue = ($theValue != "") ? $theDefinedValue : $theNotDefinedValue;
      break;
  }
  return $theValue;
}
}

mysql_select_db($database_MySQL, $MySQL);
$query_rsListadoCursoMateria = sprintf("select cm.idCursoMateria, c.ano, c.curso,a.area,m.materia,cm.porcentajeArea 
from cursomateria as cm 
inner join materia as m 
    on m.idMateria = cm.idMateria 
inner join curso as c 
    on c.idCurso = cm.idCurso 
inner join area as a 
    on m.idArea = a.idArea 
where cm.idCurso = %s 
order by c.orden, a.orden, m.materia", GetSQLValueString($_GET['idCurso'], "int"));

$rsListadoCursoMateria = mysql_query($query_rsListadoCursoMateria, $MySQL) or die(mysql_error());
$row_rsListadoCursoMateria = mysql_fetch_assoc($rsListadoCursoMateria);
$totalRows_rsListadoCursoMateria = mysql_num_rows($rsListadoCursoMateria);
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>

<title>:: Curso Materia ::</title>

</head>

<body>
<table border="1" >
<thead>
  <tr>
    <td>No.</td>
    <td>A&ntilde;o</td>
    <td>Curso</td>
    <td>&Aacute;rea</td>
    <td>Materia</td>
    <td>Porcentaje</td>
    <td>Acci&oacute;n</td>
  </tr>
  </thead>
  <?php $i = 1; do { ?>
    <tr>
    	<td align="center"><? echo $i++; ?></td>
      <td><?php echo $row_rsListadoCursoMateria['ano']; ?></td>
      <td><?php echo $row_rsListadoCursoMateria['curso']; ?></td>
      <td><?php echo $row_rsListadoCursoMateria['area']; ?></td>
      <td><?php echo $row_rsListadoCursoMateria['materia']; ?></td>    
      <td><?php echo $row_rsListadoCursoMateria['porcentajeArea']; ?></td>
      <td><input type="button" name="Eliminar" value="Eliminar" onclick="javascript:fCursoMateriaEliminar(<?php echo $row_rsListadoCursoMateria['idCursoMateria']; ?>);" /></td>
    </tr>
    <?php } while ($row_rsListadoCursoMateria = mysql_fetch_assoc($rsListadoCursoMateria)); ?>
</table>
<?php
mysql_free_result($rsListadoCursoMateria);



mysql_select_db($database_MySQL, $MySQL);
$query_rsListadoCursoMateria = sprintf("select  c.ano, c.curso,a.area,  sum(cm.porcentajeArea) as porcentajeArea
from cursomateria as cm 
inner join materia as m 
    on m.idMateria = cm.idMateria 
inner join curso as c 
    on c.idCurso = cm.idCurso 
inner join area as a 
    on m.idArea = a.idArea 
where cm.idCurso = %s
group by  c.ano, c.curso,a.area
order by c.orden, a.orden", GetSQLValueString($_GET['idCurso'], "int"));

$rsListadoCursoMateria = mysql_query($query_rsListadoCursoMateria, $MySQL) or die(mysql_error());
$row_rsListadoCursoMateria = mysql_fetch_assoc($rsListadoCursoMateria);
$totalRows_rsListadoCursoMateria = mysql_num_rows($rsListadoCursoMateria);

?>
<br>
<table border="1" >
<thead>
  <tr>
    <td>No.</td>
    <td>A&ntilde;o</td>
    <td>Curso</td>
    <td>&Aacute;rea</td>
    <td>Porcentaje</td>
  </tr>
  </thead>
  <?php $i = 1; do { ?>
    <tr>
    	<td align="center"><? echo $i++; ?></td>
      <td><?php echo $row_rsListadoCursoMateria['ano']; ?></td>
      <td><?php echo $row_rsListadoCursoMateria['curso']; ?></td>
      <td><?php echo $row_rsListadoCursoMateria['area']; ?></td>
      <td><?php echo $row_rsListadoCursoMateria['porcentajeArea']; ?></td>
    </tr>
    <?php } while ($row_rsListadoCursoMateria = mysql_fetch_assoc($rsListadoCursoMateria)); ?>
</table>
<?
mysql_free_result($rsListadoCursoMateria);
?>

</body>
</html>

