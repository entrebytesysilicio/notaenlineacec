<?

class EstudianteInformeValorativo
{
	public $idAlumno;
	public $idCurso;
	public $periodo;
	public $ano;
	
	function materiasPerdidasAlumno($idCurso,$idAlumno)
	{
		global $MySQL;


		$sql1 = "
		select sum(cmp.intensidadHoraria) as IH, m.materia, m.idMateria, a.Area 
		from cursomateriaprofesor as cmp 
		inner join materia as m on m.idMateria = cmp.idMateria
		inner join area as a on m.idArea = a.idArea
		where cmp.idCurso = ".$idCurso."
		group by  m.materia, m.idMateria, a.Area
		order by a.orden, m.materia
		";

		mysql_select_db($database_MySQL, $MySQL);					
		$resultado1 = mysql_query($sql1,$MySQL);
		if($filas1 = mysql_fetch_array($resultado1))
		{
			do {
				unset($promedio);

				for($i = 1; $i <= 4 ; $i++)
				{
					////Saca la nota de determinado alumno, en determinado curso, para determinada materia, en determinado semestre.
					$sql2 = "select ifnull(((((nota1 + nota2 +nota3 +nota4 +nota5 +nota6 )/(case when nota1 > 0 then 1 else 0 end 	+case when nota2 > 0 then 1 else 0 end 	+case when nota3 > 0 then 1 else 0 end 	+case when nota4 > 0 then 1 else 0 end 	+case when nota5 > 0 then 1 else 0 end 	+case when nota6 > 0 then 1 else 0 end 	)) * 40 )/100),0)+ifnull(((((nota7 +nota8 +nota9 +nota10 +nota11 +nota12 )/(case when nota7 > 0 then 1 else 0 end 	+case when nota8 > 0 then 1 else 0 end 	+case when nota9 > 0 then 1 else 0 end 	+case when nota10 > 0 then 1 else 0 end 	+case when nota11 > 0 then 1 else 0 end 	+case when nota12 > 0 then 1 else 0 end 	)						) * 40 )/100),0)+ifnull(((((nota13 +nota14 )/(case when nota13 > 0 then 1 else 0 end 	+case when nota14 > 0 then 1 else 0 end 							)) * 20 )/100),0) as nota,recuperacion as recuperacion,recuperacionFinal as recuperacionFinal from 	boletin where 	idAlumno = ".$idAlumno." and idCurso = ".$idCurso." and idMateria = ".$filas1['idMateria']." and bimestre = ".$i;
					$resultado2 = mysql_query($sql2,$MySQL);
					if ($filas2 = mysql_fetch_array($resultado2))
					{
						$nota = $filas2['nota'];
						$recuperacion = $filas2['recuperacion'];

					}
					else
					{
						$nota = 0;
						$recuperacion = 0;
					}	

					if ($nota > 0 && $nota > $recuperacion)
					{
						$promedio[$i] = $nota;			
					}
					else
					{
						if ($recuperacion > 0)
						{
							$promedio[$i] = $recuperacion;			
						}								
					}
				}

				$notaPromedio = array_sum($promedio) / count($promedio);

				if(number_format($notaPromedio,1) < 3.5)
				{
					$materiasPerdidas[$filas1['idMateria']] = number_format($notaPromedio,1);
				}

			}while($filas1 = mysql_fetch_array($resultado1));
		}
		else
		{
			echo mysql_error();
		}			  

		$totalMateriasPerdidas = count($materiasPerdidas);
		return $totalMateriasPerdidas;
	}













	public function nombreEstudiante ()
	{
		$sql = 'select concat(nombres,\' \',apellidos) from alumno where idAlumno = '.$this->idAlumno;
		echo $this->consultaAVariable($sql);
	}

	public function identificacionEstudiante ()
	{
		$sql = 'select identificacion from alumno where idAlumno = '.$this->idAlumno;
		echo $this->consultaAVariable($sql);
	}

	private function cursoAlumno ()
	{
		$sql = "select c.idCurso from curso as c 
		inner join alumnocurso as ac on ac.idCurso = c.idCurso
		where c.ano = ".$this->ano."
		and ac.idAlumno = ".$this->idAlumno;
		$this->idCurso = $this->consultaAVariable($sql);
	}

	public function nombreDirectorCurso ()
	{
		if(!$this->idCurso)
		{
			$this->cursoAlumno();			
		}
		$sql = "select u.nombre from curso as c inner join usuario as u on u.idUsuario = c.idDirectorCurso where c.idCurso = ".$this->idCurso;
		echo $this->consultaAVariable($sql);
	}

	public function nombreCurso ()
	{
		$sql = "select curso from curso where idCurso = ".$this->idCurso;
		echo $this->consultaAVariable($sql);
	}		


	private function intensidadHoraria($idMateria)
	{
		$sql = "select sum(cmp.intensidadHoraria) 
		from cursomateriaprofesor as cmp
		inner join curso as c on c.idCurso = cmp.idCurso
		where c.idCurso = ".$this->idCurso."
		and cmp.idMateria = ".$idMateria."
		group by cmp.intensidadHoraria";	
		return $this->consultaAVariable($sql);
	}

	private function consultaAVariable ($sql)
	{	
		global $MySQL;
		if($resultado = mysql_query($sql,$MySQL))
		{
			if ($filas = mysql_fetch_array($resultado))
			{
				return $filas[0];
			}
			else
			{
				echo mysql_error();	
			}
		}
		else
		{
			echo mysql_error();	
		}
	}

}

?>