<?php require_once('Connections/MySQL.php'); ?>
<?php
if (!isset($_SESSION)) {
  session_start();
}
$MM_authorizedUsers = "Administrador,Profesor";
$MM_donotCheckaccess = "false";

// *** Restrict Access To Page: Grant or deny access to this page
function isAuthorized($strUsers, $strGroups, $UserName, $UserGroup) { 
  // For security, start by assuming the visitor is NOT authorized. 
  $isValid = False; 

  // When a visitor has logged into this site, the Session variable MM_Username set equal to their username. 
  // Therefore, we know that a user is NOT logged in if that Session variable is blank. 
  if (!empty($UserName)) { 
    // Besides being logged in, you may restrict access to only certain users based on an ID established when they login. 
    // Parse the strings into arrays. 
    $arrUsers = Explode(",", $strUsers); 
    $arrGroups = Explode(",", $strGroups); 
    if (in_array($UserName, $arrUsers)) { 
      $isValid = true; 
    } 
    // Or, you may restrict access to only certain users based on their username. 
    if (in_array($UserGroup, $arrGroups)) { 
      $isValid = true; 
    } 
    if (($strUsers == "") && false) { 
      $isValid = true; 
    } 
  } 
  return $isValid; 
}

$MM_restrictGoTo = "error.php";
if (!((isset($_SESSION['MM_Username'])) && (isAuthorized("",$MM_authorizedUsers, $_SESSION['MM_Username'], $_SESSION['MM_UserGroup'])))) {   
  $MM_qsChar = "?";
  $MM_referrer = $_SERVER['PHP_SELF'];
  if (strpos($MM_restrictGoTo, "?")) $MM_qsChar = "&";
  if (isset($QUERY_STRING) && strlen($QUERY_STRING) > 0) 
  $MM_referrer .= "?" . $QUERY_STRING;
  $MM_restrictGoTo = $MM_restrictGoTo. $MM_qsChar . "accesscheck=" . urlencode($MM_referrer);
  header("Location: ". $MM_restrictGoTo); 
  exit;
}
?>
<?php
if (!function_exists("GetSQLValueString")) {
function GetSQLValueString($theValue, $theType, $theDefinedValue = "", $theNotDefinedValue = "") 
{
  if (PHP_VERSION < 6) {
    $theValue = get_magic_quotes_gpc() ? stripslashes($theValue) : $theValue;
  }

  $theValue = function_exists("mysql_real_escape_string") ? mysql_real_escape_string($theValue) : mysql_escape_string($theValue);

  switch ($theType) {
    case "text":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;    
    case "long":
    case "int":
      $theValue = ($theValue != "") ? intval($theValue) : "NULL";
      break;
    case "double":
      $theValue = ($theValue != "") ? doubleval($theValue) : "NULL";
      break;
    case "date":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;
    case "defined":
      $theValue = ($theValue != "") ? $theDefinedValue : $theNotDefinedValue;
      break;
  }
  return $theValue;
}
}

if ($_GET["ano"] > 0 && $_GET["ano"] <> "TODOS")
	{
	@$where .= " and c.ano = ".$_GET["ano"];
	}
	
if ($_GET["idCurso"] > 0)
	{
	$where .= " and c.idCurso = ".$_GET["idCurso"];
	}	

if ($_GET["idArea"] > 0)
	{
	$where .= " and a.idArea = ".$_GET["idArea"];
	}	
	
if ($_GET["bimestre"] > 0)
	{
	$where .= " and b.bimestre = ".$_GET["bimestre"];
	}		
	
if ($_GET["idMateria"] > 0)
	{
	$where .= " and m.idMateria = ".$_GET["idMateria"];
	}	
	
if ($_GET["idProfesor"] > 0)
	{
	$where .= " and pro.idUsuario = ".$_GET["idProfesor"];
	}		
	
	
mysql_select_db($database_MySQL, $MySQL);
$query_Reporte = "select	c.ano, 
							b.bimestre, 
							c.curso, 
							a.area, 
							m.materia, 
							pro.nombre, 
							dp.desempeno, 
							count(1) as alumnos 
					from boletin as b 
					inner join curso as c on b.idCurso = c.idCurso 
					inner join materia as m on b.idMateria = m.idMateria 
					inner join area as a on m.idArea = a.idArea 
					inner join cursomateriaprofesor as cmp on c.idCurso = cmp.idCurso and m.idMateria = cmp.idMateria
					inner join usuario as pro on cmp.idProfesor = pro.idUsuario 
					left join desempeno as dp on c.ano = dp.ano                         
											and 
              round(ifnull(((((nota1 +	nota2 +	nota3 +	nota4 +	nota5 +	nota6 )
              /
              (
              case when nota1 > 0 then 1 else 0 end
              +case when nota2 > 0 then 1 else 0 end
              +case when nota3 > 0 then 1 else 0 end
              +case when nota4 > 0 then 1 else 0 end
              +case when nota5 > 0 then 1 else 0 end
              +case when nota6 > 0 then 1 else 0 end
              )
              ) * 40 )/100),0)
              +
              ifnull(((((nota7 +	nota8 +	nota9 +	nota10 + nota11 + nota12 )
              /
              (
              case when nota7 > 0 then 1 else 0 end
              +case when nota8 > 0 then 1 else 0 end
              +case when nota9 > 0 then 1 else 0 end
              +case when nota10 > 0 then 1 else 0 end
              +case when nota11 > 0 then 1 else 0 end
              +case when nota12 > 0 then 1 else 0 end
              )
              ) * 30 )/100),0)
              +
              ifnull(((((nota13 +nota14 )
              /
              (
              case when nota13 > 0 then 1 else 0 end
              +case when nota14 > 0 then 1 else 0 end
              )
              ) * 10 )/100),0)
              +
              ifnull((((nota15
              /
              (
              case when nota15 > 0 then 1 else 0 end
              )
              ) * 20 )/100),0)


,1) between notaMinima and notaMaxima 
where 1 = 1
".$where."
group by c.ano, b.bimestre, c.curso, a.area, m.materia, pro.nombre, dp.desempeno 
order by c.ano, c.orden, a.orden, m.materia, pro.nombre, dp.notaMinima";
$Reporte = mysql_query($query_Reporte, $MySQL) or die(mysql_error());
$row_Reporte = mysql_fetch_assoc($Reporte);
$totalRows_Reporte = mysql_num_rows($Reporte);
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title>:: Reporte Resumen de Desempe&ntilde;o ::</title>
<? include "header.php"; ?>

</head>

<body>
<table border="1" align="center">
<thead>
  <tr>
    <td>A�o</td>
    <td>Bimestre</td>
    <td>Curso</td>
    <td>&Aacute;rea</td>
    <td>Materia</td>
    <td>Nombre</td>
    <td>Desempeno</td>
    <td>Alumnos</td>
  </tr>
  </thead>
  <?php do { ?>
    <tr>
      <td align="center"><?php echo $row_Reporte['ano']; ?></td>
      <td align="center"><?php echo $row_Reporte['bimestre']; ?></td>
      <td><?php echo $row_Reporte['curso']; ?></td>
      <td><?php echo $row_Reporte['area']; ?></td>
      <td><?php echo $row_Reporte['materia']; ?></td>
      <td><?php echo $row_Reporte['nombre']; ?></td>
      <td align="center"><?php echo $row_Reporte['desempeno']; ?></td>
      <td align="center"><?php echo $row_Reporte['alumnos']; ?></td>
    </tr>
    <?php } while ($row_Reporte = mysql_fetch_assoc($Reporte)); ?>
</table>
</body>
</html>
<?php
mysql_free_result($Reporte);
?>
