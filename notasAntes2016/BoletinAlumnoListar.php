<?php require_once('Connections/MySQL.php'); ?>
<?php require_once('clases.php'); ?>
<?php
if (!isset($_SESSION)) {
  session_start();
}
$MM_authorizedUsers = "Administrador,Profesor";
$MM_donotCheckaccess = "false";

// *** Restrict Access To Page: Grant or deny access to this page
function isAuthorized($strUsers, $strGroups, $UserName, $UserGroup) { 
  // For security, start by assuming the visitor is NOT authorized. 
  $isValid = False; 

  // When a visitor has logged into this site, the Session variable MM_Username set equal to their username. 
  // Therefore, we know that a user is NOT logged in if that Session variable is blank. 
  if (!empty($UserName)) { 
    // Besides being logged in, you may restrict access to only certain users based on an ID established when they login. 
    // Parse the strings into arrays. 
    $arrUsers = Explode(",", $strUsers); 
    $arrGroups = Explode(",", $strGroups); 
    if (in_array($UserName, $arrUsers)) { 
      $isValid = true; 
    } 
    // Or, you may restrict access to only certain users based on their username. 
    if (in_array($UserGroup, $arrGroups)) { 
      $isValid = true; 
    } 
    if (($strUsers == "") && false) { 
      $isValid = true; 
    } 
  } 
  return $isValid; 
}

$MM_restrictGoTo = "error.php";
if (!((isset($_SESSION['MM_Username'])) && (isAuthorized("",$MM_authorizedUsers, $_SESSION['MM_Username'], $_SESSION['MM_UserGroup'])))) {   
  $MM_qsChar = "?";
  $MM_referrer = $_SERVER['PHP_SELF'];
  if (strpos($MM_restrictGoTo, "?")) $MM_qsChar = "&";
  if (isset($QUERY_STRING) && strlen($QUERY_STRING) > 0) 
    $MM_referrer .= "?" . $QUERY_STRING;
  $MM_restrictGoTo = $MM_restrictGoTo. $MM_qsChar . "accesscheck=" . urlencode($MM_referrer);
  header("Location: ". $MM_restrictGoTo); 
  exit;
}
?>
<?php
if (!function_exists("GetSQLValueString")) {
  function GetSQLValueString($theValue, $theType, $theDefinedValue = "", $theNotDefinedValue = "") 
  {
    if (PHP_VERSION < 6) {
      $theValue = get_magic_quotes_gpc() ? stripslashes($theValue) : $theValue;
    }

    $theValue = function_exists("mysql_real_escape_string") ? mysql_real_escape_string($theValue) : mysql_escape_string($theValue);

    switch ($theType) {
      case "text":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;    
      case "long":
      case "int":
      $theValue = ($theValue != "") ? intval($theValue) : "NULL";
      break;
      case "double":
      $theValue = ($theValue != "") ? doubleval($theValue) : "NULL";
      break;
      case "date":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;
      case "defined":
      $theValue = ($theValue != "") ? $theDefinedValue : $theNotDefinedValue;
      break;
    }
    return $theValue;
  }
}

$Notas = new Notas();

$colname_rsAlumnos = "-1";
if (isset($_GET['idCurso'])) {
  $colname_rsAlumnos = $_GET['idCurso'];
}
mysql_select_db($database_MySQL, $MySQL);
$query_rsAlumnos = "SELECT	ac.idAlumnoCurso, 
a.nombres, 
a.apellidos, 
a.identificacion,
b.nota,
b.nota1,
b.nota2,
b.nota3,
b.nota4,
b.nota5,
b.nota6,
b.nota7,
b.nota8,
b.nota9,
b.nota10,
b.nota11,
b.nota12,
b.nota13,
b.nota14,							
b.fallas,
b.recuperacion,
b.idBoletin,
a.idAlumno,
c.idCurso,
".$_GET['idMateria']." as idMateria,
".$_GET['bimestre']." as bimestre
FROM alumnocurso as ac
inner join alumno as a on a.idAlumno = ac.idAlumno
inner join curso as c on c.idCurso = ac.idCurso						   
left join boletin as b 	on b.idAlumno = a.idAlumno 
and b.idCurso = c.idCurso 
and b.idMateria = ".$_GET['idMateria']."	
and b.bimestre = ".$_GET['bimestre']."
WHERE ac.idCurso = ".$_GET['idCurso']." ORDER BY a.apellidos asc, a.nombres ASC";
$rsAlumnos = mysql_query($query_rsAlumnos, $MySQL) or die(mysql_error());
$row_rsAlumnos = mysql_fetch_assoc($rsAlumnos);
$totalRows_rsAlumnos = mysql_num_rows($rsAlumnos);
?>		
<table border="1" width="100%">
  <thead>
    <tr align="center">
      <td colspan="5"></td>
      <td colspan="6">Cognitivo (40%)</td>
      <td colspan="6">Procedimental (40%)</td>
      <td colspan="2">Actitudinal (20%)</td>
      <td colspan="5"></td>
    </tr>
    <tr align="center">

      <td>No.</td>
      <td>Apellidos</td>    
      <td>Nombres</td>
      <td>Identificacion</td>
      <td>Estado</td>
      <td>Nota 1</td>
      <td>Nota 2</td>
      <td>Nota 3</td>
      <td>Nota 4</td>
      <td>Nota 5</td>
      <td>Nota 6</td>
      <td>Nota 7</td>
      <td>Nota 8</td>
      <td>Nota 9</td>
      <td>Nota 10</td>
      <td>Nota 11</td>
      <td>Nota 12</td>
      <td>Nota 13</td>
      <td>Nota 14</td>
      <td>Rec.</td> 
      <td>Definitiva</td>       
      <td>Fallas</td>
      <td>Promedio proyectado</td>
      <td>Promedio acumulado</td>

    </tr>
  </thead>
  <?php $i = 1;  do { ?>
  <tr align="center" id="tr_<? echo $row_rsAlumnos['idAlumno']; ?>">
    <td><? echo $i++; ?></td>
    <td align="left"><?php echo $row_rsAlumnos['apellidos']; ?></td>
    <td align="left"><?php echo $row_rsAlumnos['nombres']; ?></td>      
    <td align="left"><?php echo $row_rsAlumnos['identificacion']; ?></td>
    <td align="left"><div id="resultado_<? echo $row_rsAlumnos['idAlumno']; ?>"></div></td>
    <td align="center" bgcolor="#70F48F"><input type="text" onfocus="sombrearTr(<? echo $row_rsAlumnos['idAlumno']; ?>)" onchange="ColocarNota(<? echo $row_rsAlumnos['idAlumno']; ?>,<? echo $_GET['idCurso']; ?>,<? echo $_GET['idMateria']; ?>,<? echo $_GET['bimestre']; ?>,this.id,this.value);" class="inputtextnota" name="nota1" id="nota1_<? echo $row_rsAlumnos['idAlumno']; ?>" value="<?php echo $row_rsAlumnos['nota1']; ?>"><div id="resultado_<? echo $row_rsAlumnos['idAlumno']; ?>"></div></td>
    <td align="center" bgcolor="#70F48F"><input type="text" onfocus="sombrearTr(<? echo $row_rsAlumnos['idAlumno']; ?>)" onchange="ColocarNota(<? echo $row_rsAlumnos['idAlumno']; ?>,<? echo $_GET['idCurso']; ?>,<? echo $_GET['idMateria']; ?>,<? echo $_GET['bimestre']; ?>,this.id,this.value);" class="inputtextnota" name="nota2" id="nota2_<? echo $row_rsAlumnos['idAlumno']; ?>" value="<?php echo $row_rsAlumnos['nota2']; ?>"></td>
    <td align="center" bgcolor="#70F48F"><input type="text" onfocus="sombrearTr(<? echo $row_rsAlumnos['idAlumno']; ?>)" onchange="ColocarNota(<? echo $row_rsAlumnos['idAlumno']; ?>,<? echo $_GET['idCurso']; ?>,<? echo $_GET['idMateria']; ?>,<? echo $_GET['bimestre']; ?>,this.id,this.value);" class="inputtextnota" name="nota3" id="nota3_<? echo $row_rsAlumnos['idAlumno']; ?>" value="<?php echo $row_rsAlumnos['nota3']; ?>"></td>
    <td align="center" bgcolor="#70F48F"><input type="text" onfocus="sombrearTr(<? echo $row_rsAlumnos['idAlumno']; ?>)" onchange="ColocarNota(<? echo $row_rsAlumnos['idAlumno']; ?>,<? echo $_GET['idCurso']; ?>,<? echo $_GET['idMateria']; ?>,<? echo $_GET['bimestre']; ?>,this.id,this.value);" class="inputtextnota" name="nota4" id="nota4_<? echo $row_rsAlumnos['idAlumno']; ?>" value="<?php echo $row_rsAlumnos['nota4']; ?>"></td>
    <td align="center" bgcolor="#70F48F"><input type="text" onfocus="sombrearTr(<? echo $row_rsAlumnos['idAlumno']; ?>)" onchange="ColocarNota(<? echo $row_rsAlumnos['idAlumno']; ?>,<? echo $_GET['idCurso']; ?>,<? echo $_GET['idMateria']; ?>,<? echo $_GET['bimestre']; ?>,this.id,this.value);" class="inputtextnota" name="nota5" id="nota5_<? echo $row_rsAlumnos['idAlumno']; ?>" value="<?php echo $row_rsAlumnos['nota5']; ?>"></td>
    <td align="center" bgcolor="#70F48F"><input type="text" onfocus="sombrearTr(<? echo $row_rsAlumnos['idAlumno']; ?>)" onchange="ColocarNota(<? echo $row_rsAlumnos['idAlumno']; ?>,<? echo $_GET['idCurso']; ?>,<? echo $_GET['idMateria']; ?>,<? echo $_GET['bimestre']; ?>,this.id,this.value);" class="inputtextnota" name="nota6" id="nota6_<? echo $row_rsAlumnos['idAlumno']; ?>" value="<?php echo $row_rsAlumnos['nota6']; ?>"></td>
    <td align="center" bgcolor="#70D5F4"><input type="text" onfocus="sombrearTr(<? echo $row_rsAlumnos['idAlumno']; ?>)" onchange="ColocarNota(<? echo $row_rsAlumnos['idAlumno']; ?>,<? echo $_GET['idCurso']; ?>,<? echo $_GET['idMateria']; ?>,<? echo $_GET['bimestre']; ?>,this.id,this.value);" class="inputtextnota" name="nota7" id="nota7_<? echo $row_rsAlumnos['idAlumno']; ?>" value="<?php echo $row_rsAlumnos['nota7']; ?>"></td>
    <td align="center" bgcolor="#70D5F4"><input type="text" onfocus="sombrearTr(<? echo $row_rsAlumnos['idAlumno']; ?>)" onchange="ColocarNota(<? echo $row_rsAlumnos['idAlumno']; ?>,<? echo $_GET['idCurso']; ?>,<? echo $_GET['idMateria']; ?>,<? echo $_GET['bimestre']; ?>,this.id,this.value);" class="inputtextnota" name="nota8" id="nota8_<? echo $row_rsAlumnos['idAlumno']; ?>" value="<?php echo $row_rsAlumnos['nota8']; ?>"></td>
    <td align="center" bgcolor="#70D5F4"><input type="text" onfocus="sombrearTr(<? echo $row_rsAlumnos['idAlumno']; ?>)" onchange="ColocarNota(<? echo $row_rsAlumnos['idAlumno']; ?>,<? echo $_GET['idCurso']; ?>,<? echo $_GET['idMateria']; ?>,<? echo $_GET['bimestre']; ?>,this.id,this.value);" class="inputtextnota" name="nota9" id="nota9_<? echo $row_rsAlumnos['idAlumno']; ?>" value="<?php echo $row_rsAlumnos['nota9']; ?>"></td>
    <td align="center" bgcolor="#70D5F4"><input type="text" onfocus="sombrearTr(<? echo $row_rsAlumnos['idAlumno']; ?>)" onchange="ColocarNota(<? echo $row_rsAlumnos['idAlumno']; ?>,<? echo $_GET['idCurso']; ?>,<? echo $_GET['idMateria']; ?>,<? echo $_GET['bimestre']; ?>,this.id,this.value);" class="inputtextnota" name="nota10" id="nota10_<? echo $row_rsAlumnos['idAlumno']; ?>" value="<?php echo $row_rsAlumnos['nota10']; ?>"></td>
    <td align="center" bgcolor="#70D5F4"><input type="text" onfocus="sombrearTr(<? echo $row_rsAlumnos['idAlumno']; ?>)" onchange="ColocarNota(<? echo $row_rsAlumnos['idAlumno']; ?>,<? echo $_GET['idCurso']; ?>,<? echo $_GET['idMateria']; ?>,<? echo $_GET['bimestre']; ?>,this.id,this.value);" class="inputtextnota" name="nota11" id="nota11_<? echo $row_rsAlumnos['idAlumno']; ?>" value="<?php echo $row_rsAlumnos['nota11']; ?>"></td>
    <td align="center" bgcolor="#70D5F4"><input type="text" onfocus="sombrearTr(<? echo $row_rsAlumnos['idAlumno']; ?>)" onchange="ColocarNota(<? echo $row_rsAlumnos['idAlumno']; ?>,<? echo $_GET['idCurso']; ?>,<? echo $_GET['idMateria']; ?>,<? echo $_GET['bimestre']; ?>,this.id,this.value);" class="inputtextnota" name="nota12" id="nota12_<? echo $row_rsAlumnos['idAlumno']; ?>" value="<?php echo $row_rsAlumnos['nota12']; ?>"></td>
    <td align="center" bgcolor="#F470E2"><input type="text" onfocus="sombrearTr(<? echo $row_rsAlumnos['idAlumno']; ?>)" onchange="ColocarNota(<? echo $row_rsAlumnos['idAlumno']; ?>,<? echo $_GET['idCurso']; ?>,<? echo $_GET['idMateria']; ?>,<? echo $_GET['bimestre']; ?>,this.id,this.value);" class="inputtextnota" name="nota13" id="nota13_<? echo $row_rsAlumnos['idAlumno']; ?>" value="<?php echo $row_rsAlumnos['nota13']; ?>"></td>
    <td align="center" bgcolor="#F470E2"><input type="text" onfocus="sombrearTr(<? echo $row_rsAlumnos['idAlumno']; ?>)" onchange="ColocarNota(<? echo $row_rsAlumnos['idAlumno']; ?>,<? echo $_GET['idCurso']; ?>,<? echo $_GET['idMateria']; ?>,<? echo $_GET['bimestre']; ?>,this.id,this.value);" class="inputtextnota" name="nota14" id="nota14_<? echo $row_rsAlumnos['idAlumno']; ?>" value="<?php echo $row_rsAlumnos['nota14']; ?>"></td>
    <td align="center" bgcolor="#F2993F"><input type="text" onfocus="sombrearTr(<? echo $row_rsAlumnos['idAlumno']; ?>)" onchange="ColocarNota(<? echo $row_rsAlumnos['idAlumno']; ?>,<? echo $_GET['idCurso']; ?>,<? echo $_GET['idMateria']; ?>,<? echo $_GET['bimestre']; ?>,this.id,this.value);" class="inputtextnota" name="recuperacion" id="recuperacion_<? echo $row_rsAlumnos['idAlumno']; ?>" value="<?php echo $row_rsAlumnos['recuperacion']; ?>"></td>
    
    <td bgcolor="#E9F470">
      <input type="text" onfocus="sombrearTr(<? echo $row_rsAlumnos['idAlumno']; ?>)"  class="inputtextnota" readonly="readonly" name="definitiva" id="definitiva_<? echo $row_rsAlumnos['idAlumno']; ?>" value="<?php      
      $paraPromediar1 = 0;
      if ($row_rsAlumnos['nota1'] > 0) { $paraPromediar1++;	}
      if ($row_rsAlumnos['nota2'] > 0) { $paraPromediar1++;	}
      if ($row_rsAlumnos['nota3'] > 0) { $paraPromediar1++;	}
      if ($row_rsAlumnos['nota4'] > 0) { $paraPromediar1++;	}
      if ($row_rsAlumnos['nota5'] > 0) { $paraPromediar1++;	}
      if ($row_rsAlumnos['nota6'] > 0) { $paraPromediar1++;	}
      if ($paraPromediar1 == 0) {  $paraPromediar1 = 1;}
      $paraPromediar2 = 0;
      if ($row_rsAlumnos['nota7'] > 0) { $paraPromediar2++;	}
      if ($row_rsAlumnos['nota8'] > 0) { $paraPromediar2++;	}
      if ($row_rsAlumnos['nota9'] > 0) { $paraPromediar2++;	}
      if ($row_rsAlumnos['nota10'] > 0) { $paraPromediar2++;	}
      if ($row_rsAlumnos['nota11'] > 0) { $paraPromediar2++;	}
      if ($row_rsAlumnos['nota12'] > 0) { $paraPromediar2++;	}
      if ($paraPromediar2 == 0) {  $paraPromediar2 = 1;}

      $paraPromediar3 = 0;
      if ($row_rsAlumnos['nota13'] > 0) { $paraPromediar3++;	}
      if ($row_rsAlumnos['nota14'] > 0) { $paraPromediar3++;	}
      if ($paraPromediar3 == 0) {  $paraPromediar3 = 1;}

      echo number_format(
        (((($row_rsAlumnos['nota1'] +
         $row_rsAlumnos['nota2'] +
         $row_rsAlumnos['nota3'] +
         $row_rsAlumnos['nota4'] +
         $row_rsAlumnos['nota5'] +
         $row_rsAlumnos['nota6'] )/$paraPromediar1) * 40 )/100)
+
(((($row_rsAlumnos['nota7'] +
	$row_rsAlumnos['nota8'] +
	$row_rsAlumnos['nota9'] +
	$row_rsAlumnos['nota10'] +
	$row_rsAlumnos['nota11'] +
	$row_rsAlumnos['nota12'] )/$paraPromediar2) * 40 )/100)	
+
(((($row_rsAlumnos['nota13'] +
$row_rsAlumnos['nota14'] )/$paraPromediar3) * 20 )/100),1);		?>">
</td>
<td align="center" bgcolor="#F47070">
  <input type="text" onfocus="sombrearTr(<? echo $row_rsAlumnos['idAlumno']; ?>)" onchange="ColocarNota(<? echo $row_rsAlumnos['idAlumno']; ?>,<? echo $_GET['idCurso']; ?>,<? echo $_GET['idMateria']; ?>,<? echo $_GET['bimestre']; ?>,this.id,this.value);" class="inputtextnota" name="fallas" id="fallas_<? echo $row_rsAlumnos['idAlumno']; ?>" value="<?php echo $row_rsAlumnos['fallas']; ?>">
</td>
  <?
  $promedioActualAcumulado = 0;
  $promediosEncontrados = array();
  for($bimestreActual = 1 ; $bimestreActual <= $_GET['bimestre']; $bimestreActual++)
  {
  $promediosEncontrados[$bimestreActual] = $Notas->notaFinalBimestre ($row_rsAlumnos['idAlumno'],$_GET['idCurso'],$_GET['idMateria'],$bimestreActual); 
  }
  $promedioActualAcumulado = array_sum($promediosEncontrados) / count($promediosEncontrados) ;

  $promedioProyectado = ($promedioActualAcumulado + $Notas->notaFinalBimestre ($row_rsAlumnos['idAlumno'],$_GET['idCurso'],$_GET['idMateria'],$_GET['bimestre']))/ 2;
  ?>

<td>
 <input class="inputtextnota" type="text" name="promedioProyectado_<? echo $row_rsAlumnos['idAlumno']; ?>" value="<? echo number_format($promedioProyectado,1); ?>" id="promedioProyectado_<? echo $row_rsAlumnos['idAlumno']; ?>" readonly="readonly" >
  </td>
<td>
  <input class="inputtextnota" type="text" name="promedioActualAcumulado_<? echo $row_rsAlumnos['idAlumno']; ?>" value="<? echo number_format($promedioActualAcumulado,1); ?>" id="promedioActualAcumulado_<? echo $row_rsAlumnos['idAlumno']; ?>" readonly="readonly" >

</td>
</tr>
<?php } while ($row_rsAlumnos = mysql_fetch_assoc($rsAlumnos)); ?>
</table>
<?php
mysql_free_result($rsAlumnos);
?>
