<?php require_once('Connections/MySQL.php'); ?>
<?php
if (!isset($_SESSION)) {
  session_start();
}
$MM_authorizedUsers = "Administrador,Profesor";
$MM_donotCheckaccess = "false";

// *** Restrict Access To Page: Grant or deny access to this page
function isAuthorized($strUsers, $strGroups, $UserName, $UserGroup) { 
  // For security, start by assuming the visitor is NOT authorized. 
  $isValid = False; 

  // When a visitor has logged into this site, the Session variable MM_Username set equal to their username. 
  // Therefore, we know that a user is NOT logged in if that Session variable is blank. 
  if (!empty($UserName)) { 
    // Besides being logged in, you may restrict access to only certain users based on an ID established when they login. 
    // Parse the strings into arrays. 
    $arrUsers = Explode(",", $strUsers); 
    $arrGroups = Explode(",", $strGroups); 
    if (in_array($UserName, $arrUsers)) { 
      $isValid = true; 
    } 
    // Or, you may restrict access to only certain users based on their username. 
    if (in_array($UserGroup, $arrGroups)) { 
      $isValid = true; 
    } 
    if (($strUsers == "") && false) { 
      $isValid = true; 
    } 
  } 
  return $isValid; 
}

$MM_restrictGoTo = "error.php";
if (!((isset($_SESSION['MM_Username'])) && (isAuthorized("",$MM_authorizedUsers, $_SESSION['MM_Username'], $_SESSION['MM_UserGroup'])))) {   
  $MM_qsChar = "?";
  $MM_referrer = $_SERVER['PHP_SELF'];
  if (strpos($MM_restrictGoTo, "?")) $MM_qsChar = "&";
  if (isset($QUERY_STRING) && strlen($QUERY_STRING) > 0) 
  $MM_referrer .= "?" . $QUERY_STRING;
  $MM_restrictGoTo = $MM_restrictGoTo. $MM_qsChar . "accesscheck=" . urlencode($MM_referrer);
  header("Location: ". $MM_restrictGoTo); 
  exit;
}
?>
<?php
if (!function_exists("GetSQLValueString")) {
function GetSQLValueString($theValue, $theType, $theDefinedValue = "", $theNotDefinedValue = "") 
{
  if (PHP_VERSION < 6) {
    $theValue = get_magic_quotes_gpc() ? stripslashes($theValue) : $theValue;
  }

  $theValue = function_exists("mysql_real_escape_string") ? mysql_real_escape_string($theValue) : mysql_escape_string($theValue);

  switch ($theType) {
    case "text":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;    
    case "long":
    case "int":
      $theValue = ($theValue != "") ? intval($theValue) : "NULL";
      break;
    case "double":
      $theValue = ($theValue != "") ? doubleval($theValue) : "NULL";
      break;
    case "date":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;
    case "defined":
      $theValue = ($theValue != "") ? $theDefinedValue : $theNotDefinedValue;
      break;
  }
  return $theValue;
}
}

mysql_select_db($database_MySQL, $MySQL);
if($_GET['curso'] == "TODOS" || $_GET['curso'] == "")
	{
	$curso = " 1 = 1 ";	
	}
	else
	{
	$curso = " c.idCurso = ".$_GET['curso'];	
	}	
$query_rsReporteAsignacionAcademica = "select	c.curso as Curso,  				u.nombre as Profesor,  				m.materia as Materia,  				cmp.intensidadHoraria as Intensidad from curso as c  inner join cursomateriaprofesor as cmp on cmp.idCurso = c.idCurso inner join materia as m on m.idMateria = cmp.idMateria inner join usuario as u on u.idUsuario = cmp.idProfesor  where ano = ".$_GET['ano']." and ".$curso."   order by c.orden asc, u.nombre asc";
$rsReporteAsignacionAcademica = mysql_query($query_rsReporteAsignacionAcademica, $MySQL) or die(mysql_error());
$row_rsReporteAsignacionAcademica = mysql_fetch_assoc($rsReporteAsignacionAcademica);
$totalRows_rsReporteAsignacionAcademica = mysql_num_rows($rsReporteAsignacionAcademica);
?><br />

<table  align="center">
<thead>
  <tr>
    <td>Curso</td>
    <td>Profesor</td>
    <td>Materia</td>
    <td>Intensidad</td>
  </tr>
  </thead>
  <tbody>
  <?php do { ?>
    <tr>
      <td><?php echo $row_rsReporteAsignacionAcademica['Curso']; ?></td>
      <td><?php echo $row_rsReporteAsignacionAcademica['Profesor']; ?></td>
      <td><?php echo $row_rsReporteAsignacionAcademica['Materia']; ?></td>
      <td align="center"><?php echo $row_rsReporteAsignacionAcademica['Intensidad']; ?></td>
    </tr>
    <?php } while ($row_rsReporteAsignacionAcademica = mysql_fetch_assoc($rsReporteAsignacionAcademica)); ?>
    </tbody>
</table>
<?php
mysql_free_result($rsReporteAsignacionAcademica);
?>
