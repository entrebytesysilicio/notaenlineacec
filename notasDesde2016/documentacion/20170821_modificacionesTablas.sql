﻿alter table cursomateriaprofesornota
  add column `fechaActividad` datetime null comment 'Fecha en la que se realizará la actividad.' after `fechaCreacion`;

alter table matricula
  add column `fechaMatricula` datetime null comment 'Fecha de la matrícula.' after `matricula`;