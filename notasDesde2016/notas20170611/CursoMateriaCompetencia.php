<?php require_once('Connections/MySQL.php'); ?>
<?php
if (!isset($_SESSION)) {
  session_start();
}
$MM_authorizedUsers = "Administrador,Profesor";
$MM_donotCheckaccess = "false";

// *** Restrict Access To Page: Grant or deny access to this page
function isAuthorized($strUsers, $strGroups, $UserName, $UserGroup) { 
  // For security, start by assuming the visitor is NOT authorized. 
  $isValid = False; 

  // When a visitor has logged into this site, the Session variable MM_Username set equal to their username. 
  // Therefore, we know that a user is NOT logged in if that Session variable is blank. 
  if (!empty($UserName)) { 
    // Besides being logged in, you may restrict access to only certain users based on an ID established when they login. 
    // Parse the strings into arrays. 
    $arrUsers = Explode(",", $strUsers); 
    $arrGroups = Explode(",", $strGroups); 
    if (in_array($UserName, $arrUsers)) { 
      $isValid = true; 
    } 
    // Or, you may restrict access to only certain users based on their username. 
    if (in_array($UserGroup, $arrGroups)) { 
      $isValid = true; 
    } 
    if (($strUsers == "") && false) { 
      $isValid = true; 
    } 
  } 
  return $isValid; 
}

$MM_restrictGoTo = "error.php";
if (!((isset($_SESSION['MM_Username'])) && (isAuthorized("",$MM_authorizedUsers, $_SESSION['MM_Username'], $_SESSION['MM_UserGroup'])))) {   
  $MM_qsChar = "?";
  $MM_referrer = $_SERVER['PHP_SELF'];
  if (strpos($MM_restrictGoTo, "?")) $MM_qsChar = "&";
  if (isset($QUERY_STRING) && strlen($QUERY_STRING) > 0) 
  $MM_referrer .= "?" . $QUERY_STRING;
  $MM_restrictGoTo = $MM_restrictGoTo. $MM_qsChar . "accesscheck=" . urlencode($MM_referrer);
  header("Location: ". $MM_restrictGoTo); 
  exit;
}
?>
<?php
if (!function_exists("GetSQLValueString")) {
function GetSQLValueString($theValue, $theType, $theDefinedValue = "", $theNotDefinedValue = "") 
{
  if (PHP_VERSION < 6) {
    $theValue = get_magic_quotes_gpc() ? stripslashes($theValue) : $theValue;
  }

  $theValue = function_exists("mysql_real_escape_string") ? mysql_real_escape_string($theValue) : mysql_escape_string($theValue);

  switch ($theType) {
    case "text":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;    
    case "long":
    case "int":
      $theValue = ($theValue != "") ? intval($theValue) : "NULL";
      break;
    case "double":
      $theValue = ($theValue != "") ? doubleval($theValue) : "NULL";
      break;
    case "date":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;
    case "defined":
      $theValue = ($theValue != "") ? $theDefinedValue : $theNotDefinedValue;
      break;
  }
  return $theValue;
}
}

mysql_select_db($database_MySQL, $MySQL);
$query_rsCurso = "SELECT * FROM curso ORDER BY ano desc, curso asc";
$rsCurso = mysql_query($query_rsCurso, $MySQL) or die(mysql_error());
$row_rsCurso = mysql_fetch_assoc($rsCurso);
$totalRows_rsCurso = mysql_num_rows($rsCurso);

mysql_select_db($database_MySQL, $MySQL);
$query_rsMateria = "SELECT * FROM materia ORDER BY materia ASC";
$rsMateria = mysql_query($query_rsMateria, $MySQL) or die(mysql_error());
$row_rsMateria = mysql_fetch_assoc($rsMateria);
$totalRows_rsMateria = mysql_num_rows($rsMateria);
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title>:: Curso / Materia / Competencia ::</title>
<script language="javascript" type="text/javascript">
function fCursoMateriaCompetenciaCrear()
	{
	var idCurso, idMateria, bimestre, competencia;
	idCurso =document.getElementById('idCurso');
	idCurso = idCurso.options[idCurso.selectedIndex].value;	
	idMateria =document.getElementById('idMateria');
	idMateria = idMateria.options[idMateria.selectedIndex].value;	
	bimestre =document.getElementById('bimestre');
	bimestre = bimestre.options[bimestre.selectedIndex].value;	
	competencia =document.getElementById('competencia').value;
	mostrar_url('CursoMateriaCompetenciaCrear.php','MM_insert=form1&idCurso='+idCurso+'&idMateria='+idMateria+'&bimestre='+bimestre+'&competencia='+competencia,'divCursoMateriaCompetencia','get');
	demorar('fCursoMateriaCompetenciaListar()',2);	
	}
function fCursoMateriaCompetenciaListar()
	{
	var idCurso, idMateria, bimestre ;
	idCurso =document.getElementById('idCurso');
	idCurso = idCurso.options[idCurso.selectedIndex].value;	
	idMateria =document.getElementById('idMateria');
	idMateria = idMateria.options[idMateria.selectedIndex].value;	
	bimestre =document.getElementById('bimestre');
	bimestre = bimestre.options[bimestre.selectedIndex].value;	
	mostrar_url('CursoMateriaCompetenciaListar.php','idCurso='+idCurso+'&idMateria='+idMateria+'&bimestre='+bimestre,'divCursoMateriaCompetencia','get');
	}	
	
function fCursoMateriaCompetenciaEliminar(id)
	{
		if(confirm('Desea eliminar el registro?.'))
			{
			mostrar_url('CursoMateriaCompetenciaEliminar.php','idCursoMateriaCompetencia='+id,'divCursoMateriaCompetencia','get');
			demorar('fCursoMateriaCompetenciaListar()',2);				
			}
	}		
</script>
<? include "header.php"; ?>
</head>

<body>
<? include "menu.php"; ?>
<table border="1">
<tr valign="top"><td>
<table>
<thead>
<tr><td colspan="2">Curso / Materia / Competencia</td></tr>
</thead>
<tr><td>Curso:</td><td><select id="idCurso" name="idCurso" onchange="javascript:fCursoMateriaCompetenciaListar();"><option></option>
  <?php
do {  
?>
  <option value="<?php echo $row_rsCurso['idCurso']?>"><?php echo $row_rsCurso['ano']?> - <?php echo $row_rsCurso['curso']?></option>
  <?php
} while ($row_rsCurso = mysql_fetch_assoc($rsCurso));
  $rows = mysql_num_rows($rsCurso);
  if($rows > 0) {
      mysql_data_seek($rsCurso, 0);
	  $row_rsCurso = mysql_fetch_assoc($rsCurso);
  }
?>
</select></td></tr>
<tr><td>Materia:</td><td><select id="idMateria" name="idMateria" onchange="javascript:fCursoMateriaCompetenciaListar();"><option></option>
  <?php
do {  
?>
  <option value="<?php echo $row_rsMateria['idMateria']?>"><?php echo $row_rsMateria['materia']?></option>
  <?php
} while ($row_rsMateria = mysql_fetch_assoc($rsMateria));
  $rows = mysql_num_rows($rsMateria);
  if($rows > 0) {
      mysql_data_seek($rsMateria, 0);
	  $row_rsMateria = mysql_fetch_assoc($rsMateria);
  }
?>
</select></td></tr>
<tr><td>Periodo:</td><td><select id="bimestre" name="bimestre"  onchange="javascript:fCursoMateriaCompetenciaListar();"><option></option><option>1</option><option>2</option><option>3</option><option>4</option></select></td></tr>
<tr><td colspan="2">Competencia:<br />
<textarea id="competencia" name="competencia"></textarea></td></tr>
<thead><tr><td colspan="2"><input type="button" name="Crear" value="Crear" onclick="javascript:fCursoMateriaCompetenciaCrear()" /></td></tr></thead>
</table>
</td>
<td width="100%">
<div id="divCursoMateriaCompetencia"></div>
</td>
</table>
</body>
</html>
<?php
mysql_free_result($rsCurso);

mysql_free_result($rsMateria);
?>
