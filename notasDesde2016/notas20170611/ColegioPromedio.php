<?php
if (!isset($_SESSION)) {
  session_start();
}
$MM_authorizedUsers = "Administrador,Padre,Profesor";
$MM_donotCheckaccess = "false";

// *** Restrict Access To Page: Grant or deny access to this page
function isAuthorized($strUsers, $strGroups, $UserName, $UserGroup) { 
  // For security, start by assuming the visitor is NOT authorized. 
  $isValid = False; 

  // When a visitor has logged into this site, the Session variable MM_Username set equal to their username. 
  // Therefore, we know that a user is NOT logged in if that Session variable is blank. 
  if (!empty($UserName)) { 
    // Besides being logged in, you may restrict access to only certain users based on an ID established when they login. 
    // Parse the strings into arrays. 
    $arrUsers = Explode(",", $strUsers); 
    $arrGroups = Explode(",", $strGroups); 
    if (in_array($UserName, $arrUsers)) { 
      $isValid = true; 
    } 
    // Or, you may restrict access to only certain users based on their username. 
    if (in_array($UserGroup, $arrGroups)) { 
      $isValid = true; 
    } 
    if (($strUsers == "") && false) { 
      $isValid = true; 
    } 
  } 
  return $isValid; 
}

$MM_restrictGoTo = "error.php";
if (!((isset($_SESSION['MM_Username'])) && (isAuthorized("",$MM_authorizedUsers, $_SESSION['MM_Username'], $_SESSION['MM_UserGroup'])))) {   
  $MM_qsChar = "?";
  $MM_referrer = $_SERVER['PHP_SELF'];
  if (strpos($MM_restrictGoTo, "?")) $MM_qsChar = "&";
  if (isset($_SERVER['QUERY_STRING']) && strlen($_SERVER['QUERY_STRING']) > 0) 
  $MM_referrer .= "?" . $_SERVER['QUERY_STRING'];
  $MM_restrictGoTo = $MM_restrictGoTo. $MM_qsChar . "accesscheck=" . urlencode($MM_referrer);
  header("Location: ". $MM_restrictGoTo); 
  exit;
}
?>
<?
		////busca el promedio del alumno en el periodo 
		////si el periodo es 5, se debe buscar el promedio durante todo el año escolar
		$sql7 = "
						select 	b.idAlumno,
						avg(
						((((nota1 +
						nota2 +
						nota3 +
						nota4 +
						nota5 +
						nota6 )/
						
						(
						case when nota1 > 0 then 1 else 0 end 	
						+
						case when nota2 > 0 then 1 else 0 end 	
						+
						case when nota3 > 0 then 1 else 0 end 	
						+
						case when nota4 > 0 then 1 else 0 end 	
						+
						case when nota5 > 0 then 1 else 0 end 	
						+
						case when nota6 > 0 then 1 else 0 end 	
						)
						) * 40 )/100)
						+
						((((nota7 +
						nota8 +
						nota9 +
						nota10 +
						nota11 +
						nota12 )/
						(
						case when nota7 > 0 then 1 else 0 end 	
						+
						case when nota8 > 0 then 1 else 0 end 	
						+
						case when nota9 > 0 then 1 else 0 end 	
						+
						case when nota10 > 0 then 1 else 0 end 	
						+
						case when nota11 > 0 then 1 else 0 end 	
						+
						case when nota12 > 0 then 1 else 0 end 	
						)						
						
						) * 40 )/100)	
						+
						((((nota13 +
						nota14 )/
						(
						case when nota11 > 0 then 1 else 0 end 	
						+
						case when nota12 > 0 then 1 else 0 end 							
						)
						) * 20 )/100)) as promedio	
						
						from 	boletin as b
						inner join alumno as a on a.idAlumno = b.idAlumno
						inner join curso as c on b.idCurso = c.idCurso
						where 		b.idCurso = ".$_GET['idCurso'];
		if ($_GET['periodo'] != 5)
			{
			$sql7 .= " and b.bimestre = ".$_GET['periodo'];
			}
		$sql7 .= "	group by 	b.idAlumno order by 2 desc";
		$resultado7 = mysql_query($sql7,$MySQL);
		$filas7 = mysql_fetch_array($resultado7);
		$vandera = 0;
		$puesto = 1;
		do {
				////si el alumno buscado es igual al alumno que se esta recorriendo en el cursor, entonces guardo las variables para poder mostrarlas luego.
				if ($_GET['idAlumno'] == $filas7['idAlumno'] )
					{
					$puesto_alumno = $puesto;
					////Se coloca la vandera, por si el programa encuentra al alumno, termine la ejecucion del ciclo
					$vandera = 1;
					$promedio = $filas7['promedio']; 
					}
					$puesto++;
				}while( ($filas7 = mysql_fetch_array($resultado7)) and $vandera == 0);
	////muestra el promedio.
	echo substr($promedio,0,4);
	?>