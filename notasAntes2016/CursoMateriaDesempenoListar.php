<?php require_once('Connections/MySQL.php'); ?>
<?php
if (!isset($_SESSION)) {
  session_start();
}
$MM_authorizedUsers = "Administrador,Profesor";
$MM_donotCheckaccess = "false";

// *** Restrict Access To Page: Grant or deny access to this page
function isAuthorized($strUsers, $strGroups, $UserName, $UserGroup) { 
  // For security, start by assuming the visitor is NOT authorized. 
  $isValid = False; 

  // When a visitor has logged into this site, the Session variable MM_Username set equal to their username. 
  // Therefore, we know that a user is NOT logged in if that Session variable is blank. 
  if (!empty($UserName)) { 
    // Besides being logged in, you may restrict access to only certain users based on an ID established when they login. 
    // Parse the strings into arrays. 
    $arrUsers = Explode(",", $strUsers); 
    $arrGroups = Explode(",", $strGroups); 
    if (in_array($UserName, $arrUsers)) { 
      $isValid = true; 
    } 
    // Or, you may restrict access to only certain users based on their username. 
    if (in_array($UserGroup, $arrGroups)) { 
      $isValid = true; 
    } 
    if (($strUsers == "") && false) { 
      $isValid = true; 
    } 
  } 
  return $isValid; 
}

$MM_restrictGoTo = "error.php";
if (!((isset($_SESSION['MM_Username'])) && (isAuthorized("",$MM_authorizedUsers, $_SESSION['MM_Username'], $_SESSION['MM_UserGroup'])))) {   
  $MM_qsChar = "?";
  $MM_referrer = $_SERVER['PHP_SELF'];
  if (strpos($MM_restrictGoTo, "?")) $MM_qsChar = "&";
  if (isset($QUERY_STRING) && strlen($QUERY_STRING) > 0) 
  $MM_referrer .= "?" . $QUERY_STRING;
  $MM_restrictGoTo = $MM_restrictGoTo. $MM_qsChar . "accesscheck=" . urlencode($MM_referrer);
  header("Location: ". $MM_restrictGoTo); 
  exit;
}
?>
<?php
if (!function_exists("GetSQLValueString")) {
function GetSQLValueString($theValue, $theType, $theDefinedValue = "", $theNotDefinedValue = "") 
{
  if (PHP_VERSION < 6) {
    $theValue = get_magic_quotes_gpc() ? stripslashes($theValue) : $theValue;
  }

  $theValue = function_exists("mysql_real_escape_string") ? mysql_real_escape_string($theValue) : mysql_escape_string($theValue);

  switch ($theType) {
    case "text":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;    
    case "long":
    case "int":
      $theValue = ($theValue != "") ? intval($theValue) : "NULL";
      break;
    case "double":
      $theValue = ($theValue != "") ? doubleval($theValue) : "NULL";
      break;
    case "date":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;
    case "defined":
      $theValue = ($theValue != "") ? $theDefinedValue : $theNotDefinedValue;
      break;
  }
  return $theValue;
}
}

$colname_rsCursoMateriaDesempeno = "-1";
if (isset($_GET['idCurso'])) {
  $colname_rsCursoMateriaDesempeno = $_GET['idCurso'];
}
mysql_select_db($database_MySQL, $MySQL);
$query_rsCursoMateriaDesempeno =	"
										 SELECT cmd.idCursoMateriaDesempeno, 
										 		c.ano, 
												c.curso, 
												m.materia, 
												cmd.bimestre, 
												d.desempeno, 
												d.notaMinima, 
												d.notaMaxima,
												cmd.observacion
										 FROM 	cursomateriadesempeno as cmd
										 	inner join curso as c on c.idCurso = cmd.idCurso
											inner join materia as m on m.idMateria = cmd.idMateria
											inner join desempeno as d on d.idDesempeno = cmd.idDesempeno
										 WHERE 	1 = 1 ";
										 
if($_GET['idCurso'])
	{
		$query_rsCursoMateriaDesempeno .= " and cmd.idCurso = ".$_GET['idCurso'];
	}

if($_GET['idMateria'])
	{
		$query_rsCursoMateriaDesempeno .= " and cmd.idMateria = ".$_GET['idMateria'];
	}
	
if($_GET['idDesempeno'])
	{
		$query_rsCursoMateriaDesempeno .= " and cmd.idDesempeno = ".$_GET['idDesempeno'];
	}	
	
if($_GET['bimestre'])
	{
		$query_rsCursoMateriaDesempeno .= " and cmd.bimestre = ".$_GET['bimestre'];
	}	
							
$query_rsCursoMateriaDesempeno .= " order by d.notaMinima asc";							
$rsCursoMateriaDesempeno = mysql_query($query_rsCursoMateriaDesempeno, $MySQL) or die(mysql_error());
$row_rsCursoMateriaDesempeno = mysql_fetch_assoc($rsCursoMateriaDesempeno);
$totalRows_rsCursoMateriaDesempeno = mysql_num_rows($rsCursoMateriaDesempeno);
if ($totalRows_rsCursoMateriaDesempeno > 0)
	{
?>

<table border="1" width="100%">
<thead>
  <tr>
    <td>No.</td>
    <td>Año</td>
    <td>Curso</td>
    <td>Materia</td>
    <td>Periodo</td>
    <td>Desempeño</td>
    <td>Nota Minima</td>
    <td>Nota Maxima</td>
    <td>Observacion</td>  
     <td>Accion</td>  
  </tr>
  </thead>
  <?php $i = 1; do { ?>
    <tr valign="top">
      <td align="center"><? echo $i++; ?></td>
      <td align="center"><?php echo $row_rsCursoMateriaDesempeno['ano']; ?></td>
      <td><?php echo $row_rsCursoMateriaDesempeno['curso']; ?></td>
      <td><?php echo $row_rsCursoMateriaDesempeno['materia']; ?></td>
      <td align="center"><?php echo $row_rsCursoMateriaDesempeno['bimestre']; ?></td>
      <td><?php echo $row_rsCursoMateriaDesempeno['desempeno']; ?></td>
      <td align="center"><?php echo $row_rsCursoMateriaDesempeno['notaMinima']; ?></td>
      <td align="center"><?php echo $row_rsCursoMateriaDesempeno['notaMaxima']; ?></td>
      <td><div class="divCampoActualizable" id="divcmd<?php echo $row_rsCursoMateriaDesempeno['idCursoMateriaDesempeno']; ?>" onclick="fCursoMateriaDesempenoMostrarFormularioActualizar(<?php echo $row_rsCursoMateriaDesempeno['idCursoMateriaDesempeno']; ?>);"><?php echo $row_rsCursoMateriaDesempeno['observacion']; ?></div><div id="divcmd<?php echo $row_rsCursoMateriaDesempeno['idCursoMateriaDesempeno']; ?>formulario"  style="display:none;"><textarea id="observacion<?php echo $row_rsCursoMateriaDesempeno['idCursoMateriaDesempeno']; ?>"><?php echo $row_rsCursoMateriaDesempeno['observacion']; ?></textarea><br><input type="button" name="Actualiza" value="Actualizar" onClick="javascript:fCursoMateriaDesempenoActualizar(<?php echo $row_rsCursoMateriaDesempeno['idCursoMateriaDesempeno']; ?>);"></div></td>
      <td><input type="button" name="Eliminar" value="Eliminar" onclick="javascript:fCursoMateriaDesempenoEliminar(<?php echo $row_rsCursoMateriaDesempeno['idCursoMateriaDesempeno']; ?>);" /></td>
    </tr>
    <?php } while ($row_rsCursoMateriaDesempeno = mysql_fetch_assoc($rsCursoMateriaDesempeno)); ?>
</table>
<?php		
	}
	else
	{
	?>
	Aun no estan creados.
	<?	
	}

mysql_free_result($rsCursoMateriaDesempeno);
?>
