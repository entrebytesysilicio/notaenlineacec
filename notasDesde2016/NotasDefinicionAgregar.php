<?php require_once('Connections/MySQL.php'); ?>
<?php
if (!isset($_SESSION)) {
session_start();
}
$MM_authorizedUsers = "Administrador,Profesor";
$MM_donotCheckaccess = "false";
// *** Restrict Access To Page: Grant or deny access to this page
function isAuthorized($strUsers, $strGroups, $UserName, $UserGroup) {
// For security, start by assuming the visitor is NOT authorized.
$isValid = False;
// When a visitor has logged into this site, the Session variable MM_Username set equal to their username.
// Therefore, we know that a user is NOT logged in if that Session variable is blank.
if (!empty($UserName)) {
// Besides being logged in, you may restrict access to only certain users based on an ID established when they login.
// Parse the strings into arrays.
$arrUsers = Explode(",", $strUsers);
$arrGroups = Explode(",", $strGroups);
if (in_array($UserName, $arrUsers)) {
$isValid = true;
}
// Or, you may restrict access to only certain users based on their username.
if (in_array($UserGroup, $arrGroups)) {
$isValid = true;
}
if (($strUsers == "") && false) {
$isValid = true;
}
}
return $isValid;
}
$MM_restrictGoTo = "error.php";
if (!((isset($_SESSION['MM_Username'])) && (isAuthorized("",$MM_authorizedUsers, $_SESSION['MM_Username'], $_SESSION['MM_UserGroup'])))) {
$MM_qsChar = "?";
$MM_referrer = $_SERVER['PHP_SELF'];
if (strpos($MM_restrictGoTo, "?")) $MM_qsChar = "&";
if (isset($QUERY_STRING) && strlen($QUERY_STRING) > 0)
$MM_referrer .= "?" . $QUERY_STRING;
$MM_restrictGoTo = $MM_restrictGoTo. $MM_qsChar . "accesscheck=" . urlencode($MM_referrer);
header("Location: ". $MM_restrictGoTo);
exit;
}

if(isset($_POST['idCursoMateriaProfesor']))
{
////Contador para el bimestre (4 en total)
for ($i=1;$i <= 4; $i++ )
	{
	////Contador para las notas (14 en total)
	for ($j=1; $j <= 14 ; $j++) 
		{ 
			$sql = "insert into cursomateriaprofesornota (idCursoMateriaProfesor,bimestre,nota)  
			select ".$_POST['idCursoMateriaProfesor'].",".$i.",".$j." from dual where not exists (select 'x' from cursomateriaprofesornota as cmpn where cmpn.idCursoMateriaProfesor = ".$_POST['idCursoMateriaProfesor']." and bimestre = ".$i." and nota = ".$j." )";
			$resultado = mysql_query($sql);
		}
	}

?>
<div class="panel-group" id="collapse" role="tablist" aria-multiselectable="false">
	<?
for ($i=1;$i <= 4; $i++ )
	{
	?>
	<div class="panel panel-default">
		<div class="panel-heading" role="tab" id="headingTabBimestre<? echo $i; ?>">
			<h4 class="panel-title">
				<a role="button" data-toggle="collapse" data-parent="#tabBimestre" href="#tabBimestre<? echo $i; ?>" aria-expanded="true" aria-controls="tabBimestre<? echo $i; ?>">
					Bimestre: <?php echo $i; ?>
				</a>
			</h4>
		</div>
        <div id="tabBimestre<? echo $i; ?>" class="panel-collapse collapse <? if ($i == 1) { ?> in <? } ?>" role="tabpanel" aria-labelledby="tabBimestre<? echo $i; ?>">
            <div class="panel-body"><?php
			for ($j=1; $j <= 14 ; $j++)
				{
					$codigoFormulario = $_POST['idCursoMateriaProfesor']."_".$i."_".$j;
					$nombreFormulario = "form_".$codigoFormulario;
					$sql = "select notaNombre, notaDescripcion,fechaActividad from cursomateriaprofesornota where idCursoMateriaProfesor = ".$_POST['idCursoMateriaProfesor']." and bimestre = ".$i." and nota = ".$j;
					$resultado = mysql_query($sql,$MySQL);
					$fila = mysql_fetch_array($resultado);
				?>
                <form name="<? echo $nombreFormulario; ?>" id="" role="form">
                    <div class="row">
                        <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4"><?
							if($j <= 6) { ?>
                            Cognitiva<? echo $j; }
							if($j > 6 && $j <= 12) { ?>
                            Procedimental<? echo $j-6; }
							if($j > 12 && $j <= 14) { ?>
                            Actitudinal<? echo $j-12;	}
							?>
                            <br />Bimestre: <?php echo $i; ?>
                        </div>
                        <div class="col-xs-7 col-sm-7 col-md-7 col-lg-7">
							<input type="date" name="fechaActividad" id="fechaActividad" class="form-control" value="<?php echo substr($fila['fechaActividad'],0,10); ?>" onchange="javascript:fNotaActualizar('<? echo $codigoFormulario; ?>');" required="required" title="Fecha actividad" placeholder="Seleccione la fecha de la actividad..." />                            
                        </div>
						<div class="col-xs-1 col-sm-1 col-md-1 col-lg-1">
							<div id="divNotaActualizarMensaje_<? echo $codigoFormulario; ?>" name="divNotaActualizarMensaje_<? echo $codigoFormulario; ?>"></div>
						</div>
                    </div>
					<div class="row">
						<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
							<input type="text" name="notaNombre" id="notaNombre" onchange="javascript:fNotaActualizar('<? echo $codigoFormulario; ?>');" class="form-control" value="<?php echo $fila['notaNombre']; ?>" placeholder="Nombre de la nota..." />
						</div>
					</div>
            <div class="row">
				<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
					<textarea name="notaDescripcion" id="notaDescripcion" onchange="javascript:fNotaActualizar('<? echo $codigoFormulario; ?>');" class="form-control" rows="2" required="required" placeholder="Descripci&oacute;n de la nota..."><?php echo $fila['notaDescripcion']; ?></textarea>
				</div>

            </div>
            <input type="hidden" name="idCursoMateriaProfesor" id="idCursoMateriaProfesor" class="form-control" value="<? echo $_POST['idCursoMateriaProfesor']; ?>" />
            <input type="hidden" name="bimestre" id="bimestre" class="form-control" value="<? echo $i; ?>" />
            <input type="hidden" name="nota" id="nota" class="form-control" value="<? echo $j; ?>" />
            </form>
            <br /><?
				}
				?>
        </div>
		</div>
	</div>

	<?
	}
	?>
</div>
<?
}
else
{
	?>
    <div class="alert">
        No se han enviado los par&aacute;metros.
    </div><?
}
          ?>
