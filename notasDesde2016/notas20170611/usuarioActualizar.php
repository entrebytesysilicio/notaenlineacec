<?php require_once('Connections/MySQL.php'); ?><?php
if (!isset($_SESSION)) {
  session_start();
}
$MM_authorizedUsers = "Administrador";
$MM_donotCheckaccess = "false";

// *** Restrict Access To Page: Grant or deny access to this page
function isAuthorized($strUsers, $strGroups, $UserName, $UserGroup) { 
  // For security, start by assuming the visitor is NOT authorized. 
  $isValid = False; 

  // When a visitor has logged into this site, the Session variable MM_Username set equal to their username. 
  // Therefore, we know that a user is NOT logged in if that Session variable is blank. 
  if (!empty($UserName)) { 
    // Besides being logged in, you may restrict access to only certain users based on an ID established when they login. 
    // Parse the strings into arrays. 
    $arrUsers = Explode(",", $strUsers); 
    $arrGroups = Explode(",", $strGroups); 
    if (in_array($UserName, $arrUsers)) { 
      $isValid = true; 
    } 
    // Or, you may restrict access to only certain users based on their username. 
    if (in_array($UserGroup, $arrGroups)) { 
      $isValid = true; 
    } 
    if (($strUsers == "") && false) { 
      $isValid = true; 
    } 
  } 
  return $isValid; 
}

$MM_restrictGoTo = "error.php";
if (!((isset($_SESSION['MM_Username'])) && (isAuthorized("",$MM_authorizedUsers, $_SESSION['MM_Username'], $_SESSION['MM_UserGroup'])))) {   
  $MM_qsChar = "?";
  $MM_referrer = $_SERVER['PHP_SELF'];
  if (strpos($MM_restrictGoTo, "?")) $MM_qsChar = "&";
  if (isset($QUERY_STRING) && strlen($QUERY_STRING) > 0) 
  $MM_referrer .= "?" . $QUERY_STRING;
  $MM_restrictGoTo = $MM_restrictGoTo. $MM_qsChar . "accesscheck=" . urlencode($MM_referrer);
  header("Location: ". $MM_restrictGoTo); 
  exit;
}
?>
<?php
if (!function_exists("GetSQLValueString")) {
function GetSQLValueString($theValue, $theType, $theDefinedValue = "", $theNotDefinedValue = "") 
{
  if (PHP_VERSION < 6) {
    $theValue = get_magic_quotes_gpc() ? stripslashes($theValue) : $theValue;
  }

  $theValue = function_exists("mysql_real_escape_string") ? mysql_real_escape_string($theValue) : mysql_escape_string($theValue);

  switch ($theType) {
    case "text":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;    
    case "long":
    case "int":
      $theValue = ($theValue != "") ? intval($theValue) : "NULL";
      break;
    case "double":
      $theValue = ($theValue != "") ? doubleval($theValue) : "NULL";
      break;
    case "date":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;
    case "defined":
      $theValue = ($theValue != "") ? $theDefinedValue : $theNotDefinedValue;
      break;
  }
  return $theValue;
}
}

function GetSQLValueString($theValue, $theType, $theDefinedValue = "", $theNotDefinedValue = "") 
{
  $theValue = (!get_magic_quotes_gpc()) ? addslashes($theValue) : $theValue;

  switch ($theType) {
    case "text":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;    
    case "long":
    case "int":
      $theValue = ($theValue != "") ? intval($theValue) : "NULL";
      break;
    case "double":
      $theValue = ($theValue != "") ? "'" . doubleval($theValue) . "'" : "NULL";
      break;
    case "date":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;
    case "defined":
      $theValue = ($theValue != "") ? $theDefinedValue : $theNotDefinedValue;
      break;
  }
  return $theValue;
}

$editFormAction = $_SERVER['PHP_SELF'];
if (isset($_SERVER['QUERY_STRING'])) {
  $editFormAction .= "?" . htmlentities($_SERVER['QUERY_STRING']);
}

if ((isset($_POST["MM_update"])) && ($_POST["MM_update"] == "form1")) {
	if ($_POST['clave'])
		{
		$updateSQL = sprintf("UPDATE usuario SET usuario=%s, clave=%s, nombre=%s, idPerfil=%s, activo=%s WHERE idUsuario=%s",
												 GetSQLValueString($_POST['usuario'], "text"),
												 GetSQLValueString(md5($_POST['clave']), "text"),
												 GetSQLValueString($_POST['nombre'], "text"),
												 GetSQLValueString($_POST['idPerfil'], "int"),
												 GetSQLValueString($_POST['activo'], "int"),
												 GetSQLValueString($_POST['idUsuario'], "int"));			
		}
		else
		{
		$updateSQL = sprintf("UPDATE usuario SET usuario=%s, nombre=%s, idPerfil=%s, activo=%s WHERE idUsuario=%s",
												 GetSQLValueString($_POST['usuario'], "text"),
												 GetSQLValueString($_POST['nombre'], "text"),
												 GetSQLValueString($_POST['idPerfil'], "int"),
												 GetSQLValueString($_POST['activo'], "int"),
												 GetSQLValueString($_POST['idUsuario'], "int"));						
		}

	

  mysql_select_db($database_MySQL, $MySQL);
  $Result1 = mysql_query($updateSQL, $MySQL) or die(mysql_error());

  $updateGoTo = "usuarioActualizar.php";
  if (isset($_SERVER['QUERY_STRING'])) {
    $updateGoTo .= (strpos($updateGoTo, '?')) ? "&" : "?";
    $updateGoTo .= $_SERVER['QUERY_STRING'];
  }
  header(sprintf("Location: %s", $updateGoTo));
}
?>
<?php
mysql_select_db($database_MySQL, $MySQL);
$query_rsPerfil = "SELECT * FROM perfil WHERE activo = '1' ORDER BY perfil ASC";
$rsPerfil = mysql_query($query_rsPerfil, $MySQL) or die(mysql_error());
$row_rsPerfil = mysql_fetch_assoc($rsPerfil);
$totalRows_rsPerfil = mysql_num_rows($rsPerfil);

$colname_rsUsuario = "-1";
if (isset($_GET['idUsuario'])) {
  $colname_rsUsuario = (get_magic_quotes_gpc()) ? $_GET['idUsuario'] : addslashes($_GET['idUsuario']);
}
mysql_select_db($database_MySQL, $MySQL);
$query_rsUsuario = sprintf("SELECT * FROM usuario WHERE idUsuario = %s", $colname_rsUsuario);
$rsUsuario = mysql_query($query_rsUsuario, $MySQL) or die(mysql_error());
$row_rsUsuario = mysql_fetch_assoc($rsUsuario);
$totalRows_rsUsuario = mysql_num_rows($rsUsuario);

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>

<title>:: Actualizar Usuario ::</title>
<link href="estilo.css" rel="stylesheet" type="text/css" />
<script src="SpryAssets/SpryCollapsiblePanel.js" type="text/javascript"></script>
<link href="SpryAssets/SpryCollapsiblePanel.css" rel="stylesheet" type="text/css" />
<script language="javascript" type="text/javascript">
function fUsuarioActualizarBuscarAlumnos (nombre)
	{
	mostrar_url('Parametros.php','accion=buscaralumnos&alumno='+nombre,'divUsuarioActualizarHijoResultado','get');	
	}	
	
	function fUsuarioActualizarMostrarHijos ()
	{
	var idUsuario;
	idUsuario = document.getElementById('idUsuario').value;
	mostrar_url('AlumnoAcudienteListar.php','idAcudiente='+idUsuario,'divUsuarioActualizarListadoHijos','get');	
	}	
	
	function fAlumnoAcudienteEliminar (idAlumnoAcudiente)
		{
			mostrar_url('AlumnoAcudienteEliminar.php','idAlumnoAcudiente='+idAlumnoAcudiente,'divUsuarioActualizarListadoHijos','get');
				setTimeout('fUsuarioActualizarMostrarHijos ()', 2*1000); //segundos * mil, el parametro es en ms es decir 5 * 1000 = 5 segundos
		}
		
	function fAlumnoAcudienteAgregar()
		{
		var idAlumno, idAcudiente;
		idAcudiente = document.getElementById('idUsuario').value;
		idAlumno =document.getElementById('slCursoAlumnoAlumnosEncontrados');
		idAlumno = idAlumno.options[idAlumno.selectedIndex].value;	
		mostrar_url('AlumnoAcudienteAgregar.php','MM_insert=form1&idAlumno='+idAlumno+"&idAcudiente="+idAcudiente,'divUsuarioActualizarListadoHijos','get');
		
				setTimeout('fUsuarioActualizarMostrarHijos ()', 2*1000); //segundos * mil, el parametro es en ms es decir 5 * 1000 = 5 segundos
			
			
			}
		
</script>
<? include "header.php"; ?>
</head>

<body onload="javascript:fUsuarioActualizarMostrarHijos ();">
<? include "menu.php"; ?>
<div class="divInsertar">
<table align="center">
<tr valign="top"><td>
<form method="post" name="form1" action="<?php echo $editFormAction; ?>">
  <table align="center">
  <thead>
  	<tr><td colspan="2">Actualizar usuario</td></tr>
  </thead>
    <tr valign="baseline">
      <td nowrap align="right">Usuario:</td>
      <td><input type="text" name="usuario" value="<?php echo $row_rsUsuario['usuario']; ?>" size="32" readonly="readonly"></td>
    </tr>
    <tr valign="baseline">
      <td nowrap align="right">Clave:</td>
      <td><input type="text" name="clave" value="<?php //echo $row_rsUsuario['clave']; ?>" size="32"></td>
    </tr>
    <tr valign="baseline">
      <td nowrap align="right">Nombre:</td>
      <td><input type="text" name="nombre" value="<?php echo $row_rsUsuario['nombre']; ?>" size="32"></td>
    </tr>
    <tr valign="baseline">
      <td nowrap align="right">Perfil:</td>
      <td><select name="idPerfil">
        <?php 
do {  
?>
        <option value="<?php echo $row_rsPerfil['idPerfil']?>" <?php if (!(strcmp($row_rsPerfil['idPerfil'], $row_rsUsuario['idPerfil']))) {echo "SELECTED";} ?>><?php echo $row_rsPerfil['perfil']?></option>
        <?php
} while ($row_rsPerfil = mysql_fetch_assoc($rsPerfil));
?>
      </select>
      </td>
    <tr>
    <tr valign="baseline">
      <td nowrap align="right">Activo:</td>
      <td><select name="activo">
        <option value="1" <?php if (!(strcmp(1, $row_rsUsuario['activo']))) {echo "SELECTED";} ?>>Si</option>
        <option value="0" <?php if (!(strcmp(0, $row_rsUsuario['activo']))) {echo "SELECTED";} ?>>No</option>
      </select>
      </td>
    </tr>
    <thead>
    <tr >
      <td colspan="2"><input type="submit" value="Actualizar"></td>
    </tr>
    </thead>
  </table>
  <input type="hidden" name="MM_update" value="form1">
  <input type="hidden" name="idUsuario" id="idUsuario" value="<?php echo $row_rsUsuario['idUsuario']; ?>">
</form>

</td><td>
<table>
<thead>
<tr><td>Hijos</td></tr>
</thead>
<tr><td>Nombre: <input type="text" name="tbUsuarioActualizarHijo" id="tbUsuarioActualizarHijo" onkeyup="javascript:fUsuarioActualizarBuscarAlumnos(this.value);" /></td></tr>
<tr><td><div id="divUsuarioActualizarHijoResultado" class="slCursoAlumnoAlumnosEncontrados"></div></td></tr>
<thead>
<tr><td><input type="button" name="Agregar hijo" value="Agregar hijo" onclick="javascript:fAlumnoAcudienteAgregar();" /></td></tr>
</thead>
</table>
</td></tr>
<tr><td colspan="2">
<table align="center" width="100%">
<thead>
<tr><td>Hijos de <?php echo $row_rsUsuario['nombre']; ?></td></tr>
</thead>
<tr><td>

<div id="divUsuarioActualizarListadoHijos"></div>
</td></tr>
<thead>
<tr><td>&nbsp;</td></tr>
</thead>
</table>
</td></tr>

</table>

  




</div>
</body>
</html>
<?php
mysql_free_result($rsPerfil);

mysql_free_result($rsUsuario);

?>
