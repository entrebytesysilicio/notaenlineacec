<?php require_once('Connections/MySQL.php'); ?>
<?php
if (!isset($_SESSION)) {
  session_start();
}
$MM_authorizedUsers = "Administrador";
$MM_donotCheckaccess = "false";

// *** Restrict Access To Page: Grant or deny access to this page
function isAuthorized($strUsers, $strGroups, $UserName, $UserGroup) { 
  // For security, start by assuming the visitor is NOT authorized. 
  $isValid = False; 

  // When a visitor has logged into this site, the Session variable MM_Username set equal to their username. 
  // Therefore, we know that a user is NOT logged in if that Session variable is blank. 
  if (!empty($UserName)) { 
    // Besides being logged in, you may restrict access to only certain users based on an ID established when they login. 
    // Parse the strings into arrays. 
    $arrUsers = Explode(",", $strUsers); 
    $arrGroups = Explode(",", $strGroups); 
    if (in_array($UserName, $arrUsers)) { 
      $isValid = true; 
    } 
    // Or, you may restrict access to only certain users based on their username. 
    if (in_array($UserGroup, $arrGroups)) { 
      $isValid = true; 
    } 
    if (($strUsers == "") && false) { 
      $isValid = true; 
    } 
  } 
  return $isValid; 
}

$MM_restrictGoTo = "error.php";
if (!((isset($_SESSION['MM_Username'])) && (isAuthorized("",$MM_authorizedUsers, $_SESSION['MM_Username'], $_SESSION['MM_UserGroup'])))) {   
  $MM_qsChar = "?";
  $MM_referrer = $_SERVER['PHP_SELF'];
  if (strpos($MM_restrictGoTo, "?")) $MM_qsChar = "&";
  if (isset($QUERY_STRING) && strlen($QUERY_STRING) > 0) 
  $MM_referrer .= "?" . $QUERY_STRING;
  $MM_restrictGoTo = $MM_restrictGoTo. $MM_qsChar . "accesscheck=" . urlencode($MM_referrer);
  header("Location: ". $MM_restrictGoTo); 
  exit;
}
?>
<?php
function GetSQLValueString($theValue, $theType, $theDefinedValue = "", $theNotDefinedValue = "") 
{
  $theValue = (!get_magic_quotes_gpc()) ? addslashes($theValue) : $theValue;

  switch ($theType) {
    case "text":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;    
    case "long":
    case "int":
      $theValue = ($theValue != "") ? intval($theValue) : "NULL";
      break;
    case "double":
      $theValue = ($theValue != "") ? "'" . doubleval($theValue) . "'" : "NULL";
      break;
    case "date":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;
    case "defined":
      $theValue = ($theValue != "") ? $theDefinedValue : $theNotDefinedValue;
      break;
  }
  return $theValue;
}

$editFormAction = $_SERVER['PHP_SELF'];
if (isset($_SERVER['QUERY_STRING'])) {
  $editFormAction .= "?" . htmlentities($_SERVER['QUERY_STRING']);
}

if ((isset($_POST["MM_insert"])) && ($_POST["MM_insert"] == "form1")) {
  $insertSQL = sprintf("INSERT INTO usuario (usuario, clave, nombre, idPerfil,direccion,telefono,celular,email) VALUES (%s, %s, %s, %s,%s, %s, %s, %s)",
                       GetSQLValueString($_POST['usuario'], "text"),
                       GetSQLValueString(md5($_POST['clave']), "text"),
                       GetSQLValueString($_POST['nombre'], "text"),
                       GetSQLValueString($_POST['idPerfil'], "int"),
                        GetSQLValueString($_POST['direccion'], "text"),
                        GetSQLValueString($_POST['telefono'], "text"),
                        GetSQLValueString($_POST['celular'], "text"),
                        GetSQLValueString($_POST['email'], "text")
                       );

  mysql_select_db($database_MySQL, $MySQL);
  $Result1 = mysql_query($insertSQL, $MySQL) or die(mysql_error());

  $insertGoTo = "usuarioCrear.php";
  if (isset($_SERVER['QUERY_STRING'])) {
    $insertGoTo .= (strpos($insertGoTo, '?')) ? "&" : "?";
    $insertGoTo .= $_SERVER['QUERY_STRING'];
  }
  header(sprintf("Location: %s", $insertGoTo));
}

mysql_select_db($database_MySQL, $MySQL);
$query_rsPerfil = "SELECT * FROM perfil WHERE activo = '1' ORDER BY perfil ASC";
$rsPerfil = mysql_query($query_rsPerfil, $MySQL) or die(mysql_error());
$row_rsPerfil = mysql_fetch_assoc($rsPerfil);
$totalRows_rsPerfil = mysql_num_rows($rsPerfil);
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>

<title>:: Usuarios ::</title>
<link href="css/bootstrap.min.css" rel="stylesheet">
<!--<link href="css/bootstrap-theme.min.css" rel="stylesheet">-->
<script src="js/jquery-1.10.2.min.js"></script>
<script src="js/bootstrap.min.js"></script>
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<link href="estilo.css" rel="stylesheet" type="text/css" />
<script src="SpryAssets/SpryTabbedPanels.js" type="text/javascript"></script>
<link href="SpryAssets/SpryTabbedPanels.css" rel="stylesheet" type="text/css" />
<script language="javascript" type="text/javascript">
function fUsuarioCrearListar(nombre)
	{
	mostrar_url('UsuarioCrearListar.php','nombre='+nombre,'divUsuarioCrearResultados','get');	
	}

</script>
<? include "header.php"; ?>
</head>
<body>
<? include "menu.php"; ?>
<div class="container">
<h1>Usuarios</h1>
<div id="TabbedPanels1" class="TabbedPanels">
  <ul class="TabbedPanelsTabGroup">
    <li class="TabbedPanelsTab" tabindex="0">Crear</li>
    <li class="TabbedPanelsTab" tabindex="0">Importar</li>
    <li class="TabbedPanelsTab" tabindex="0">Buscar</li>
  </ul>
  <div class="TabbedPanelsContentGroup">
    <div class="TabbedPanelsContent">
      <form method="post" name="form1" action="<?php echo $editFormAction; ?>">
        <br />
        <table align="center">
          <thead>
            <tr>
              <td colspan="2">Crear usuario</td>
            </tr>
          </thead>
          <tr >
            <td >Usuario:</td>
            <td><input type="text" name="usuario" value="" size="32"></td>
          </tr>
          <tr >
            <td >Clave:</td>
            <td><input type="text" name="clave" value="" size="32"></td>
          </tr>
          <tr >
            <td >Nombre:</td>
            <td><input type="text" name="nombre" value="" size="32"></td>
          </tr>
          <tr >
            <td >Direcci&oacute;n:</td>
            <td><input type="text" name="direccion" value="" size="32"></td>
          </tr>
          <tr >
            <td >Tel&eacute;fono:</td>
            <td><input type="text" name="telefono" value="" size="32"></td>
          </tr>
          <tr >
            <td >Celular:</td>
            <td><input type="text" name="celular" value="" size="32"></td>
          </tr>
          <tr >
            <td >Email:</td>
            <td><input type="text" name="email" value="" size="32"></td>
          </tr>                                        
          <tr >
            <td >Perfil:</td>
            <td><select name="idPerfil">
                <?php 
do {  
?>
                <option value="<?php echo $row_rsPerfil['idPerfil']?>" ><?php echo $row_rsPerfil['perfil']?></option>
                <?php
} while ($row_rsPerfil = mysql_fetch_assoc($rsPerfil));
?>
              </select></td>
          <tr>
          <thead>
            <tr >
              <td colspan="2"><input type="submit" value="Crear"></td>
            </tr>
          </thead>
        </table>
        <input type="hidden" name="MM_insert" value="form1">
        <br />
      </form>
    </div>
    <div class="TabbedPanelsContent">
      <form name="importar" action="usuarioImportarPadre.php">
        <br />
        <table align="center" width="250px">
          <thead>
            <tr>
              <td>Importar usuarios</td>
            </tr>
          </thead>
          <tr>
            <td>Copiar y pegar desde Excel, formato (nombre completo del padre,identificacion,clave,direccion,telefono,celular,email) uno por linea.</td>
          </tr>
          <tr>
            <td><textarea name="padres" cols="60"></textarea></td>
          </tr>
          <thead>
            <tr >
              <td><input type="submit" name="Importar" value="Importar" /></td>
            </tr>
          </thead>
        </table>
        <br />
      </form>
    </div>
    <div class="TabbedPanelsContent"><br />

      <table align="center">
        <thead>
          <tr>
            <td>Buscar usuarios</td>
          </tr>
        </thead>
        <tr>
          <td>Usuario: <input type="text" name="tbUsuarioCrearNombre" id="tbUsuarioCrearNombre" onkeyup="javascript:fUsuarioCrearListar(this.value);" /></td>
        </tr>
        <tr>
          <td>Resultados:</td>
        </tr>
        <tr>
          <td><div id="divUsuarioCrearResultados" class="divUsuarioCrearResultados"></div></td>
        </tr>
        <thead>
          <tr>
            <td>&nbsp;</td>
          </tr>
        </thead>
      </table>
    </div>
  </div>
</div>

<h1>Usuarios diferentes a Padres...</h1>
<?
$base = new BD();
$sql = "select  u.nombre as Nombre,
    u.usuario as Usuario, 
    p.perfil as Perfil, 
    case u.activo when 1 then 'Si' else 'No' end as Estado 
from  usuario as u 
inner join perfil as p on p.idPerfil = u.idPerfil
order by u.nombre";
$base->consultaATabla($sql);
?>

<script type="text/javascript">
var TabbedPanels1 = new Spry.Widget.TabbedPanels("TabbedPanels1");
</script>
</div>
</body>
</html>
<?php
mysql_free_result($rsPerfil);
?>
