<?php require_once('Connections/MySQL.php'); ?>
<?php
if (!isset($_SESSION)) {
session_start();
}
$MM_authorizedUsers = "Administrador";
$MM_donotCheckaccess = "false";
// *** Restrict Access To Page: Grant or deny access to this page
function isAuthorized($strUsers, $strGroups, $UserName, $UserGroup) {
// For security, start by assuming the visitor is NOT authorized.
$isValid = False;
// When a visitor has logged into this site, the Session variable MM_Username set equal to their username.
// Therefore, we know that a user is NOT logged in if that Session variable is blank.
if (!empty($UserName)) {
// Besides being logged in, you may restrict access to only certain users based on an ID established when they login.
// Parse the strings into arrays.
$arrUsers = Explode(",", $strUsers);
$arrGroups = Explode(",", $strGroups);
if (in_array($UserName, $arrUsers)) {
$isValid = true;
}
// Or, you may restrict access to only certain users based on their username.
if (in_array($UserGroup, $arrGroups)) {
$isValid = true;
}
if (($strUsers == "") && false) {
$isValid = true;
}
}
return $isValid;
}
$MM_restrictGoTo = "error.php";
if (!((isset($_SESSION['MM_Username'])) && (isAuthorized("",$MM_authorizedUsers, $_SESSION['MM_Username'], $_SESSION['MM_UserGroup'])))) {
$MM_qsChar = "?";
$MM_referrer = $_SERVER['PHP_SELF'];
if (strpos($MM_restrictGoTo, "?")) $MM_qsChar = "&";
if (isset($QUERY_STRING) && strlen($QUERY_STRING) > 0)
$MM_referrer .= "?" . $QUERY_STRING;
$MM_restrictGoTo = $MM_restrictGoTo. $MM_qsChar . "accesscheck=" . urlencode($MM_referrer);
header("Location: ". $MM_restrictGoTo);
exit;
}
if(isset($_POST['idAlumno']))
{
$sql = "select m.idMatricula, m.fecha, m.folio, m.matricula, c.curso, c.ano, m.idAlumno, m.fechaMatricula
from matricula as m
left join curso as c on c.idCurso = m.idCurso
where m.idAlumno = ".$_POST['idAlumno']."
	order by ano desc";
if($resultado = mysql_query($sql,$MySQL))
	{
		if(mysql_num_rows($resultado) > 0)
		{
?>
<table class="table table-hover table-striped">
	<thead>
		<tr>
			<th class="text-center">No.</th>
			<th class="text-center">Fecha creaci&oacute;n</th>
            <th class="text-center">Fecha matr&iacute;cula</th>
			<th class="text-center">Folio</th>
			<th class="text-center">Matr&iacute;cula</th>
			<th class="text-center">Curso</th>
			<th class="text-center">A&ntilde;o</th>
			<th class="text-center">Acciones</th>
		</tr>
	</thead>
	<tbody>
		<?php
		$fila = mysql_fetch_array($resultado);
		$i = 1;
		do {
		?>
		<tr>
			<td class="text-center"><?php echo $i; ?></td>
			<td class="text-center"><?php echo substr($fila['fecha'],0,16); ?></td>
			<td class="text-center">
				<?php echo substr($fila['fechaMatricula'],0,10); ?>
			</td>
			<td class="text-center"><?php echo $fila['folio']; ?></td>
			<td class="text-center"><?php echo $fila['matricula']; ?></td>
			<td class="text-center"><?php echo $fila['curso']; ?></td>
			<td class="text-center"><?php echo $fila['ano']; ?></td>
			<td class="text-center">
			<?php 
			if( substr($fila['ano'], 0,4) >= date("Y"))
			{
				?>
					<button type="button" class="btn btn-danger" onclick="fMatriculaEliminar(<?php echo $fila['idMatricula']; ?>,<?php echo $fila['idAlumno']; ?>);">Eliminar</button>
					<button type="button" class="btn btn-warning" onclick="fMatriculaActualizar(<?php echo $fila['idMatricula']; ?>,<?php echo $fila['idAlumno']; ?>);">Actualizar</button>

				<?
			}
			?>
		<button type="button" class="btn btn-default" onclick="fMatriculaImprimir(<?php echo $fila['idMatricula']; ?>);">Imprimir</button>
			</td>
		</tr>
		<?
		$i++;
		} while ($fila = mysql_fetch_array($resultado));
		?>
	</tbody>
</table>
<?

}
else
{
?>
<div class="alert alert-warning">
	No hay matr&iacute;culas para el alumno con id: <?php echo $_POST['idAlumno']; ?>
</div>
<?
}
}
else
{
?>
<div class="alert alert-danger">
	<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
	Error al consultar la base de datos. <br><?php echo mysql_error(); ?>
</div>
<?
}
}
else
{
?>
<div class="alert alert-danger">
	Debe enviar el id del alumno.
</div>
<?
}
?>