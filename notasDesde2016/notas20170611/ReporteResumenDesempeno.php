<?php require_once('Connections/MySQL.php'); ?>
<?php
if (!isset($_SESSION)) {
  session_start();
}
$MM_authorizedUsers = "Administrador,Profesor";
$MM_donotCheckaccess = "false";

// *** Restrict Access To Page: Grant or deny access to this page
function isAuthorized($strUsers, $strGroups, $UserName, $UserGroup) { 
  // For security, start by assuming the visitor is NOT authorized. 
  $isValid = False; 

  // When a visitor has logged into this site, the Session variable MM_Username set equal to their username. 
  // Therefore, we know that a user is NOT logged in if that Session variable is blank. 
  if (!empty($UserName)) { 
    // Besides being logged in, you may restrict access to only certain users based on an ID established when they login. 
    // Parse the strings into arrays. 
    $arrUsers = Explode(",", $strUsers); 
    $arrGroups = Explode(",", $strGroups); 
    if (in_array($UserName, $arrUsers)) { 
      $isValid = true; 
    } 
    // Or, you may restrict access to only certain users based on their username. 
    if (in_array($UserGroup, $arrGroups)) { 
      $isValid = true; 
    } 
    if (($strUsers == "") && false) { 
      $isValid = true; 
    } 
  } 
  return $isValid; 
}

$MM_restrictGoTo = "error.php";
if (!((isset($_SESSION['MM_Username'])) && (isAuthorized("",$MM_authorizedUsers, $_SESSION['MM_Username'], $_SESSION['MM_UserGroup'])))) {   
  $MM_qsChar = "?";
  $MM_referrer = $_SERVER['PHP_SELF'];
  if (strpos($MM_restrictGoTo, "?")) $MM_qsChar = "&";
  if (isset($QUERY_STRING) && strlen($QUERY_STRING) > 0) 
  $MM_referrer .= "?" . $QUERY_STRING;
  $MM_restrictGoTo = $MM_restrictGoTo. $MM_qsChar . "accesscheck=" . urlencode($MM_referrer);
  header("Location: ". $MM_restrictGoTo); 
  exit;
}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title>:: Reporte Resumen de Desempe&ntilde;o ::</title>
<? include "header.php"; ?>

</head>

<body>
<table align="center">
<tr>
<td>A&ntilde;o</td>
<td>Bimestre:</td>
<td>Curso:</td>
<td>&Aacute;rea:</td>
<td>Materia:</td>
<td>Profesor:</td>
<td></td>
</tr>
<tr>
<td>
<?
$listaAnos = new Utils;
$listaAnos->listadoAnosSelect("anos","anos","fMostrarCursosResumenDesempeno(this.value);","");
?>
</td>

<td>
<select name="bimestre" id="bimestre">
<option value=""></option>
<option value="">Todos</option>
<option value="1">1</option>
<option value="2">2</option>
<option value="3">3</option>
<option value="4">4</option>
</select>
</td>

<td>
<div id="divCurso">
<select name="idCurso" id="idCurso"><option></option></select>
</div>
</td>

<td>
<div id="divArea">
<select name="idArea" id="idArea"><option></option></select>
</div>
</td>

<td>
<div id="divMateria">
<select name="idMateria" id="idMateria"><option></option></select>
</div>
</td>
<td>
<?
$listaAnos->listadoProfesor ($_SESSION['MM_UserGroup'],$_SESSION['MM_Username']);
?>
</td>
<td>
<input type="button" name="Ver" value="Ver" onclick="fMostrarReporteResumenDesempeno()" />
</td>

</tr>
</table>
<br />
<center>
<div id="divReporteResumenDesempeno"></div>
</center>

</body>
</html>