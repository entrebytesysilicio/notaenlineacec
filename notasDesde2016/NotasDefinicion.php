<?php require_once('Connections/MySQL.php'); ?>
<?php
if (!isset($_SESSION)) {
	session_start();
}
$MM_authorizedUsers = "Administrador,Profesor";
$MM_donotCheckaccess = "false";
// *** Restrict Access To Page: Grant or deny access to this page
function isAuthorized($strUsers, $strGroups, $UserName, $UserGroup) {
	// For security, start by assuming the visitor is NOT authorized.
	$isValid = False;
	// When a visitor has logged into this site, the Session variable MM_Username set equal to their username.
	// Therefore, we know that a user is NOT logged in if that Session variable is blank.
	if (!empty($UserName)) {
		// Besides being logged in, you may restrict access to only certain users based on an ID established when they login.
		// Parse the strings into arrays.
		$arrUsers = Explode(",", $strUsers);
		$arrGroups = Explode(",", $strGroups);
		if (in_array($UserName, $arrUsers)) {
			$isValid = true;
		}
		// Or, you may restrict access to only certain users based on their username.
		if (in_array($UserGroup, $arrGroups)) {
			$isValid = true;
		}
		if (($strUsers == "") && false) {
			$isValid = true;
		}
	}
	return $isValid;
}
$MM_restrictGoTo = "error.php";
if (!((isset($_SESSION['MM_Username'])) && (isAuthorized("",$MM_authorizedUsers, $_SESSION['MM_Username'], $_SESSION['MM_UserGroup'])))) {
	$MM_qsChar = "?";
	$MM_referrer = $_SERVER['PHP_SELF'];
	if (strpos($MM_restrictGoTo, "?")) $MM_qsChar = "&";
	if (isset($QUERY_STRING) && strlen($QUERY_STRING) > 0)
		$MM_referrer .= "?" . $QUERY_STRING;
	$MM_restrictGoTo = $MM_restrictGoTo. $MM_qsChar . "accesscheck=" . urlencode($MM_referrer);
	header("Location: ". $MM_restrictGoTo);
	exit;
}
?>
<!DOCTYPE html>
<html lang="en">
    <head>
		<title>Definici&oacute;n de notas</title>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <? include "clases.php"; ?>
        <!--<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />-->
        <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
        <script language="javascript" type="text/javascript" src="js/jquery-1.10.2.min.js"></script>
        <script language="javascript" type="text/javascript" src="js/bootstrap.min.js"></script>
        <script language="javascript" type="text/javascript" src="ajax.js"></script>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
        <!-- Bootstrap -->
        <link href="css/bootstrap.min.css" rel="stylesheet">
        <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->
        <script type="text/javascript">
            
        function fNotasDefinicionAgregar(idCursoMateriaProfesor,idProfesor,profesorNombre,ano,curso,area,materia)
        {
        document.getElementById('divNotasProfesor').innerHTML = profesorNombre;
        document.getElementById('divNotasAno').innerHTML = ano;
        document.getElementById('divNotasCurso').innerHTML = curso;
        document.getElementById('divNotasArea').innerHTML = area;
        document.getElementById('divNotasMateria').innerHTML = materia;
        mostrar_url('NotasDefinicionAgregar.php','idCursoMateriaProfesor='+idCursoMateriaProfesor,'divNotasEditar','post');
        }

        function fNotaActualizar(idFormulario)
        {
        var formulario = document.forms["form_"+idFormulario];
        var notaNombre = formulario["notaNombre"].value;
        var notaDescripcion = formulario["notaDescripcion"].value;
        var bimestre = formulario["bimestre"].value;
        var nota = formulario["nota"].value;
        var idCursoMateriaProfesor = formulario["idCursoMateriaProfesor"].value;
        var fechaActividad = formulario["fechaActividad"].value;
        mostrar_url('NotasDefinicionActualizar.php','idCursoMateriaProfesor='+idCursoMateriaProfesor+"&bimestre="+bimestre+"&nota="+nota+"&notaNombre="+notaNombre+"&notaDescripcion="+notaDescripcion+"&fechaActividad="+fechaActividad,'divNotaActualizarMensaje_'+idFormulario,'post');
        }
    </script>
    </head>
    <body>
        <div class="container">
            <?php include "menu.php"; ?>
			<div class="row">
                <form action="<? echo $_SERVER['PHP_SELF']; ?>" method="post" enctype="application/x-www-form-urlencoded">

                    <div class="col-xs-3 col-sm-3 col-md-3 col-lg-3">
                        <div class="form-group">
                            <label class="control-label" for="anoSeleccionado">A&ntilde;o: </label>
                            <select class="form-control" id="anoSeleccionado" name="anoSeleccionado" onchange="this.form.submit();">
                                <option value="%">Todos</option>
								<?

								$anoSeleccionado = "%";
								if(isset($_POST['anoSeleccionado']))
								{
									$anoSeleccionado = $_POST['anoSeleccionado'];
								}

								$cursoSeleccionado ="%";
								if(isset($_POST['cursoSeleccionado']))
								{
									$cursoSeleccionado = $_POST['cursoSeleccionado'];
								}

								////Listado de años
								$sql = "select ano from curso group by ano order by ano desc";
								if($resultado = mysql_query($sql,$MySQL))
								{
									if(mysql_num_rows($resultado) > 0)
									{
										$fila = mysql_fetch_assoc($resultado);
										do{
										?>
										<option value="<?php echo $fila['ano']; ?>" <? if ($fila['ano'] == $anoSeleccionado) { ?> selected="selected" <?} ?> ><?php echo $fila['ano']; ?></option>
										<?
										} while($fila = mysql_fetch_assoc($resultado));
									}
									else
									{
                                    ?>
									<option value="%">No se encontraron datos.</option>
									<?
									}
								}
								else
								{
                                                                                   ?>
                                <option value="%">Error al consultar los a&ntilde;s.</option><?
																}
                                                                                             ?>

                            </select>
                        </div>                
				</div>

				<div class="col-xs-3 col-sm-3 col-md-3 col-lg-3">
					<div class="form-group">
						<label class="control-label" for="cursoSeleccionado">Curso: </label>
						<select class="form-control" id="cursoSeleccionado" name="cursoSeleccionado" onchange="this.form.submit();">
							<option value="%">Todos</option>
								<?
								////Listado de años
								$sql = "select idCurso, concat(ano,' - ',curso) curso from curso ";

								if( $anoSeleccionado != '%')
									{
										$sql = $sql." where ano like ".$anoSeleccionado;
									}

								$sql = $sql." order by ano desc, orden asc";
								if($resultado = mysql_query($sql,$MySQL))
								{
									if(mysql_num_rows($resultado) > 0)
									{
										$fila = mysql_fetch_assoc($resultado);
										do{
                                ?>
							<option value="<?php echo $fila['idCurso']; ?>"<? if ($fila['idCurso'] == $cursoSeleccionado) { ?> selected="selected" <?} ?> ><?php echo $fila['curso']; ?></option><?
										} while($fila = mysql_fetch_assoc($resultado));
									}
									else
									{
                                    ?>
									<option value="%">No se encontraron datos.</option>
									<?
									}
								}
								else
								{
                                ?>
								<option value="%">Error al consultar los cursos.</option>
								<?
								}
                                ?>
							</select>
						</div>                
					</div>

				</form>
			</div>
            <div class="row">
                <div class="col-xs-7 col-sm-7 col-md-7 col-lg-7">
					<?php
                    if($_SESSION['MM_UserGroup'] == 'Administrador' )
                    {
						$sql = "select cmp.idCursoMateriaProfesor, u.nombre as profesorNombre,u.idUsuario as idProfesor, c.ano, c.curso, m.area, m.materia
								from cursomateriaprofesor as cmp
								inner join curso as c on cmp.idCurso = c.idCurso
								inner join materia as m on cmp.idMateria = m.idMateria
								inner join usuario as u on cmp.idProfesor = u.idUsuario
								where 1 = 1
								";

						if(isset($_POST['anoSeleccionado']) && $_POST['anoSeleccionado'] != '%')
						{
							$sql = $sql." and c.ano like ".$_POST['anoSeleccionado'];
						}

						if(isset($_POST['cursoSeleccionado'])  && $_POST['cursoSeleccionado'] != '%')
						{
							$sql = $sql." and c.idCurso like ".$_POST['cursoSeleccionado'];
						}

                    $sql = $sql." order by c.ano desc,   c.orden asc, m.area asc,  m.materia asc, u.nombre asc ";
                    }
                    else
                    {
						$sql = "select cmp.idCursoMateriaProfesor, u.nombre as profesorNombre,u.idUsuario as idProfesor, c.ano, c.curso, m.area, m.materia
                    from cursomateriaprofesor as cmp
                    inner join curso as c on cmp.idCurso = c.idCurso
                    inner join materia as m on cmp.idMateria = m.idMateria
                    inner join usuario as u on cmp.idProfesor = u.idUsuario
                    where cmp.idProfesor = ".$_SESSION['idUsuario'];

						if(isset($_POST['anoSeleccionado']) && $_POST['anoSeleccionado'] != '%')
						{
							$sql = $sql." and c.ano = ".$_POST['anoSeleccionado'];
						}

						if(isset($_POST['cursoSeleccionado']) && $_POST['cursoSeleccionado'] != '%')
						{
							$sql = $sql." and c.idCurso like ".$_POST['cursoSeleccionado'];
						}

                    $sql = $sql." order by c.ano desc,   c.orden asc, m.area asc,  m.materia asc, u.nombre asc ";
                    }
                    if($resultado = mysql_query($sql,$MySQL))
                    {
						if(mysql_num_rows($resultado) > 0)
						{
                    ?>
                    <table class="table table-hover table-striped table-condensed">
                        <thead>
                            <tr>
                                <th class="text-center">No.</th>
								<th class="text-center">A&ntilde;o</th>
								<th class="text-center">Curso</th>
								<th class="text-center">&Aacute;rea</th>
								<th class="text-center">Materia</th>
                                <th class="text-center">Profesor</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                            $i = 1;
                            $fila = mysql_fetch_array($resultado);
                            do {
                            ?>
                            <tr onclick="javascript:fNotasDefinicionAgregar(<?php echo $fila['idCursoMateriaProfesor']; ?>,<?php echo $fila['idProfesor']; ?>,'<?php echo $fila['profesorNombre']; ?>','<?php echo $fila['ano']; ?>','<?php echo $fila['curso']; ?>','<?php echo $fila['area']; ?>','<?php echo $fila['materia']; ?>');">
                                <td class="text-center"><?php echo $i; $i++; ?></td>
								<td class="text-center">
									<?php echo $fila['ano']; ?>
								</td>
								<td>
									<?php echo $fila['curso']; ?>
								</td>
								<td>
									<?php echo $fila['area']; ?>
								</td>
								<td>
									<?php echo $fila['materia']; ?>
								</td>
								<td>
									<?php echo $fila['profesorNombre']; ?>
								</td>
                            </tr>
                            <?php
                            } while ($fila = mysql_fetch_array($resultado));
                            ?>
                        </tbody>
                    </table>
                    <?
							
						}
						else
						{
                    ?>
                    <div class="alert alert-warning">
                        No se encontr&oacute; la parametriaci&oacute;n de (Cursos/Materia/Profesor), por favor informar al administrador.
                    </div>
                    <?
						}
                    }
                    else
                    {
                    ?>
                    <div class="alert alert-danger">
                        Error al consultar la informaci&oacute;n de materias y cursos asociadas al profesor.
                    </div>
                    <?
                    }
                    ?>
                </div>
                <div class="col-xs-5 col-sm-5 col-md-5 col-lg-5">
                <legend class="text-info h4 text-center">Definici&oacute;n de notas </legend>
                <table class="table table-hover">
                        <tr>
                            <td class="text-muted">Profesor</td>
                            <td><div name="divNotasProfesor" id="divNotasProfesor" class="text-info"></div></td>
                        </tr>
                        <tr>
                            <td class="text-muted">A&ntilde;o</td>
                            <td><div name="divNotasAno" id="divNotasAno" class="text-info"></div></td>
                        </tr>
                        <tr>
                            <td class="text-muted">Curso</td>
                            <td><div name="divNotasCurso" id="divNotasCurso" class="text-info"></div></td>
                        </tr>
                        <tr>
                            <td class="text-muted">&Aacute;rea</td>
                            <td><div name="divNotasArea" id="divNotasArea" class="text-info"></div></td>
                        </tr>
                        <tr>
                            <td class="text-muted">Materia</td>
                            <td><div name="divNotasMateria" id="divNotasMateria" class="text-info"></div></td>
                        </tr>
                </table>

                <div name="divNotasEditar" id="divNotasEditar"></div>
                    
                </div>
            </div>
        </div>  
    </body>
</html>