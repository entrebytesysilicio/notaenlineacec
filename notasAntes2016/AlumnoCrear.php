<?php require_once('Connections/MySQL.php'); ?><?php
if (!isset($_SESSION)) {
  session_start();
}
$MM_authorizedUsers = "Administrador,Profesor";
$MM_donotCheckaccess = "false";

// *** Restrict Access To Page: Grant or deny access to this page
function isAuthorized($strUsers, $strGroups, $UserName, $UserGroup) { 
  // For security, start by assuming the visitor is NOT authorized. 
  $isValid = False; 

  // When a visitor has logged into this site, the Session variable MM_Username set equal to their username. 
  // Therefore, we know that a user is NOT logged in if that Session variable is blank. 
  if (!empty($UserName)) { 
    // Besides being logged in, you may restrict access to only certain users based on an ID established when they login. 
    // Parse the strings into arrays. 
    $arrUsers = Explode(",", $strUsers); 
    $arrGroups = Explode(",", $strGroups); 
    if (in_array($UserName, $arrUsers)) { 
      $isValid = true; 
    } 
    // Or, you may restrict access to only certain users based on their username. 
    if (in_array($UserGroup, $arrGroups)) { 
      $isValid = true; 
    } 
    if (($strUsers == "") && false) { 
      $isValid = true; 
    } 
  } 
  return $isValid; 
}

$MM_restrictGoTo = "error.php";
if (!((isset($_SESSION['MM_Username'])) && (isAuthorized("",$MM_authorizedUsers, $_SESSION['MM_Username'], $_SESSION['MM_UserGroup'])))) {   
  $MM_qsChar = "?";
  $MM_referrer = $_SERVER['PHP_SELF'];
  if (strpos($MM_restrictGoTo, "?")) $MM_qsChar = "&";
  if (isset($QUERY_STRING) && strlen($QUERY_STRING) > 0) 
  $MM_referrer .= "?" . $QUERY_STRING;
  $MM_restrictGoTo = $MM_restrictGoTo. $MM_qsChar . "accesscheck=" . urlencode($MM_referrer);
  header("Location: ". $MM_restrictGoTo); 
  exit;
}
?><?php
function GetSQLValueString($theValue, $theType, $theDefinedValue = "", $theNotDefinedValue = "") 
{
  $theValue = (!get_magic_quotes_gpc()) ? addslashes($theValue) : $theValue;

  switch ($theType) {
    case "text":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;    
    case "long":
    case "int":
      $theValue = ($theValue != "") ? intval($theValue) : "NULL";
      break;
    case "double":
      $theValue = ($theValue != "") ? "'" . doubleval($theValue) . "'" : "NULL";
      break;
    case "date":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;
    case "defined":
      $theValue = ($theValue != "") ? $theDefinedValue : $theNotDefinedValue;
      break;
  }
  return $theValue;
}

$editFormAction = $_SERVER['PHP_SELF'];
if (isset($_SERVER['QUERY_STRING'])) {
  $editFormAction .= "?" . htmlentities($_SERVER['QUERY_STRING']);
}

if ((isset($_POST["MM_insert"])) && ($_POST["MM_insert"] == "form1")) {
  $insertSQL = sprintf("INSERT INTO alumno (identificacion, nombres, apellidos) VALUES (%s, %s, %s)",
                       GetSQLValueString($_POST['identificacion'], "text"),
                       GetSQLValueString($_POST['nombres'], "text"),
                       GetSQLValueString($_POST['apellidos'], "text"));

  mysql_select_db($database_MySQL, $MySQL);
  $Result1 = mysql_query($insertSQL, $MySQL) or die(mysql_error());

  $insertGoTo = "AlumnoCrear.php";
  if (isset($_SERVER['QUERY_STRING'])) {
    $insertGoTo .= (strpos($insertGoTo, '?')) ? "&" : "?";
    $insertGoTo .= $_SERVER['QUERY_STRING'];
  }
  header(sprintf("Location: %s", $insertGoTo));
}

?><!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>

<title>:: Alumnos ::</title>
<link href="estilo.css" rel="stylesheet" type="text/css" />
<script src="SpryAssets/SpryTabbedPanels.js" type="text/javascript"></script>
<link href="SpryAssets/SpryTabbedPanels.css" rel="stylesheet" type="text/css" />
<script language="javascript" type="text/javascript">
function fAlumnoCrearListar(nombre)
	{
	mostrar_url('AlumnoCrearListar.php','nombre='+nombre,'divAlumnoCrearResultados','get');	
	}
    </script>
    <? include "header.php"; ?>
</head>

<body>
<? include "menu.php"; ?>

<h1>Alumnos</h1>

<div id="TabbedPanels1" class="TabbedPanels">
  <ul class="TabbedPanelsTabGroup">
    <li class="TabbedPanelsTab" tabindex="0">Crear</li>
    <li class="TabbedPanelsTab" tabindex="0">Importar</li>
    <li class="TabbedPanelsTab" tabindex="0">Buscar</li>   
    <li class="TabbedPanelsTab" tabindex="0">Activar/Inactivar</li>  
  </ul>
  <div class="TabbedPanelsContentGroup">
    <div class="TabbedPanelsContent">
<form method="post" name="form1" action="<?php echo $editFormAction; ?>">
  <table align="center">
  <thead>
  <tr>
  <td colspan="2">Crear usuario</td>
  </tr>
  </thead>
      <tr valign="baseline">
        <td >Identificaci&oacute;n:</td>
        <td><input type="text" name="identificacion" value="" size="32"></td>
      </tr>
      <tr >
        <td>Nombres:</td>
        <td><input type="text" name="nombres" value="" size="32"></td>
      </tr>
      <tr >
        <td>Apellidos:</td>
        <td><input type="text" name="apellidos" value="" size="32"></td>
      </tr>
      <thead>
      <tr >
        <td colspan="2"><input type="submit" value="Crear"></td>
      </tr>
      </thead>
  </table>
    <input type="hidden" name="MM_insert" value="form1">
</form>    
    </div>
    <div class="TabbedPanelsContent">
   <form name="importar" action="AlumnoImportar.php"><br />
      <table align="center" width="250px">
      <thead><tr><td>Importar alumnos</td></tr></thead>
    <tr>
      <td>Copiar y pegar desde Excel, formato (nombres,apellidos,identificaci&oacute;n) uno por linea.</td></tr>
    <tr><td>
    <textarea name="alumnos" cols="60"></textarea>
    </td>
    </tr>
    <thead>
    <tr >
    <td>
    <input type="submit" name="Importar" value="Importar" />
    </td></tr></thead></table><br />
    </form>    
    </div>
    <div class="TabbedPanelsContent">
     <table align="center">
        <thead>
          <tr>
            <td>Buscar alumnos</td>
          </tr>
        </thead>
        <tr>
          <td>Alumno: <input type="text" name="tbAlumnoCrearNombre" id="tbAlumnoCrearNombre" onkeyup="javascript:fAlumnoCrearListar(this.value);" /></td>
        </tr>
        <tr>
          <td>Resultados:</td>
        </tr>
        <tr>
          <td><div id="divAlumnoCrearResultados" class="divAlumnoCrearResultados"></div></td>
        </tr>
        <thead>
          <tr>
            <td>&nbsp;</td>
          </tr>
        </thead>
      </table>    
    </div> 
    <div class="TabbedPanelsContent">
   <form name="importar" action="AlumnoActivarInactivar.php"><br />
      <table align="center" width="250px">
      <thead><tr><td>Activar/inactivar Alumnos</td></tr></thead>
    <tr>
      <td>Copiar y pegar desde Excel, formato (identificaci&oacute;n) uno por linea.</td></tr>
    <tr><td>Activar: <input type="checkbox" name="activar" checked="checked" /></td></tr>
    <tr><td>
    <textarea name="alumnos" cols="60"></textarea>
    </td>
    </tr>
    <thead>
    <tr >
    <td>
    <input type="submit" name="Activar/Inactivar" value="Activar/Inactivar" />
    </td></tr></thead></table><br />
    </form>     
    </div>   
  </div>
</div>

<script type="text/javascript">
var TabbedPanels1 = new Spry.Widget.TabbedPanels("TabbedPanels1");
</script>
</body>
</html>
