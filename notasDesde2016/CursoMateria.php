<?php require_once('Connections/MySQL.php'); ?>
<?php
if (!isset($_SESSION)) {
  session_start();
}
$MM_authorizedUsers = "Administrador,Profesor";
$MM_donotCheckaccess = "false";

// *** Restrict Access To Page: Grant or deny access to this page
function isAuthorized($strUsers, $strGroups, $UserName, $UserGroup) { 
  // For security, start by assuming the visitor is NOT authorized. 
  $isValid = False; 

  // When a visitor has logged into this site, the Session variable MM_Username set equal to their username. 
  // Therefore, we know that a user is NOT logged in if that Session variable is blank. 
  if (!empty($UserName)) { 
    // Besides being logged in, you may restrict access to only certain users based on an ID established when they login. 
    // Parse the strings into arrays. 
    $arrUsers = Explode(",", $strUsers); 
    $arrGroups = Explode(",", $strGroups); 
    if (in_array($UserName, $arrUsers)) { 
      $isValid = true; 
    } 
    // Or, you may restrict access to only certain users based on their username. 
    if (in_array($UserGroup, $arrGroups)) { 
      $isValid = true; 
    } 
    if (($strUsers == "") && false) { 
      $isValid = true; 
    } 
  } 
  return $isValid; 
}

$MM_restrictGoTo = "error.php";
if (!((isset($_SESSION['MM_Username'])) && (isAuthorized("",$MM_authorizedUsers, $_SESSION['MM_Username'], $_SESSION['MM_UserGroup'])))) {   
  $MM_qsChar = "?";
  $MM_referrer = $_SERVER['PHP_SELF'];
  if (strpos($MM_restrictGoTo, "?")) $MM_qsChar = "&";
  if (isset($QUERY_STRING) && strlen($QUERY_STRING) > 0) 
  $MM_referrer .= "?" . $QUERY_STRING;
  $MM_restrictGoTo = $MM_restrictGoTo. $MM_qsChar . "accesscheck=" . urlencode($MM_referrer);
  header("Location: ". $MM_restrictGoTo); 
  exit;
}
?>
<?php
if (!function_exists("GetSQLValueString")) {
function GetSQLValueString($theValue, $theType, $theDefinedValue = "", $theNotDefinedValue = "") 
{
  if (PHP_VERSION < 6) {
    $theValue = get_magic_quotes_gpc() ? stripslashes($theValue) : $theValue;
  }

  $theValue = function_exists("mysql_real_escape_string") ? mysql_real_escape_string($theValue) : mysql_escape_string($theValue);

  switch ($theType) {
    case "text":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;    
    case "long":
    case "int":
      $theValue = ($theValue != "") ? intval($theValue) : "NULL";
      break;
    case "double":
      $theValue = ($theValue != "") ? doubleval($theValue) : "NULL";
      break;
    case "date":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;
    case "defined":
      $theValue = ($theValue != "") ? $theDefinedValue : $theNotDefinedValue;
      break;
  }
  return $theValue;
}
}

mysql_select_db($database_MySQL, $MySQL);
$query_rsCurso = "SELECT * FROM curso ORDER BY ano desc, orden asc";
$rsCurso = mysql_query($query_rsCurso, $MySQL) or die(mysql_error());
$row_rsCurso = mysql_fetch_assoc($rsCurso);
$totalRows_rsCurso = mysql_num_rows($rsCurso);

mysql_select_db($database_MySQL, $MySQL);
$query_rsArea = "select a.idArea, a.area from area as a order by a.area";
$rsArea = mysql_query($query_rsArea, $MySQL) or die(mysql_error());
$row_rsArea = mysql_fetch_assoc($rsArea);
$totalRows_rsArea = mysql_num_rows($rsArea);

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title>:: Curso / Materia ::</title>
<script language="javascript" type="text/javascript">

function fCursoMateriaCrear()
	{
	var idCurso, idMateria;
	idCurso =document.getElementById('idCurso');
	idCurso = idCurso.options[idCurso.selectedIndex].value;	
	idMateria =document.getElementById('idMateria');
	idMateria = idMateria.options[idMateria.selectedIndex].value;	
  porcentajeArea = document.getElementById('porcentajeArea').value;
	mostrar_url('CursoMateriaCrear.php','MM_insert=form1&idCurso='+idCurso+'&idMateria='+idMateria+'&porcentajeArea='+porcentajeArea,'divCursoMateria','get');
	demorar('fCursoMateriaListar()',2);	
	}

function fCursoMateriaListar()
	{
	var idCurso, idMateria ;
	idCurso =document.getElementById('idCurso');
	idCurso = idCurso.options[idCurso.selectedIndex].value;	
	mostrar_url('CursoMateriaListar.php','idCurso='+idCurso,'divCursoMateria','get');
	}	

  function fMateriaListar()
	{
	var idArea ;
	idArea =document.getElementById('idArea');
	idArea = idArea.options[idArea.selectedIndex].value;		
	mostrar_url('MateriaListar.php','idArea='+idArea,'divMateria','get');
	}	
	
function fCursoMateriaEliminar(id)
	{
		if(confirm('Desea eliminar el registro?.'))
			{
			mostrar_url('CursoMateriaEliminar.php','idCursoMateria='+id,'divCursoMateria','get');
			demorar('fCursoMateriaListar()',2);				
			}
	}

</script>
<? include "header.php"; ?>
</head>
<body>
<? include "menu.php"; ?>


<table border="1">
<tr valign="top">
  <td>
  <table>
  <thead>
  <tr>
    <td colspan="2">Curso / Materia </td>
  </tr>
  </thead>
  <tr>
    <td>Curso:</td>
    <td><select id="idCurso" name="idCurso" onchange="javascript:fCursoMateriaListar();">
        <option></option>
        <?php
        do {  
            ?>
              <option value="<?php echo $row_rsCurso['idCurso']?>"><?php echo $row_rsCurso['ano']?> - <?php echo $row_rsCurso['curso']?></option>
              <?php
            } while ($row_rsCurso = mysql_fetch_assoc($rsCurso));
            $rows = mysql_num_rows($rsCurso);
            if($rows > 0) 
              {
                  mysql_data_seek($rsCurso, 0);
                $row_rsCurso = mysql_fetch_assoc($rsCurso);
              }
            ?>
      </select>
    </td>
  </tr>
  <tr>
    <td>&Aacute;rea:</td>
    <td>
      <select id="idArea" name="idArea" onchange="javascript:fMateriaListar();">
      <option></option>
      <?php
      do {  
          ?>
            <option value="<?php echo $row_rsArea['idArea']?>"><?php echo $row_rsArea['area']?></option>
            <?php
          } while ($row_rsArea = mysql_fetch_assoc($rsArea));
        $rows = mysql_num_rows($rsArea);
        if($rows > 0) 
          {
            mysql_data_seek($rsArea, 0);
            $row_rsArea = mysql_fetch_assoc($rsArea);
          }
          ?>
      </select>
    </td>
  </tr>

  <tr>
    <td>Materia:</td>
    <td>
      <div id="divMateria"></div>
    </td>
  </tr>
  <tr>
    <td>Porcentaje &Aacute;rea:</td>
    <td>
      <input type="text" name="porcentajeArea" id="porcentajeArea"  />
    </td>
  </tr>

  <thead>
    <tr>
      <td colspan="2">
        <input type="button" name="Crear" value="Crear" onclick="javascript:fCursoMateriaCrear()" />
      </td>
    </tr>
  </thead>
  </table>
  </td>
  <td width="100%">
    <div id="divCursoMateria"></div>
  </td>
</tr>
</table>
</body>
</html>
<?php
mysql_free_result($rsCurso);

mysql_free_result($rsArea);
?>