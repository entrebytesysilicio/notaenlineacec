<?php require_once('Connections/MySQL.php'); ?>
<?php
if (!isset($_SESSION)) {
session_start();
}
$MM_authorizedUsers = "Administrador,Profesor";
$MM_donotCheckaccess = "false";
// *** Restrict Access To Page: Grant or deny access to this page
function isAuthorized($strUsers, $strGroups, $UserName, $UserGroup) {
// For security, start by assuming the visitor is NOT authorized.
$isValid = False;
// When a visitor has logged into this site, the Session variable MM_Username set equal to their username.
// Therefore, we know that a user is NOT logged in if that Session variable is blank.
if (!empty($UserName)) {
// Besides being logged in, you may restrict access to only certain users based on an ID established when they login.
// Parse the strings into arrays.
$arrUsers = Explode(",", $strUsers);
$arrGroups = Explode(",", $strGroups);
if (in_array($UserName, $arrUsers)) {
$isValid = true;
}
// Or, you may restrict access to only certain users based on their username.
if (in_array($UserGroup, $arrGroups)) {
$isValid = true;
}
if (($strUsers == "") && false) {
$isValid = true;
}
}
return $isValid;
}
$MM_restrictGoTo = "error.php";
if (!((isset($_SESSION['MM_Username'])) && (isAuthorized("",$MM_authorizedUsers, $_SESSION['MM_Username'], $_SESSION['MM_UserGroup'])))) {
$MM_qsChar = "?";
$MM_referrer = $_SERVER['PHP_SELF'];
if (strpos($MM_restrictGoTo, "?")) $MM_qsChar = "&";
if (isset($QUERY_STRING) && strlen($QUERY_STRING) > 0)
$MM_referrer .= "?" . $QUERY_STRING;
$MM_restrictGoTo = $MM_restrictGoTo. $MM_qsChar . "accesscheck=" . urlencode($MM_referrer);
header("Location: ". $MM_restrictGoTo);
exit;
}

if($_POST['idCurso'] != "" && $_POST['idAlumno'] != "")
{
$sql = "insert into matricula (idAlumno, idCurso, folio, matricula,fechaMatricula)
		select ".$_POST['idAlumno'].",".$_POST['idCurso'].",'".$_POST['folio']."','".$_POST['matricula']."','".$_POST['fechaMatricula']."' from dual
		where not exists (select 'x' from matricula as m where m.idAlumno = ".$_POST['idAlumno']." and m.idCurso = ".$_POST['idCurso']." )";
$resultado = mysql_query($sql);
if(mysql_affected_rows() > 0)
	{
?>
<div class="alert alert-success">
	Se cre&oacute; la matricula correctamente.
</div>
<?
}
else
{
?>
<div class="alert alert-warning">
	Error al crear el registro, si ya existe una matr&iacute;cula no se puede crear otra.
</div>
<?
}
}
else
{
?>
<div class="alert alert-danger">
	Por favor coloque todos los datos solicitados.
</div>
<?
}
?>