<?php require_once('Connections/MySQL.php'); ?>
<?php
if (!isset($_SESSION)) {
  session_start();
}
$MM_authorizedUsers = "Administrador,Profesor";
$MM_donotCheckaccess = "false";

// *** Restrict Access To Page: Grant or deny access to this page
function isAuthorized($strUsers, $strGroups, $UserName, $UserGroup) { 
  // For security, start by assuming the visitor is NOT authorized. 
  $isValid = False; 

  // When a visitor has logged into this site, the Session variable MM_Username set equal to their username. 
  // Therefore, we know that a user is NOT logged in if that Session variable is blank. 
  if (!empty($UserName)) { 
    // Besides being logged in, you may restrict access to only certain users based on an ID established when they login. 
    // Parse the strings into arrays. 
    $arrUsers = Explode(",", $strUsers); 
    $arrGroups = Explode(",", $strGroups); 
    if (in_array($UserName, $arrUsers)) { 
      $isValid = true; 
    } 
    // Or, you may restrict access to only certain users based on their username. 
    if (in_array($UserGroup, $arrGroups)) { 
      $isValid = true; 
    } 
    if (($strUsers == "") && false) { 
      $isValid = true; 
    } 
  } 
  return $isValid; 
}

$MM_restrictGoTo = "error.php";
if (!((isset($_SESSION['MM_Username'])) && (isAuthorized("",$MM_authorizedUsers, $_SESSION['MM_Username'], $_SESSION['MM_UserGroup'])))) {   
  $MM_qsChar = "?";
  $MM_referrer = $_SERVER['PHP_SELF'];
  if (strpos($MM_restrictGoTo, "?")) $MM_qsChar = "&";
  if (isset($QUERY_STRING) && strlen($QUERY_STRING) > 0) 
  $MM_referrer .= "?" . $QUERY_STRING;
  $MM_restrictGoTo = $MM_restrictGoTo. $MM_qsChar . "accesscheck=" . urlencode($MM_referrer);
  header("Location: ". $MM_restrictGoTo); 
  exit;
}
?>
<?php
if (!function_exists("GetSQLValueString")) {
function GetSQLValueString($theValue, $theType, $theDefinedValue = "", $theNotDefinedValue = "") 
{
  if (PHP_VERSION < 6) {
    $theValue = get_magic_quotes_gpc() ? stripslashes($theValue) : $theValue;
  }

  $theValue = function_exists("mysql_real_escape_string") ? mysql_real_escape_string($theValue) : mysql_escape_string($theValue);

  switch ($theType) {
    case "text":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;    
    case "long":
    case "int":
      $theValue = ($theValue != "") ? intval($theValue) : "NULL";
      break;
    case "double":
      $theValue = ($theValue != "") ? doubleval($theValue) : "NULL";
      break;
    case "date":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;
    case "defined":
      $theValue = ($theValue != "") ? $theDefinedValue : $theNotDefinedValue;
      break;
  }
  return $theValue;
}
}

$colname_rsListadoAlumnosCurso = "-1";
if (isset($_GET['idCurso'])) {
  $colname_rsListadoAlumnosCurso = $_GET['idCurso'];
}
mysql_select_db($database_MySQL, $MySQL);
$query_rsListadoAlumnosCurso = sprintf("SELECT alumnocurso.idAlumnoCurso, 
									   			curso.curso, 
												curso.ano, 
												alumno.nombres, 
												alumno.apellidos,
												alumno.identificacion 
										FROM 	alumnocurso 
						   				inner join curso on curso.idCurso = alumnocurso.idCurso
						   				inner join alumno on alumno.idAlumno = alumnocurso.idAlumno 
										WHERE alumnocurso.idCurso = %s order by alumno.nombres, alumno.apellidos ", GetSQLValueString($colname_rsListadoAlumnosCurso, "int"));

$rsListadoAlumnosCurso = mysql_query($query_rsListadoAlumnosCurso, $MySQL) or die(mysql_error());
$row_rsListadoAlumnosCurso = mysql_fetch_assoc($rsListadoAlumnosCurso);
$totalRows_rsListadoAlumnosCurso = mysql_num_rows($rsListadoAlumnosCurso);
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>

<title>:: Alumnos del curso <? echo $_GET['idCurso']; ?> ::</title>

</head>

<body>
Alumnos en este curso: <? echo $totalRows_rsListadoAlumnosCurso; ?>
<table border="1" >
<thead>
  <tr>
    <td>No.</td>
    <td>Año</td>
    <td>Curso</td>
    <td>Nombres</td>
    <td>Apellidos</td>
    <td>Identificacion</td>
    <td>Acciones</td>
  </tr>
  </thead>
  <?php $i = 1; do { ?>
    <tr>
    	<td align="center"><? echo $i++; ?></td>
      <td><?php echo $row_rsListadoAlumnosCurso['ano']; ?></td>
      <td><?php echo $row_rsListadoAlumnosCurso['curso']; ?></td>
      <td><?php echo $row_rsListadoAlumnosCurso['nombres']; ?></td>    
      <td><?php echo $row_rsListadoAlumnosCurso['apellidos']; ?></td>
      <td><?php echo $row_rsListadoAlumnosCurso['identificacion']; ?></td>
      <td><input type="button" name="Eliminar" value="Eliminar" onclick="javascript:fCursoAlumnoEliminar(<?php echo $row_rsListadoAlumnosCurso['idAlumnoCurso']; ?>);" /></td>
    </tr>
    <?php } while ($row_rsListadoAlumnosCurso = mysql_fetch_assoc($rsListadoAlumnosCurso)); ?>
</table>
</body>
</html>
<?php
mysql_free_result($rsListadoAlumnosCurso);
?>
