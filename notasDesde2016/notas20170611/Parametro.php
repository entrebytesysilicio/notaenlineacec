<?php require_once('Connections/MySQL.php'); ?>
<?php
if (!isset($_SESSION)) {
  session_start();
}
$MM_authorizedUsers = "Administrador";
$MM_donotCheckaccess = "false";

// *** Restrict Access To Page: Grant or deny access to this page
function isAuthorized($strUsers, $strGroups, $UserName, $UserGroup) { 
  // For security, start by assuming the visitor is NOT authorized. 
  $isValid = False; 

  // When a visitor has logged into this site, the Session variable MM_Username set equal to their username. 
  // Therefore, we know that a user is NOT logged in if that Session variable is blank. 
  if (!empty($UserName)) { 
    // Besides being logged in, you may restrict access to only certain users based on an ID established when they login. 
    // Parse the strings into arrays. 
    $arrUsers = Explode(",", $strUsers); 
    $arrGroups = Explode(",", $strGroups); 
    if (in_array($UserName, $arrUsers)) { 
      $isValid = true; 
    } 
    // Or, you may restrict access to only certain users based on their username. 
    if (in_array($UserGroup, $arrGroups)) { 
      $isValid = true; 
    } 
    if (($strUsers == "") && false) { 
      $isValid = true; 
    } 
  } 
  return $isValid; 
}

$MM_restrictGoTo = "error.php";
if (!((isset($_SESSION['MM_Username'])) && (isAuthorized("",$MM_authorizedUsers, $_SESSION['MM_Username'], $_SESSION['MM_UserGroup'])))) {   
  $MM_qsChar = "?";
  $MM_referrer = $_SERVER['PHP_SELF'];
  if (strpos($MM_restrictGoTo, "?")) $MM_qsChar = "&";
  if (isset($QUERY_STRING) && strlen($QUERY_STRING) > 0) 
  $MM_referrer .= "?" . $QUERY_STRING;
  $MM_restrictGoTo = $MM_restrictGoTo. $MM_qsChar . "accesscheck=" . urlencode($MM_referrer);
  header("Location: ". $MM_restrictGoTo); 
  exit;
}
?>
<?php
if (!function_exists("GetSQLValueString")) {
function GetSQLValueString($theValue, $theType, $theDefinedValue = "", $theNotDefinedValue = "") 
{
  if (PHP_VERSION < 6) {
    $theValue = get_magic_quotes_gpc() ? stripslashes($theValue) : $theValue;
  }

  $theValue = function_exists("mysql_real_escape_string") ? mysql_real_escape_string($theValue) : mysql_escape_string($theValue);

  switch ($theType) {
    case "text":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;    
    case "long":
    case "int":
      $theValue = ($theValue != "") ? intval($theValue) : "NULL";
      break;
    case "double":
      $theValue = ($theValue != "") ? doubleval($theValue) : "NULL";
      break;
    case "date":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;
    case "defined":
      $theValue = ($theValue != "") ? $theDefinedValue : $theNotDefinedValue;
      break;
  }
  return $theValue;
}
}

$editFormAction = $_SERVER['PHP_SELF'];
if (isset($_SERVER['QUERY_STRING'])) {
  $editFormAction .= "?" . htmlentities($_SERVER['QUERY_STRING']);
}

if ((isset($_POST["MM_insert"])) && ($_POST["MM_insert"] == "form1")) {
  $insertSQL = sprintf("INSERT INTO parametro (parametro, valor) VALUES (%s, %s)",
                       GetSQLValueString($_POST['parametro'], "text"),
                       GetSQLValueString($_POST['valor'], "text"));

  mysql_select_db($database_MySQL, $MySQL);
  $Result1 = mysql_query($insertSQL, $MySQL) or die(mysql_error());

  $insertGoTo = "Parametro.php";
  if (isset($_SERVER['QUERY_STRING'])) {
    $insertGoTo .= (strpos($insertGoTo, '?')) ? "&" : "?";
    $insertGoTo .= $_SERVER['QUERY_STRING'];
  }
  header(sprintf("Location: %s", $insertGoTo));
}

mysql_select_db($database_MySQL, $MySQL);
$query_rsParametro = "SELECT * FROM parametro ORDER BY parametro ASC";
$rsParametro = mysql_query($query_rsParametro, $MySQL) or die(mysql_error());
$row_rsParametro = mysql_fetch_assoc($rsParametro);
$totalRows_rsParametro = mysql_num_rows($rsParametro);
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>

<title>:: Crear Parametros ::</title>
<? include "header.php"; ?> 
<script src="SpryAssets/SpryCollapsiblePanel.js" type="text/javascript"></script>
<link href="SpryAssets/SpryCollapsiblePanel.css" rel="stylesheet" type="text/css" />
</head>

<body>
<? include "menu.php"; ?>

<div id="CollapsiblePanel1" class="CollapsiblePanel">
  <div class="CollapsiblePanelTab" tabindex="0">Crear Par&aacute;metro...</div>
  <div class="CollapsiblePanelContent">
  
  <form action="<?php echo $editFormAction; ?>" method="post" name="form1" id="form1"><br />

  <table align="center">
  <thead>
    <tr>
      <td colspan="2">Par&aacute;metros</td>
    </tr>  
    </thead>
    <tr valign="baseline">
      <td nowrap="nowrap" align="right">Par&aacute;metro:</td>
      <td><input type="text" name="parametro" value="" size="32" /></td>
    </tr>
    <tr valign="baseline">
      <td nowrap="nowrap" align="right">Valor:</td>
      <td><input type="text" name="valor" value="" size="32" /></td>
    </tr>
    <thead>
    <tr >
      <td colspan="2"><input type="submit" value="Crear" /></td>
    </tr>
    </thead>
  </table>
  <input type="hidden" name="MM_insert" value="form1" /><br />

</form>

  
  
  </div>
</div>
<br />
<table  align="center">
<thead>
  <tr>
    <td>No.</td>
    <td>par&aacute;metro</td>	    
    <td>valor</td>

 <td>Acci&oacute;n</td>    
  </tr>
  </thead>
  <?php $i = 1; do { ?>
    <tr>
      <td align="center"><? echo $i++?></td>
      <td><?php echo $row_rsParametro['parametro']; ?></td>
      <td><?php echo $row_rsParametro['valor']; ?></td>
      
      <td><a href="ParametroActualizar.php?idParametro=<?php echo $row_rsParametro['idParametro']; ?>">Actualizar</a> <a href="ParametroEliminar.php?idParametro=<?php echo $row_rsParametro['idParametro']; ?>" onclick="return confirm('Desea eliminar el registro?.'); ">Eliminar</a>
      
      
      </td>
    </tr>
    <?php } while ($row_rsParametro = mysql_fetch_assoc($rsParametro)); ?>
</table>
<p><b>Nota:</b> Debe existir el par&aacute;metro <b>bimestreActual</b>, que <b>corresponde</b> al <b>periodo actual</b>. Puede contener los valores 1,2,3 o 4. Cuando este par&aacute;metro no existe, los profesores no pueden ingresar notas, por lo tanto sirve como un control para restringir el ingreso de las notas al sistema.</p>
<script type="text/javascript">
<!--
var CollapsiblePanel1 = new Spry.Widget.CollapsiblePanel("CollapsiblePanel1");
//-->
  </script>
</body>

</html>
<?php
mysql_free_result($rsParametro);
?>
