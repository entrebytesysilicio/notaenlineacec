<?php require_once('Connections/MySQL.php'); ?>
<?php require_once('InformeValorativoClase.php'); ?>
<?php
  //header('Content-type: application/vnd.ms-excel');

  //header('Content-type: application/msword');
ini_set('display_errors', 0);

if (!isset($_SESSION)) {
	session_start();
}
$MM_authorizedUsers = "Administrador,Profesor,Padre";
$MM_donotCheckaccess = "false";

  // *** Restrict Access To Page: Grant or deny access to this page
function isAuthorized($strUsers, $strGroups, $UserName, $UserGroup) { 
	// For security, start by assuming the visitor is NOT authorized. 
	$isValid = False; 

	// When a visitor has logged into this site, the Session variable MM_Username set equal to their username. 
	// Therefore, we know that a user is NOT logged in if that Session variable is blank. 
	if (!empty($UserName)) { 
	  // Besides being logged in, you may restrict access to only certain users based on an ID established when they login. 
	  // Parse the strings into arrays. 
		$arrUsers = Explode(",", $strUsers); 
		$arrGroups = Explode(",", $strGroups); 
		if (in_array($UserName, $arrUsers)) { 
			$isValid = true; 
		} 
	  // Or, you may restrict access to only certain users based on their username. 
		if (in_array($UserGroup, $arrGroups)) { 
			$isValid = true; 
		} 
		if (($strUsers == "") && false) { 
			$isValid = true; 
		} 
	} 
	return $isValid; 
}

$MM_restrictGoTo = "error.php";
if (!((isset($_SESSION['MM_Username'])) && (isAuthorized("",$MM_authorizedUsers, $_SESSION['MM_Username'], $_SESSION['MM_UserGroup'])))) {   
	$MM_qsChar = "?";
	$MM_referrer = $_SERVER['PHP_SELF'];
	if (strpos($MM_restrictGoTo, "?")) $MM_qsChar = "&";
	if (isset($QUERY_STRING) && strlen($QUERY_STRING) > 0) 
		$MM_referrer .= "?" . $QUERY_STRING;
	$MM_restrictGoTo = $MM_restrictGoTo. $MM_qsChar . "accesscheck=" . urlencode($MM_referrer);
	header("Location: ". $MM_restrictGoTo); 
	exit;
}
$iv = new EstudianteInformeValorativo();
$iv->idAlumno = $_GET['idAlumno'];
$iv->idCurso = $_GET['idCurso'];
$bimestreActual = $_GET['periodo'];
if($_GET['pdf'])
{
	?>
	<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
	<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
		<link href="estilo_boletin.css" rel="stylesheet" type="text/css" />
		<style type="text/css">
		<!--
		.t1 {
			text-align: center;
			font-size: 9px;
		}
		-->
		</style>
	</head>
	<body onload="window.print();">
		<?	
	}
	?>
	<center>
		<div class="tamanoCarta">
			<table width="100%">
				<tr>
					<td align="left"><? //$im = @imagecreatefromjpeg("imagenes/logo.tiff"); imagejpeg($im); ?>
						<img src="imagenes/logo.png" height="96px" /></td>
						<td width="100%" align="center"><H3>HERMANAS MERCEDARIAS DEL SANTISIMO SACRAMENTO<br />
							COLEGIO EUCAR�STICO CAMPESTRE </H3>
							<h4>INFORME VALORATIVO<br />
								<? echo $_GET['ano']; ?></H4></td>
								<td align="right">&nbsp;</td>
							</tr>
						</table>
						<p style="width:5px"></p>
						<table border="0" width="100%" cellpadding="1" cellspacing="1">
							<tr>
								<td colspan="9"></td>
							</tr>
							<tr>
								<td><b>Estudiante:</b></td>
								<td class="bordeBoletin"><? $iv->nombreEstudiante(); ?></td>
								<td><b>Curso:</b></td>
								<td class="bordeBoletin"  align="center"><? $iv->nombreCurso(); ?></td>
								<td><b>Promedio:</b></td>
								<td class="bordeBoletin" align="center"><?
		  ////busca el promedio del alumno en el periodo 
		  ////si el periodo es 5, se debe buscar el promedio durante todo el a�o escolar
								$sql7 = "
								select 	b.idAlumno,
								avg(
									ifnull(((((nota1 +
										nota2 +
										nota3 +
										nota4 +
										nota5 +
										nota6 )/

(
	case when nota1 > 0 then 1 else 0 end 	
	+
	case when nota2 > 0 then 1 else 0 end 	
	+
	case when nota3 > 0 then 1 else 0 end 	
	+
	case when nota4 > 0 then 1 else 0 end 	
	+
	case when nota5 > 0 then 1 else 0 end 	
	+
	case when nota6 > 0 then 1 else 0 end 	
	)
) * 40 )/100),0)
+
ifnull(((((nota7 +
	nota8 +
	nota9 +
	nota10 +
	nota11 +
	nota12 )/
(
	case when nota7 > 0 then 1 else 0 end 	
	+
	case when nota8 > 0 then 1 else 0 end 	
	+
	case when nota9 > 0 then 1 else 0 end 	
	+
	case when nota10 > 0 then 1 else 0 end 	
	+
	case when nota11 > 0 then 1 else 0 end 	
	+
	case when nota12 > 0 then 1 else 0 end 	
	)						

) * 40 )/100),0)
+
ifnull(((((nota13 +
	nota14 )/
(
	case when nota13 > 0 then 1 else 0 end 	
	+
	case when nota14 > 0 then 1 else 0 end 							
	)
) * 20 )/100),0)) as promedio	

from 	boletin as b
inner join alumno as a on a.idAlumno = b.idAlumno
inner join curso as c on b.idCurso = c.idCurso
where 		b.idCurso = ".$_GET['idCurso'];
if ($_GET['periodo'] != 5)
{
	$sql7 .= " and b.bimestre = ".$_GET['periodo'];
}
$sql7 .= "	group by 	b.idAlumno order by 2 desc";
$resultado7 = mysql_query($sql7,$MySQL);
$filas7 = mysql_fetch_array($resultado7);
$vandera = 0;
$puesto = 1;
do {
				  ////si el alumno buscado es igual al alumno que se esta recorriendo en el cursor, entonces guardo las variables para poder mostrarlas luego.
	if ($_GET['idAlumno'] == $filas7['idAlumno'] )
	{
		$puesto_alumno = $puesto;
					  ////Se coloca la vandera, por si el programa encuentra al alumno, termine la ejecucion del ciclo
		$vandera = 1;
		$promedio = $filas7['promedio']; 
	}
	$puesto++;
}while( ($filas7 = mysql_fetch_array($resultado7)) and $vandera == 0);
	  ////muestra el promedio.
	  //echo substr($promedio,0,4);
echo number_format($promedio,1);
?></td>
<td><b>Puesto:</b></td>
<td  class="bordeBoletin" align="center"><? 
		  ////Muestra el puesto que ocupo el alumno.
echo $puesto_alumno; 
?></td>
<td></td>
</tr>
<tr>
	<td colspan="9"></td>
</tr>
<tr>
	<td><b>Director:</b></td>
	<td class="bordeBoletin"><? $iv->nombreDirectorCurso (); ?></td>
	<td><b>Periodo:</b></td>
	<? ////Imprime el nombre del periodo. ?>
	<td class="bordeBoletin"  align="center"><? switch ($_GET['periodo']) { case 1: echo "Primero"; break; case 2: echo "Segundo"; break; case 3: echo "Tercero"; break; case 4: echo "Cuarto"; break; case 5: echo "Final"; break; } ?></td>
	<td>&nbsp;</td>
	<td>&nbsp;</td>
	<td>&nbsp;</td>
	<td>&nbsp;</td>
	<td>&nbsp;</td>
</tr>
<tr>
	<td colspan="9"></td>
</tr>
</table>
<p style="width:5px"></p>
<table border="1" width="100%">
	<?

	?>
	<tr valign="middle"  align="center"  class="tablaBoletin1">
		<td rowspan="2"><strong>&Aacute;rea</strong></td>
		<td rowspan="2"><strong>Asignatura</strong></td>
		<td rowspan="2"><strong>IH</strong></td>
		<td rowspan="2"><strong>Fallas*</strong></td>
		<td colspan="8"><strong>PERIODOS</strong></td>
		<td  rowspan="2"><strong>Acumulado</strong></td>
	</tr>
	<tr valign="middle" align="center"  class="tablaBoletin1">
		<td colspan="2"><strong>1</strong></td>
		<td colspan="2" ><strong>2</strong></td>
		<td colspan="2" ><strong>3</strong></td>
		<td colspan="2" ><strong>4</strong></td>
	</tr>
	<tr align="center">
		<td colspan="4">
		</td>
		<td>V</td>
		<td>R</td>      
		<td>V</td>
		<td>R</td>      
		<td>V</td>
		<td>R</td>      
		<td>V</td>
		<td>R</td>
		<td>&nbsp;</td>
	</tr>
	<?	
	////determina la cantidad de materias por area
	/*
	Se cambia debido a que cuando un curso tiene mas de una materia, se expresa mal el resultado.
	$sql11 = "
	select count(1) as materias, a.Area 
	from cursomateriaprofesor as cmp 
	inner join materia as m on m.idMateria = cmp.idMateria
	inner join area as a on m.idArea = a.idArea
	where cmp.idCurso = ".$_GET['idCurso']."
	group by a.Area
	order by a.orden
	";
	*/
	$sql11 = "
	select count(1) as materias, a.Area
	from  materia as m
	inner join area as a on m.idArea = a.idArea
	where m.idMateria in (select idMateria from cursomateriaprofesor as cmp where  cmp.idCurso = ".$_GET['idCurso'].")
	group by a.Area
	order by a.orden
	";

	$resultado11 = mysql_query($sql11,$MySQL);
	if($filas11 = mysql_fetch_array($resultado11))
	{
		do {
			$areas[$filas11['Area']] = $filas11['materias'];
		}while($filas11 = mysql_fetch_array($resultado11));
	}
	else
	{
		echo mysql_error();
	}


	////identifica la intensidad horaria de cada una de las materias que se dictan en determinado curso
	////
	////Recorre la consulta para sacar el resto de la informacion, llendo ordenado por el nombre de las materias
	////Por cada materia imprime el nombre, coloca la intensidad horaria, y saca los promedios y desempe�os semestre a semestre
	$sql1 = "
	select sum(cmp.intensidadHoraria) as IH, m.materia, m.idMateria, a.Area 
	from cursomateriaprofesor as cmp 
	inner join materia as m on m.idMateria = cmp.idMateria
	inner join area as a on m.idArea = a.idArea
	where cmp.idCurso = ".$_GET['idCurso']."
	group by  m.materia, m.idMateria, a.Area
	order by a.orden, m.materia
	";
	mysql_select_db($database_MySQL, $MySQL);					
	$resultado1 = mysql_query($sql1,$MySQL);
	if($filas1 = mysql_fetch_array($resultado1))
	{

		$areaActual = "";
		do {
			?>
			<tr valign="top" align="center">
				<? 
			  ////Imprime el nombre del area
				if ($areaActual != $filas1['Area'])
				{
					?>
					<td align="left" rowspan="<? echo $areas[$filas1['Area']];?>" ><b>
						<?
						echo $filas1['Area']; 
						$areaActual = 		$filas1['Area'];			
						?>
					</b></td>
					<?
				}
				?>
				<td align="left" ><? 
			  ////Imprime el nombre de la materia
				echo $filas1['materia']; 
				?></td>
				<td><? 
			  ////Imprime la intensidad horaria de la materia
				echo $filas1['IH']; 
				?></td>
				<?

				$sql21 = "select sum(case when bimestre = ".$_GET['periodo']." then fallas else 0 end) fallas, sum(fallas) as fallasAcumuladas				
				from boletin where idAlumno = ".$_GET['idAlumno']." and idCurso = ".$_GET['idCurso']." and idMateria = ".$filas1['idMateria'] ;

				$resultado21 = mysql_query($sql21,$MySQL);
				if ($filas21 = mysql_fetch_array($resultado21))
				{
					if($filas21['fallas']>0 || $filas21['fallasAcumuladas'] > 0)
					{
						$fallas = $filas21['fallas']." (".$filas21['fallasAcumuladas'].")";	
					}
					else
					{
						$fallas = "";	
					}

				}						
				?>
				<td><? 
						  ////Muestra las fallas
				echo $fallas; 
				?></td>
				<?				


					  ////Calcula el promedio para la materia que se esta consultando en el momento
				unset($promedio);


				for($i = 1; $i <= 4 ; $i++)
				{
						  ////Saca la nota de determinado alumno, en determinado curso, para determinada materia, en determinado semestre.
					$sql2 = "select 

					ifnull(((((nota1 +
						nota2 +
						nota3 +
						nota4 +
						nota5 +
						nota6 )/

(
	case when nota1 > 0 then 1 else 0 end 	
	+
	case when nota2 > 0 then 1 else 0 end 	
	+
	case when nota3 > 0 then 1 else 0 end 	
	+
	case when nota4 > 0 then 1 else 0 end 	
	+
	case when nota5 > 0 then 1 else 0 end 	
	+
	case when nota6 > 0 then 1 else 0 end 	
	)
) * 40 )/100),0)
+
ifnull(((((nota7 +
	nota8 +
	nota9 +
	nota10 +
	nota11 +
	nota12 )/
(
	case when nota7 > 0 then 1 else 0 end 	
	+
	case when nota8 > 0 then 1 else 0 end 	
	+
	case when nota9 > 0 then 1 else 0 end 	
	+
	case when nota10 > 0 then 1 else 0 end 	
	+
	case when nota11 > 0 then 1 else 0 end 	
	+
	case when nota12 > 0 then 1 else 0 end 	
	)						

) * 40 )/100),0)
+
ifnull(((((nota13 +
	nota14 )/
(
	case when nota13 > 0 then 1 else 0 end 	
	+
	case when nota14 > 0 then 1 else 0 end 							
	)
) * 20 )/100),0) 
as nota,
recuperacion 
as recuperacion

from 	boletin 
where 	idAlumno = ".$_GET['idAlumno']." 
and idCurso = ".$_GET['idCurso']." 
and idMateria = ".$filas1['idMateria']." 
and bimestre = ".$i;
$resultado2 = mysql_query($sql2,$MySQL);
if ($filas2 = mysql_fetch_array($resultado2))
{
	$nota = $filas2['nota'];
	$recuperacion = $filas2['recuperacion'];
}
else
{
	$nota = 0;
	$recuperacion = 0;
}							
if ($nota > 0 && $nota > $recuperacion)
{
							////Solo se coloca la nota para que recalcule el promedio
							////si esta sacando la nota de determinado promedio.
							////Es decir si se selecciona el promedio 1 solo va a mostrar
							////Las notas hasta el promedio 1 y el promedio general solo
							////se va a mostrar con las notas del promedio 1.
	if ($i <= $bimestreActual)
	{
		$promedio[$i] = $nota;			
	}								
}
else
{
							////Solo se coloca la nota para que recalcule el promedio
							////si esta sacando la nota de determinado promedio.
							////Es decir si se selecciona el promedio 1 solo va a mostrar
							////Las notas hasta el promedio 1 y el promedio general solo
							////se va a mostrar con las notas del promedio 1.
	if ($i <= $bimestreActual && $recuperacion > 0)
	{
		$promedio[$i] = $recuperacion;			
	}								
}
?>
<td>
	<? 		
                        ////Solo muestra la nota hasta el bimestre seleccionado
	if($nota > 0 && $i <= $bimestreActual)
	{
                        ////Muestra la nota
                        //echo substr($nota,0,3); 
		echo number_format($nota,1);

	}
	?>
</td>
<td>
	<? 		
                        ////Solo muestra la nota hasta el bimestre seleccionado
	if($recuperacion > 0 && $i <= $bimestreActual)
	{
                        ////Muestra la nota
                        //echo substr($nota,0,3); 
		echo number_format($recuperacion,1);

	}
	?>
</td>




<!--<td> -->
<?
						  /* if($nota > 0)
							  {
							  ////Consulta el desempeno
							  $sql3 =" select idDesempeno, desempeno from desempeno where ano = ".$_GET['ano']." and ".$nota." between notaMinima and notaMaxima";
							  $resultado3 = mysql_query($sql3,$MySQL);
							  if($filas3 = mysql_fetch_array($resultado3))
								  {
									  echo $filas3['desempeno'];
								  }
								  else
								  {
									  echo mysql_error();
								  }	
								} */


								?>
								<!--</td> -->

								<?
							}
							?>
							<td><? 
							$notaPromedio = array_sum($promedio) / count($promedio);
							if($notaPromedio > 0)
							{
				  ////Imprime promedio general acumulado
				  //echo substr($notaPromedio,0,3); 	
								echo number_format($notaPromedio,1);
							}

							?></td>
							<!--   <td> -->
							<?

	  /* 		if ($notaPromedio > 0)
				  {
				  ////Imprime el texto relacionado con el promedio calculado en el a�o
				  $sql4 =" select desempeno from desempeno where ano = ".$_GET['ano']." and ".$notaPromedio." between notaMinima and notaMaxima";
				  //echo $sql4;
				  $resultado4 = mysql_query($sql4,$MySQL);
				  if($filas4 = mysql_fetch_array($resultado4))
					{
					echo $filas4['desempeno'];
					}
					else
					{
					echo mysql_error();
					}  					
				} */

				?>
				<!--  </td> -->

				<?

				?>
			</tr>
			<?
		}while($filas1 = mysql_fetch_array($resultado1));
	}
	else
	{
		echo mysql_error();
	}
	?>
</table>
<p style="width:5px"></p>
<table width="100%" border="0">
	<tr>
		<td> *Fallas actuales (Fallas acumuladas) - V Valoraci&oacute;n del Periodo - R Valoraci&oacute;n de Recuperaci&oacute;n<br /></td>
	</tr>
</table>
<p style="width:5px"></p>
<table width="100%" border="0">
	<tr>
		<td><b>Escala de valoraci&oacute;n:</b><br />
			<?
  //$sql8 = "select concat(' De ',cast(notaMinima as char), ' a ',cast(notaMaxima as char),' ',desempeno) as desempeno from desempeno where ano = ".date("Y")." order by notaMinima";
			$sql8 = "select notaMinima, notaMaxima, desempeno, concat(' De ',cast(notaMinima as char), ' a ',cast(notaMaxima as char),' ',desempeno) as desempeno2 from desempeno where ano = ".date("Y")." order by notaMinima";

			$resultado8 = mysql_query($sql8,$MySQL);
			$filas8 = mysql_fetch_array($resultado8);

			?>

          <!--<table align="center" border="0" width="60%">
          <tr align="center"> -->

          	<?
          	do{
          		?>

          		<!--<td> -->

          		<?
	  //echo $filas8['desempeno']."&nbsp;&nbsp;&nbsp;";
          		echo "De ".number_format($filas8['notaMinima'],1)." A ".number_format($filas8['notaMaxima'],1)." ".$filas8['desempeno']."&nbsp;&nbsp;&nbsp;";
          		?>

          		<!--  </td> -->

          		<?

          	}while($filas8 = mysql_fetch_array($resultado8));

          	?>

          	<!--</tr></table> --></td>
          </tr>
      </table>
      <p style="width:5px"></p>
      <table width="100%" border="0">
      	<tr>
      		<td colspan="2"><strong>OBSERVACIONES Y/O RECOMENDACIONES</strong><br />
      			<br />
      			<hr />
      			<br />
      			<hr />
      		</td>
      	</tr>
      	<tr valign="top">
      		<td><br />
      			<br />
      			_________________________<br />
      			<strong>FIRMA DIRECTOR DE GRUPO</strong><br />
      			<? $iv->nombreDirectorCurso (); ?></td>
      			<td>&nbsp; 
          <!--<br /><br /><br />
  _______________________<br />
  FIRMA RECTOR&Iacute;A<br /> --></td>
</tr>
</table>
</div>
</center>
<?
if($_GET['pdf'])
{
	?>
</body>
</html>
<?	
}
?>