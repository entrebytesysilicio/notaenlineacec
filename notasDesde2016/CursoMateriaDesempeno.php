<?php require_once('Connections/MySQL.php'); ?>
<?php
if (!isset($_SESSION)) {
  session_start();
}
$MM_authorizedUsers = "Administrador,Profesor";
$MM_donotCheckaccess = "false";

// *** Restrict Access To Page: Grant or deny access to this page
function isAuthorized($strUsers, $strGroups, $UserName, $UserGroup) { 
  // For security, start by assuming the visitor is NOT authorized. 
  $isValid = False; 

  // When a visitor has logged into this site, the Session variable MM_Username set equal to their username. 
  // Therefore, we know that a user is NOT logged in if that Session variable is blank. 
  if (!empty($UserName)) { 
    // Besides being logged in, you may restrict access to only certain users based on an ID established when they login. 
    // Parse the strings into arrays. 
    $arrUsers = Explode(",", $strUsers); 
    $arrGroups = Explode(",", $strGroups); 
    if (in_array($UserName, $arrUsers)) { 
      $isValid = true; 
    } 
    // Or, you may restrict access to only certain users based on their username. 
    if (in_array($UserGroup, $arrGroups)) { 
      $isValid = true; 
    } 
    if (($strUsers == "") && false) { 
      $isValid = true; 
    } 
  } 
  return $isValid; 
}

$MM_restrictGoTo = "error.php";
if (!((isset($_SESSION['MM_Username'])) && (isAuthorized("",$MM_authorizedUsers, $_SESSION['MM_Username'], $_SESSION['MM_UserGroup'])))) {   
  $MM_qsChar = "?";
  $MM_referrer = $_SERVER['PHP_SELF'];
  if (strpos($MM_restrictGoTo, "?")) $MM_qsChar = "&";
  if (isset($QUERY_STRING) && strlen($QUERY_STRING) > 0) 
  $MM_referrer .= "?" . $QUERY_STRING;
  $MM_restrictGoTo = $MM_restrictGoTo. $MM_qsChar . "accesscheck=" . urlencode($MM_referrer);
  header("Location: ". $MM_restrictGoTo); 
  exit;
}
?>
<?php
if (!function_exists("GetSQLValueString")) {
function GetSQLValueString($theValue, $theType, $theDefinedValue = "", $theNotDefinedValue = "") 
{
  if (PHP_VERSION < 6) {
    $theValue = get_magic_quotes_gpc() ? stripslashes($theValue) : $theValue;
  }

  $theValue = function_exists("mysql_real_escape_string") ? mysql_real_escape_string($theValue) : mysql_escape_string($theValue);

  switch ($theType) {
    case "text":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;    
    case "long":
    case "int":
      $theValue = ($theValue != "") ? intval($theValue) : "NULL";
      break;
    case "double":
      $theValue = ($theValue != "") ? doubleval($theValue) : "NULL";
      break;
    case "date":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;
    case "defined":
      $theValue = ($theValue != "") ? $theDefinedValue : $theNotDefinedValue;
      break;
  }
  return $theValue;
}
}

mysql_select_db($database_MySQL, $MySQL);
$query_rsCurso = "SELECT * FROM curso ORDER BY ano desc, orden asc";
$rsCurso = mysql_query($query_rsCurso, $MySQL) or die(mysql_error());
$row_rsCurso = mysql_fetch_assoc($rsCurso);
$totalRows_rsCurso = mysql_num_rows($rsCurso);


?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>

<title>:: Curso Materia Desempeno ::</title>
<? include "header.php";?>
<script language="javascript" type="text/javascript">

	function fMostrarDesempeno()
		{
		idCurso =document.getElementById('idCurso');
		idCurso = idCurso.options[idCurso.selectedIndex].value;	
		mostrar_url('Parametros.php','accion=buscardesempeno&idCurso='+idCurso,'divDesempeno','get');
		}
		
	function fCursoMateriaDesempenoCrear ()
		{
		var idCurso,idMateria,bimestre,observacion,slDesempeno;
		idCurso =document.getElementById('idCurso');
		idCurso = idCurso.options[idCurso.selectedIndex].value;	
		idMateria =document.getElementById('idMateria');
		idMateria = idMateria.options[idMateria.selectedIndex].value;	
		bimestre =document.getElementById('bimestre');
		bimestre = bimestre.options[bimestre.selectedIndex].value;	
		observacion =document.getElementById('observacion').value;
		idDesempeno =document.getElementById('slDesempeno');
		idDesempeno= idDesempeno.options[idDesempeno.selectedIndex].value;	
		mostrar_url('CursoMateriaDesempenoCrear.php','MM_insert=form1&idCurso='+idCurso+"&idMateria="+idMateria+"&bimestre="+bimestre+"&observacion="+ observacion+"&idDesempeno="+idDesempeno,'divCursoMateriaDesempeno','get');
		divCMDActual = 0;
		setTimeout('fCursoMateriaDesempenoListar()', 2*1000); //segundos * mil, el parametro es en ms es decir 5 * 1000 = 5 segundos
		}
			
	function fCursoMateriaDesempenoListar()
		{
		var idCurso,idMateria,bimestre,slDesempeno;
		idCurso = document.getElementById('idCurso');
		idCurso = idCurso.options[idCurso.selectedIndex].value;	
		idMateria = document.getElementById('idMateria');
		idMateria = idMateria.options[idMateria.selectedIndex].value;	
		bimestre = document.getElementById('bimestre');
		bimestre = bimestre.options[bimestre.selectedIndex].value;	
		idDesempeno = document.getElementById('slDesempeno');
		idDesempeno = idDesempeno.options[idDesempeno.selectedIndex].value;		
		mostrar_url('CursoMateriaDesempenoListar.php','idCurso='+idCurso+"&idMateria="+idMateria+"&bimestre="+bimestre+"&idDesempeno="+idDesempeno,'divCursoMateriaDesempeno','get');	
		divCMDActual = 0;
		}
			
	function fCursoMateriaDesempenoEliminar(id)
		{
		if(confirm('Desea eliminar el registro?.'))
			{
			mostrar_url('CursoMateriaDesempenoEliminar.php','idCursoMateriaDesempeno='+id,'divCursoMateriaDesempeno','get');
			setTimeout('fCursoMateriaDesempenoListar()', 2*1000); //segundos * mil, el parametro es en ms es decir 5 * 1000 = 5 segundos							
			}
		}

	function fCursoMateriaDesempenoMostrarFormularioActualizar(id)
		{
		if(divCMDActual>0)
			{
			document.getElementById('divcmd'+divCMDActual).style.display = "";
			document.getElementById('divcmd'+divCMDActual+'formulario').style.display = "none";	
			}
		document.getElementById('divcmd'+id).style.display = "none";
		document.getElementById('divcmd'+id+'formulario').style.display = "";
		divCMDActual = id;
		}

	function fCursoMateriaDesempenoActualizar (id)
		{
		mostrar_url('CursoMateriaDesempenoActualizar.php','MM_update=form1&idCursoMateriaDesempeno='+id+"&observacion="+document.getElementById('observacion'+id).value,'divcmd'+id+'formulario','get');
		setTimeout('fCursoMateriaDesempenoListar()', 2*1000); //segundos * mil, el parametro es en ms es decir 5 * 1000 = 5 segundos
		}

	function fCursoMateriaDesempenoMostrarCursos (idCurso)
		{
		mostrar_url('CursoMateriaDesempenoClase.php','idCurso='+idCurso,'divCursoMateriaDesempenoidMateria','get');		
		}

	var divCMDActual =0;
</script>
</head>

<body>
<? include "menu.php"; ?>
<center>
<h1>Curso / Materia / Desempe&ntilde;o</h1>
</center>
<table width="100%" border="1">
<tr valign="top">
	<td width="0%">
		<table align="center">
		    <tr>
			    <td><center><strong>Curso/Materia/Desempe&ntilde;o</strong></center></td>
		    </tr>
			<tr>
				<td><strong>Curso:</strong></td>
			</tr>
			<tr>
				<td>
					<select name="idCurso" id="idCurso" onchange="fMostrarDesempeno(); fCursoMateriaDesempenoMostrarCursos (this.value); fCursoMateriaDesempenoListar();">
					<option></option>
					<?php
					do {  
						?>
						<option value="<?php echo $row_rsCurso['idCurso']?>"><?php echo $row_rsCurso['ano']?> | <?php echo $row_rsCurso['curso']?></option>
						<?php
						} while ($row_rsCurso = mysql_fetch_assoc($rsCurso));
					?>
					</select>
				</td>
			</tr>
			<tr>
				<td><strong>Materia:</strong></td>
			</tr>
			<tr>
				<td>
					<div id="divCursoMateriaDesempenoidMateria">
		    			<select name="idMateria" id="idMateria">
		    				<option></option>
		    			</select>						
					</div>
				</td>
		    </tr>
		    <tr>
		    	<td><strong>Periodo:</strong></td>
		    </tr>
		    <tr>
		    	<td>
			    	<select name="bimestre" id="bimestre" onchange="javascript:fCursoMateriaDesempenoListar();">
				    	<option></option>
				    	<option value="1">1</option>
				    	<option value="2">2</option>
				    	<option value="3">3</option>
				    	<option value="4">4</option>
			    	</select>
		    	</td>
			</tr>		    
		    <tr>
		    	<td><strong>Desempe&ntilde;o:</strong></td>
		    </tr>
			<tr>    	
		    	<td>
		    		<div id="divDesempeno">
		    			<select name="slDesempeno" id="slDesempeno">
		    				<option></option>
		    			</select>
		    		</div>
		    	</td>
		    </tr>
		    <tr >
			    <td>
			    	<strong>Observaci&oacute;n:</strong><br />
					<textarea id="observacion" name="observacion"></textarea>
				</td>
		    </tr>
			<td>
				<input type="button" name="Crear" value="Crear" onclick="javascript:fCursoMateriaDesempenoCrear();" />
			</td>
		</table>    
    </td>
    <td width="100%">
 		<div id="divCursoMateriaDesempeno"></div>   
    </td>
</tr>
</table>
</body>
</html>
<?php
mysql_free_result($rsCurso);
?>
