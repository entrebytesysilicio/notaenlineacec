<?php require_once('Connections/MySQL.php'); ?><?php
if (!isset($_SESSION)) {
  session_start();
}
$MM_authorizedUsers = "Administrador,Profesor";
$MM_donotCheckaccess = "false";

// *** Restrict Access To Page: Grant or deny access to this page
function isAuthorized($strUsers, $strGroups, $UserName, $UserGroup) { 
  // For security, start by assuming the visitor is NOT authorized. 
  $isValid = False; 

  // When a visitor has logged into this site, the Session variable MM_Username set equal to their username. 
  // Therefore, we know that a user is NOT logged in if that Session variable is blank. 
  if (!empty($UserName)) { 
    // Besides being logged in, you may restrict access to only certain users based on an ID established when they login. 
    // Parse the strings into arrays. 
    $arrUsers = Explode(",", $strUsers); 
    $arrGroups = Explode(",", $strGroups); 
    if (in_array($UserName, $arrUsers)) { 
      $isValid = true; 
    } 
    // Or, you may restrict access to only certain users based on their username. 
    if (in_array($UserGroup, $arrGroups)) { 
      $isValid = true; 
    } 
    if (($strUsers == "") && false) { 
      $isValid = true; 
    } 
  } 
  return $isValid; 
}

$MM_restrictGoTo = "error.php";
if (!((isset($_SESSION['MM_Username'])) && (isAuthorized("",$MM_authorizedUsers, $_SESSION['MM_Username'], $_SESSION['MM_UserGroup'])))) {   
  $MM_qsChar = "?";
  $MM_referrer = $_SERVER['PHP_SELF'];
  if (strpos($MM_restrictGoTo, "?")) $MM_qsChar = "&";
  if (isset($QUERY_STRING) && strlen($QUERY_STRING) > 0) 
  $MM_referrer .= "?" . $QUERY_STRING;
  $MM_restrictGoTo = $MM_restrictGoTo. $MM_qsChar . "accesscheck=" . urlencode($MM_referrer);
  header("Location: ". $MM_restrictGoTo); 
  exit;
}
?>
<?php
if (!function_exists("GetSQLValueString")) {
function GetSQLValueString($theValue, $theType, $theDefinedValue = "", $theNotDefinedValue = "") 
{
  if (PHP_VERSION < 6) {
    $theValue = get_magic_quotes_gpc() ? stripslashes($theValue) : $theValue;
  }

  $theValue = function_exists("mysql_real_escape_string") ? mysql_real_escape_string($theValue) : mysql_escape_string($theValue);

  switch ($theType) {
    case "text":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;    
    case "long":
    case "int":
      $theValue = ($theValue != "") ? intval($theValue) : "NULL";
      break;
    case "double":
      $theValue = ($theValue != "") ? doubleval($theValue) : "NULL";
      break;
    case "date":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;
    case "defined":
      $theValue = ($theValue != "") ? $theDefinedValue : $theNotDefinedValue;
      break;
  }
  return $theValue;
}
}

function GetSQLValueString($theValue, $theType, $theDefinedValue = "", $theNotDefinedValue = "") 
{
  $theValue = (!get_magic_quotes_gpc()) ? addslashes($theValue) : $theValue;

  switch ($theType) {
    case "text":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;    
    case "long":
    case "int":
      $theValue = ($theValue != "") ? intval($theValue) : "NULL";
      break;
    case "double":
      $theValue = ($theValue != "") ? "'" . doubleval($theValue) . "'" : "NULL";
      break;
    case "date":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;
    case "defined":
      $theValue = ($theValue != "") ? $theDefinedValue : $theNotDefinedValue;
      break;
  }
  return $theValue;
}

$editFormAction = $_SERVER['PHP_SELF'];
if (isset($_SERVER['QUERY_STRING'])) {
  $editFormAction .= "?" . htmlentities($_SERVER['QUERY_STRING']);
}

if ((isset($_POST["MM_update"])) && ($_POST["MM_update"] == "form1")) {
  $updateSQL = sprintf("UPDATE curso SET ano=%s, curso=%s, idDirectorCurso = %s WHERE idCurso=%s",
                       GetSQLValueString($_POST['ano'], "int"),
                       GetSQLValueString($_POST['curso'], "text"),
					   GetSQLValueString($_POST['idDirectorCurso'], "int"),
                       GetSQLValueString($_POST['idCurso'], "int"));

  mysql_select_db($database_MySQL, $MySQL);
  $Result1 = mysql_query($updateSQL, $MySQL) or die(mysql_error());

  $updateGoTo = "CursoCrear.php";
  if (isset($_SERVER['QUERY_STRING'])) {
    $updateGoTo .= (strpos($updateGoTo, '?')) ? "&" : "?";
    $updateGoTo .= $_SERVER['QUERY_STRING'];
  }
  header(sprintf("Location: %s", $updateGoTo));
}
?>
<?php
$colname_rsCusro = "-1";
if (isset($_GET['idCurso'])) {
  $colname_rsCusro = (get_magic_quotes_gpc()) ? $_GET['idCurso'] : addslashes($_GET['idCurso']);
}
mysql_select_db($database_MySQL, $MySQL);
$query_rsCusro = sprintf("SELECT * FROM curso WHERE idCurso = %s", $colname_rsCusro);
$rsCusro = mysql_query($query_rsCusro, $MySQL) or die(mysql_error());
$row_rsCusro = mysql_fetch_assoc($rsCusro);
$totalRows_rsCusro = mysql_num_rows($rsCusro);

mysql_select_db($database_MySQL, $MySQL);
$query_rsDirectorCurso = "	SELECT usu.idUsuario, usu.nombre 
							FROM usuario as usu
							inner join perfil as per on usu.idPerfil = per.idPerfil
							and per.perfil = 'Profesor'
							and usu.activo = 1 
							ORDER BY usu.nombre ASC";
$rsDirectorCurso = mysql_query($query_rsDirectorCurso, $MySQL) or die(mysql_error());
$row_rsDirectorCurso = mysql_fetch_assoc($rsDirectorCurso);
$totalRows_rsDirectorCurso = mysql_num_rows($rsDirectorCurso);
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>

<title>:: Actualizar Curso ::</title>
<link href="estilo.css" rel="stylesheet" type="text/css" />
</head>

<body>
<? include "menu.php"; ?>
  <form method="post" name="form1" action="<?php echo $editFormAction; ?>"><br />
    <table align="center">
    <thead><tr><td colspan="2">Actualizar curso</td></tr></thead>
      <tr >
        <td >A�o:</td>
        <td><input type="text" name="ano" value="<?php echo $row_rsCusro['ano']; ?>" size="32"></td>
      </tr>
      <tr >
        <td >Curso:</td>
        <td><input type="text" name="curso" value="<?php echo $row_rsCusro['curso']; ?>" size="32"></td>
      </tr>
      <tr >
        <td >Director curso:</td>
        <td><select name="idDirectorCurso" id="idDirectorCurso"><option></option>
          <?php
do {  
?>
          <option value="<?php echo $row_rsDirectorCurso['idUsuario']?>"<?php if (!(strcmp($row_rsDirectorCurso['idUsuario'], $row_rsCusro['idDirectorCurso']))) {echo "selected=\"selected\"";} ?>><?php echo $row_rsDirectorCurso['nombre']?></option>
          <?php
} while ($row_rsDirectorCurso = mysql_fetch_assoc($rsDirectorCurso));
  $rows = mysql_num_rows($rsDirectorCurso);
  if($rows > 0) {
      mysql_data_seek($rsDirectorCurso, 0);
	  $row_rsDirectorCurso = mysql_fetch_assoc($rsDirectorCurso);
  }
?>
        </select></td>
      </tr>      
      <thead>
      <tr >
        <td colspan="2"><input type="submit" value="Actualizar registro"></td>
      </tr>
      </thead>
    </table>
    <input type="hidden" name="MM_update" value="form1">
    <input type="hidden" name="idCurso" value="<?php echo $row_rsCusro['idCurso']; ?>"><br />
  </form>
  <p>&nbsp;</p>
</div>
</body>
</html>
<?php
mysql_free_result($rsCusro);

mysql_free_result($rsDirectorCurso);
?>
