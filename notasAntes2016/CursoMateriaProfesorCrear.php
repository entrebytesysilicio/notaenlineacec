<?php require_once('Connections/MySQL.php'); ?>
<?php
if (!function_exists("GetSQLValueString")) {
function GetSQLValueString($theValue, $theType, $theDefinedValue = "", $theNotDefinedValue = "") 
{
  if (PHP_VERSION < 6) {
    $theValue = get_magic_quotes_gpc() ? stripslashes($theValue) : $theValue;
  }

  $theValue = function_exists("mysql_real_escape_string") ? mysql_real_escape_string($theValue) : mysql_escape_string($theValue);

  switch ($theType) {
    case "text":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;    
    case "long":
    case "int":
      $theValue = ($theValue != "") ? intval($theValue) : "NULL";
      break;
    case "double":
      $theValue = ($theValue != "") ? doubleval($theValue) : "NULL";
      break;
    case "date":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;
    case "defined":
      $theValue = ($theValue != "") ? $theDefinedValue : $theNotDefinedValue;
      break;
  }
  return $theValue;
}
}

$editFormAction = $_SERVER['PHP_SELF'];
if (isset($_SERVER['QUERY_STRING'])) {
  $editFormAction .= "?" . htmlentities($_SERVER['QUERY_STRING']);
}

if ((isset($_GET["MM_insert"])) && ($_GET["MM_insert"] == "form1")) {
  $insertSQL = sprintf("INSERT INTO cursomateriaprofesor (idCurso, idMateria, idProfesor, intensidadHoraria) VALUES (%s, %s, %s, %s)",
                       GetSQLValueString($_GET['idCurso'], "int"),
                       GetSQLValueString($_GET['idMateria'], "int"),
                       GetSQLValueString($_GET['idProfesor'], "int"),
					   GetSQLValueString($_GET['intensidadHoraria'], "int"));

  mysql_select_db($database_MySQL, $MySQL);
  
  if ($Result1 = mysql_query($insertSQL, $MySQL))
  	{
	echo "Guardado correctamente.";	
	}
	else
	{
	echo mysql_error();
	}
}
?>
