<?php require_once('Connections/MySQL.php'); ?>
<?php
if (!isset($_SESSION)) {
  session_start();
}
$MM_authorizedUsers = "Administrador,Profesor";
$MM_donotCheckaccess = "false";

// *** Restrict Access To Page: Grant or deny access to this page
function isAuthorized($strUsers, $strGroups, $UserName, $UserGroup) { 
  // For security, start by assuming the visitor is NOT authorized. 
  $isValid = False; 

  // When a visitor has logged into this site, the Session variable MM_Username set equal to their username. 
  // Therefore, we know that a user is NOT logged in if that Session variable is blank. 
  if (!empty($UserName)) { 
    // Besides being logged in, you may restrict access to only certain users based on an ID established when they login. 
    // Parse the strings into arrays. 
    $arrUsers = Explode(",", $strUsers); 
    $arrGroups = Explode(",", $strGroups); 
    if (in_array($UserName, $arrUsers)) { 
      $isValid = true; 
    } 
    // Or, you may restrict access to only certain users based on their username. 
    if (in_array($UserGroup, $arrGroups)) { 
      $isValid = true; 
    } 
    if (($strUsers == "") && false) { 
      $isValid = true; 
    } 
  } 
  return $isValid; 
}

$MM_restrictGoTo = "error.php";
if (!((isset($_SESSION['MM_Username'])) && (isAuthorized("",$MM_authorizedUsers, $_SESSION['MM_Username'], $_SESSION['MM_UserGroup'])))) {   
  $MM_qsChar = "?";
  $MM_referrer = $_SERVER['PHP_SELF'];
  if (strpos($MM_restrictGoTo, "?")) $MM_qsChar = "&";
  if (isset($QUERY_STRING) && strlen($QUERY_STRING) > 0) 
  $MM_referrer .= "?" . $QUERY_STRING;
  $MM_restrictGoTo = $MM_restrictGoTo. $MM_qsChar . "accesscheck=" . urlencode($MM_referrer);
  header("Location: ". $MM_restrictGoTo); 
  exit;
}
?>
<?php
if (!function_exists("GetSQLValueString")) {
function GetSQLValueString($theValue, $theType, $theDefinedValue = "", $theNotDefinedValue = "") 
{
  if (PHP_VERSION < 6) {
    $theValue = get_magic_quotes_gpc() ? stripslashes($theValue) : $theValue;
  }

  $theValue = function_exists("mysql_real_escape_string") ? mysql_real_escape_string($theValue) : mysql_escape_string($theValue);

  switch ($theType) {
    case "text":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;    
    case "long":
    case "int":
      $theValue = ($theValue != "") ? intval($theValue) : "NULL";
      break;
    case "double":
      $theValue = ($theValue != "") ? doubleval($theValue) : "NULL";
      break;
    case "date":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;
    case "defined":
      $theValue = ($theValue != "") ? $theDefinedValue : $theNotDefinedValue;
      break;
  }
  return $theValue;
}
}

$editFormAction = $_SERVER['PHP_SELF'];
if (isset($_SERVER['QUERY_STRING'])) {
  $editFormAction .= "?" . htmlentities($_SERVER['QUERY_STRING']);
}

$updateSQL1 = "";
$updateSQL2 = "";
$updateSQL3 = "";
////Identifica si ya existe registro de nota para los parámetros enviados.
$sql = "select * from boletin where idAlumno = ".$_GET['idAlumno']." and idCurso = ".$_GET['idCurso']." and idMateria = ".$_GET['idMateria']." and bimestre = ".$_GET['bimestre'];
mysql_select_db($database_MySQL, $MySQL);
if($resultado = mysql_query($sql, $MySQL))
	{
	if(mysql_num_rows($resultado) > 0)
		{
		////Como ya existe, lo actualiza
    ////Si es nota cuantitativa actualiza de manera individual por nota, de lo contrario, es decir, si es una nota cualitativa, coloca todas las notas con el mismo valor
    if(isset($_GET["calificacionTipo"]) == false || strtoupper($_GET["calificacionTipo"]) == 'CUANTITATIVA')
    {
    $updateSQL1 = " update boletin 
                    set   ".$_GET['nota']."=".$_GET['valor']."  
                    where idAlumno = ".$_GET['idAlumno']." 
                      and idCurso = ".$_GET['idCurso']." 
                      and idMateria = ".$_GET['idMateria']." 
                      and bimestre = ".$_GET['bimestre'];        
    }
    else
    {
    $updateSQL1 = " update boletin 
                    set   nota1 =".$_GET['valor'].",
                          nota2 =".$_GET['valor'].",
                          nota3 =".$_GET['valor'].",
                          nota4 =".$_GET['valor'].",
                          nota5 =".$_GET['valor'].",
                          nota6 =".$_GET['valor'].",
                          nota7 =".$_GET['valor'].",
                          nota8 =".$_GET['valor'].",
                          nota9 =".$_GET['valor'].",
                          nota10 =".$_GET['valor'].",
                          nota11 =".$_GET['valor'].",
                          nota12 =".$_GET['valor'].",
                          nota13 =".$_GET['valor'].",
                          nota14 =".$_GET['valor'].",
                          nota15 =".$_GET['valor'].",
                          recuperacion =".$_GET['valor']."
                    where idAlumno = ".$_GET['idAlumno']." 
                      and idCurso = ".$_GET['idCurso']." 
                      and idMateria = ".$_GET['idMateria']." 
                      and bimestre = ".$_GET['bimestre'];      
    }

		if ($updateSQL1 != "")
			{
				mysql_select_db($database_MySQL, $MySQL);
				if($Result1 = mysql_query($updateSQL1, $MySQL))
					{
					?>
					<center><img src="imagenes/bien.png"  height="32"  /></center>
					<?	
					}
					else
					{
					?><center><img src="imagenes/mal.png" height="32" onmouseover="alert('<? echo str_replace("'","\'",mysql_error()); ?>')" /></center><?
					}						
			}
	
		}
		else
		{
    ////Como las notas no existen, se debe ingresar el registro nuevo
    ////Si es nota cuantitativa crea el registro de manera individual por nota, de lo contrario, es decir, si es una nota cualitativa, crea el registro colocando todas las notas con el misma valor
    if(isset($_GET["calificacionTipo"]) == false || strtoupper($_GET["calificacionTipo"]) == 'CUANTITATIVA')
      {
  		$insertSQL = sprintf("insert
                            into  boletin 
                                  (
                                  idAlumno, 
                                  idCurso, 
                                  idMateria, 
                                  bimestre, 
                                  ".$_GET['nota'].") 
                          VALUES (%s, %s, %s, %s, %s)",
  							   GetSQLValueString($_GET['idAlumno'], "int"),
  							   GetSQLValueString($_GET['idCurso'], "int"),
  							   GetSQLValueString($_GET['idMateria'], "int"),
  							   GetSQLValueString($_GET['bimestre'], "int"),
  							   GetSQLValueString($_GET['valor'], "double"));
      }
      else
      {
      $insertSQL = sprintf("insert 
                            into  boletin 
                                  (
                                  idAlumno,
                                  idCurso,
                                  idMateria,
                                  bimestre,
                                  nota1,
                                  nota2,
                                  nota3,
                                  nota4,
                                  nota5,
                                  nota6,
                                  nota7,
                                  nota8,
                                  nota9,
                                  nota10,
                                  nota11,
                                  nota12,
                                  nota13,
                                  nota14,
                                  nota15,
                                  recuperacion
                                  ) 
                            values (%s, %s, %s, %s, %s,%s, %s, %s, %s, %s,%s, %s, %s, %s, %s,%s, %s, %s, %s, %s)",
                  GetSQLValueString($_GET['idAlumno'], "int"),
                  GetSQLValueString($_GET['idCurso'], "int"),
                  GetSQLValueString($_GET['idMateria'], "int"),
                  GetSQLValueString($_GET['bimestre'], "int"),
                  GetSQLValueString($_GET['valor'], "double"),
                  GetSQLValueString($_GET['valor'], "double"),
                  GetSQLValueString($_GET['valor'], "double"),
                  GetSQLValueString($_GET['valor'], "double"),
                  GetSQLValueString($_GET['valor'], "double"),
                  GetSQLValueString($_GET['valor'], "double"),
                  GetSQLValueString($_GET['valor'], "double"),
                  GetSQLValueString($_GET['valor'], "double"),
                  GetSQLValueString($_GET['valor'], "double"),
                  GetSQLValueString($_GET['valor'], "double"),
                  GetSQLValueString($_GET['valor'], "double"),
                  GetSQLValueString($_GET['valor'], "double"),
                  GetSQLValueString($_GET['valor'], "double"),
                  GetSQLValueString($_GET['valor'], "double"),
                  GetSQLValueString($_GET['valor'], "double"),
                  GetSQLValueString($_GET['valor'], "double")
                 );        
      }
		
		  mysql_select_db($database_MySQL, $MySQL);
		  if($Result1 = mysql_query($insertSQL, $MySQL))
			{
			?>
			<center><img src="imagenes/bien.png"  height="32"  /></center>
			<?	
			}
			else
			{
						?><center><img src="imagenes/mal.png" height="32" onmouseover="alert('<? echo str_replace("'","\'",mysql_error()); ?>')" /></center><?
			}	
		}
	}
	else
	{
	echo mysql_error();
	}
?>