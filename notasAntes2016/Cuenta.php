<?php require_once('Connections/MySQL.php'); ?>
<?php
if (!isset($_SESSION)) {
  session_start();
}
$MM_authorizedUsers = "Administrador,Profesor,Padre";
$MM_donotCheckaccess = "false";

// *** Restrict Access To Page: Grant or deny access to this page
function isAuthorized($strUsers, $strGroups, $UserName, $UserGroup) { 
  // For security, start by assuming the visitor is NOT authorized. 
  $isValid = False; 

  // When a visitor has logged into this site, the Session variable MM_Username set equal to their username. 
  // Therefore, we know that a user is NOT logged in if that Session variable is blank. 
  if (!empty($UserName)) { 
    // Besides being logged in, you may restrict access to only certain users based on an ID established when they login. 
    // Parse the strings into arrays. 
    $arrUsers = Explode(",", $strUsers); 
    $arrGroups = Explode(",", $strGroups); 
    if (in_array($UserName, $arrUsers)) { 
      $isValid = true; 
    } 
    // Or, you may restrict access to only certain users based on their username. 
    if (in_array($UserGroup, $arrGroups)) { 
      $isValid = true; 
    } 
    if (($strUsers == "") && false) { 
      $isValid = true; 
    } 
  } 
  return $isValid; 
}

$MM_restrictGoTo = "error.php";
if (!((isset($_SESSION['MM_Username'])) && (isAuthorized("",$MM_authorizedUsers, $_SESSION['MM_Username'], $_SESSION['MM_UserGroup'])))) {   
  $MM_qsChar = "?";
  $MM_referrer = $_SERVER['PHP_SELF'];
  if (strpos($MM_restrictGoTo, "?")) $MM_qsChar = "&";
  if (isset($QUERY_STRING) && strlen($QUERY_STRING) > 0) 
  $MM_referrer .= "?" . $QUERY_STRING;
  $MM_restrictGoTo = $MM_restrictGoTo. $MM_qsChar . "accesscheck=" . urlencode($MM_referrer);
  header("Location: ". $MM_restrictGoTo); 
  exit;
}
?>
<?php
if (!function_exists("GetSQLValueString")) {
function GetSQLValueString($theValue, $theType, $theDefinedValue = "", $theNotDefinedValue = "") 
{
  if (PHP_VERSION < 6) {
    $theValue = get_magic_quotes_gpc() ? stripslashes($theValue) : $theValue;
  }

  $theValue = function_exists("mysql_real_escape_string") ? mysql_real_escape_string($theValue) : mysql_escape_string($theValue);

  switch ($theType) {
    case "text":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;    
    case "long":
    case "int":
      $theValue = ($theValue != "") ? intval($theValue) : "NULL";
      break;
    case "double":
      $theValue = ($theValue != "") ? doubleval($theValue) : "NULL";
      break;
    case "date":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;
    case "defined":
      $theValue = ($theValue != "") ? $theDefinedValue : $theNotDefinedValue;
      break;
  }
  return $theValue;
}
}

$editFormAction = $_SERVER['PHP_SELF'];
if (isset($_SERVER['QUERY_STRING'])) {
  $editFormAction .= "?" . htmlentities($_SERVER['QUERY_STRING']);
}

if ((isset($_POST["MM_update"])) && ($_POST["MM_update"] == "form1")) {
	if (GetSQLValueString($_POST['clave'], "text") != "NULL")
		{
	  $updateSQL = sprintf("UPDATE usuario SET clave=%s, nombre=%s WHERE idUsuario=%s",
						   GetSQLValueString(md5($_POST['clave']), "text"),
						   GetSQLValueString($_POST['nombre'], "text"),
						   GetSQLValueString($_POST['idUsuario'], "int"));			
		}
		else
		{
	  $updateSQL = sprintf("UPDATE usuario SET nombre=%s WHERE idUsuario=%s",
						   GetSQLValueString($_POST['nombre'], "text"),
						   GetSQLValueString($_POST['idUsuario'], "int"));		
		}



  

  mysql_select_db($database_MySQL, $MySQL);
  $Result1 = mysql_query($updateSQL, $MySQL) or die(mysql_error());

  $updateGoTo = "Cuenta.php";
  if (isset($_SERVER['QUERY_STRING'])) {
    $updateGoTo .= (strpos($updateGoTo, '?')) ? "&" : "?";
    $updateGoTo .= $_SERVER['QUERY_STRING'];
  }
  header(sprintf("Location: %s", $updateGoTo));
}

$colname_rsUsuario = "-1";
if (isset($_SESSION['idUsuario'])) {
  $colname_rsUsuario = $_SESSION['idUsuario'];
}
mysql_select_db($database_MySQL, $MySQL);
$query_rsUsuario = sprintf("SELECT * FROM usuario WHERE idUsuario = %s", GetSQLValueString($colname_rsUsuario, "int"));
$rsUsuario = mysql_query($query_rsUsuario, $MySQL) or die(mysql_error());
$row_rsUsuario = mysql_fetch_assoc($rsUsuario);
$totalRows_rsUsuario = mysql_num_rows($rsUsuario);
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title>:: Informacion de la cuenta ::</title>
<? include "header.php"; ?>
</head>

<body>
<? include "menu.php"; ?>
<form action="<?php echo $editFormAction; ?>" method="post" name="form1" id="form1"><br />

  <table align="center">
  <thead>
    <tr >
      <td colspan="2">Mi informaci&oacute;n</td>
    </tr>
    </thead>  
    <tr valign="baseline">
      <td nowrap="nowrap" align="right">Nombre:</td>
      <td><input type="text" name="nombre" value="<?php echo htmlentities($row_rsUsuario['nombre'], ENT_COMPAT, 'utf-8'); ?>" size="32" /></td>
    </tr>    
    <tr valign="baseline">
      <td nowrap="nowrap" align="right">Clave:</td>
      <td> <input type="text" name="clave" value="" size="32" /></td>
    </tr>

    <thead>
    <tr >
      <td colspan="2"><input type="submit" value="Actualizar" /></td>
    </tr>
    </thead>
  </table>
  <input type="hidden" name="MM_update" value="form1" />
  <input type="hidden" name="idUsuario" value="<?php echo $row_rsUsuario['idUsuario']; ?>" /><br />

</form>
</body>

</html>
<?php
mysql_free_result($rsUsuario);
?>
