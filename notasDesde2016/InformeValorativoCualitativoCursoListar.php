<?php require_once('Connections/MySQL.php'); ?>
<?php require_once('InformeValorativoClase.php'); ?>
<?php
  //header('Content-type: application/vnd.ms-excel');

  //header('Content-type: application/msword');
//ini_set('display_errors', 0);

if (!isset($_SESSION)) {
	session_start();
}
$MM_authorizedUsers = "Administrador,Profesor,Padre";
$MM_donotCheckaccess = "false";

  // *** Restrict Access To Page: Grant or deny access to this page
function isAuthorized($strUsers, $strGroups, $UserName, $UserGroup) { 
	// For security, start by assuming the visitor is NOT authorized. 
	$isValid = False; 

	// When a visitor has logged into this site, the Session variable MM_Username set equal to their username. 
	// Therefore, we know that a user is NOT logged in if that Session variable is blank. 
	if (!empty($UserName)) { 
	  // Besides being logged in, you may restrict access to only certain users based on an ID established when they login. 
	  // Parse the strings into arrays. 
		$arrUsers = Explode(",", $strUsers); 
		$arrGroups = Explode(",", $strGroups); 
		if (in_array($UserName, $arrUsers)) { 
			$isValid = true; 
		} 
	  // Or, you may restrict access to only certain users based on their username. 
		if (in_array($UserGroup, $arrGroups)) { 
			$isValid = true; 
		} 
		if (($strUsers == "") && false) { 
			$isValid = true; 
		} 
	} 
	return $isValid; 
}

$MM_restrictGoTo = "error.php";
if (!((isset($_SESSION['MM_Username'])) && (isAuthorized("",$MM_authorizedUsers, $_SESSION['MM_Username'], $_SESSION['MM_UserGroup'])))) {   
	$MM_qsChar = "?";
	$MM_referrer = $_SERVER['PHP_SELF'];
	if (strpos($MM_restrictGoTo, "?")) $MM_qsChar = "&";
	if (isset($QUERY_STRING) && strlen($QUERY_STRING) > 0) 
		$MM_referrer .= "?" . $QUERY_STRING;
	$MM_restrictGoTo = $MM_restrictGoTo. $MM_qsChar . "accesscheck=" . urlencode($MM_referrer);
	header("Location: ". $MM_restrictGoTo); 
	exit;
}
$iv = new EstudianteInformeValorativo();

$iv->idCurso = $_GET['idCurso'];
$bimestreActual = $_GET['bimestre'];
if($_GET['pdf'])
	{
	?>
	<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
	<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<link href="estilo_boletin.css" rel="stylesheet" type="text/css" />
		<style type="text/css">

.tamanoCarta {
height: auto;
    width: 816px;
    margin: 2px;
}

		<!--
		.t1 {
			text-align: center;
			font-size: 9px;
		}
		-->
		</style>
	</head>
	<body onload="window.print();">
		<?			
	}


$sql_alumnosCurso = "select ac.idAlumno, c.ano
					from alumnocurso as ac
					inner join alumno as a 
						on a.idAlumno = ac.idAlumno
					inner join curso as c
						on c.idCurso = ac.idCurso
					where ac.idCurso = ".$_GET['idCurso']."
					order by a.apellidos asc";

$alumnosCurso_resultado = mysql_query($sql_alumnosCurso,$MySQL) or die(mysql_error());
if(mysql_num_rows($alumnosCurso_resultado) > 0)
{
while($fila_alumnosCurso = mysql_fetch_assoc($alumnosCurso_resultado))
	{
		$iv->idAlumno = $fila_alumnosCurso['idAlumno'];
	?>
	<center>
		<div class="tamanoCarta">
			<table width="100%" border="0">
				<tr>
					<td align="left"><? //$im = @imagecreatefromjpeg("imagenes/logo.tiff"); imagejpeg($im); ?>
						<img src="imagenes/logo.png" height="96px" /></td>
						<td width="100%" align="center"><H3>HERMANAS MERCEDARIAS DEL SANTISIMO SACRAMENTO<br />
							COLEGIO EUCAR&Iacute;STICO CAMPESTRE </H3>
							<h4>INFORME VALORATIVO<br />
								<? echo $fila_alumnosCurso['ano']; ?></H4></td>
								<td align="right">&nbsp;</td>
							</tr>
						</table>
<?
$sql_boletin = "select	al.nombres, al.apellidos, c.curso, a.area, m.materia, d.desempeno, cmp.intensidadHoraria, cmd.observacion, sum(b.fallas) as fallas
				from 	boletin as b
				inner join materia as m
					on b.idMateria = m.idMateria
				inner join area = a
					on a.idArea = m.idArea
				inner join cursomateriadesempeno as cmd
					on cmd.idCurso = b.idCurso
				    and cmd.idMateria = b.idMateria
				    and cmd.bimestre = b.bimestre
				inner join curso as c 
					on c.idCurso = b.idCurso
				inner join desempeno as d
					on d.ano = c.ano
				    and b.nota1 between d.notaMinima and d.notaMaxima
				    and cmd.idDesempeno = d.idDesempeno
				inner join cursomateriaprofesor as cmp
					on cmp.idCurso = b.idCurso
				    and cmp.idMateria = b.idMateria
				inner join alumno as al
					on al.idAlumno = b.idAlumno				    
				where 	b.idCurso = ".$_GET['idCurso']."
					and b.idAlumno = ".$fila_alumnosCurso['idAlumno']."
					and b.bimestre = ".$_GET['bimestre']."
				group by al.nombres, al.apellidos, a.area, m.materia, d.desempeno, cmp.intensidadHoraria, cmd.observacion
				order by a.orden asc, m.materia asc
				";
$resultado_boletin = mysql_query($sql_boletin,$MySQL) or die(mysql_error());
$fila_boletin = mysql_fetch_assoc($resultado_boletin)
?>
<table border="0" cellpadding="0" cellspacing="0" width="100%" >
	<tr>
		<td>Estudiante:</td>
		<td><? echo strtoupper($fila_boletin['nombres']); ?> <? echo strtoupper($fila_boletin['apellidos']); ?></td>
		<td>Grado:</td>
		<td><? echo strtoupper($fila_boletin['curso']); ?></td>		
	</tr>
	<tr>
		<td>Docente:</td>
		<td><? $iv->nombreDirectorCurso (); ?></td>
		<td>Periodo:</td>
		<td><? echo $_GET['bimestre']; ?></td>		
	</tr>	
</table>
<br>

<table border="1" cellpadding="0" cellspacing="0" >
	<tr>
		<td><strong>Dimensiones</strong></td>
		<td><strong>I/H</strong></td>
		<td><strong>Ausencias</strong></td>
		<td><strong>Definitiva</strong></td>
	</tr>

<?

$areaAnterior = "";
do	{
	if($areaAnterior != $fila_boletin['area'])
		{
		$areaAnterior = $fila_boletin['area'];	
		?>
		<tr>
			<td colspan="4"><strong><? echo strtoupper($fila_boletin['area']); ?></strong></td>
		</tr>
		<?		
		}
	?>
	<tr>
		<td><strong ><font color="gray"><? echo strtoupper($fila_boletin['materia']); ?></font></strong><br><? echo $fila_boletin['observacion']; ?></td>
		<td align="center"><? echo $fila_boletin['intensidadHoraria']; ?></td>
		<td align="center"><? echo $fila_boletin['fallas']; ?></td>
		<td align="center"><? echo $fila_boletin['desempeno']; ?></td>
	</tr>
	<?
	} while($fila_boletin = mysql_fetch_assoc($resultado_boletin))

?>
</table>
     <table width="100%" border="0">
      	<tr>
      		<td colspan="2"><strong>OBSERVACIONES Y/O RECOMENDACIONES</strong><br />
      			<br />
      			<hr />
      			<br />
      			<hr />
      		</td>
      	</tr>
      	<tr valign="top" align="center">
      		<td>
      			<br />
      			<br />
      			_________________________________<br />
      			<strong>RECTOR&Iacute;A</strong><br />
      			MARIA CRISTINA TRILLOS NAUSA
      		</td>
      		<td>
      			<br />
      			<br />
      			_________________________________<br />
      			<strong>DIRECTOR(A) DE GRUPO</strong><br />
      			<? $iv->nombreDirectorCurso (); ?>
      		</td>      		
		</tr>
	</table>
</div>
</center>
<br>

<div class="page-break"></div>
	<?		
	}
}
else
{
	?>
	No se encontraron alumnos para el curso seleccionado.
	<?
}
?>

<?
if($_GET['pdf'])
	{
	?>
	</body>
	</html>
	<?	
	}
?>						
