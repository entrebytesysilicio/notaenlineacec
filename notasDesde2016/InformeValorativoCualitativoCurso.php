<?php require_once('Connections/MySQL.php'); ?>
<?php
if (!isset($_SESSION)) {
  session_start();
}
$MM_authorizedUsers = "Administrador,Padre";
$MM_donotCheckaccess = "false";

// *** Restrict Access To Page: Grant or deny access to this page
function isAuthorized($strUsers, $strGroups, $UserName, $UserGroup) { 
  // For security, start by assuming the visitor is NOT authorized. 
  $isValid = False; 

  // When a visitor has logged into this site, the Session variable MM_Username set equal to their username. 
  // Therefore, we know that a user is NOT logged in if that Session variable is blank. 
  if (!empty($UserName)) { 
    // Besides being logged in, you may restrict access to only certain users based on an ID established when they login. 
    // Parse the strings into arrays. 
    $arrUsers = Explode(",", $strUsers); 
    $arrGroups = Explode(",", $strGroups); 
    if (in_array($UserName, $arrUsers)) { 
      $isValid = true; 
    } 
    // Or, you may restrict access to only certain users based on their username. 
    if (in_array($UserGroup, $arrGroups)) { 
      $isValid = true; 
    } 
    if (($strUsers == "") && false) { 
      $isValid = true; 
    } 
  } 
  return $isValid; 
}

$MM_restrictGoTo = "error.php";
if (!((isset($_SESSION['MM_Username'])) && (isAuthorized("",$MM_authorizedUsers, $_SESSION['MM_Username'], $_SESSION['MM_UserGroup'])))) {   
  $MM_qsChar = "?";
  $MM_referrer = $_SERVER['PHP_SELF'];
  if (strpos($MM_restrictGoTo, "?")) $MM_qsChar = "&";
  if (isset($_SERVER['QUERY_STRING']) && strlen($_SERVER['QUERY_STRING']) > 0) 
  $MM_referrer .= "?" . $_SERVER['QUERY_STRING'];
  $MM_restrictGoTo = $MM_restrictGoTo. $MM_qsChar . "accesscheck=" . urlencode($MM_referrer);
  header("Location: ". $MM_restrictGoTo); 
  exit;
}


		mysql_select_db($database_MySQL, $MySQL);
		
				if($_SESSION['MM_UserGroup'] == 'Padre')
					{
		$query_rsAlumno = "	select 	distinct 
									idCurso, 
									curso, 
									ano 
							from 	curso 
							where 	ano = ".date("Y")."
								and idCurso in (
												select distinct ac.idCurso from alumnocurso as ac where ac.idAlumno in 
													(
													select aa.idAlumno from alumnoacudiente as aa
													inner join usuario as u on u.idUsuario = aa.idAcudiente
													where u.usuario = '".$_SESSION['MM_Username']."'
													)
												)
							order by orden asc
							";						
					}
					else
					{
						$query_rsAlumno = "	select distinct idCurso, curso, ano from curso where ano = ".date("Y")." order by orden asc";
					}

		$rsAlumno = mysql_query($query_rsAlumno, $MySQL) or die(mysql_error());
		$row_rsAlumno = mysql_fetch_assoc($rsAlumno);
		$totalRows_rsAlumno = mysql_num_rows($rsAlumno);
		
		?>
        
        <table>
        
        
        <tr>
        <td>Seleccione el bimestre:</td>
        <td><select name="bimestre" id="bimestre"><option value=""></option><option value="1">1</option><option value="2">2</option><option value="3">3</option><option value="4">4</option></select></td>
        <td>Seleccione el curso:</td>
        <td>
        <select name="idCurso" id="idCurso" onchange=""><option value="TODOS">TODOS</option><option></option>
        <?php
        do {  
            ?>
            <option value="<?php echo $row_rsAlumno['idCurso']?>"><?php echo $row_rsAlumno['ano']?> | <?php echo $row_rsAlumno['curso']?></option>
            <?php
            } while ($row_rsAlumno = mysql_fetch_assoc($rsAlumno));
        mysql_free_result($rsAlumno);
        ?>
        </select>		        
        
        </td>
        <td><input type="button" name="Imprimir" value="Imprimir" onclick="javascript:fReporteImpresionBoletinCualitativoListar();" /></td>
        </tr>
        </table>
        
        
        

	
