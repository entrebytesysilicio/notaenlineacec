<?php require_once('Connections/MySQL.php'); ?>
<?php
if (!isset($_SESSION)) {
  session_start();
}
$MM_authorizedUsers = "Administrador,Profesor";
$MM_donotCheckaccess = "false";

// *** Restrict Access To Page: Grant or deny access to this page
function isAuthorized($strUsers, $strGroups, $UserName, $UserGroup) { 
  // For security, start by assuming the visitor is NOT authorized. 
  $isValid = False; 

  // When a visitor has logged into this site, the Session variable MM_Username set equal to their username. 
  // Therefore, we know that a user is NOT logged in if that Session variable is blank. 
  if (!empty($UserName)) { 
    // Besides being logged in, you may restrict access to only certain users based on an ID established when they login. 
    // Parse the strings into arrays. 
    $arrUsers = Explode(",", $strUsers); 
    $arrGroups = Explode(",", $strGroups); 
    if (in_array($UserName, $arrUsers)) { 
      $isValid = true; 
    } 
    // Or, you may restrict access to only certain users based on their username. 
    if (in_array($UserGroup, $arrGroups)) { 
      $isValid = true; 
    } 
    if (($strUsers == "") && false) { 
      $isValid = true; 
    } 
  } 
  return $isValid; 
}

$MM_restrictGoTo = "error.php";
if (!((isset($_SESSION['MM_Username'])) && (isAuthorized("",$MM_authorizedUsers, $_SESSION['MM_Username'], $_SESSION['MM_UserGroup'])))) {   
  $MM_qsChar = "?";
  $MM_referrer = $_SERVER['PHP_SELF'];
  if (strpos($MM_restrictGoTo, "?")) $MM_qsChar = "&";
  if (isset($QUERY_STRING) && strlen($QUERY_STRING) > 0) 
  $MM_referrer .= "?" . $QUERY_STRING;
  $MM_restrictGoTo = $MM_restrictGoTo. $MM_qsChar . "accesscheck=" . urlencode($MM_referrer);
  header("Location: ". $MM_restrictGoTo); 
  exit;
}
?>
<?php
if (!function_exists("GetSQLValueString")) {
function GetSQLValueString($theValue, $theType, $theDefinedValue = "", $theNotDefinedValue = "") 
{
  if (PHP_VERSION < 6) {
    $theValue = get_magic_quotes_gpc() ? stripslashes($theValue) : $theValue;
  }

  $theValue = function_exists("mysql_real_escape_string") ? mysql_real_escape_string($theValue) : mysql_escape_string($theValue);

  switch ($theType) {
    case "text":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;    
    case "long":
    case "int":
      $theValue = ($theValue != "") ? intval($theValue) : "NULL";
      break;
    case "double":
      $theValue = ($theValue != "") ? doubleval($theValue) : "NULL";
      break;
    case "date":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;
    case "defined":
      $theValue = ($theValue != "") ? $theDefinedValue : $theNotDefinedValue;
      break;
  }
  return $theValue;
}
}

mysql_select_db($database_MySQL, $MySQL);
$query_rsCursoMateriaCompetencia = "SELECT 	cmc.idCursoMateriaCompetencia, 
											c.ano, 
											c.curso,
											m.materia,
											cmc.bimestre,
											cmc.competencia
									FROM cursomateriacompetencia as cmc
									inner join curso as c on cmc.idCurso = c.idCurso
									inner join materia as m on m.idMateria = cmc.idMateria
									where 1 = 1 ";
if($_GET['idCurso'])
	{
		$query_rsCursoMateriaCompetencia .= " and cmc.idCurso = ".$_GET['idCurso'];
	}

if($_GET['idMateria'])
	{
		$query_rsCursoMateriaCompetencia .= " and cmc.idMateria = ".$_GET['idMateria'];
	}
if($_GET['idProfesor'])
	{
		$query_rsCursoMateriaCompetencia .= " and cmc.bimestre = ".$_GET['bimestre'];
	}	
	
$query_rsCursoMateriaCompetencia .= " ORDER BY c.ano desc, c.curso asc, m.materia asc, cmc.bimestre asc,cmc.competencia  ASC";
$rsCursoMateriaCompetencia = mysql_query($query_rsCursoMateriaCompetencia, $MySQL) or die(mysql_error());
$row_rsCursoMateriaCompetencia = mysql_fetch_assoc($rsCursoMateriaCompetencia);
$totalRows_rsCursoMateriaCompetencia = mysql_num_rows($rsCursoMateriaCompetencia);
if ($totalRows_rsCursoMateriaCompetencia > 0)
	{
	?>
	<table width="100%">
	<thead>
	  <tr>
		<td>No.</td>
		<td>Año</td>
		<td>Curso</td>
		<td>Materia</td>
		<td>Periodo</td>
		<td>Competencia</td>
		<td>Accion</td>    
	  </tr>
	  </thead>
	  <?php $i = 1; do { ?>
	  <tr>
		<td align="center"><?php echo $i++; ?></td>
		<td align="center"><?php echo $row_rsCursoMateriaCompetencia['ano']; ?></td>
		<td><?php echo $row_rsCursoMateriaCompetencia['curso']; ?></td>
		<td><?php echo $row_rsCursoMateriaCompetencia['materia']; ?></td>
		<td align="center"><?php echo $row_rsCursoMateriaCompetencia['bimestre']; ?></td>
		<td><?php echo $row_rsCursoMateriaCompetencia['competencia']; ?></td>
		<td align="center"><input type="button" name="Eliminar" value="Eliminar" onclick="javascript:fCursoMateriaCompetenciaEliminar(<?php echo $row_rsCursoMateriaCompetencia['idCursoMateriaCompetencia']; ?>);" /></td>    
	  </tr>
	  <?php } while ($row_rsCursoMateriaCompetencia = mysql_fetch_assoc($rsCursoMateriaCompetencia)); ?>
	</table>
	<?php		
	}
	else
	{
	?>
	No se encontraron datos.
	<?	
	}

mysql_free_result($rsCursoMateriaCompetencia);
?>
