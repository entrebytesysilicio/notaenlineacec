<?php require_once('Connections/MySQL.php'); ?>
<?php
if (!isset($_SESSION)) {
  session_start();
}
$MM_authorizedUsers = "Administrador,Profesor";
$MM_donotCheckaccess = "false";

// *** Restrict Access To Page: Grant or deny access to this page
function isAuthorized($strUsers, $strGroups, $UserName, $UserGroup) { 
  // For security, start by assuming the visitor is NOT authorized. 
  $isValid = False; 

  // When a visitor has logged into this site, the Session variable MM_Username set equal to their username. 
  // Therefore, we know that a user is NOT logged in if that Session variable is blank. 
  if (!empty($UserName)) { 
    // Besides being logged in, you may restrict access to only certain users based on an ID established when they login. 
    // Parse the strings into arrays. 
    $arrUsers = Explode(",", $strUsers); 
    $arrGroups = Explode(",", $strGroups); 
    if (in_array($UserName, $arrUsers)) { 
      $isValid = true; 
    } 
    // Or, you may restrict access to only certain users based on their username. 
    if (in_array($UserGroup, $arrGroups)) { 
      $isValid = true; 
    } 
    if (($strUsers == "") && false) { 
      $isValid = true; 
    } 
  } 
  return $isValid; 
}

$MM_restrictGoTo = "error.php";
if (!((isset($_SESSION['MM_Username'])) && (isAuthorized("",$MM_authorizedUsers, $_SESSION['MM_Username'], $_SESSION['MM_UserGroup'])))) {   
  $MM_qsChar = "?";
  $MM_referrer = $_SERVER['PHP_SELF'];
  if (strpos($MM_restrictGoTo, "?")) $MM_qsChar = "&";
  if (isset($QUERY_STRING) && strlen($QUERY_STRING) > 0) 
  $MM_referrer .= "?" . $QUERY_STRING;
  $MM_restrictGoTo = $MM_restrictGoTo. $MM_qsChar . "accesscheck=" . urlencode($MM_referrer);
  header("Location: ". $MM_restrictGoTo); 
  exit;
}
?>
<?php
if (!function_exists("GetSQLValueString")) {
function GetSQLValueString($theValue, $theType, $theDefinedValue = "", $theNotDefinedValue = "") 
{
  if (PHP_VERSION < 6) {
    $theValue = get_magic_quotes_gpc() ? stripslashes($theValue) : $theValue;
  }

  $theValue = function_exists("mysql_real_escape_string") ? mysql_real_escape_string($theValue) : mysql_escape_string($theValue);

  switch ($theType) {
    case "text":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;    
    case "long":
    case "int":
      $theValue = ($theValue != "") ? intval($theValue) : "NULL";
      break;
    case "double":
      $theValue = ($theValue != "") ? doubleval($theValue) : "NULL";
      break;
    case "date":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;
    case "defined":
      $theValue = ($theValue != "") ? $theDefinedValue : $theNotDefinedValue;
      break;
  }
  return $theValue;
}
}

mysql_select_db($database_MySQL, $MySQL);
$query_rsCurso = "SELECT * FROM curso where curso.ano = ".date("Y")." ORDER BY ano DESC, orden asc";
$rsCurso = mysql_query($query_rsCurso, $MySQL) or die(mysql_error());
$row_rsCurso = mysql_fetch_assoc($rsCurso);
$totalRows_rsCurso = mysql_num_rows($rsCurso);

mysql_select_db($database_MySQL, $MySQL);
$query_rsMateria = "SELECT * FROM materia ORDER BY materia ASC";
$rsMateria = mysql_query($query_rsMateria, $MySQL) or die(mysql_error());
$row_rsMateria = mysql_fetch_assoc($rsMateria);
$totalRows_rsMateria = mysql_num_rows($rsMateria);

mysql_select_db($database_MySQL, $MySQL);
$query_rsProfesor = "SELECT usuario.nombre, usuario.idUsuario FROM usuario inner join perfil on perfil.idPerfil = usuario.idPerfil WHERE perfil.perfil = 'Profesor' ORDER BY nombre ASC";
$rsProfesor = mysql_query($query_rsProfesor, $MySQL) or die(mysql_error());
$row_rsProfesor = mysql_fetch_assoc($rsProfesor);
$totalRows_rsProfesor = mysql_num_rows($rsProfesor);
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>

<title>:: Curso / Materia / Profesor ::</title>
<script language="javascript" type="text/javascript">
function fCursoMateriaProfesorListar ()
	{
	var idCurso,idMateria,idProfesor;
	idCurso =document.getElementById('idCurso');
	idCurso = idCurso.options[idCurso.selectedIndex].value;	
	idMateria =document.getElementById('idMateria');
	idMateria = idMateria.options[idMateria.selectedIndex].value;	
	idProfesor =document.getElementById('idProfesor');
	idProfesor = idProfesor.options[idProfesor.selectedIndex].value;	
	mostrar_url('CursoMateriaProfesorListar.php','idCurso='+idCurso+'&idMateria='+idMateria+'&idProfesor='+idProfesor,'divCursoMateriaProfesor','get');
	}

function fCursoMateriaProfesorCrear ()
	{
	var idCurso,idMateria,idProfesor,intensidadHoraria;
	idCurso =document.getElementById('idCurso');
	idCurso = idCurso.options[idCurso.selectedIndex].value;	
	idMateria =document.getElementById('idMateria');
	idMateria = idMateria.options[idMateria.selectedIndex].value;	
	idProfesor =document.getElementById('idProfesor');
	idProfesor = idProfesor.options[idProfesor.selectedIndex].value;
	intensidadHoraria =document.getElementById('intensidadHoraria').value;
	mostrar_url('CursoMateriaProfesorCrear.php','MM_insert=form1&idCurso='+idCurso+'&idMateria='+idMateria+'&idProfesor='+idProfesor+'&intensidadHoraria='+intensidadHoraria,'divCursoMateriaProfesor','get');
	demorar('fCursoMateriaProfesorListar()',2);
	}
	
	function fCursoMateriaProfesorEliminar (id)
		{ 
		mostrar_url('CursoMateriaProfesorEliminar.php','idCursoMateriaProfesor='+id,'divCursoMateriaProfesor','get');
		demorar('fCursoMateriaProfesorListar()',2);
		}

  function fCursoMateriaProfesorActualizarIntensidad (id,intensidadHoraria)
    { 
    mostrar_url('CursoMateriaProfesorActualizarIntensidad.php','idCursoMateriaProfesor='+id+'&intensidadHoraria='+intensidadHoraria,'intensidadHoraria_'+id,'get');
    //demorar('fCursoMateriaProfesorListar()',2);
    }

</script>
<? include "header.php"; ?>
</head>

<body>
<? include "menu.php"; ?>
<table width="100%" border="1">
<tr valign="top">
<td>

<table>
<thead>
<tr><td colspan="2">Curso / Materia / Profesor</td></tr></thead>
<tr><td>Curso:</td><td><select name="idCurso" id="idCurso" onchange="javascript:fCursoMateriaProfesorListar();"><option></option>
  <?php
do {  
?>
  <option value="<?php echo $row_rsCurso['idCurso']?>"><?php echo $row_rsCurso['ano']?> - <?php echo $row_rsCurso['curso']?></option>
  <?php
} while ($row_rsCurso = mysql_fetch_assoc($rsCurso));
  $rows = mysql_num_rows($rsCurso);
  if($rows > 0) {
      mysql_data_seek($rsCurso, 0);
	  $row_rsCurso = mysql_fetch_assoc($rsCurso);
  }
?>
</select></td></tr>
<tr><td>Materia:</td><td><select name="idMateria" id="idMateria" onchange="javascript:fCursoMateriaProfesorListar();"><option></option>
  <?php
do {  
?>
  <option value="<?php echo $row_rsMateria['idMateria']?>"><?php echo $row_rsMateria['materia']?></option>
  <?php
} while ($row_rsMateria = mysql_fetch_assoc($rsMateria));
  $rows = mysql_num_rows($rsMateria);
  if($rows > 0) {
      mysql_data_seek($rsMateria, 0);
	  $row_rsMateria = mysql_fetch_assoc($rsMateria);
  }
?>
</select></td></tr>
<tr><td>Profesor:</td><td><select name="idProfesor" id="idProfesor" onchange="javascript:fCursoMateriaProfesorListar();"><option></option>
  <?php
do {  
?>
  <option value="<?php echo $row_rsProfesor['idUsuario']?>"><?php echo $row_rsProfesor['nombre']?></option>
  <?php
} while ($row_rsProfesor = mysql_fetch_assoc($rsProfesor));
  $rows = mysql_num_rows($rsProfesor);
  if($rows > 0) {
      mysql_data_seek($rsProfesor, 0);
	  $row_rsProfesor = mysql_fetch_assoc($rsProfesor);
  }
?>
</select></td></tr>
<tr><td>Intensidad horaria:</td><td><input type="text" name="intensidadHoraria" id="intensidadHoraria" /></td></tr>
<thead><tr><td colspan="2"><input type="submit" name="Crear" value="Crear" onclick="javascript:fCursoMateriaProfesorCrear();" /></td></tr></thead>
</table>


</td>
<td width="100%">
<div id="divCursoMateriaProfesor"></div>
</td>
</tr>

</table>


</body>
</html>
<?php
mysql_free_result($rsCurso);

mysql_free_result($rsMateria);

mysql_free_result($rsProfesor);
?>
