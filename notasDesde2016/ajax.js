// JavaScript Document
var demora_ocultar_mensaje = 5; 

function crearObjetoAjax() {
	var xmlhttp = false;
	try 
	{
		xmlhttp = new ActiveXObject("Msxml2.XMLHTTP");
	}
	catch (e) 
	{
		try 
		{
			xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
		}
		catch (E) 
		{
			xmlhttp = false;
		}
	}

	if (!xmlhttp && typeof XMLHttpRequest != 'undefined') 
	{
		xmlhttp = new XMLHttpRequest();
	}
	return xmlhttp;
}

function mostrar_url(url, parametros, div, metodo) {
	var contenedor, ajax;
	contenedor = document.getElementById(div);
	ajax = crearObjetoAjax();

	ajax.onreadystatechange = function() {
		if (ajax.readyState == 4) 
		{
			contenedor.innerHTML = ajax.responseText;
		}
		else
		{
			contenedor.innerHTML = "<center><img src=\"imagenes/loader.gif\"  height=\"32\" /></center>";
		}
	}

	if (metodo == 'get') 
	{	
		ajax.open("GET", url + '?' + parametros, true);		
		ajax.setRequestHeader("Content-Type", "text/html;charset=UTF-8");
		ajax.send(null);
	}
	else 
	{
		ajax.open("POST", url, true);		
		ajax.setRequestHeader("Content-type", "application/x-www-form-urlencoded;charset=UTF-8");
		ajax.send(parametros);
	}
}


function mensaje_ubicacion() {
	var x;
	x = (window.screen.width / 2) - ((window.screen.width * 70 / 100) / 2)
	//alert(window.screen.width+' - '+x);
	return x;
}

function mensaje_mostrar(mensaje) {
	var d = document.createElement('div_mensaje');
	d.id = 'div_mensaje';
	d.innerHTML = mensaje + ' ' + '<br><a href=\"#\" onclick=\"javascript:mensaje_ocultar(); return false;\">Ocultar</a>';
	d.style.left = mensaje_ubicacion() + 'px';
	d.style.width = window.screen.width - mensaje_ubicacion() * 2 + 'px'
	document.body.appendChild(d);

	demorar('mensaje_ocultar_automatico ()', demora_ocultar_mensaje);
}

function mensaje_ocultar() {
	var d;
	d = document.getElementById('div_mensaje');
	d.parentNode.removeChild(d);
}

function mensaje_ocultar_automatico() {
		
	var d;
	d = document.getElementById('div_mensaje');
	d.parentNode.removeChild(d);
}

//Timer
function demorar(funcion, demora) {
	setTimeout(funcion, demora*1000);
}

function limpiar_div(div) {
	var contenedor;
	contenedor = document.getElementById(div);
	contenedor.innerHTML = "";
}



function NuloACero (valor)
	{
		if(valor == '') { valor = 0; }
		if(valor == "") { valor = 0; }
		if(valor == null) { valor = 0; }
		//alert(valor);
		return valor;
	}