<?php require_once('Connections/MySQL.php'); ?>
<?php
if (!isset($_SESSION)) {
  session_start();
}
$MM_authorizedUsers = "Administrador,Profesor";
$MM_donotCheckaccess = "false";

// *** Restrict Access To Page: Grant or deny access to this page
function isAuthorized($strUsers, $strGroups, $UserName, $UserGroup) { 
  // For security, start by assuming the visitor is NOT authorized. 
  $isValid = False; 

  // When a visitor has logged into this site, the Session variable MM_Username set equal to their username. 
  // Therefore, we know that a user is NOT logged in if that Session variable is blank. 
  if (!empty($UserName)) { 
    // Besides being logged in, you may restrict access to only certain users based on an ID established when they login. 
    // Parse the strings into arrays. 
    $arrUsers = Explode(",", $strUsers); 
    $arrGroups = Explode(",", $strGroups); 
    if (in_array($UserName, $arrUsers)) { 
      $isValid = true; 
    } 
    // Or, you may restrict access to only certain users based on their username. 
    if (in_array($UserGroup, $arrGroups)) { 
      $isValid = true; 
    } 
    if (($strUsers == "") && false) { 
      $isValid = true; 
    } 
  } 
  return $isValid; 
}

$MM_restrictGoTo = "error.php";
if (!((isset($_SESSION['MM_Username'])) && (isAuthorized("",$MM_authorizedUsers, $_SESSION['MM_Username'], $_SESSION['MM_UserGroup'])))) {   
  $MM_qsChar = "?";
  $MM_referrer = $_SERVER['PHP_SELF'];
  if (strpos($MM_restrictGoTo, "?")) $MM_qsChar = "&";
  if (isset($QUERY_STRING) && strlen($QUERY_STRING) > 0) 
  $MM_referrer .= "?" . $QUERY_STRING;
  $MM_restrictGoTo = $MM_restrictGoTo. $MM_qsChar . "accesscheck=" . urlencode($MM_referrer);
  header("Location: ". $MM_restrictGoTo); 
  exit;
}
?>
<?php
if (!function_exists("GetSQLValueString")) {
function GetSQLValueString($theValue, $theType, $theDefinedValue = "", $theNotDefinedValue = "") 
{
  if (PHP_VERSION < 6) {
    $theValue = get_magic_quotes_gpc() ? stripslashes($theValue) : $theValue;
  }

  $theValue = function_exists("mysql_real_escape_string") ? mysql_real_escape_string($theValue) : mysql_escape_string($theValue);

  switch ($theType) {
    case "text":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;    
    case "long":
    case "int":
      $theValue = ($theValue != "") ? intval($theValue) : "NULL";
      break;
    case "double":
      $theValue = ($theValue != "") ? doubleval($theValue) : "NULL";
      break;
    case "date":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;
    case "defined":
      $theValue = ($theValue != "") ? $theDefinedValue : $theNotDefinedValue;
      break;
  }
  return $theValue;
}
}

if ($_GET['nombre'] != "" && strlen($_GET['nombre']) > 2)
	{
mysql_select_db($database_MySQL, $MySQL);
$query_rsUsuario = "SELECT usuario.idUsuario,usuario.nombre, usuario.usuario,  usuario.activo, usuario.fechaCreacion, perfil.perfil FROM   usuario inner join perfil on perfil.idPerfil = usuario.idPerfil where usuario like '".$_GET['nombre']."' or usuario.nombre like '%".$_GET['nombre']."%' or perfil.perfil = '".$_GET['nombre']."'  ORDER BY nombre ASC";
$rsUsuario = mysql_query($query_rsUsuario, $MySQL) or die(mysql_error());
$row_rsUsuario = mysql_fetch_assoc($rsUsuario);
$totalRows_rsUsuario = mysql_num_rows($rsUsuario);
?>

<table border="1">
<thead>
  <tr>
    <td>No.</td>
    <td>Nombre</td>
    <td>Usuario</td>
   <!-- <td>Clave</td> -->
    <td>Activo</td>
    <td>Perfil</td>
    <td>Fecha Creacion</td>
    <td>Accion</td>
  </tr>
  </thead>
  <?php $i = 1; do { ?>
    <tr>
      <td align="center"><?php echo $i++; ?></td>
      <td><?php echo $row_rsUsuario['nombre']; ?></td>
      <td><?php echo $row_rsUsuario['usuario']; ?></td>
      <!--<td><?php //echo $row_rsUsuario['clave']; ?></td> -->
      <td><?php if($row_rsUsuario['activo']) { echo "Si"; } else { echo "No"; } ?></td>
      <td><?php echo $row_rsUsuario['perfil']; ?></td>
      <td><?php echo $row_rsUsuario['fechaCreacion']; ?></td>
      <td><a href="usuarioActualizar.php?idUsuario=<?php echo $row_rsUsuario['idUsuario']; ?>">Actualizar</a></td>
    </tr>
    <?php } while ($row_rsUsuario = mysql_fetch_assoc($rsUsuario)); ?>
</table>
<?php
mysql_free_result($rsUsuario);
	}
	else
	{
	?>
	utilice 3 o mas letras.
	<?	
	}
?>
