alter table `notaenlinea_cec`.`alumno` add column `nacimientoFecha` date null comment 'Fecha de nacimiento' after `identificacion`, add column `nacimientoLugar` varchar(255) null comment 'Lugar de nacimiento' after `nacimientoFecha`, add column `grupoSanguineo` varchar(10) null comment 'Grupo sanguineo' after `nacimientoLugar`, add column `direccionResidencia` varchar(1000) null comment 'Dirección de residencia' after `grupoSanguineo`, change `telefono` `telefono` varchar(50) charset latin1 collate latin1_spanish_ci null comment 'Teléfono' after `direccionResidencia`, add column `padreNombre` varchar(1000) null comment 'Nombre del padre' after `telefono`, add column `padreProfesion` varchar(1000) null comment 'Profesión del padre' after `padreNombre`, add column `madreNombre` varchar(1000) null comment 'Nombre de la madre' after `padreProfesion`, add column `madreProfesion` varchar(1000) null comment 'Profesión de la madre' after `madreNombre`; 

alter table `notaenlinea_cec`.`alumno`   
  change `activo` `activo` tinyint(1) default 1 null comment 'Activo o no para consultar información',
  change `codigo` `codigo` varchar(30) charset utf8 collate utf8_spanish_ci not null  after `activo`;

  alter table `notaenlinea_cec`.`alumno`   
  change `identificacion` `identificacion` varchar(45) charset utf8 collate utf8_general_ci not null comment 'Identificacion'  after `idAlumno`,
  change `telefono` `telefono` varchar(50) charset latin1 collate latin1_spanish_ci null comment 'Teléfono'  after `madreProfesion`,
  change `direccionResidencia` `direccionResidencia` varchar(1000) charset latin1 collate latin1_spanish_ci null comment 'Dirección de residencia'  after `telefono`;


  create table `notaenlinea_cec`.`matricula`( `idMatricula` int not null auto_increment comment 'Id que identifica al registro', `idAlumno` int not null comment 'Id que identifica al alumno.', `idCurso` int not null comment 'Id del curso al que el alumno será matriculado', `fecha` datetime not null default current_timestamp comment 'Fecha en la que se creó la matricula', `folio` varchar(50) comment 'Número de folio de la matricula', `matricula` varchar(50) comment 'Número de la matricula', primary key (`idMatricula`) ); 

  	create table `notaenlinea_cec`.`cursomateriaprofesornota`(  
  `idCursoMateriaProfesorNota` int not null auto_increment comment 'Id que identifica el registro.',
  `idCursoMateriaProfesor` int not null comment 'Id que identifica el curso, la materia y el profesor.',
  `bimestre` int not null comment 'Semestre al que aplíca',
  `notaNombre` varchar(1000) not null comment 'Nombre de la nota',
  `notaDescripcion` varchar(2000) not null comment 'Descripción de la nota',
  `fechaCreacion` datetime not null default current_timestamp comment 'Fecha en la que se creó el registro',
  primary key (`idCursoMateriaProfesorNota`)
);

alter table `notaenlinea_cec`.`cursomateriaprofesornota`   
  add column `nota` int null comment 'Identifica la nota' after `bimestre`;