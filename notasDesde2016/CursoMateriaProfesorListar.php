<?php require_once('Connections/MySQL.php'); ?>
<?php
if (!isset($_SESSION)) {
  session_start();
}
$MM_authorizedUsers = "Administrador,Profesor";
$MM_donotCheckaccess = "false";

// *** Restrict Access To Page: Grant or deny access to this page
function isAuthorized($strUsers, $strGroups, $UserName, $UserGroup) { 
  // For security, start by assuming the visitor is NOT authorized. 
  $isValid = False; 

  // When a visitor has logged into this site, the Session variable MM_Username set equal to their username. 
  // Therefore, we know that a user is NOT logged in if that Session variable is blank. 
  if (!empty($UserName)) { 
    // Besides being logged in, you may restrict access to only certain users based on an ID established when they login. 
    // Parse the strings into arrays. 
    $arrUsers = Explode(",", $strUsers); 
    $arrGroups = Explode(",", $strGroups); 
    if (in_array($UserName, $arrUsers)) { 
      $isValid = true; 
    } 
    // Or, you may restrict access to only certain users based on their username. 
    if (in_array($UserGroup, $arrGroups)) { 
      $isValid = true; 
    } 
    if (($strUsers == "") && false) { 
      $isValid = true; 
    } 
  } 
  return $isValid; 
}

$MM_restrictGoTo = "error.php";
if (!((isset($_SESSION['MM_Username'])) && (isAuthorized("",$MM_authorizedUsers, $_SESSION['MM_Username'], $_SESSION['MM_UserGroup'])))) {   
  $MM_qsChar = "?";
  $MM_referrer = $_SERVER['PHP_SELF'];
  if (strpos($MM_restrictGoTo, "?")) $MM_qsChar = "&";
  if (isset($QUERY_STRING) && strlen($QUERY_STRING) > 0) 
  $MM_referrer .= "?" . $QUERY_STRING;
  $MM_restrictGoTo = $MM_restrictGoTo. $MM_qsChar . "accesscheck=" . urlencode($MM_referrer);
  header("Location: ". $MM_restrictGoTo); 
  exit;
}
?>
<?php
if (!function_exists("GetSQLValueString")) {
function GetSQLValueString($theValue, $theType, $theDefinedValue = "", $theNotDefinedValue = "") 
{
  if (PHP_VERSION < 6) {
    $theValue = get_magic_quotes_gpc() ? stripslashes($theValue) : $theValue;
  }

  $theValue = function_exists("mysql_real_escape_string") ? mysql_real_escape_string($theValue) : mysql_escape_string($theValue);

  switch ($theType) {
    case "text":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;    
    case "long":
    case "int":
      $theValue = ($theValue != "") ? intval($theValue) : "NULL";
      break;
    case "double":
      $theValue = ($theValue != "") ? doubleval($theValue) : "NULL";
      break;
    case "date":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;
    case "defined":
      $theValue = ($theValue != "") ? $theDefinedValue : $theNotDefinedValue;
      break;
  }
  return $theValue;
}
}

mysql_select_db($database_MySQL, $MySQL);
$query_rsCursoMateriaProfesor = "
								SELECT cmp.idCursoMateriaProfesor, c.ano, c.curso, m.materia, u.nombre, cmp.intensidadHoraria, cmp.idCalificacionTipo
								FROM cursomateriaprofesor as cmp
								inner join curso as c on c.idCurso = cmp.idCurso
								inner join materia as m on m.idMateria = cmp.idMateria
								inner join usuario as u on u.idUsuario = cmp.idProfesor
								where 1 = 1 ";
                                 //and ano = ".date("Y ")."              
								//";
if($_GET['idCurso'])
	{
		$query_rsCursoMateriaProfesor .= " and cmp.idCurso = ".$_GET['idCurso'];
	}

if($_GET['idMateria'])
	{
		$query_rsCursoMateriaProfesor .= " and cmp.idMateria = ".$_GET['idMateria'];
	}
if($_GET['idProfesor'])
	{
		$query_rsCursoMateriaProfesor .= " and cmp.idProfesor = ".$_GET['idProfesor'];
	}	
	
$query_rsCursoMateriaProfesor .= " ORDER BY c.ano, c.curso, m.materia, u.nombre ASC";

$rsCursoMateriaProfesor = mysql_query($query_rsCursoMateriaProfesor, $MySQL) or die(mysql_error());
$row_rsCursoMateriaProfesor = mysql_fetch_assoc($rsCursoMateriaProfesor);
$totalRows_rsCursoMateriaProfesor = mysql_num_rows($rsCursoMateriaProfesor);
if ($totalRows_rsCursoMateriaProfesor > 0)
	{
?>
<table border="1" width="100%">
<thead>
  <tr>
    <td>No.</td>
    <td>Año</td>
    <td>Curso</td>
    <td>Materia</td>
    <td>Profesor</td>
    <td>Tipo de calificaci&oacute;n</td>
    <td>Intensidad Horaria semanal</td>
    <td>Accion</td>
  </tr>
  </thead>
  <?php
  $calificacionTipos = array();
  $sql_calificacionTipo = "select idCalificacionTipo, calificacionTipo from calificaciontipo order by calificacionTipo asc";
  $resultado_calificacionTipo = mysql_query($sql_calificacionTipo,$MySQL);
  while($fila_calificacionTipo = mysql_fetch_assoc($resultado_calificacionTipo))
  {
    array_push($calificacionTipos,array($fila_calificacionTipo['idCalificacionTipo'],$fila_calificacionTipo['calificacionTipo']));
  }

   $i = 1; do { ?>
    <tr>
    <td align="center"><? echo $i++; ?></td>
      <td align="center"><?php echo $row_rsCursoMateriaProfesor['ano']; ?></td>
      <td align="center"><?php echo $row_rsCursoMateriaProfesor['curso']; ?></td>
      <td><?php echo $row_rsCursoMateriaProfesor['materia']; ?></td>
      <td><?php echo $row_rsCursoMateriaProfesor['nombre']; ?></td>
      <td align="center">
        <select id="idCalificacionTipo_<?php echo $row_rsCursoMateriaProfesor['idCursoMateriaProfesor']; ?>" 
                name="idCalificacionTipo_<?php echo $row_rsCursoMateriaProfesor['idCursoMateriaProfesor']; ?>" 
                onchange="javascript:fCursoMateriaProfesorActualizarCalificacionTipo(<?php echo $row_rsCursoMateriaProfesor['idCursoMateriaProfesor']; ?>)">
          <?
          for ($i=0; $i < count($calificacionTipos); $i++) 
                { 
                   ?>

                   <option value="<? echo $calificacionTipos[$i][0]; ?>" <? if($row_rsCursoMateriaProfesor['idCalificacionTipo'] == $calificacionTipos[$i][0] ) { ?> selected <? } ?> ><? echo $calificacionTipos[$i][1]; ?></option>
                   <?
                 }       
          ?>
        </select>
        <div id="divCalificacionTipo_<?php echo $row_rsCursoMateriaProfesor['idCursoMateriaProfesor']; ?>" name="divCalificacionTipo_<?php echo $row_rsCursoMateriaProfesor['idCursoMateriaProfesor']; ?>"></div>
        </td>
      <td align="center">
        <input 
        type="text" 
        name="intensidadHoraria" class="inputtextnota"
        onchange="javascript:fCursoMateriaProfesorActualizarIntensidad(<?php echo $row_rsCursoMateriaProfesor['idCursoMateriaProfesor']; ?>,this.value)"
        value="<?php echo $row_rsCursoMateriaProfesor['intensidadHoraria']; ?>"
        >
        <div id="intensidadHoraria_<?php echo $row_rsCursoMateriaProfesor['idCursoMateriaProfesor']; ?>"> </div>
      </td>
      <td align="center"><input type="button" name="Eliminar" value="Eliminar" onclick="javascript:fCursoMateriaProfesorEliminar(<?php echo $row_rsCursoMateriaProfesor['idCursoMateriaProfesor']; ?>);" /></td>
    </tr>
    <?php } while ($row_rsCursoMateriaProfesor = mysql_fetch_assoc($rsCursoMateriaProfesor)); ?>
</table>
<?php
	}
	else
	{
	?>
	No hay datos.
	<?	
	}
mysql_free_result($rsCursoMateriaProfesor);
?>
