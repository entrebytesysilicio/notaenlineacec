select	c.curso as Curso, 
				u.nombre as Profesor, 
				m.materia as Materia, 
				cmp.intensidadHoraria as Intensidad
from curso as c 
inner join cursomateriaprofesor as cmp on cmp.idCurso = c.idCurso
inner join materia as m on m.idMateria = cmp.idMateria
inner join usuario as u on u.idUsuario = cmp.idProfesor
 where ano = 2013 
 order by c.orden asc, u.nombre asc
 