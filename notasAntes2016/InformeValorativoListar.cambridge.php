<?php require_once('Connections/MySQL.php'); ?>
<?php require_once('InformeValorativoClase.php'); ?>
<?php
//header('Content-type: application/vnd.ms-excel');

//header('Content-type: application/msword');


if (!isset($_SESSION)) {
  session_start();
}
$MM_authorizedUsers = "Administrador,Profesor,Padre";
$MM_donotCheckaccess = "false";

// *** Restrict Access To Page: Grant or deny access to this page
function isAuthorized($strUsers, $strGroups, $UserName, $UserGroup) { 
  // For security, start by assuming the visitor is NOT authorized. 
  $isValid = False; 

  // When a visitor has logged into this site, the Session variable MM_Username set equal to their username. 
  // Therefore, we know that a user is NOT logged in if that Session variable is blank. 
  if (!empty($UserName)) { 
    // Besides being logged in, you may restrict access to only certain users based on an ID established when they login. 
    // Parse the strings into arrays. 
    $arrUsers = Explode(",", $strUsers); 
    $arrGroups = Explode(",", $strGroups); 
    if (in_array($UserName, $arrUsers)) { 
      $isValid = true; 
    } 
    // Or, you may restrict access to only certain users based on their username. 
    if (in_array($UserGroup, $arrGroups)) { 
      $isValid = true; 
    } 
    if (($strUsers == "") && false) { 
      $isValid = true; 
    } 
  } 
  return $isValid; 
}

$MM_restrictGoTo = "error.php";
if (!((isset($_SESSION['MM_Username'])) && (isAuthorized("",$MM_authorizedUsers, $_SESSION['MM_Username'], $_SESSION['MM_UserGroup'])))) {   
  $MM_qsChar = "?";
  $MM_referrer = $_SERVER['PHP_SELF'];
  if (strpos($MM_restrictGoTo, "?")) $MM_qsChar = "&";
  if (isset($QUERY_STRING) && strlen($QUERY_STRING) > 0) 
  $MM_referrer .= "?" . $QUERY_STRING;
  $MM_restrictGoTo = $MM_restrictGoTo. $MM_qsChar . "accesscheck=" . urlencode($MM_referrer);
  header("Location: ". $MM_restrictGoTo); 
  exit;
}
$iv = new EstudianteInformeValorativo();
$iv->idAlumno = $_GET['idAlumno'];
$iv->idCurso = $_GET['idCurso'];
if($_GET['pdf'])
	{
	?>
  <!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
  <html xmlns="http://www.w3.org/1999/xhtml">
  <head>
  <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" /> 
  <link href="estilo_boletin.css" rel="stylesheet" type="text/css" />
  <style type="text/css">
<!--
.t1 {
	text-align: center;
	font-size: 9px;
}
-->
</style>
</head>
  <body onload="window.print();" style="font-size:8px; font-weight: bold;"><strong></strong>	
	<?	
	}
?>
<div class="tamanoCarta">
<table width="100%">
<tr><td align="left"><? //$im = @imagecreatefromjpeg("imagenes/logo_izquierdo.jpg"); imagejpeg($im); ?><img src="imagenes/logo_izquierdo.jpg" /></td><td width="100%" align="center"><H1>INFORME VALORATIVO</H1></td><td align="right"><img src="imagenes/logo_derecho.jpg" /></td></tr>
</table>
<p style="width:5px"></p>
<table border="0" width="100%" cellpadding="1" cellspacing="1">
<tr><td colspan="9"></td></tr>
<tr>
  <td><b>Apellidos y nombres:</b></td>
  <td class="bordeBoletin"><? $iv->nombreEstudiante(); ?></td>
  <td><b>Grado:</b></td>
  <td class="bordeBoletin"  align="center"><? $iv->nombreCurso(); ?></td>
  <td><b>Promedio:</b></td>
  <td class="bordeBoletin" align="center">
    <?
		////busca el promedio del alumno en el periodo 
		////si el periodo es 5, se debe buscar el promedio durante todo el a�o escolar
		$sql7 = "
						select 	b.idAlumno,avg(nota) as promedio
						from 	boletin as b
						inner join alumno as a on a.idAlumno = b.idAlumno
						inner join curso as c on b.idCurso = c.idCurso
						where 		b.idCurso = ".$_GET['idCurso'];
		if ($_GET['periodo'] != 5)
			{
			$sql7 .= " and b.bimestre = ".$_GET['periodo'];
			}
		$sql7 .= "	group by 	b.idAlumno order by 2 desc";
		$resultado7 = mysql_query($sql7,$MySQL);
		$filas7 = mysql_fetch_array($resultado7);
		$vandera = 0;
		$puesto = 1;
		do {
				////si el alumno buscado es igual al alumno que se esta recorriendo en el cursor, entonces guardo las variables para poder mostrarlas luego.
				if ($_GET['idAlumno'] == $filas7['idAlumno'] )
					{
					$puesto_alumno = $puesto;
					////Se coloca la vandera, por si el programa encuentra al alumno, termine la ejecucion del ciclo
					$vandera = 1;
					$promedio = $filas7['promedio']; 
					}
					$puesto++;
				}while( ($filas7 = mysql_fetch_array($resultado7)) and $vandera == 0);
	////muestra el promedio.
	echo substr($promedio,0,4);
	?>
    
	</td>
  <td><b>Puesto:</b></td>
  <td  class="bordeBoletin" align="center">
		<? 
		////Muestra el puesto que ocupo el alumno.
		echo $puesto_alumno; 
		?>
  </td>
  <td></td>
</tr>
<tr><td colspan="9"></td></tr>
<tr>
	<td><b>Director de curso:</b></td>
	<td class="bordeBoletin"><? $iv->nombreDirectorCurso (); ?></td>
    <td><b>Periodo:</b></td>
    <? ////Imprime el nombre del periodo. ?>
    <td class="bordeBoletin"  align="center"><? switch ($_GET['periodo']) { case 1: echo "Primero"; break; case 2: echo "Segundo"; break; case 3: echo "Tercero"; break; case 4: echo "Cuarto"; break; case 5: echo "Final"; break; } ?></td>
    <td><b>A&ntilde;o:</b></td>
    <td class="bordeBoletin"  align="center"><? echo $_GET['ano']; ?></td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
</tr>
<tr><td colspan="9"></td></tr>
</table>
<p style="width:5px"></p>
<table border="1" width="100%">
<?
if ($_GET['periodo'] == 5)
	{
	?>
  <tr valign="middle" align="center" class="tablaBoletin1">
    <td >&Aacute;rea</td>
    <td >Asignatura</td>
    <td >IH</td>
    <td >Final</td>    
    <td >Recuperaci�n</td>    
    <td >Descripci�n del proceso escolar</td>
  </tr>
	<?			
	}
	else
	{
	?>
  <tr valign="middle" align="center"  class="tablaBoletin1">
    <td rowspan="3">&Aacute;rea</td>
    <td rowspan="3">Asignatura</td>
    <td rowspan="3">IH</td>
    <td colspan="10">PERIODOS</td>
    <td>DESCRIPCION DEL PROCESO ESCOLAR</td>
  </tr>
  <tr valign="middle" align="center"  class="tablaBoletin1">
    <td colspan="2">1</td>
    <td colspan="2">2</td>
    <td colspan="2">3</td>
    <td colspan="2">4</td>
    <td colspan="2">Acumulado</td>
    <td rowspan="2">COMPETENCIAS Y DESEMPE&Ntilde;OS</td>
  </tr>
  <tr align="center" valign="middle"  class="tablaBoletin1">
    <td>CT</td>
    <td>CL</td>
    <td>CT</td>
    <td>CL</td>
    <td>CT</td>
    <td>CL</td>
    <td>CT</td>
    <td>CL</td>
    <td>CT</td>
    <td>CL</td>
   
  </tr>
<?		
	}
	////identifica la intensidad horaria de cada una de las materias que se dictan en determinado curso
	////
	////Recorre la consulta para sacar el resto de la informacion, llendo ordenado por el nombre de las materias
	////Por cada materia imprime el nombre, coloca la intensidad horaria, y saca los promedios y desempe�os semestre a semestre
	$sql1 = "
							select sum(cmp.intensidadHoraria) as IH, m.materia, m.idMateria, a.Area 
						from cursomateriaprofesor as cmp 
						inner join materia as m on m.idMateria = cmp.idMateria
						inner join area as a on m.idArea = a.idArea
						where cmp.idCurso = ".$_GET['idCurso']."
						group by  m.materia, m.idMateria, a.Area
						order by a.Area, m.materia
						";
	mysql_select_db($database_MySQL, $MySQL);					
	$resultado1 = mysql_query($sql1,$MySQL);
	if($filas1 = mysql_fetch_array($resultado1))
		{
		do {
				?>
				<tr valign="top" align="center">
						<td align="left" class="tablaBoletin1">
						<? 
            ////Imprime el nombre de la materia
            echo $filas1['Area']; 
						?>
            </td>
						<td align="left" class="tablaBoletin1">
						<? 
            ////Imprime el nombre de la materia
            echo $filas1['materia']; 
						?>
            </td>            
						<td>
						<? 
            ////Imprime la intensidad horaria de la materia
            echo $filas1['IH']; 
						?>
            </td>
						<?
	  				////Calcula el promedio para la materia que se esta consultando en el momento
						unset($promedio);
			
						if ($_GET['periodo'] == 5)
							{		
							////Saca la nota de determinado alumno, en determinado curso, para determinada materia, en determinado semestre.
							$sql2 = "select sum(nota)/4 as nota from boletin where idAlumno = ".$_GET['idAlumno']." and idCurso = ".$_GET['idCurso']." and idMateria = ".$filas1['idMateria'];
							//print $sql2;
							$resultado2 = mysql_query($sql2,$MySQL);
							if ($filas2 = mysql_fetch_array($resultado2))
								{
								$nota = $filas2['nota'];
								}
								else
								{
									$nota = 0;
								}
							?>
							<td>
							<? 
							////Muestra la nota
							echo number_format($nota,1); 
							?>
							</td>
              <?
              
							$sql7 = "select sum(recuperacion) as recuperacion from boletin where idAlumno = ".$_GET['idAlumno']." and idCurso = ".$_GET['idCurso']." and idMateria = ".$filas1['idMateria'];
							//print $sql2;
							$resultado7 = mysql_query($sql7,$MySQL);
							if ($filas7 = mysql_fetch_array($resultado7))
								{
								$recuperacion = $filas7['recuperacion'];
								}
								else
								{
									$recuperacion = 0;
								}
							
							?>
              <td>
              <?
              ////Muestra la recuperacion
							if($recuperacion > 0)
								{
								echo number_format($recuperacion,1); 
								}
							?>
</td>
							<td>
							<?
							////Consulta el desempeno
							$sql3 =" select idDesempeno, desempeno from desempeno where ano = ".$_GET['ano']." and ".$nota." between notaMinima and notaMaxima";
							$resultado3 = mysql_query($sql3,$MySQL);
							if($filas3 = mysql_fetch_array($resultado3))
								{
									echo $filas3['desempeno'];
								}
								else
								{
									echo mysql_error();
								}							
							?>
							</td>
					<?
          }
          else
          {
					for($i = 1; $i <= 4 ; $i++)
						{
						////Saca la nota de determinado alumno, en determinado curso, para determinada materia, en determinado semestre.
						$sql2 = "select nota from boletin where idAlumno = ".$_GET['idAlumno']." and idCurso = ".$_GET['idCurso']." and idMateria = ".$filas1['idMateria']." and bimestre = ".$i;
						$resultado2 = mysql_query($sql2,$MySQL);
						if ($filas2 = mysql_fetch_array($resultado2))
							{
								$nota = $filas2['nota'];
							}
							else
							{
								$nota = 0;
							}							
						if($nota > 0)
							{
							$promedio[$i] = $nota;
							}
						?>
						<td>
						<? 
						////Muestra la nota
						echo $nota; 
						?>
						</td>
						<td>
						<?
						////Consulta el desempeno
						$sql3 =" select idDesempeno, desempeno from desempeno where ano = ".$_GET['ano']." and ".$nota." between notaMinima and notaMaxima";
						$resultado3 = mysql_query($sql3,$MySQL);
						if($filas3 = mysql_fetch_array($resultado3))
							{
								echo $filas3['desempeno'];
							}
							else
							{
								echo mysql_error();
							}
						if($_GET['periodo'] == $i)
							{
								$idDesempenoPeriodo = $filas3['idDesempeno'];
							}
						?>
						</td>
           
						<?
						}		
						?>
            <td>
            <? 
            ////Imprime promedio general acumulado
            echo number_format(array_sum($promedio) / count($promedio),1); 
            ?>
            </td>
            <td>
            <?
            ////Imprime el texto relacionado con el promedio calculado en el a�o
            $sql4 =" select desempeno from desempeno where ano = ".$_GET['ano']." and ".array_sum($promedio) / count($promedio)." between notaMinima and notaMaxima";
            $resultado4 = mysql_query($sql4,$MySQL);
            if($filas4 = mysql_fetch_array($resultado4))
              {
              echo $filas4['desempeno'];
              }
              else
              {
              echo mysql_error();
              }      
            ?>
            </td>
            <td align="left" width="100%">
            Competencia:<br />
            <?
            $sql5 = "select competencia from cursomateriacompetencia where idCurso = ".$_GET['idCurso']." and idMateria = ".$filas1['idMateria']." and bimestre = ".$_GET['periodo'];
            if($resultado5 = mysql_query($sql5,$MySQL))
              {
              if($filas5 = mysql_fetch_array($resultado5))
                {
                do{
                  echo $filas5['competencia'];
                  }while($filas5 = mysql_fetch_array($resultado5));
                }
                else
                {
                echo "";
                } 					
              }
            ?>
            <br />
            Desempe&ntilde;o:
            <br />
            <?
            $sql6 = "select observacion from cursomateriadesempeno where idCurso = ".$_GET['idCurso']." and idMateria = ".$filas1['idMateria']." and bimestre = ".$_GET['periodo']." and idDesempeno = ".$idDesempenoPeriodo;
            if($resultado6 = mysql_query($sql6,$MySQL))
              {
              if($filas6 = mysql_fetch_array($resultado6))
                {
                do{
                  echo $filas6['observacion'];
                  }while($filas6 = mysql_fetch_array($resultado6));
                }
                else
                {
                echo "";
                } 					
              }
            ?>
            </td> 						
					<?
					}
			?>
			</tr>
			<?
			}while($filas1 = mysql_fetch_array($resultado1));
		}
		else
		{
			echo mysql_error();
		}
	?>
	</table>
  
  
  
	<p>ESCALA ALFANUM&Eacute;RICA Y CRITERIOS DE VALORACI&Oacute;N<br />
SUPERIOR 4.6 A 5 Supera con amplitud los logros propuestos<br />
ALTO 4 A 4.5 Alcanza los logros propuestos con limitaciones no relievantes<br />
B&Aacute;SICO 3.5 A 3.9 Apenas alcanza los logros m&iacute;nimos<br />
BAJO MENOR A 3.5 Presenta dificultades fundamentales en la superaci&oacute;n de los logros previstos</p>
<p><br />
OBSERVACIONES Y/O RECOMENDACIONES<br />
------------------------------------------------------------------------------------------------------------------------------------------------------------------------</p>
<p>------------------------------------------------------------------------------------------------------------------------------------------------------------------------</p>
<p>------------------------------------------------------------------------------------------------------------------------------------------------------------------------</p>
<p>------------------------------------------------------------------------------------------------------------------------------------------------------------------------<br />
FIRMA DIRECTOR DE GRUPO </p>
<p>------------------------------------------------------------------------------------------------------------------------------------------------------------------------</p>
<p>FIRMA RECTOR&Iacute;A<br />
------------------------------------------------------------------------------------------------------------------------------------------------------------------------</p>
<p><br />
<span class="t1">Admisiones: Autopista Norte No 100-34 Oficina 203 Edificio Multileasing Tels: 2181106 - 2181115 Telefax: 6231190<br />
Sede Campestre: Vereda La Aurora a diez minutos de la carrera 7 con calle 175 Tels: 8742163 - 8742245 - Fax: 8742236<br />
www.colegiocambridge.edu.co</span></p>
<p>&nbsp;</p>

</div>
<?
if($_GET['pdf'])
	{
	?>
    </body>
    </html>	
	<?	
	}
?>