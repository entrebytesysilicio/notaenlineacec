<?php require_once('Connections/MySQL.php'); ?>
<?php
if (!isset($_SESSION)) {
  session_start();
}
$MM_authorizedUsers = "Administrador,Profesor";
$MM_donotCheckaccess = "false";

// *** Restrict Access To Page: Grant or deny access to this page
function isAuthorized($strUsers, $strGroups, $UserName, $UserGroup) { 
  // For security, start by assuming the visitor is NOT authorized. 
  $isValid = False; 

  // When a visitor has logged into this site, the Session variable MM_Username set equal to their username. 
  // Therefore, we know that a user is NOT logged in if that Session variable is blank. 
  if (!empty($UserName)) { 
    // Besides being logged in, you may restrict access to only certain users based on an ID established when they login. 
    // Parse the strings into arrays. 
    $arrUsers = Explode(",", $strUsers); 
    $arrGroups = Explode(",", $strGroups); 
    if (in_array($UserName, $arrUsers)) { 
      $isValid = true; 
    } 
    // Or, you may restrict access to only certain users based on their username. 
    if (in_array($UserGroup, $arrGroups)) { 
      $isValid = true; 
    } 
    if (($strUsers == "") && false) { 
      $isValid = true; 
    } 
  } 
  return $isValid; 
}

$MM_restrictGoTo = "error.php";
if (!((isset($_SESSION['MM_Username'])) && (isAuthorized("",$MM_authorizedUsers, $_SESSION['MM_Username'], $_SESSION['MM_UserGroup'])))) {   
  $MM_qsChar = "?";
  $MM_referrer = $_SERVER['PHP_SELF'];
  if (strpos($MM_restrictGoTo, "?")) $MM_qsChar = "&";
  if (isset($QUERY_STRING) && strlen($QUERY_STRING) > 0) 
  $MM_referrer .= "?" . $QUERY_STRING;
  $MM_restrictGoTo = $MM_restrictGoTo. $MM_qsChar . "accesscheck=" . urlencode($MM_referrer);
  header("Location: ". $MM_restrictGoTo); 
  exit;
}
if($_GET['idCurso'])
	{
		?>
		<table  border="0" width="100%">
    <tr>
		<?
		for ($i = 1; $i <= 4 ; $i++)
			{
			$sql = "
						select 		c.curso,avg(
						
						
						ifnull(((((nota1 +
						nota2 +
						nota3 +
						nota4 +
						nota5 +
						nota6 )/
						
						(
						case when nota1 > 0 then 1 else 0 end 	
						+
						case when nota2 > 0 then 1 else 0 end 	
						+
						case when nota3 > 0 then 1 else 0 end 	
						+
						case when nota4 > 0 then 1 else 0 end 	
						+
						case when nota5 > 0 then 1 else 0 end 	
						+
						case when nota6 > 0 then 1 else 0 end 	
						)
						) * 40 )/100),0)
						+
						ifnull(((((nota7 +
						nota8 +
						nota9 +
						nota10 +
						nota11 +
						nota12 )/
						(
						case when nota7 > 0 then 1 else 0 end 	
						+
						case when nota8 > 0 then 1 else 0 end 	
						+
						case when nota9 > 0 then 1 else 0 end 	
						+
						case when nota10 > 0 then 1 else 0 end 	
						+
						case when nota11 > 0 then 1 else 0 end 	
						+
						case when nota12 > 0 then 1 else 0 end 	
						)						
						
						) * 40 )/100),0)
						+
						ifnull(((((nota13 +
						nota14 )/
						(
						case when nota13 > 0 then 1 else 0 end 	
						+
						case when nota14 > 0 then 1 else 0 end 							
						)
						) * 20 )/100),0)
						
						
						) as promedio,a.apellidos, a.nombres
						from 			boletin as b
						inner join alumno as a on a.idAlumno = b.idAlumno
						inner join curso as c on b.idCurso = c.idCurso
						where 		b.idCurso = ".$_GET['idCurso']."
						and b.bimestre = ".$i."
						group by 	b.idAlumno
						order by 2 desc
						";	
						
			if($resultado = mysql_query($sql,$MySQL))
				{
				if($filas = mysql_fetch_array($resultado))
					{
					?>
          <td align="center"><br />

					<table border="1">
          <thead><tr>
            <td colspan="5">
						<? 
							switch ($i) 
								{ 
								case 1: echo "Primer"; break; 
								case 2: echo "Segundo"; break; 
								case 3: echo "Tercer"; break; 
								case 4: echo "Cuarto"; break; 
								} 
							?> Periodo</td></tr></thead>
							<thead>
							<tr><td>No.</td><td>Curso</td><td>Promedio</td><td>Apellidos</td><td>Nombres</td></tr>
              </thead>
							<?
					$a = 1;
					do{
						?>
						<tr>
            <td align="center"><? echo $a++; ?></td>
            <td><? echo $filas['curso']; ?></td>
            <td align="center">
			<? 
			//echo substr($filas['promedio'],0,3); 
			echo number_format($filas['promedio'],1);
			?>
            </td>
            <td><? echo $filas['apellidos']; ?></td>
            <td><? echo $filas['nombres']; ?></td>
            </tr>
						<?
						}while($filas = mysql_fetch_array($resultado));
						?>
						<thead><tr><td colspan="5"></td></tr></thead>
            </table><br />

            </td>
						<?
					}
					else
					{
					echo mysql_error();
					}
				}
				else
				{
				echo mysql_error();
				}
				if($i == 2)
					{
					?>
					</tr>
					<?	
					}
			}
			?>
			</table>
			<?
	}
	else
	{
	?>
	Por favor seleccione el curso.
	<?	
	}
?>
