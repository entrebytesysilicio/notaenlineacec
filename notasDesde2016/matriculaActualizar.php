<?php require_once('Connections/MySQL.php'); ?>
<?php
if (!isset($_SESSION)) {
  session_start();
}
$MM_authorizedUsers = "Administrador";
$MM_donotCheckaccess = "false";

header('Content-Type: text/html; charset=utf-8');
// *** Restrict Access To Page: Grant or deny access to this page
function isAuthorized($strUsers, $strGroups, $UserName, $UserGroup) { 
  // For security, start by assuming the visitor is NOT authorized. 
  $isValid = False; 

  // When a visitor has logged into this site, the Session variable MM_Username set equal to their username. 
  // Therefore, we know that a user is NOT logged in if that Session variable is blank. 
  if (!empty($UserName)) { 
    // Besides being logged in, you may restrict access to only certain users based on an ID established when they login. 
    // Parse the strings into arrays. 
    $arrUsers = Explode(",", $strUsers); 
    $arrGroups = Explode(",", $strGroups); 
    if (in_array($UserName, $arrUsers)) { 
      $isValid = true; 
    } 
    // Or, you may restrict access to only certain users based on their username. 
    if (in_array($UserGroup, $arrGroups)) { 
      $isValid = true; 
    } 
    if (($strUsers == "") && false) { 
      $isValid = true; 
    } 
  } 
  return $isValid; 
}

$MM_restrictGoTo = "error.php";
if (!((isset($_SESSION['MM_Username'])) && (isAuthorized("",$MM_authorizedUsers, $_SESSION['MM_Username'], $_SESSION['MM_UserGroup'])))) {   
  $MM_qsChar = "?";
  $MM_referrer = $_SERVER['PHP_SELF'];
  if (strpos($MM_restrictGoTo, "?")) $MM_qsChar = "&";
  if (isset($QUERY_STRING) && strlen($QUERY_STRING) > 0) 
  $MM_referrer .= "?" . $QUERY_STRING;
  $MM_restrictGoTo = $MM_restrictGoTo. $MM_qsChar . "accesscheck=" . urlencode($MM_referrer);
  header("Location: ". $MM_restrictGoTo); 
  exit;
}

include "clases.php"; 
$base = new BD();

if(isset($_POST['actualizar']))
{
  $sql = "update matricula 
          set folio = '".$_POST['matriculaFolio']."',
              matricula = '".$_POST['matriculaMatricula']."',
              idCurso = ".$_POST['matriculaIdCurso'].",
			  fechaMatricula = '".$_POST['fechaMatricula']."'
          where idMatricula = ".$_POST['idMatricula'];
  if($base->consultar($sql))
  {
     header('Location: AlumnoActualizar.php?idAlumno='.$_POST['idAlumno']);
  }
  else 
  {
  echo mysql_error();  
  }

}


?>
<!DOCTYPE html>
<html lang="en">
  <head>

<!-- El charset debe estar sincronizado con el usado por MySQL, ver archivo Connections\MySQL.php -->
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" /> 
<!-- <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" /> -->
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
<!-- Bootstrap -->
<link href="css/bootstrap.min.css" rel="stylesheet">
<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<!--[if lt IE 9]>
<script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
<![endif]-->
<script language="javascript" type="text/javascript" src="ajax.js"></script>
<script language="javascript" type="text/javascript" src="js/jquery-1.10.2.min.js"></script>
<script language="javascript" type="text/javascript" src="js/bootstrap.min.js"></script>

<?php 
  date_default_timezone_set('America/Bogota');
  setlocale(LC_ALL,"es_CO");
 ?>
  </head>
  <body>
    <div class="container"><br><br>
<div class="row">
  <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
    
  </div>
  <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">

<?

if($_GET['idMatricula'] && $_GET['idAlumno'])
{
  $sql = "select * from matricula where idMatricula = ".$_GET['idMatricula'];
  if($resultado = mysql_query($sql))
  {
    if(mysql_num_rows($resultado) > 0)
    {
      $fila = mysql_fetch_assoc($resultado);
      ?>
      <div class="panel panel-default">
          <div class="panel-heading">
            <h3 class="panel-title">Actualizar Matr&iacute;cula</h3>
          </div>
          <div class="panel-body">
            <form action="<? echo $_SERVER['PHP_SELF']; ?>" id="formActualizarMatricula" name="formActualizarMatricula" method="POST" role="form">
              <div class="form-group">
                <label for="fecha">Fecha:</label>
                <input type="text" class="form-control" readonly="readonly" name="fecha" value="<?php echo date("Y/M/d",strtotime ($fila['fecha'])); ?>" />
              </div>
                <div class="form-group">
                    <label for="fechaMatricula">Fecha matr&iacute;cula:</label>
                    <input type="date" class="form-control" name="fechaMatricula" id="fechaMatricula" value="<?php echo substr($fila['fechaMatricula'],0,10); ?>" />
                </div>              
              <div class="form-group">
                <label for="matriculaFolio">Folio:</label>
                <input type="text" class="form-control" name="matriculaFolio" id="matriculaFolio" value="<?php echo $fila['folio']; ?>"  placeholder="N&uacute;mero del folio..." />
              </div>
              <div class="form-group">
                <label for="matriculaMatricula">Matr&iacute;cula:</label>
                <input type="text" class="form-control" name="matriculaMatricula" id="matriculaMatricula" value="<?php echo $fila['matricula']; ?>"  placeholder="N&uacute;mero de la matr&iacute;cula..." />
              </div>
              <div class="form-group">
                <label for="matriculaIdCurso">Curso:</label>
                <select name="matriculaIdCurso" id="matriculaIdCurso" class="form-control" required="required">
                  <option value=""></option>
                  <?php
                  $base = new BD();
                  $sqlCursos = "select idCurso, curso, ano from curso as c order by c.ano desc, c.orden  asc";
                  $resultadoCursos = $base->consultar($sqlCursos);
                  if($filaCursos = mysql_fetch_array($resultadoCursos))
                  {
                  do {
                  ?>
                  <option value="<? echo $filaCursos['idCurso']; ?>" <?php if($filaCursos['idCurso'] == $fila['idCurso']) { ?> selected="selected" <?} ?> ><? echo $filaCursos['ano']; ?> - <? echo $filaCursos['curso']; ?></option>
                  <?
                  } while ($filaCursos = mysql_fetch_array($resultadoCursos));
                  }
                  ?>
                </select>
              </div>
              <input type="hidden" name="idAlumno" id="idAlumno" value="<?php echo $_GET['idAlumno']; ?>">
              <input type="hidden" name="idMatricula" id="idMatricula" value="<?php echo $_GET['idMatricula']; ?>">
              <input type="hidden" name="actualizar" id="actualizar" value="actualizar">
              <button type="submit" class="btn btn-warning form-control">Actualizar matr&iacute;cula</button>
            </form>    
        </div>
      <?
    }
    else
    {
      ?>
      <div class="alert alert-info">
      No se encontraron datos para la informaci&oacute;n consultada.
      </div>
      <?
    }

  }
  else 
  {
  ?>
  <div class="alert alert-danger">
  Error al realizar la consulta en la base de datos.
  </div>
  <?  
  }


}
else
{
  ?>
  <div class="alert alert-danger">
    Por favor env&iacute;e todos los datos.
  </div>
  <?
}

?>



  <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
    
  </div>

</div>

    </div>

  </body>
</html>