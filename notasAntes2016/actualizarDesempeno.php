<?php require_once('Connections/MySQL.php'); ?>
<?php
if (!isset($_SESSION)) {
  session_start();
}
$MM_authorizedUsers = "Administrador,Profesor";
$MM_donotCheckaccess = "false";

// *** Restrict Access To Page: Grant or deny access to this page
function isAuthorized($strUsers, $strGroups, $UserName, $UserGroup) { 
  // For security, start by assuming the visitor is NOT authorized. 
  $isValid = False; 				

  // When a visitor has logged into this site, the Session variable MM_Username set equal to their username. 
  // Therefore, we know that a user is NOT logged in if that Session variable is blank. 
  if (!empty($UserName)) { 
    // Besides being logged in, you may restrict access to only certain users based on an ID established when they login. 
    // Parse the strings into arrays. 
    $arrUsers = Explode(",", $strUsers); 
    $arrGroups = Explode(",", $strGroups); 
    if (in_array($UserName, $arrUsers)) { 
      $isValid = true; 
    } 
    // Or, you may restrict access to only certain users based on their username. 
    if (in_array($UserGroup, $arrGroups)) { 
      $isValid = true; 
    } 
    if (($strUsers == "") && false) { 
      $isValid = true; 
    } 
  } 
  return $isValid; 
}

$MM_restrictGoTo = "error.php";
if (!((isset($_SESSION['MM_Username'])) && (isAuthorized("",$MM_authorizedUsers, $_SESSION['MM_Username'], $_SESSION['MM_UserGroup'])))) {   
  $MM_qsChar = "?";
  $MM_referrer = $_SERVER['PHP_SELF'];
  if (strpos($MM_restrictGoTo, "?")) $MM_qsChar = "&";
  if (isset($QUERY_STRING) && strlen($QUERY_STRING) > 0) 
  $MM_referrer .= "?" . $QUERY_STRING;
  $MM_restrictGoTo = $MM_restrictGoTo. $MM_qsChar . "accesscheck=" . urlencode($MM_referrer);
  header("Location: ". $MM_restrictGoTo); 
  exit;
}
?>
<?php
function GetSQLValueString($theValue, $theType, $theDefinedValue = "", $theNotDefinedValue = "") 
{
  $theValue = (!get_magic_quotes_gpc()) ? addslashes($theValue) : $theValue;

  switch ($theType) {
    case "text":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;    
    case "long":
    case "int":
      $theValue = ($theValue != "") ? intval($theValue) : "NULL";
      break;
    case "double":
      $theValue = ($theValue != "") ? "'" . doubleval($theValue) . "'" : "NULL";
      break;
    case "date":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;
    case "defined":
      $theValue = ($theValue != "") ? $theDefinedValue : $theNotDefinedValue;
      break;
  }
  return $theValue;
}

$editFormAction = $_SERVER['PHP_SELF'];
if (isset($_SERVER['QUERY_STRING'])) {
  $editFormAction .= "?" . htmlentities($_SERVER['QUERY_STRING']);
}

if ((isset($_POST["MM_update"])) && ($_POST["MM_update"] == "form1")) {
  $updateSQL = sprintf("UPDATE desempeno SET ano=%s, desempeno=%s, notaMinima=%s, notaMaxima=%s WHERE iddesempeno=%s",
                       GetSQLValueString($_POST['ano'], "int"),
                       GetSQLValueString($_POST['desempeno'], "text"),
                       GetSQLValueString($_POST['notaMinima'], "double"),
                       GetSQLValueString($_POST['notaMaxima'], "double"),
                       GetSQLValueString($_POST['iddesempeno'], "int"));

  mysql_select_db($database_MySQL, $MySQL);
  $Result1 = mysql_query($updateSQL, $MySQL) or die(mysql_error());

  $updateGoTo = "desempenoCrear.php";
  if (isset($_SERVER['QUERY_STRING'])) {
    $updateGoTo .= (strpos($updateGoTo, '?')) ? "&" : "?";
    $updateGoTo .= $_SERVER['QUERY_STRING'];
  }
  header(sprintf("Location: %s", $updateGoTo));
}

$colname_rsDesempeno = "-1";
if (isset($_GET['idDesempeno'])) {
  $colname_rsDesempeno = (get_magic_quotes_gpc()) ? $_GET['idDesempeno'] : addslashes($_GET['idDesempeno']);
}
mysql_select_db($database_MySQL, $MySQL);
$query_rsDesempeno = sprintf("SELECT * FROM desempeno WHERE iddesempeno = %s", $colname_rsDesempeno);
$rsDesempeno = mysql_query($query_rsDesempeno, $MySQL) or die(mysql_error());
$row_rsDesempeno = mysql_fetch_assoc($rsDesempeno);
$totalRows_rsDesempeno = mysql_num_rows($rsDesempeno);
?><!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>:: Actualizar Desempeno ::</title>
<link href="estilo.css" rel="stylesheet" type="text/css" />
</head>

<body>
<? include "menu.php"; ?>
<div class="divInsertar">
<form method="post" name="form1" action="<?php echo $editFormAction; ?>">
  <table align="center">
    <tr valign="baseline">
      <td nowrap align="right">A�o:</td>
      <td><input type="text" name="ano" value="<?php echo $row_rsDesempeno['ano']; ?>" size="32"></td>
    </tr>
    <tr valign="baseline">
      <td nowrap align="right">Desempe&ntilde;o:</td>
      <td><input type="text" name="desempeno" value="<?php echo $row_rsDesempeno['desempeno']; ?>" size="32"></td>
    </tr>
    <tr valign="baseline">
      <td nowrap align="right">Nota M&iacute;nima:</td>
      <td><input type="text" name="notaMinima" value="<?php echo $row_rsDesempeno['notaMinima']; ?>" size="32"></td>
    </tr>
    <tr valign="baseline">
      <td nowrap align="right">Nota M&aacute;xima:</td>
      <td><input type="text" name="notaMaxima" value="<?php echo $row_rsDesempeno['notaMaxima']; ?>" size="32"></td>
    </tr>
    <tr valign="baseline">
      <td nowrap align="right">&nbsp;</td>
      <td><input type="submit" value="Actualizar registro"></td>
    </tr>
  </table>
  <input type="hidden" name="MM_update" value="form1">
  <input type="hidden" name="iddesempeno" value="<?php echo $row_rsDesempeno['iddesempeno']; ?>">
</form>
</div>
</body>
</html>
<?php
mysql_free_result($rsDesempeno);
?>
