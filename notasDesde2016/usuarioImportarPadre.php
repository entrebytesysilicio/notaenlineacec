<?php require_once('Connections/MySQL.php'); ?><?php
if (!isset($_SESSION)) {
  session_start();
}
$MM_authorizedUsers = "Administrador,Profesor";
$MM_donotCheckaccess = "false";

// *** Restrict Access To Page: Grant or deny access to this page
function isAuthorized($strUsers, $strGroups, $UserName, $UserGroup) { 
  // For security, start by assuming the visitor is NOT authorized. 
  $isValid = False; 

  // When a visitor has logged into this site, the Session variable MM_Username set equal to their username. 
  // Therefore, we know that a user is NOT logged in if that Session variable is blank. 
  if (!empty($UserName)) { 
    // Besides being logged in, you may restrict access to only certain users based on an ID established when they login. 
    // Parse the strings into arrays. 
    $arrUsers = Explode(",", $strUsers); 
    $arrGroups = Explode(",", $strGroups); 
    if (in_array($UserName, $arrUsers)) { 
      $isValid = true; 
    } 
    // Or, you may restrict access to only certain users based on their username. 
    if (in_array($UserGroup, $arrGroups)) { 
      $isValid = true; 
    } 
    if (($strUsers == "") && false) { 
      $isValid = true; 
    } 
  } 
  return $isValid; 
}

$MM_restrictGoTo = "error.php";
if (!((isset($_SESSION['MM_Username'])) && (isAuthorized("",$MM_authorizedUsers, $_SESSION['MM_Username'], $_SESSION['MM_UserGroup'])))) {   
  $MM_qsChar = "?";
  $MM_referrer = $_SERVER['PHP_SELF'];
  if (strpos($MM_restrictGoTo, "?")) $MM_qsChar = "&";
  if (isset($QUERY_STRING) && strlen($QUERY_STRING) > 0) 
  $MM_referrer .= "?" . $QUERY_STRING;
  $MM_restrictGoTo = $MM_restrictGoTo. $MM_qsChar . "accesscheck=" . urlencode($MM_referrer);
  header("Location: ". $MM_restrictGoTo); 
  exit;
}
?><!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>

<title>:: Importar Padres ::</title>
<link href="estilo.css" rel="stylesheet" type="text/css" />
</head>
<body>
<? include "menu.php";?>
<div class="divListado">
<br />
<?

$alumnos = explode('<br />',nl2br(htmlentities($_GET['padres'])));
?>
<table align="center">
		<thead>
			<tr ><td>No.</td><td>Padre</td><td>Identificaci&oacute;n</td><td>Clave Generada</td><td>Direcci&oacute;n</td><td>Tel&eacute;no</td><td>Celular</td><td>Email</td><td>OK?</td></tr>
      </thead>
 <?
for ( $i = 0; $i <= count($alumnos); $i++)
	{
	$alumnos2 = explode("\t",$alumnos[$i]);
	if(count($alumnos2) > 1)
		{
		$sql = 'insert into usuario (nombre,usuario,clave,idperfil,direccion,telefono,celular,email) values (\''.ltrim(rtrim($alumnos2[0])).'\',\''.ltrim(rtrim($alumnos2[1])).'\',\''.md5(ltrim(rtrim($alumnos2[2]))).'\',3,\''.ltrim(rtrim($alumnos2[3])).'\',\''.ltrim(rtrim($alumnos2[4])).'\',\''.ltrim(rtrim($alumnos2[5])).'\',\''.ltrim(rtrim($alumnos2[6])).'\')';
		if($resultado = mysql_query($sql, $MySQL))
			{
			?>
			<tr><td><? echo $i + 1; ?></td><td><? echo ltrim(rtrim($alumnos2[0])); ?></td><td><? echo ltrim(rtrim($alumnos2[1])); ?></td><td><? echo ltrim(rtrim($alumnos2[2])); ?></td><td><? echo ltrim(rtrim($alumnos2[3])); ?></td><td><? echo ltrim(rtrim($alumnos2[4])); ?></td><td><? echo ltrim(rtrim($alumnos2[5])); ?></td><td><? echo ltrim(rtrim($alumnos2[6])); ?></td><td>OK</td></tr>
			<?
			}
			else
			{
			?>
			<tr><td><? echo $i + 1;  ?></td><td><? echo ltrim(rtrim($alumnos2[0])); ?></td><td><? echo ltrim(rtrim($alumnos2[1])); ?></td><td><? echo ltrim(rtrim($alumnos2[2])); ?></td><td><? echo ltrim(rtrim($alumnos2[3])); ?></td><td><? echo ltrim(rtrim($alumnos2[4])); ?></td><td><? echo ltrim(rtrim($alumnos2[5])); ?></td><td><? echo ltrim(rtrim($alumnos2[6])); ?></td><td><? echo mysql_error($MySQL); ?></td></tr>			
			<?
			}
		}
	}
?>
</table>
<br />
</div>

</body>
</html>
