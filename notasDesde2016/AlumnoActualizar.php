<?php require_once('Connections/MySQL.php'); ?>
<?php
if (!isset($_SESSION)) {
session_start();
}
    
date_default_timezone_set('America/Bogota');
setlocale(LC_ALL,"es_CO");     
include "clases.php";
$MM_authorizedUsers = "Administrador,Profesor";
$MM_donotCheckaccess = "false";
// *** Restrict Access To Page: Grant or deny access to this page
function isAuthorized($strUsers, $strGroups, $UserName, $UserGroup) {
// For security, start by assuming the visitor is NOT authorized.
$isValid = False;
// When a visitor has logged into this site, the Session variable MM_Username set equal to their username.
// Therefore, we know that a user is NOT logged in if that Session variable is blank.
if (!empty($UserName)) {
// Besides being logged in, you may restrict access to only certain users based on an ID established when they login.
// Parse the strings into arrays.
$arrUsers = Explode(",", $strUsers);
$arrGroups = Explode(",", $strGroups);
if (in_array($UserName, $arrUsers)) {
$isValid = true;
}
// Or, you may restrict access to only certain users based on their username.
if (in_array($UserGroup, $arrGroups)) {
$isValid = true;
}
if (($strUsers == "") && false) {
$isValid = true;
}
}
return $isValid;
}
$MM_restrictGoTo = "error.php";
if (!((isset($_SESSION['MM_Username'])) && (isAuthorized("",$MM_authorizedUsers, $_SESSION['MM_Username'], $_SESSION['MM_UserGroup'])))) {
$MM_qsChar = "?";
$MM_referrer = $_SERVER['PHP_SELF'];
if (strpos($MM_restrictGoTo, "?")) $MM_qsChar = "&";
if (isset($QUERY_STRING) && strlen($QUERY_STRING) > 0)
$MM_referrer .= "?" . $QUERY_STRING;
$MM_restrictGoTo = $MM_restrictGoTo. $MM_qsChar . "accesscheck=" . urlencode($MM_referrer);
header("Location: ". $MM_restrictGoTo);
exit;
}
?>
<?php
if (!function_exists("GetSQLValueString")) {
function GetSQLValueString($theValue, $theType, $theDefinedValue = "", $theNotDefinedValue = "")
{
if (PHP_VERSION < 6) {
$theValue = get_magic_quotes_gpc() ? stripslashes($theValue) : $theValue;
}
$theValue = function_exists("mysql_real_escape_string") ? mysql_real_escape_string($theValue) : mysql_escape_string($theValue);
switch ($theType) {
case "text":
$theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
break;
case "long":
case "int":
$theValue = ($theValue != "") ? intval($theValue) : "NULL";
break;
case "double":
$theValue = ($theValue != "") ? doubleval($theValue) : "NULL";
break;
case "date":
$theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
break;
case "defined":
$theValue = ($theValue != "") ? $theDefinedValue : $theNotDefinedValue;
break;
}
return $theValue;
}
}
$editFormAction = $_SERVER['PHP_SELF'];
if (isset($_SERVER['QUERY_STRING'])) {
$editFormAction .= "?" . $_SERVER['QUERY_STRING'];
}
if(isset($_POST['activo']))
{
$activar = ($_POST['activo'] == 'on') ? "1":"0";
}
else
{
$activar = "0";
}
if ((isset($_POST["MM_update"])) && ($_POST["MM_update"] == "form1")) {
$updateSQL = sprintf("UPDATE alumno
SET nombres=%s,
apellidos=%s,
identificacion=%s,
activo=%s,
nacimientoFecha = %s,
nacimientoLugar = %s,
grupoSanguineo = %s,
telefono = %s,
direccionResidencia = %s,
padreNombre = %s,
padreProfesion = %s,
madreNombre = %s,
madreProfesion = %s
WHERE idAlumno=%s",
GetSQLValueString($_POST['nombres'], "text"),
GetSQLValueString($_POST['apellidos'], "text"),
GetSQLValueString($_POST['identificacion'], "text"),
GetSQLValueString($activar, "int"),
GetSQLValueString($_POST['nacimientoFecha'], "text"),
GetSQLValueString($_POST['nacimientoLugar'], "text"),
GetSQLValueString($_POST['grupoSanguineo'], "text"),
GetSQLValueString($_POST['telefono'], "text"),
GetSQLValueString($_POST['direccionResidencia'], "text"),
GetSQLValueString($_POST['padreNombre'], "text"),
GetSQLValueString($_POST['padreProfesion'], "text"),
GetSQLValueString($_POST['madreNombre'], "text"),
GetSQLValueString($_POST['madreProfesion'], "text"),
GetSQLValueString($_POST['idAlumno'], "int"));
mysql_select_db($database_MySQL, $MySQL);
$Result1 = mysql_query($updateSQL, $MySQL) or die(mysql_error());
$updateGoTo = "AlumnoActualizar.php";
if (isset($_SERVER['QUERY_STRING'])) {
$updateGoTo .= (strpos($updateGoTo, '?')) ? "&" : "?";
$updateGoTo .= $_SERVER['QUERY_STRING'];
}
header(sprintf("Location: %s", $updateGoTo));
}

$sqlAlumno ="select * from alumno where idAlumno = ".$_GET['idAlumno'];
$resultadoAlumno = mysql_query($sqlAlumno,$MySQL);
$filaAlumno = mysql_fetch_assoc($resultadoAlumno);

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
  <head>
    <title>:: Alumno Actualizar ::</title>
    <!-- El charset debe estar sincronizado con el usado por MySQL, ver archivo Connections\MySQL.php -->
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" /> 
    <!-- <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" /> -->
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <!-- Bootstrap -->
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
    <script language="javascript" type="text/javascript" src="ajax.js"></script>
    <script language="javascript" type="text/javascript" src="js/jquery-1.10.2.min.js"></script>
    <script language="javascript" type="text/javascript" src="js/bootstrap.min.js"></script>

    <script language="javascript" type="text/javascript">
    function fUsuarioActualizarMostrarAcudientes ()
    {
    var idUsuario;
    idUsuario = document.getElementById('idAlumno').value;
    mostrar_url('AcudienteAlumnoListar.php','idAlumno='+idUsuario,'divAlumnoActualizarListadoAcudientes','get');
    }
    
    function fAlumnoActualizarBuscarAcudiente (nombre)
    {
    mostrar_url('Parametros.php','accion=buscaracudiente&acudiente='+nombre,'divAlumnoActualizarAcudienteResultado','get');
    }
    
    function fAlumnoAcudienteAgregar()
    {
    var idAlumno, idAcudiente;
    idAlumno = document.getElementById('idAlumno').value;
    idAcudiente =document.getElementById('slAlumnoAcudienteEncontrados');
    idAcudiente = idAcudiente.options[idAcudiente.selectedIndex].value;
    mostrar_url('AlumnoAcudienteAgregar.php','MM_insert=form1&idAlumno='+idAlumno+"&idAcudiente="+idAcudiente,'divAlumnoActualizarListadoAcudientes','get');
    
    setTimeout('fUsuarioActualizarMostrarAcudientes ()', 2*1000); //segundos * mil, el parametro es en ms es decir 5 * 1000 = 5 segundos
    
    
    }
    
    function fAcudienteAlumnoEliminar (idAlumnoAcudiente)
    {
    mostrar_url('AlumnoAcudienteEliminar.php','idAlumnoAcudiente='+idAlumnoAcudiente,'divAlumnoActualizarListadoAcudientes','get');
    setTimeout('fUsuarioActualizarMostrarAcudientes ()', 2*1000); //segundos * mil, el parametro es en ms es decir 5 * 1000 = 5 segundos
    }
    
    function fAlumnoCursoListar ()
    {
    idAlumno = document.getElementById('idAlumno').value;
    mostrar_url('cursoAlumno.php','actividad=historialAcademicoListar&idAlumno='+idAlumno,'divAlumnoCursoListado','post');
    }
    function fMatriculaCrear()
    {
    idCurso = document.getElementById('matriculaIdCurso').value;
    idAlumno = document.getElementById('matriculaIdAlumno').value;
    folio = document.getElementById('matriculaFolio').value;
    matricula = document.getElementById('matriculaMatricula').value;
    fechaMatricula = document.getElementById('matriculaFecha').value;
    mostrar_url('MatriculaCrear.php', 'idCurso=' + idCurso + "&idAlumno=" + idAlumno + "&folio=" + folio + "&matricula=" + matricula + "&fechaMatricula=" + fechaMatricula, 'matriculaMensaje', 'post');
    setTimeout('fMatriculaListar(idAlumno)', 1*1000);
    }
    
    function fMatriculaListar(idAlumno)
    {
    mostrar_url('MatriculaListar.php',"idAlumno="+idAlumno,'divMatriculaListado','post');
    }
    function fMatriculaEliminar(idMatricula,idAlumno)
    {
      if(confirm('¿Desea eliminar la matr&iacute;cula?'))
      {
      mostrar_url('MatriculaEliminar.php',"idMatricula="+idMatricula,'divMatriculaListado','post');
      setTimeout('fMatriculaListar(idAlumno)', 1*1000);        
    }
    }

    function fMatriculaActualizar(idMatricula,idAlumno)
    {
      window.location="matriculaActualizar.php?idMatricula="+idMatricula+"&idAlumno="+idAlumno;
    }    

    function fMatriculaImprimir(idMatricula)
    {
      window.open('MatriculaImprimir.php?idMatricula='+idMatricula,'_MatriculaImprimir','fullscreen=1,location=0,menubar=0,resizable=1,scrollbars=1,status=1,titlebar=1,toolbar=0,left=0px,top=0px')
    }


    function fCursoOtrasInstitucionesAgregar ()
    {
    ano = document.getElementById("cursoOtrasInstitucionesAno").value;
    curso = document.getElementById("cursoOtrasInstitucionesCurso").value;
    institucion = document.getElementById("cursoOtrasInstitucionesInstitucion").value;
    idAlumno = document.getElementById("cursoOtrasInstitucionesIdAlumno").value;
    promovido = document.getElementById("cursoOtrasInstitucionesPromovido1");

    if(promovido.checked) { promovido = 1; } else { promovido= 0; }
    
    mostrar_url('cursoOtrasInstituciones.php',"actividad=agregar&ano="+ano+"&curso="+curso+"&institucion="+institucion+"&promovido="+promovido+"&idAlumno="+idAlumno,'divAlumnoCursoOtrasInstitucionesAgregarMensaje','post');
    setTimeout('fCursoOtrasInstitucionesListar(idAlumno)', 1*1000);
    setTimeout('fAlumnoCursoListar(idAlumno)', 1*1000);    
    }

    function fCursoOtrasInstitucionesListar (idAlumno)
    {
    mostrar_url('cursoOtrasInstituciones.php',"actividad=listar&idAlumno="+idAlumno,'divAlumnoCursoOtrasInstitucionesListado','post');
    }

    function fcursoOtrasInstitucionesEliminar(idCursoOtrasInstituciones)
    {
     idAlumno = document.getElementById("cursoOtrasInstitucionesIdAlumno").value;
     if(confirm('¿Desea eliminar el registro seleccionado?'))
     {
     mostrar_url('cursoOtrasInstituciones.php',"actividad=eliminar&idCursoOtrasInstituciones="+idCursoOtrasInstituciones,'divAlumnoCursoOtrasInstitucionesEliminar','post'); 
     setTimeout('fCursoOtrasInstitucionesListar(idAlumno)', 1*1000);      
     setTimeout('fAlumnoCursoListar(idAlumno)', 1*1000);
     }

    }



    </script>
  </head>
  <body onload="javascript:fUsuarioActualizarMostrarAcudientes (); javascript:fAlumnoCursoListar (); javascript:fMatriculaListar(<?php echo $_GET['idAlumno']; ?>); javascript:fCursoOtrasInstitucionesListar(<?php echo $_GET['idAlumno']; ?>);">
    <? include "menu.php"; ?>
    <div class="container">
      <div class="row">
        <center><h3><?php echo $filaAlumno['nombres']; ?> <?php echo $filaAlumno['apellidos']; ?></h3></center>
        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
          <legend><h4>Informaci&oacute;n del alumno</h4></legend>
          <div class="row">
            <form action="<?php echo $editFormAction; ?>" method="post" name="form1" id="form1">
              <div class="col-xs-3 col-sm-3 col-md-3 col-lg-3">
                <div class="form-group">
                  <label for="identificacion">Identificaci&oacute;n:</label>
                  <input type="text" class="form-control" name="identificacion" value="<?php echo $filaAlumno['identificacion']; ?>"  placeholder="Identificaci&oacute;n del alumno" />
                </div>
                <div class="form-group">
                  <label for="nombres">Nombres:</label>
                  <input type="text" class="form-control" name="nombres" id="nombres" value="<?php echo $filaAlumno['nombres']; ?>" placeholder="Nombre del alumno">
                </div>
                <div class="form-group">
                  <label for="apellidos">Apellidos:</label>
                  <input type="text" class="form-control" name="apellidos" value="<?php echo $filaAlumno['apellidos']; ?>" placeholder="Apellidos del alumno"/>
                </div>
                <div class="form-group">
                  <label for="nacimientoLugar">Lugar de nacimiento:</label>
                  <input type="text" class="form-control" name="nacimientoLugar" value="<?php echo $filaAlumno['nacimientoLugar']; ?>"  placeholder="Lugar de nacimiento" />
                </div>
              </div>
              <div class="col-xs-3 col-sm-3 col-md-3 col-lg-3">
                <div class="form-group">
                  <label for="nacimientoFecha">Fecha de nacimiento:</label>
                  <input type="date" class="form-control" name="nacimientoFecha" value="<?php echo $filaAlumno['nacimientoFecha']; ?>"  placeholder="Fecha de nacimiento" />
                </div>
                <div class="form-group">
                  <label for="grupoSanguineo">Grupo sanguineo:</label>
                  <select name="grupoSanguineo" id="grupoSanguineo" class="form-control">
                    <option value="Desconoce" <?php if($filaAlumno['grupoSanguineo'] == "Desconoce") { ?> checked="checked" <?} ?>  >Desconoce</option>
                    <option value="A-" <?php if($filaAlumno['grupoSanguineo'] == "A-") { ?> selected <?} ?>  >A-</option>
                    <option value="A+" <?php if($filaAlumno['grupoSanguineo'] == "A+") { ?> selected <?} ?>  >A+</option>
                    <option value="B-" <?php if($filaAlumno['grupoSanguineo'] == "B-") { ?> selected <?} ?>  >B-</option>
                    <option value="B+" <?php if($filaAlumno['grupoSanguineo'] == "B+") { ?> selected <?} ?>  >B+</option>
                    <option value="AB-" <?php if($filaAlumno['grupoSanguineo'] == "AB-") { ?> selected <?} ?>  >AB-</option>
                    <option value="AB+" <?php if($filaAlumno['grupoSanguineo'] == "AB+") { ?> selected <?} ?>  >AB+</option>
		    <option value="O+" <?php if($filaAlumno['grupoSanguineo'] == "O+") { ?> selected <?} ?>  >O+</option>
                    <option value="O-" <?php if($filaAlumno['grupoSanguineo'] == "O-") { ?> selected <?} ?>  >O-</option>
                  </select>
                </div>
                <div class="form-group">
                  <label for="telefono">Tel&eacute;fono:</label>
                  <input type="number" class="form-control" name="telefono" value="<?php echo $filaAlumno['telefono']; ?>"  placeholder="Tel&eacute;fono" />
                </div>
                <div class="form-group">
                  <label for="direccionResidencia">Direcci&oacute;n de residencia:</label>
                  <input type="text" class="form-control" name="direccionResidencia" value="<?php echo $filaAlumno['direccionResidencia']; ?>"  placeholder="Direcci&oacute;n de residencia" />
                </div>
              </div>
              <div class="col-xs-3 col-sm-3 col-md-3 col-lg-3">
                <div class="form-group">
                  <label for="padreNombre">Nombre del padre:</label>
                  <input type="text" class="form-control" name="padreNombre" value="<?php echo $filaAlumno['padreNombre']; ?>"  placeholder="Nombre del padre" />              
                </div>
                <div class="form-group">
                  <label for="padreProfesion">Profesi&oacute;n del padre:</label>
                  <input type="text" class="form-control" name="padreProfesion" value="<?php echo $filaAlumno['padreProfesion']; ?>"  placeholder="Profesi&oacute;n del padre" />
                </div>
                <div class="form-group">
                  <label for="madreNombre">Nombre de la madre:</label>
                  <input type="text" class="form-control" name="madreNombre" value="<?php echo $filaAlumno['madreNombre']; ?>"  placeholder="Nombre de la madre" />
                </div>
                <div class="form-group">
                  <label for="madreProfesion">Profesi&oacute;n de la madre:</label>
                  <input type="text" class="form-control" name="madreProfesion" value="<?php echo $filaAlumno['madreProfesion']; ?>"  placeholder="Profesi&oacute;n de la madre" />
                </div>
              </div>
              <div class="col-xs-3 col-sm-3 col-md-3 col-lg-3">
                <div class="form-group">
                  <label for="activo">Activo:</label>
                  <input type="checkbox" <?php if (!(strcmp($filaAlumno['activo'],1))) {echo "checked=\"checked\"";} ?> name="activo" id="activo" />
                </div>
                <button type="submit" class="btn btn-success form-control">Actualizar</button>
                <input type="hidden" name="MM_update" value="form1" />
                <input type="hidden" id="idAlumno" name="idAlumno" value="<?php echo $filaAlumno['idAlumno']; ?>" />
              </div>
            </form>
          </div>
        </div>
      </div>
      <br>
      <!-- Acudientes -->
      <div class="row">
        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
          <legend><h4>Acudientes</h4></legend>
          <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
            <div id="divAlumnoActualizarListadoAcudientes"></div>
          </div>
          <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
            <div class="panel panel-default">
              <div class="panel-heading">
                <h3 class="panel-title">Agregar acudiente...</h3>
              </div>
              <div class="panel-body">
                <div class="form-group">
                  <label for="tbAlumnoActualizarAcudiente">Dig&iacute;te parte del nombre:</label>
                  <input type="text" class="form-control" name="tbAlumnoActualizarAcudiente" id="tbAlumnoActualizarAcudiente" onkeyup="javascript:fAlumnoActualizarBuscarAcudiente(this.value);" />
                  <div id="divAlumnoActualizarAcudienteResultado" style="height: 70px;border: 1px;border-style: solid; border-color: #F3F3F3; " class="divAlumnoActualizarAcudienteResultado"></div>
                  <br>
                  <input type="button" class="btn btn-success" name="Agregar acudiente" value="Agregar acudiente" onclick="javascript:fAlumnoAcudienteAgregar();" />
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <!-- Historial de matrículas -->
      <div class="row">
        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
          <legend><h4>Matr&iacute;culas</h4></legend>
          <div class="col-xs-8 col-sm-8 col-md-8 col-lg-8">
            <dir name="divMatriculaListado" id="divMatriculaListado"></dir>
          </div>
          <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
            <div class="panel panel-default">
              <div class="panel-heading">
                <h3 class="panel-title">Crear Matr&iacute;cula</h3>
              </div>
              <div class="panel-body">
                <form action="<? echo $_SERVER['PHP_SELF']; ?>" id="crearMatricula" method="POST" role="form">
                  <div class="form-group">
                    <label for="fecha">Fecha creaci&oacute;n:</label>
                    <input type="text" class="form-control" readonly="readonly" name="fecha" value="<?php echo date("Y/M/d");  ?>" />
                  </div>
                    <div class="form-group">
                        <label for="fecha">Fecha matr&iacute;cula:</label>
                        <input type="date" name="matriculaFecha" id="matriculaFecha" class="form-control" value="" required="required" title="Fecha matr&iacute;cula" placeholder="Seleccione la fecha de la matr&iacute;cula..." />                            
                    </div>
                  
                  <div class="form-group">
                    <label for="matriculaFolio">Folio:</label>
                    <input type="text" class="form-control" name="matriculaFolio" id="matriculaFolio" value=""  placeholder="N&uacute;mero del folio..." />
                  </div>
                  <div class="form-group">
                    <label for="matriculaMatricula">Matr&iacute;cula:</label>
                    <input type="text" class="form-control" name="matriculaMatricula" id="matriculaMatricula" value=""  placeholder="N&uacute;mero de la matr&iacute;cula..." />
                  </div>
                  <div class="form-group">
                    <label for="matriculaIdCurso">Curso:</label>
                    <select name="matriculaIdCurso" id="matriculaIdCurso" class="form-control" required="required">
                      <option value=""></option>
                      <?php
                      $base = new BD();
                      $sql = "select idCurso, curso, ano from curso as c order by c.ano desc, c.orden  asc";
                      $resultado = $base->consultar($sql);
                      if($filas = mysql_fetch_array($resultado))
                      {
                      do {
                      ?>
                      <option value="<? echo $filas['idCurso']; ?>"><? echo $filas['ano']; ?> - <? echo $filas['curso']; ?></option>
                      <?
                      } while ($filas = mysql_fetch_array($resultado));
                      }
                      ?>
                    </select>
                  </div>
                  <input type="hidden" name="matriculaIdAlumno" id="matriculaIdAlumno" value="<?php echo $filaAlumno['idAlumno']; ?>">
                  <button type="button" onclick="fMatriculaCrear();" class="btn btn-success form-control">Crear matr&iacute;cula</button>
                  <br><br>
                  <div id="matriculaMensaje" name="matriculaMensaje"></div>
                </form>
                
              </div>
            </div>
          </div>
        </div>
      </div>
      <!-- FIN - Historial de matrículas -->
      <!-- Historial de cursos -->
      <div class="row">
        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
          <legend><h4>Historial de cursos</h4></legend>
          <div id="divAlumnoCursoListado"></div>
        </div>
      </div>
      <div class="row">
        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
          <legend><h4>Historial de cursos en otras instituciones</h4></legend>
          <div class="row">
            <div class="col-xs-8 col-sm-8 col-md-8 col-lg-8">
              <div id="divAlumnoCursoOtrasInstitucionesEliminar" name="divAlumnoCursoOtrasInstitucionesEliminar"></div>
              <div id="divAlumnoCursoOtrasInstitucionesListado"></div>
            </div>
            <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
           
            <div class="panel panel-default">
                <div class="panel-heading">
                  <h3 class="panel-title">Agregar curso de otras instituciones</h3>
                </div>
                <div class="panel-body">
               
                <form name="formCursoOtrasInstituciones" role="form">
                    <div class="form-group">
                      <label for="cursoOtrasInstitucionesInstitucion">Instituci&oacute;n</label>
                      <input type="text" class="form-control" name="cursoOtrasInstitucionesInstitucion" id="cursoOtrasInstitucionesInstitucion" placeholder="Nombre de la instituci&oacute;n">
                    </div>

                    <div class="form-group">
                      <label for="cursoOtrasInstitucionesAno">A&ntilde;o</label>
                      <input type="number" class="form-control" name="cursoOtrasInstitucionesAno" id="cursoOtrasInstitucionesAno" placeholder="A&ntilde;o">
                    </div>

                    <div class="form-group">
                      <label for="cursoOtrasInstitucionesCurso">Curso</label>
                      <input type="text" class="form-control" name="cursoOtrasInstitucionesCurso" id="cursoOtrasInstitucionesCurso" placeholder="Nombre del curso">
                    </div>

                      <div class="radio-inline ">
                      <label>
                        <input type="radio" name="cursoOtrasInstitucionesPromovido" id="cursoOtrasInstitucionesPromovido1" value="1" checked="checked">
                        Si
                      </label>
                      </div>
                      <div class="radio-inline radio-inline">
                      <label>
                        <input type="radio" name="cursoOtrasInstitucionesPromovido" id="cursoOtrasInstitucionesPromovido2" value="0" >
                        No
                      </label>                      
                    </div>    
                    <input type="hidden" name="cursoOtrasInstitucionesIdAlumno" id="cursoOtrasInstitucionesIdAlumno" class="form-control" value="<? echo $_GET['idAlumno']; ?>">                
                    <button type="button" class="btn btn-success form-control" onclick="fCursoOtrasInstitucionesAgregar();">Agregar curso</button>
                  </form><br>
                  <div name="divAlumnoCursoOtrasInstitucionesAgregarMensaje" id="divAlumnoCursoOtrasInstitucionesAgregarMensaje"></div>
                </div>
              </div>



            </div>
          </div>
        </div>
      </div>      
      <!-- FIN - Historial de cursos -->
    </div>
    <br>
  </div>
</body>
</html>
