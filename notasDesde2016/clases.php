<?
include "Connections/MySQL.php";

date_default_timezone_set('America/Bogota');
setlocale(LC_ALL,"es_CO");



class utils {
	////Lista las materias que tiene asignadas un profesor.
	////Si es un administrador, le muestra todas las materias.
	public function listdoMateriaProfesor($perfil,$usuario,$ano,$funcionJs)
	{
		$filtro_usuario = "";

		if ($perfil != "Administrador"){ $filtro_usuario = " and u.nombre = '".$usuario."' "; }
		
		$sql = 	"
		select 	m.idMateria,
		concat(ar.area,' / ',m.Materia)
		from 	curso as c 
		inner join cursomateriaprofesor as cmp on cmp.idCurso = c.idCurso
		inner join materia as m on m.idMateria = cmp.idMateria
		inner join usuario as u on u.idUsuario = cmp.idProfesor
		inner join area as ar on ar.idArea = m.idArea
		where c.ano = ".$ano."
		".$filtro_usuario."
		group by m.idMateria, m.Materia
		order by ar.orden, m.materia
		"; 
		$this->consultaAListaHtml('idMateria',$sql,'');
	}

	////Lista los cursos de las materias seleccionadas
	////Si es un administrador, le muestra todos los cursos, si es un profesor solo le muestra sus cursos
	public function listadoCursoProfesor ($perfil,$usuario,$ano,$funcionJs)
	{
		$filtro_usuario = "";

		if ($perfil != "Administrador"){ $filtro_usuario = " and u.nombre = '".$usuario."' "; }


		$sql = "
		select	c.idCurso, 
		c.curso 
		from 	curso as c 
		inner join cursomateriaprofesor as cmp on cmp.idCurso = c.idCurso
		inner join materia as m on m.idMateria = cmp.idMateria
		inner join usuario as u on u.idUsuario = cmp.idProfesor
		where c.ano = ".$ano."
		".$filtro_usuario."
		group by c.idCurso, c.curso 
		order by c.orden
		";
		$this->consultaAListaHtml('idCurso',$sql,'');
	}

	///En la sentencia sql el campo 0 debe contener el valor y el campo 1 la descripcion
	private function consultaAListaHtml($idLista,$sql,$funcionJs)
	{
		global $MySQL;
		$rs = mysql_query($sql, $MySQL) or die(mysql_error());
		$filas = mysql_fetch_array($rs);
		$totalFilas = mysql_num_rows($rs);
		
		?>
		<select name="<? echo $idLista; ?>" id="<? echo $idLista; ?>" onchange="<? echo $funcionJs;?>">
			<option></option>
			<?php
			do {  
				?>
				<option value="<?php echo $filas[0]?>"><?php echo $filas[1]?></option>
				<?php
			} while ($filas = mysql_fetch_array($rs));
			$rows = mysql_num_rows($rs);
			if($rows > 0) {
				mysql_data_seek($rs, 0);
				$filas = mysql_fetch_array($rs);
			}
			mysql_free_result($rs);
			?>
		</select>		
		<?		

	}

	public function consutaATablaHtml($sql)
	{
			////Ejecuta la consulta sql
		global $MySQL;
		$rs = mysql_query($sql, $MySQL) or die(mysql_error());
		$filas = mysql_fetch_assoc($rs);
		$totalFilas = mysql_num_rows($rs);	
		if ($totalFilas > 0)
		{

			?>
			<table align="center">
				<tr><td>Registros encontrados: <? echo mysql_num_rows($rs); ?></td></tr>
				<tr><td>
					<table align="center" border="1" cellpadding="3" cellspacing="0">
						<?
						$nombresColumnas = array_keys($filas);
						$this->tablaMostrarFilaDatos($nombresColumnas,1,'No.');
						$i = 1;

						do{
							$this->tablaMostrarFilaDatos($filas,0,$i);
							$i++;
						}while ($filas = mysql_fetch_assoc($rs))
						?>
					</table>
				</td>
			</tr>
		</table>
		<?
	}	
	else
	{
		?><center>No se encontraron datos.</center><?
	}	
			////Extrae los titulos de las columnas y crea la primera fila de la tabla.
			////Empieza a crear registro por registro
}



private function tablaMostrarFilaDatos($Arreglo,$titulo,$numeroFila)
{
	$nombresColumnas = array_keys($Arreglo);
	?><tr <? if($titulo == 1) { ?> align="center" <? } ?> >
	<td align="center">
		<? if($titulo == 1) { ?> <b> <? } ?>
		<? echo $numeroFila; ?>
		<? if($titulo == 1) { ?> </b> <? } ?>
	</td>
	<? 
	for($i = 0; $i < count($Arreglo); $i++ ) 
	{	
		?>
		<td>
			<? if($titulo == 1) { ?> <b> <? } ?>
			<? echo $Arreglo[$nombresColumnas[$i]]; ?>
			<? if($titulo == 1) { ?> </b> <? } ?>
		</td>
		<?	
	} 
	?></tr><?
}

public function listadoProfesor ($perfil,$usuario)
{
	global $MySQL;
	if ($perfil == "Administrador")
	{
		$sql = "select u.idUsuario, u.nombre
		from usuario as u 
		inner join perfil as p on p.idPerfil = u.idPerfil
		and p.perfil = 'Profesor'
		order by u.nombre";				
	}
	else
	{
		$sql = "select u.idUsuario, u.nombre
		from usuario as u 
		inner join perfil as p on p.idPerfil = u.idPerfil
		and u.usuario = '".$usuario."'
		order by u.nombre";				
	}
	$this->consultaAListaHtml('idProfesor',$sql,'');
}

public function	listadoAnosSelect ($nombreLista, $idLista, $onChange, $claseCss)
{
	global $MySQL;
	$sql_1 = "select min(ano) from curso";
	$resultado_1 = mysql_query($sql_1,$MySQL);
	if(mysql_num_rows($resultado_1) > 0)
	{
		$filas_1 = mysql_fetch_array($resultado_1);
		$anos = $filas_1[0];
	}

	if ($anos < 2010 || $anos > 2100)
	{
		$anos = 2010;	
	}
	?>
	<select name="<? echo $nombreLista; ?>" id="<? echo $idLista; ?>" class="<? echo $claseCss; ?>" onchange="<? echo $onChange; ?>" >

		<option value="">&nbsp;</option>
		<option value="TODOS">
			TODOS
		</option>            
		<?
		for($i = $anos ; $i <= ($anos + 20); $i++)
		{
			?>
			<option value="<? echo $i; ?>"><? echo $i; ?></option>
			<?
		}
		?>
	</select>
	<?
}
}

///Eliminacion Listado Insercion Actualizacion ajaX
class eliax {
	//global $MySQL;
	public $tabla = '';
	
	
	//public function 
	
	
	public function agregarRegistroMostrarFormulario ()
	{
		global $MySQL;
		$sql = "
		SELECT table_name, 
		column_name, 
		is_nullable, 
		data_type,
		character_maximum_length, 
		column_comment, 
		column_key,
		extra  
		from 	information_schema.COLUMNS 
		where 	table_schema 	= database() 
		and table_name 		= '".$this->tabla."'
		order by information_schema.COLUMNS.ORDINAL_POSITION asc			
		";
		if ($MySQL)
		{
			if($resultado = mysql_query($sql,$MySQL))
			{

				?>
				<div class="titulo_actividad">:: Agregar registro: (<? echo $this->tabla; ?>)</div><br>
				<form name="f_eliax_agregar_resgistro" id="id_f_eliax_agregar_resgistro">
					<table border="1" cellpadding="0" cellspacing="0">
						<?
						if($filas = mysql_fetch_array($resultado))
						{
							do {
								if ($filas['column_key'] != 'PRI')
								{
									?>
									<tr>
										<td><? echo $filas['column_comment']; ?></td>
										<td><? $this->mostrarObjetoHtml($filas['data_type'],$filas['column_name'],$filas['table_name'],$filas['character_maximum_length'],'idfa'); ?></td>
									</tr>
									<?
								}
							} while ($filas = mysql_fetch_array($resultado));
							?>
							<tr><td colspan="2" align="center"><input type="button" name="Guardar" value="Guardar" onClick="eliaxGuardarRegistro('<? echo $this->tabla; ?>')"></td></tr>
						</table>
					</form><br>
					<div class="titulo_actividad">&nbsp;</div>
					<?
				}
				else
				{
					$this->mostrarErrorSql ($sql);
				}					
			}
			else
			{
				$this->mostrarErrorSql ('');
			}					
		}
		else
		{
			$this->informarError('No hay conexion con la base de datos.');
		}
	}

	function mostrarObjetoHtml ($tipoDato,$nombre,$tabla,$tamano,$tipoId)
	{
		global $MySQL;
		switch ($tipoDato)
		{
			case 'int':
			$sql = 	"
			select 	a.TABLE_SCHEMA, 
			a.TABLE_NAME, 
			b.COLUMN_NAME, 
			b.REFERENCED_TABLE_NAME, 
			b.REFERENCED_COLUMN_NAME
			from 	information_schema.TABLE_CONSTRAINTS as a ,
			information_Schema.KEY_COLUMN_USAGE as b
			where 	a.table_schema 		= database() 
			and a.table_name		= '".$tabla."' 
			and a.constraint_type 	= 'FOREIGN KEY' 
			and a.CONSTRAINT_NAME 	= b.CONSTRAINT_NAME	
			and b.COLUMN_NAME		= '".$nombre."'
			";
			if ($MySQL)
			{
				if ($resultado = mysql_query($sql,$MySQL))
				{
					if(mysql_num_rows($resultado) > 0)
					{
						if ($filas = mysql_fetch_array($resultado))
						{
							$this->crearListaHtml($filas['REFERENCED_TABLE_NAME'],$filas['REFERENCED_COLUMN_NAME'],$filas['COLUMN_NAME'],$tipoId);
						}
						else
						{
							$this->mostrarErrorSql ($sql);
						}								
					}
					else
					{
						?>
						<input type="text" name="<? echo $nombre; ?>" id="<? echo $tipoId."_".$nombre; ?>" size="10" >
						<?								
					}
				}
				else
				{
					$this->mostrarErrorSql ('');
				}						
			}
			else
			{
				$this->informarError('No hay conexion con la base de datos.');
			}
			break;
			
			case 'varchar':
			if ($tamano <= 100)
			{
				?><input type="text" name="<? echo $nombre; ?>" id="<? echo $tipoId."_".$nombre;?>"><?	
			}
			else
			{
				?><textarea name="<? echo $nombre; ?>" id="<? echo $tipoId."_".$nombre;?>"></textarea><?
			}
			break;
			
			case 'tinyint':
			?><select name="<? echo $nombre; ?>" id="<? echo $tipoId."_".$nombre;?>"><option value="1">Si</option><option value="0">No</option></select><?
			break;
			
			case 'timestamp'
			?><input type="text" name="<? echo $nombre; ?>" id="<? echo $tipoId."_".$nombre;?>"><?
			break;
			
			default:
			echo $tipoDato;
			break;
		}
	}

	function crearListaHtml ($tabla,$campo,$nombreCampo,$tipoId) //tipo idfa_ para el id en los campos del formulario de insersion
	{
		global $MySQL;
		$sql = "select ".$tabla.",".$campo." from ".$tabla." order by ".$tabla;
		if ($MySQL)
		{
			if($resultado = mysql_query($sql,$MySQL))
			{
				?>
				<select name="<? echo $nombreCampo; ?>" id="<? echo $tipoId; ?>_<? echo $nombreCampo; ?>">
					<?
					if($filas = mysql_fetch_array($resultado))
					{
						do {
							?>
							<option value="<? echo $filas[$campo]; ?>"><? echo $filas[$tabla]; ?></option>
							<?
						} while ($filas = mysql_fetch_array($resultado));
						?>
					</select>
					<?
				}
				else
				{
					$this->mostrarErrorSql ($sql);
				}					
			}
			else
			{
				$this->mostrarErrorSql ('');
			}					
		}
		else
		{
			$this->informarError('No hay conexion con la base de datos.');
		}		
	}
	
	function mostrarErrorSql ($sql)
	{
		global $MySQL;
		?><div class="error"><b>Error: </b><? echo mysql_error($MySQL); if ($sql) { ?><br><b>Consulta: </b><? echo $sql; } ?></div><?
	}

	function informarError ($error)
	{
		?><div class="error"><b>Error: </b><? echo $error; ?></div><?
	}		

	function listar ()
	{
		//seleccionar los campos que no sean llave foranea y sus tipos
		
		//
		
	}
	
	
}

class BD 
{
	
	function consultar($sql)
	{
		global $MySQL;
		$retorno = false;
		
		if($resultado = mysql_query($sql,$MySQL))
		{
			$retorno = $resultado;
		}
		else
		{
			?>
			<div class="alert alert-danger">Error al realizar la consulta... <? echo mysql_error(); ?></div>
			<?	
		}
		return $retorno;			
	}
	
	function consultarUnaFila($sql)
	{
		$resultado = $this->consultar($sql);
		
		if($filas = mysql_fetch_array($resultado))
		{
			$retorno = $filas;
			$this->liberarMemoria($resultado);
		}
		else
		{
			?>
			<div class="alert alert-danger">Error al leer el resultado de la base de datos... <? echo mysql_error(); ?></div>
			<?	
		}
		return $retorno;
	}
	
	function consultarUnValor($sql)
	{
		$resultado = $this->consultar($sql);
		if (mysql_num_rows($resultado) > 0)
		{
			$filas = mysql_fetch_array($resultado);
			$retorno = $filas[0];
			$this->liberarMemoria($resultado);				
		}
		else
		{
			unset($retorno);
		}
		return $retorno;
	}
	
	function liberarMemoria ($resultado)
	{
		mysql_free_result($resultado);	
	}

	function consultaATabla ($sql)
	{
		$base = new BD();
		$mensaje = new Mensaje();
		$resultado = $base->consultar($sql);
		
		if(mysql_num_rows($resultado)> 0)
		{
			$filas = mysql_fetch_array ($resultado);
			?>
			<table class="table table-hover table-striped table-condensed">
				<?
				$cont_0 = 1;
				//imprime los titulos
				?>
				<thead>
					<tr>
						<th>No.</th>
						<?
						$llaves = array_keys($filas);
						while ($cont_0 < count($llaves))
						{
							?>
							<th><? echo $llaves[$cont_0];?></th>
							<?
							$cont_0 = $cont_0 +2;
						}
						?>
					</tr>
				</thead>
				<tbody>
					<?
					$contadorFilas = 1;
					//Imprime las filas
					do {
						$cont_1 = 0;
						?>
						<tr>
							<td><? echo $contadorFilas++; ?></td>

							<?
							while ($cont_1 < count($llaves)/2)
							{
								?>
								<td><? echo $filas[$cont_1];?></td>
								<?
								$cont_1++;
							}
							?></tr><?
						}while ($filas = mysql_fetch_array ($resultado));				
					}
					else
					{
						$mensaje->informacion("No se encontraron registros...");
					}
					?>
				</tbody>
			</table>
			<?
		}	

	



	}

	
	class Mensaje {

		function correcto($error)		{ ?><div class="alert alert-success"><? echo $error; ?></div><? }
		function informacion($error)	{ ?><div class="alert alert-info"><? echo $error; ?></div><? }
		function alerta($error)			{ ?><div class="alert alert-warning"><? echo $error; ?></div><? }
		function error($error)			{ ?><div class="alert alert-danger"><? echo $error; ?></div><? }
	}

	class Notas {

		function notaFinalBimestre ($idAlumno,$idCurso,$idMateria,$bimestre){
			global $MySQL;
			$sql = "
			select 
			ifnull(((((nota1 + nota2 +	nota3 +	nota4 +	nota5 +	nota6 )
				/
				(case when nota1 > 0 then 1 else 0 end 	
					+ case when nota2 > 0 then 1 else 0 end 
					+ case when nota3 > 0 then 1 else 0 end 
					+ case when nota4 > 0 then 1 else 0 end 
					+ case when nota5 > 0 then 1 else 0 end 
					+ case when nota6 > 0 then 1 else 0 end 	
				)) * 40 )/100),0)
			+ifnull(((((nota7 + nota8 + nota9 +	nota10 + nota11 + nota12 )
				/
				(case when nota7 > 0 then 1 else 0 end 	
				+ case when nota8 > 0 then 1 else 0 end 	
				+ case when nota9 > 0 then 1 else 0 end 	
				+ case when nota10 > 0 then 1 else 0 end 	
				+ case when nota11 > 0 then 1 else 0 end 	
				+ case when nota12 > 0 then 1 else 0 end 	
				)) * 30 )/100),0)
			+ifnull(((((nota13 + nota14 )
				/
				(case when nota13 > 0 then 1 else 0 end 	
				+ case when nota14 > 0 then 1 else 0 end 
				)) * 10 )/100),0) 
			+ifnull(((((nota15 )
				/
				(case when nota15 > 0 then 1 else 0 end 	
				)) * 20 )/100),0) 			
			as nota, recuperacion as recuperacion
				from 	boletin 
				where 	idAlumno = ".$idAlumno."
				and idCurso = ".$idCurso."
				and idMateria = ".$idMateria."
				and bimestre = ".$bimestre."
				";
				$resultado = mysql_query($sql,$MySQL);
				if ($filas = mysql_fetch_array($resultado))
				{
					$nota = $filas['nota'];
					$recuperacion = $filas['recuperacion'];
				}
				else
				{
					$nota = 0;
					$recuperacion = 0;
				}		

				if($nota > $recuperacion)
				{
					$notaBimestre = $nota;
				}
				else
				{
					$notaBimestre = $recuperacion;
				}

				return $notaBimestre;
			}
}
?>
