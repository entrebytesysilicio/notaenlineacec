ALTER TABLE `boletin`
	DROP INDEX `UK_boletin`,
	ADD UNIQUE INDEX `UK_boletin` (`idAlumno`, `idCurso`, `idMateria`, `bimestre`);