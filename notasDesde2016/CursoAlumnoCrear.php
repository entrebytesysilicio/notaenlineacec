<?php require_once('Connections/MySQL.php'); ?>
<?php
if (!isset($_SESSION)) {
  session_start();
}
$MM_authorizedUsers = "Administrador,Profesor";
$MM_donotCheckaccess = "false";

// *** Restrict Access To Page: Grant or deny access to this page
function isAuthorized($strUsers, $strGroups, $UserName, $UserGroup) { 
  // For security, start by assuming the visitor is NOT authorized. 
  $isValid = False; 

  // When a visitor has logged into this site, the Session variable MM_Username set equal to their username. 
  // Therefore, we know that a user is NOT logged in if that Session variable is blank. 
  if (!empty($UserName)) { 
    // Besides being logged in, you may restrict access to only certain users based on an ID established when they login. 
    // Parse the strings into arrays. 
    $arrUsers = Explode(",", $strUsers); 
    $arrGroups = Explode(",", $strGroups); 
    if (in_array($UserName, $arrUsers)) { 
      $isValid = true; 
    } 
    // Or, you may restrict access to only certain users based on their username. 
    if (in_array($UserGroup, $arrGroups)) { 
      $isValid = true; 
    } 
    if (($strUsers == "") && false) { 
      $isValid = true; 
    } 
  } 
  return $isValid; 
}

$MM_restrictGoTo = "error.php";
if (!((isset($_SESSION['MM_Username'])) && (isAuthorized("",$MM_authorizedUsers, $_SESSION['MM_Username'], $_SESSION['MM_UserGroup'])))) {   
  $MM_qsChar = "?";
  $MM_referrer = $_SERVER['PHP_SELF'];
  if (strpos($MM_restrictGoTo, "?")) $MM_qsChar = "&";
  if (isset($QUERY_STRING) && strlen($QUERY_STRING) > 0) 
  $MM_referrer .= "?" . $QUERY_STRING;
  $MM_restrictGoTo = $MM_restrictGoTo. $MM_qsChar . "accesscheck=" . urlencode($MM_referrer);
  header("Location: ". $MM_restrictGoTo); 
  exit;
}
?>
<?php
if (!function_exists("GetSQLValueString")) {
function GetSQLValueString($theValue, $theType, $theDefinedValue = "", $theNotDefinedValue = "") 
{
  if (PHP_VERSION < 6) {
    $theValue = get_magic_quotes_gpc() ? stripslashes($theValue) : $theValue;
  }

  $theValue = function_exists("mysql_real_escape_string") ? mysql_real_escape_string($theValue) : mysql_escape_string($theValue);

  switch ($theType) {
    case "text":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;    
    case "long":
    case "int":
      $theValue = ($theValue != "") ? intval($theValue) : "NULL";
      break;
    case "double":
      $theValue = ($theValue != "") ? doubleval($theValue) : "NULL";
      break;
    case "date":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;
    case "defined":
      $theValue = ($theValue != "") ? $theDefinedValue : $theNotDefinedValue;
      break;
  }
  return $theValue;
}
}

mysql_select_db($database_MySQL, $MySQL);
$query_rsCusro = "SELECT * FROM curso ORDER BY ano DESC, curso asc";
$rsCusro = mysql_query($query_rsCusro, $MySQL) or die(mysql_error());
$row_rsCusro = mysql_fetch_assoc($rsCusro);
$totalRows_rsCusro = mysql_num_rows($rsCusro);
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>

<title>:: Curso Alumno ::</title>
<? include "header.php"; ?>
</head>

<body onload="fCursoAlumnoListarAlumnos ();">
<? include "menu.php"; ?>
<script language="javascript" type="text/javascript">
function fCursoAlumnoListarAlumnos ()
	{
	var idCurso;
	idCurso = document.getElementById('slCursoAlumnoCurso');
	idCurso = idCurso.options[idCurso.selectedIndex].value;
	mostrar_url('CursoAlumnoListar.php','idCurso='+idCurso,'divCursoAlumnoPrincipal','get');	
	}
function fCursoAlumnoBuscar (nombre)
	{
	mostrar_url('Parametros.php','accion=buscaralumnos&alumno='+nombre,'divCursoAlumnoAlumnoBuscado','get');	
	}	
function fCursoAlumnoAgregar ()
	{
	var idCurso,idAlumno;
	idCurso = document.getElementById('slCursoAlumnoCurso');
	idCurso = idCurso.options[idCurso.selectedIndex].value;	
	idAlumno =document.getElementById('slCursoAlumnoAlumnosEncontrados');
	idAlumno = idAlumno.options[idAlumno.selectedIndex].value;	
	mostrar_url('CursoAlumnoAgregar.php','MM_insert=form1&idAlumno='+idAlumno+'&idCurso='+idCurso,'divCursoAlumnoPrincipal','get');
	setTimeout('fCursoAlumnoListarAlumnos ()', 2*1000); //segundos * mil, el parametro es en ms es decir 5 * 1000 = 5 segundos
	}

function fCursoAlumnoEliminar(idAlumnoCurso)
	{
		var desicion;
		desicion = confirm("Desea eliminar el registro?");
		if (desicion)
			{
		mostrar_url('CursoAlumnoEliminar.php','idAlumnoCurso='+idAlumnoCurso,'divCursoAlumnoPrincipal','get');
				}
	setTimeout('fCursoAlumnoListarAlumnos ()', 2*1000); //segundos * mil, el parametro es en ms es decir 5 * 1000 = 5 segundos	 
	}

</script>
<table border="0">
<thead>
<tr><td colspan="3">Cursos / Alumnos</td></tr>
</thead>
<tr valign="top"><td>Curso:</td><td>
<select id="slCursoAlumnoCurso" name="slCursoAlumnoCurso">
  <?php
do {  
?>
  <option value="<?php echo $row_rsCusro['idCurso']?>"<?php if (!(strcmp($row_rsCusro['idCurso'], $_GET['idCurso']))) {echo "selected=\"selected\"";} ?>><?php echo $row_rsCusro['ano']?> - <?php echo $row_rsCusro['curso']?></option>
  <?php
} while ($row_rsCusro = mysql_fetch_assoc($rsCusro));
  $rows = mysql_num_rows($rsCusro);
  if($rows > 0) {
      mysql_data_seek($rsCusro, 0);
	  $row_rsCusro = mysql_fetch_assoc($rsCusro);
  }
?>
</select>
</td>
<td rowspan="2">Alumnos encontrados:<br />
<div id="divCursoAlumnoAlumnoBuscado" class="divCursoAlumnoAlumnoBuscado"></div></td>
</tr>
<tr valign="top"><td>Buscar alumno:</td><td><input type="text" name="tbCursoAlumnoAlumno" id="tbCursoAlumnoAlumno" onkeyup="fCursoAlumnoBuscar(this.value);"/></td></tr>

<thead>
<tr><td colspan="3"><input type="button" name="Listar alumnos" value="Listar alumnos" onclick="javascript:fCursoAlumnoListarAlumnos();" />
<input type="button" name="Agregar alumno seleccionado" value="Agregar alumno seleccionado" onclick="javascript:fCursoAlumnoAgregar();" />
</td></tr>
</thead>
</table><br />


<div id="divCursoAlumnoPrincipal" ></div>
</body>
</html>
<?php
mysql_free_result($rsCusro);
?>
