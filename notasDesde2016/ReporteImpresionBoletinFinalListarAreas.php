<?php require_once('Connections/MySQL.php'); ?>
<?php require_once('InformeValorativoClase.php'); ?>
<?php

$cantidadDecimales = 1;

//header('Content-type: application/vnd.ms-excel');

//header('Content-type: application/msword');
ini_set('display_errors', 0);

if (!isset($_SESSION)) {
	session_start();
}
$MM_authorizedUsers = "Administrador,Profesor,Padre";
$MM_donotCheckaccess = "false";

// *** Restrict Access To Page: Grant or deny access to this page
function isAuthorized($strUsers, $strGroups, $UserName, $UserGroup) { 
  // For security, start by assuming the visitor is NOT authorized. 
	$isValid = False; 

  // When a visitor has logged into this site, the Session variable MM_Username set equal to their username. 
  // Therefore, we know that a user is NOT logged in if that Session variable is blank. 
	if (!empty($UserName)) { 
    // Besides being logged in, you may restrict access to only certain users based on an ID established when they login. 
    // Parse the strings into arrays. 
		$arrUsers = Explode(",", $strUsers); 
		$arrGroups = Explode(",", $strGroups); 
		if (in_array($UserName, $arrUsers)) { 
			$isValid = true; 
		} 
    // Or, you may restrict access to only certain users based on their username. 
		if (in_array($UserGroup, $arrGroups)) { 
			$isValid = true; 
		} 
		if (($strUsers == "") && false) { 
			$isValid = true; 
		} 
	} 
	return $isValid; 
}

$MM_restrictGoTo = "error.php";
if (!((isset($_SESSION['MM_Username'])) && (isAuthorized("",$MM_authorizedUsers, $_SESSION['MM_Username'], $_SESSION['MM_UserGroup'])))) {   
	$MM_qsChar = "?";
	$MM_referrer = $_SERVER['PHP_SELF'];
	if (strpos($MM_restrictGoTo, "?")) $MM_qsChar = "&";
	if (isset($QUERY_STRING) && strlen($QUERY_STRING) > 0) 
		$MM_referrer .= "?" . $QUERY_STRING;
	$MM_restrictGoTo = $MM_restrictGoTo. $MM_qsChar . "accesscheck=" . urlencode($MM_referrer);
	header("Location: ". $MM_restrictGoTo); 
	exit;
}

if($_GET['pdf'])
{
	?>
	<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
	<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf8" />
		<!--<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />-->
		<link href="estilo_boletin.css" rel="stylesheet" type="text/css" />
		<link rel="stylesheet" type="text/css" href="estilo_boletin.css"/>
		<style type="text/css">
		<!--
		.t1 {
			text-align: center;
			font-size: 9px;
		}
		-->
		</style>
	</head>
	<body onload="window.print();">
		<?	
	}
	
	//$sql10 = "select valor from parametro where parametro = 'bimestreActual'";
	//$resultado10 = mysql_query($sql10,$MySQL);
	//$filas10 = mysql_fetch_array($resultado10);
	
	//$bimestreActual = $filas10['valor'];
	$bimestreActual = $_GET['bimestre'];
	
	$sql9 = "select distinct boletin.idAlumno from boletin inner join alumno on alumno.idAlumno = boletin.idAlumno  where idCurso = ".$_GET['idCurso']." order by alumno.apellidos ";
	$resultado9 = mysql_query($sql9,$MySQL);
	$filas9 = mysql_fetch_array($resultado9);
	do{
		
		$iv = new EstudianteInformeValorativo();
		$iv->idAlumno = $filas9['idAlumno'];
		$iv->idCurso = $_GET['idCurso'];
		
		
		?>
		<center>
			<div class="tamanoCarta">
				<table width="100%">
					<tr>
						<td align="left"><? //$im = @imagecreatefromjpeg("imagenes/logo.tiff"); imagejpeg($im); ?>
							<img src="imagenes/logo.png" height="86px" /></td>
							<td width="100%" align="center"><H3>HERMANAS MERCEDARIAS DEL SANTISIMO SACRAMENTO<br />
								COLEGIO EUCAR&Iacute;STICO CAMPESTRE </H3>
								<h4>INFORME VALORATIVO<br />
									<? echo $_GET['ano']; ?></H4></td>
									<td align="right">&nbsp;</td>
								</tr>
							</table>
							<p style="width:1px"></p>
							<table border="0" width="100%" cellpadding="1" cellspacing="1">
								<tr>
									<td colspan="9"></td>
								</tr>
								<tr>
									<td><b>Estudiante:</b></td>
									<td class="bordeBoletin"><? $iv->nombreEstudiante(); ?></td>
									<td><b>Curso:</b></td>
									<td class="bordeBoletin"  align="center"><? $iv->nombreCurso(); ?></td>
									<td><b>Promedio:</b></td>
									<td class="bordeBoletin" align="center"><?

		////busca el promedio del alumno en el periodo 
		////si el periodo es 5, se debe buscar el promedio durante todo el a�o escolar
									$sql7 = "
									select 	b.idAlumno,
									avg(
										ifnull(((((nota1 +
											nota2 +
											nota3 +
											nota4 +
											nota5 +
											nota6 )/

(
	case when nota1 > 0 then 1 else 0 end 	
	+
	case when nota2 > 0 then 1 else 0 end 	
	+
	case when nota3 > 0 then 1 else 0 end 	
	+
	case when nota4 > 0 then 1 else 0 end 	
	+
	case when nota5 > 0 then 1 else 0 end 	
	+
	case when nota6 > 0 then 1 else 0 end 	
	)
) * 40 )/100),0)
+
ifnull(((((nota7 +
	nota8 +
	nota9 +
	nota10 +
	nota11 +
	nota12 )/
(
	case when nota7 > 0 then 1 else 0 end 	
	+
	case when nota8 > 0 then 1 else 0 end 	
	+
	case when nota9 > 0 then 1 else 0 end 	
	+
	case when nota10 > 0 then 1 else 0 end 	
	+
	case when nota11 > 0 then 1 else 0 end 	
	+
	case when nota12 > 0 then 1 else 0 end 	
	)						

) * 30 )/100),0)
+
ifnull(((((nota13 +
	nota14 )/
(
	case when nota13 > 0 then 1 else 0 end 	
	+
	case when nota14 > 0 then 1 else 0 end 							
	)
) * 10 )/100),0)
+
ifnull((((nota15/
(
	case when nota15 > 0 then 1 else 0 end 							
	)
) * 20 )/100),0)
) 

as promedio	

from 	boletin as b

where 		b.idCurso = ".$_GET['idCurso']."
and b.bimestre = ".$bimestreActual."	
group by 	b.idAlumno order by 2 desc";

$resultado7 = mysql_query($sql7,$MySQL);
$filas7 = mysql_fetch_array($resultado7);
$vandera = 0;
$puesto = 1;
do {
				////si el alumno buscado es igual al alumno que se esta recorriendo en el cursor, entonces guardo las variables para poder mostrarlas luego.
	if ( $filas9['idAlumno'] == $filas7['idAlumno'] )
	{
		$puesto_alumno = $puesto;
					////Se coloca la vandera, por si el programa encuentra al alumno, termine la ejecucion del ciclo
		$vandera = 1;
		$promedio = $filas7['promedio']; 
	}
	$puesto++;
}while( ($filas7 = mysql_fetch_array($resultado7)) and $vandera == 0);
	////muestra el promedio.
	//echo substr($promedio,0,4);
echo number_format($promedio,$cantidadDecimales);
	//echo "<br>".$sql7;
?></td>
<td><b>Puesto:</b></td>
<td  class="bordeBoletin" align="center"><? 
		////Muestra el puesto que ocupo el alumno.
echo $puesto_alumno; 
?></td>
<td></td>
</tr>
<tr>
	<td colspan="9"></td>
</tr>
<tr>
	<td><b>Director:</b></td>
	<td class="bordeBoletin"><? $iv->nombreDirectorCurso (); ?></td>
	<td><b>Periodo:</b></td>
	<? ////Imprime el nombre del periodo. ?>
	<td class="bordeBoletin"  align="center"><? switch ($bimestreActual) { case 1: echo "Primero"; break; case 2: echo "Segundo"; break; case 3: echo "Tercero"; break; case 4: echo "Final"; break; case 5: echo "Final"; break; } ?></td>
	<td>&nbsp;</td>
	<td>&nbsp;</td>
	<td>&nbsp;</td>
	<td>&nbsp;</td>
	<td>&nbsp;</td>
</tr>
<tr>
	<td colspan="9"></td>
</tr>
</table>
<p style="width:1px"></p>
<table border="1" width="100%">
	<?

	?>
	<tr valign="middle"  align="center"  class="tablaBoletin1">
		<td rowspan="2"><strong>&Aacute;rea</strong></td>
		<td rowspan="2"><strong>Asignatura</strong></td>
		<td rowspan="2"><strong>IH</strong></td>
		<td rowspan="2"><strong>Fallas*</strong></td>
		<td colspan="8"><strong>PERIODOS</strong></td>
		<td rowspan="2" ><strong>Acumulado</strong></td>
		
	</tr>
	<tr valign="middle" align="center"  class="tablaBoletin1">
		<td colspan="2"><strong>1</strong></td>
		<td colspan="2" ><strong>2</strong></td>
		<td colspan="2" ><strong>3</strong></td>
		<td colspan="2" ><strong>4</strong></td>

	</tr>
	<tr align="center">
		<td colspan="4">
		</td>
		<td>V</td>
		<td>R</td>      
		<td>V</td>
		<td>R</td>      
		<td>V</td>
		<td>R</td>      
		<td>V</td>
		<td>R</td>
		<td></td>
	</tr>
	<?	
	
	////determina la cantidad de materias por area
	/*
	Se cambia debido a que cuando un curso tiene mas de una materia, se expresa mal el resultado.
	$sql11 = "
	select count(1) as materias, a.Area 
	from cursomateriaprofesor as cmp 
	inner join materia as m on m.idMateria = cmp.idMateria
	inner join area as a on m.idArea = a.idArea
	where cmp.idCurso = ".$_GET['idCurso']."
	group by a.Area
	order by a.orden
	";
	*/
	$sql11 = "
	select count(1) as materias, a.Area, a.idArea
	from  materia as m
	inner join area as a on m.idArea = a.idArea
	where m.idMateria in (select idMateria from cursomateriaprofesor as cmp where  cmp.idCurso = ".$_GET['idCurso'].")
	group by a.Area
	order by a.orden
	";
	$resultado11 = mysql_query($sql11,$MySQL);
	if($filas11 = mysql_fetch_array($resultado11))
	{
		do {
			$areas[$filas11['Area']] = $filas11['materias'];
		}while($filas11 = mysql_fetch_array($resultado11));
	}
	else
	{
		echo mysql_error();
	}
	
	////identifica la intensidad horaria de cada una de las materias que se dictan en determinado curso
	////
	////Recorre la consulta para sacar el resto de la informacion, llendo ordenado por el nombre de las materias
	////Por cada materia imprime el nombre, coloca la intensidad horaria, y saca los promedios y desempe�os semestre a semestre
	$sql1 = "
	select sum(cmp.intensidadHoraria) as IH, m.materia, m.idMateria, a.Area , a.idArea
	from cursomateriaprofesor as cmp 
	inner join materia as m on m.idMateria = cmp.idMateria
	inner join area as a on m.idArea = a.idArea
	where cmp.idCurso = ".$_GET['idCurso']."
	group by  m.materia, m.idMateria, a.Area
	order by a.orden, m.materia
	";
	mysql_select_db($database_MySQL, $MySQL);					
	$resultado1 = mysql_query($sql1,$MySQL);
	if($filas1 = mysql_fetch_array($resultado1))
	{
		$areaActual = "";
unset($promedioArea);
unset($areasPerdidas);
		do {
			//$materiasPerdidas = array();
                                if($areaActual != "" && $areaActual != $filas1['Area'] ) {
?>


<tr>
<td colspan="4" align="right">
<b>Total &aacute;rea: &nbsp;&nbsp;</b>
</td>
<?
$sqlNotaAreaSemestre = "
select sum(notaArea) as notaArea  
from vBoletin 
where idCurso = ".$iv->idCurso." 
and bimestre = 1 
and area = '".$areaActual."'
and idAlumno = ".$iv->idAlumno." 
group by idCurso, bimestre, idArea, idAlumno";

$notaArea = "0";
mysql_select_db($database_MySQL, $MySQL);					
$resultadoNotaAreaSemestre = mysql_query($sqlNotaAreaSemestre,$MySQL);
if($filasNotaAreaSemestre = mysql_fetch_array($resultadoNotaAreaSemestre))
	{ 
		$notaArea = number_format($filasNotaAreaSemestre['notaArea'],$cantidadDecimales);
	}
?>
<td align="right" style="font-weight: bold;" <? if((float)$notaArea < 3.5 ) { ?> style="color:red" <?} ?>>
<? echo $notaArea; ?>
</td>
<td align="right"></td>

<?

$sqlNotaAreaSemestre = "
select sum(notaArea) as notaArea  
from vBoletin 
where idCurso = ".$iv->idCurso." 
and bimestre = 2
and area = '".$areaActual."'
and idAlumno = ".$iv->idAlumno." 
group by idCurso, bimestre, idArea, idAlumno";

$notaArea = "0";
mysql_select_db($database_MySQL, $MySQL);					
$resultadoNotaAreaSemestre = mysql_query($sqlNotaAreaSemestre,$MySQL);
if($filasNotaAreaSemestre = mysql_fetch_array($resultadoNotaAreaSemestre))
	{ 
		$notaArea = number_format($filasNotaAreaSemestre['notaArea'],$cantidadDecimales);
	}
?>
<td align="right" style="font-weight: bold;" <? if((float)$notaArea < 3.5 ) { ?> style="color:red" <?} ?>>
<? echo $notaArea; ?>
</td>
<td align="right"></td>

<?
$sqlNotaAreaSemestre = "
select sum(notaArea) as notaArea  
from vBoletin 
where idCurso = ".$iv->idCurso." 
and bimestre = 3 
and area = '".$areaActual."'
and idAlumno = ".$iv->idAlumno." 
group by idCurso, bimestre, idArea, idAlumno";

$notaArea = "0";
mysql_select_db($database_MySQL, $MySQL);					
$resultadoNotaAreaSemestre = mysql_query($sqlNotaAreaSemestre,$MySQL);
if($filasNotaAreaSemestre = mysql_fetch_array($resultadoNotaAreaSemestre))
	{ 
		$notaArea = number_format($filasNotaAreaSemestre['notaArea'],$cantidadDecimales);
	}
?>
<td align="right" style="font-weight: bold;" <? if((float)$notaArea < 3.5 ) { ?> style="color:red" <?} ?>>
<? echo $notaArea; ?>
</td>
<td align="right"></td>

<?
$sqlNotaAreaSemestre = "
select sum(notaArea) as notaArea  
from vBoletin 
where idCurso = ".$iv->idCurso." 
and bimestre = 4
and area = '".$areaActual."'
and idAlumno = ".$iv->idAlumno." 
group by idCurso, bimestre, idArea, idAlumno";

$notaArea = "0";
mysql_select_db($database_MySQL, $MySQL);					
$resultadoNotaAreaSemestre = mysql_query($sqlNotaAreaSemestre,$MySQL);
if($filasNotaAreaSemestre = mysql_fetch_array($resultadoNotaAreaSemestre))
	{ 
		$notaArea = number_format($filasNotaAreaSemestre['notaArea'],$cantidadDecimales);
	}
?>
<td align="right" style="font-weight: bold;" <? if((float)$notaArea < 3.5 ) { ?> style="color:red" <?} ?>>
<? echo $notaArea; ?> 
</td>
<td align="right"></td>



<?
$sqlNotaAreaSemestre = "select avg(notaArea) as notaArea from (
select sum(notaArea) as notaArea  
from vBoletin 
where idCurso = ".$iv->idCurso." 
and area = '".$areaActual."'
and idAlumno = ".$iv->idAlumno." 
group by idCurso, bimestre, idArea, idAlumno, bimestre) datos";
$notaArea = "0";
mysql_select_db($database_MySQL, $MySQL);					
$resultadoNotaAreaSemestre = mysql_query($sqlNotaAreaSemestre,$MySQL);
if($filasNotaAreaSemestre = mysql_fetch_array($resultadoNotaAreaSemestre))
	{ 
		$notaArea = number_format($filasNotaAreaSemestre['notaArea'],$cantidadDecimales);
	}
?>
<td align="center" style="font-weight: bold;" <? if((float)$notaArea < 3.5 ) { ?> style="color:red" <?} ?>>
<? echo $notaArea; ?>
</td>


</tr>

<!-- <tr valign="top" align="center">
<td colspan="13"> <b>Promedio del &aacute;rea: -->
<?
////Imprime el promedio del área 
// $promedioAreaActual = 0;
// $promedioAreaActual = number_format(array_sum($promedioArea[$areaActual])/count($promedioArea[$areaActual]),2); 
// echo $promedioAreaActual; 

// if($promedioAreaActual < 3.5 ) {
// $areasPerdidas++;
// }

?>
<!-- 
</b>
</td>
</tr>
-->



<?
                                }
?>
			<tr valign="top" align="center">
				<?


			  ////Imprime el nombre del area
				if ($areaActual != $filas1['Area'])
				{
					?>
					<td align="left" rowspan="<? echo $areas[$filas1['Area']];?>" ><b>
						<?
						echo $filas1['Area']; 
						$areaActual = $filas1['Area'];			
						?>
					</b>


					
					</td>
					<?
				}
				?>
				<td align="left" ><? 
            ////Imprime el nombre de la materia
				echo $filas1['materia']; 
				?></td>
				<td><? 
            ////Imprime la intensidad horaria de la materia
				echo $filas1['IH']; 
				?></td>
				<?
				
				$sql21 = "select sum(case when bimestre = ".$bimestreActual." then fallas else 0 end) fallas, sum(fallas) as fallasAcumuladas				
				from boletin where idAlumno = ".$filas9['idAlumno']." and idCurso = ".$_GET['idCurso']." and idMateria = ".$filas1['idMateria'] ;
				
				$resultado21 = mysql_query($sql21,$MySQL);
				if ($filas21 = mysql_fetch_array($resultado21))
				{
					if($filas21['fallas']>0 || $filas21['fallasAcumuladas'] > 0)
					{
						$fallas = $filas21['fallas']." (".$filas21['fallasAcumuladas'].")";	
					}
					else
					{
						$fallas = "";	
					}
					
				}						
				?>
				<td><? 
						////Muestra las fallas
				echo $fallas; 
				?></td>
				<?				
				
				
					////Calcula el promedio para la materia que se esta consultando en el momento
				$promedio=array();
				
				
				for($i = 1; $i <= 4 ; $i++)
				{
						////Saca la nota de determinado alumno, en determinado curso, para determinada materia, en determinado semestre.
					$sql2 = "select * from 	vBoletin where 	idAlumno = ".$filas9['idAlumno']." and idCurso = ".$_GET['idCurso']." and idMateria = ".$filas1['idMateria']." and bimestre = ".$i;
					$resultado2 = mysql_query($sql2,$MySQL);
					if ($filas2 = mysql_fetch_array($resultado2))
					{
						$nota = $filas2['nota'];
						$recuperacion = $filas2['recuperacion'];
						
					}
					else
					{
						$nota = 0;
						$recuperacion = 0;
					}	

				if ($nota > 0 && $nota > $recuperacion)
				{
					////Solo se coloca la nota para que recalcule el promedio
					////si esta sacando la nota de determinado promedio.
					////Es decir si se selecciona el promedio 1 solo va a mostrar
					////Las notas hasta el promedio 1 y el promedio general solo
					////se va a mostrar con las notas del promedio 1.
					if ($i <= $bimestreActual)
					{
						$promedio[$i] = $nota;			
					}								
				}
				else
				{
					////Solo se coloca la nota para que recalcule el promedio
					////si esta sacando la nota de determinado promedio.
					////Es decir si se selecciona el promedio 1 solo va a mostrar
					////Las notas hasta el promedio 1 y el promedio general solo
					////se va a mostrar con las notas del promedio 1.
					if ($i <= $bimestreActual && $recuperacion > 0)
					{
						$promedio[$i] = $recuperacion;			
					}								
				}

?>
<td><? 
////Solo muestra la nota hasta el bimestre seleccionado
if($nota > 0 && $i <= $bimestreActual)
{
	////Muestra la nota
	//echo substr($nota,0,3); 
	echo number_format($nota,$cantidadDecimales); 
}
?></td>

<td>
	<? 		
                        ////Solo muestra la nota hasta el bimestre seleccionado
	if($recuperacion > 0 && $i <= $bimestreActual)
	{
                        ////Muestra la nota
                        //echo substr($nota,0,3); 
		echo number_format($recuperacion,$cantidadDecimales);
	}

	
	?>
</td>


<!--<td> -->
<?
						/* if($nota > 0)
							{
							////Consulta el desempeno
							$sql3 =" select idDesempeno, desempeno from desempeno where ano = ".$_GET['ano']." and ".$nota." between notaMinima and notaMaxima";
							$resultado3 = mysql_query($sql3,$MySQL);
							if($filas3 = mysql_fetch_array($resultado3))
								{
									echo $filas3['desempeno'];
								}
								else
								{
									echo mysql_error();
								}	
							} */


							?>
							<!--</td> -->
							
							<?
						}
						?>
						
						<td><? 
						if(count($promedio) > 0)
						{
						$notaPromedio = array_sum($promedio) / count($promedio);	
						}
						else
						{
							$notaPromedio = 0;
						}
						
						if($notaPromedio > 0)
						{
				////Imprime promedio general acumulado
				//echo substr($notaPromedio,0,3); 
				
							echo number_format($notaPromedio,1); 
							
							$promedioArea[$areaActual][count($promedioArea[$areaActual])+1] = $notaPromedio;
						}

						if(number_format($notaPromedio,$cantidadDecimales) < 3.5)
						{

							$materiasPerdidas[$filas1['idMateria']] = number_format($notaPromedio,$cantidadDecimales);
					//echo $materiasPerdidas[$filas1['idMateria']];
						}

						?> </td>
						
						
						<!--   <td> -->
						<?
						
	/* 		if ($notaPromedio > 0)
				{
				////Imprime el texto relacionado con el promedio calculado en el a�o
				$sql4 =" select desempeno from desempeno where ano = ".$_GET['ano']." and ".$notaPromedio." between notaMinima and notaMaxima";
				//echo $sql4;
				$resultado4 = mysql_query($sql4,$MySQL);
				if($filas4 = mysql_fetch_array($resultado4))
				  {
				  echo $filas4['desempeno'];
				  }
				  else
				  {
				  echo mysql_error();
				  }  					
				} */
				
				?>
				<!--  </td> -->
				
				<?
			
				?>

			</tr>
			<?
		}while($filas1 = mysql_fetch_array($resultado1));

?>




<tr>
<td colspan="4" align="right">
<b>Total &aacute;rea: &nbsp;&nbsp;</b>
</td>
<?
$sqlNotaAreaSemestre = "
select sum(notaArea) as notaArea  
from vBoletin 
where idCurso = ".$iv->idCurso." 
and bimestre = 1 
and area = '".$areaActual."'
and idAlumno = ".$iv->idAlumno." 
group by idCurso, bimestre, idArea, idAlumno";

$notaArea = "0";
mysql_select_db($database_MySQL, $MySQL);					
$resultadoNotaAreaSemestre = mysql_query($sqlNotaAreaSemestre,$MySQL);
if($filasNotaAreaSemestre = mysql_fetch_array($resultadoNotaAreaSemestre))
	{ 
		$notaArea = number_format($filasNotaAreaSemestre['notaArea'],$cantidadDecimales);
	}
?>
<td align="right" style="font-weight: bold;" <? if((float)$notaArea < 3.5 ) { ?> style="color:red" <?} ?>>
<? echo $notaArea; ?>
</td>
<td align="right"></td>

<?

$sqlNotaAreaSemestre = "
select sum(notaArea) as notaArea  
from vBoletin 
where idCurso = ".$iv->idCurso." 
and bimestre = 2
and area = '".$areaActual."'
and idAlumno = ".$iv->idAlumno." 
group by idCurso, bimestre, idArea, idAlumno";

$notaArea = "0";
mysql_select_db($database_MySQL, $MySQL);					
$resultadoNotaAreaSemestre = mysql_query($sqlNotaAreaSemestre,$MySQL);
if($filasNotaAreaSemestre = mysql_fetch_array($resultadoNotaAreaSemestre))
	{ 
		$notaArea = number_format($filasNotaAreaSemestre['notaArea'],$cantidadDecimales);
	}
?>
<td align="right" style="font-weight: bold;" <? if((float)$notaArea < 3.5 ) { ?> style="color:red" <?} ?>>
<? echo $notaArea; ?>
</td>
<td align="right"></td>

<?
$sqlNotaAreaSemestre = "
select sum(notaArea) as notaArea  
from vBoletin 
where idCurso = ".$iv->idCurso." 
and bimestre = 3 
and area = '".$areaActual."'
and idAlumno = ".$iv->idAlumno." 
group by idCurso, bimestre, idArea, idAlumno";

$notaArea = "0";
mysql_select_db($database_MySQL, $MySQL);					
$resultadoNotaAreaSemestre = mysql_query($sqlNotaAreaSemestre,$MySQL);
if($filasNotaAreaSemestre = mysql_fetch_array($resultadoNotaAreaSemestre))
	{ 
		$notaArea = number_format($filasNotaAreaSemestre['notaArea'],$cantidadDecimales);
	}
?>
<td align="right" style="font-weight: bold;" <? if((float)$notaArea < 3.5 ) { ?> style="color:red" <?} ?>>
<? echo $notaArea; ?>
</td>
<td align="right"></td>

<?
$sqlNotaAreaSemestre = "
select sum(notaArea) as notaArea  
from vBoletin 
where idCurso = ".$iv->idCurso." 
and bimestre = 4
and area = '".$areaActual."'
and idAlumno = ".$iv->idAlumno." 
group by idCurso, bimestre, idArea, idAlumno";

$notaArea = "0";
mysql_select_db($database_MySQL, $MySQL);					
$resultadoNotaAreaSemestre = mysql_query($sqlNotaAreaSemestre,$MySQL);
if($filasNotaAreaSemestre = mysql_fetch_array($resultadoNotaAreaSemestre))
	{ 
		$notaArea = number_format($filasNotaAreaSemestre['notaArea'],$cantidadDecimales);
	}
?>
<td align="right" style="font-weight: bold;" <? if((float)$notaArea < 3.5 ) { ?> style="color:red" <?} ?>>
<? echo $notaArea; ?>
</td>
<td align="right"></td>



<?
$sqlNotaAreaSemestre = "select avg(notaArea) as notaArea from (
select sum(notaArea) as notaArea  
from vBoletin 
where idCurso = ".$iv->idCurso." 
and area = '".$areaActual."'
and idAlumno = ".$iv->idAlumno." 
group by idCurso, bimestre, idArea, idAlumno, bimestre) datos";
$notaArea = "0";
mysql_select_db($database_MySQL, $MySQL);					
$resultadoNotaAreaSemestre = mysql_query($sqlNotaAreaSemestre,$MySQL);
if($filasNotaAreaSemestre = mysql_fetch_array($resultadoNotaAreaSemestre))
	{ 
		$notaArea = number_format($filasNotaAreaSemestre['notaArea'],$cantidadDecimales);
	}
?>
<td align="center" style="font-weight: bold;" <? if((float)$notaArea < 3.5 ) { ?> style="color:red" <?} ?>>
<? echo $notaArea; ?>
</td>


</tr>



<!--
<tr valign="top" align="center">
<td colspan="13"> <b>Promedio del &aacute;rea: 
-->
<?

// ////Imprime el promedio del área 
// $promedioAreaActual = 0;
// $promedioAreaActual = number_format(array_sum($promedioArea[$areaActual])/count($promedioArea[$areaActual]),1); 
// echo $promedioAreaActual; 
// if($promedioAreaActual < 3.5 ) {
// $areasPerdidas++;
// }
?>
<!--
</b>
</td>
</tr>
-->
<?

	}
	else
	{
		echo mysql_error();
	}
	?>
</table>
<p style="width:1px"></p>

<table width="100%" border="0">
	<tr valign="top">
		<td> *Fallas actuales (Fallas acumuladas) - V Valoraci&oacute;n del Periodo - R Valoraci&oacute;n de Recuperaci&oacute;n<br />

			<table width="100%" border="0" >
				<tr>
					<td><b>Escala de valoraci&oacute;n:</b> 
						<?
//$sql8 = "select concat(' De ',cast(notaMinima as char), ' a ',cast(notaMaxima as char),' ',desempeno) as desempeno from desempeno where ano = ".date("Y")." order by notaMinima";
						$sql8 = "select notaMinima, notaMaxima, desempeno, concat(' De ',cast(notaMinima as char), ' a ',cast(notaMaxima as char),' ',desempeno) as desempeno2 from desempeno where ano = ".date("Y")." order by notaMinima";
						$resultado8 = mysql_query($sql8,$MySQL);
						$filas8 = mysql_fetch_array($resultado8);

						?>
						
          <!--<table align="center" border="0" width="60%">
          <tr align="center"> -->
          	
          	<?
          	do{
          		?>
          		
          		<!--<td> -->
          		
          		<?
    //echo $filas8['desempeno']."&nbsp;&nbsp;&nbsp;";
          		echo "De ".number_format($filas8['notaMinima'],$cantidadDecimales)." A ".number_format($filas8['notaMaxima'],$cantidadDecimales)." ".$filas8['desempeno']."&nbsp;&nbsp;&nbsp;";
          		?>
          		
          		<!--  </td> -->
          		
          		<?
          		
          	}while($filas8 = mysql_fetch_array($resultado8));

          	?>
          	<!--</tr></table> --></td>
          </tr>



      </table>      
      


  </td>
  <td>

  	<table width="100%" border="0" ><tr><td align="center">Promovido(a):  <b>
  		<?

$sqlAreasPerdidas = "select count(1) as areasPerdidas from (
	select idAlumno, idArea, bimestre, sum(notaArea) as notaArea
	from vBoletin 
	where 1=1
	and idCurso = ".$iv->idCurso."  
	and idAlumno = ".$iv->idAlumno." 
	and bimestre = 4
	group by idAlumno, idArea, bimestre
	) datos
	where datos.notaArea < 3.5";

	$areasPerdidas = 0;
	
	mysql_select_db($database_MySQL, $MySQL);					
	$resultadoAreasPerdidas = mysql_query($sqlAreasPerdidas,$MySQL);
	if($filasAreasPerdidas = mysql_fetch_array($resultadoAreasPerdidas))
		{ 
			$areasPerdidas = (int)$filasAreasPerdidas['areasPerdidas'];
		}
		else
		{
			$areasPerdidas = 0;
		}


if($areasPerdidas == 0) 
{ 
	?>Si<? 
} 
else 
{ 
	if($areasPerdidas <= 2) 
	{ 
		?>Pendiente<? 
	} 
	else 
	{ 
		?>No<? 
	} 
}

/*  		if(count($materiasPerdidas) == 0)
  		{
  			?>
  			Si
  			<?	
  		}
  		else
  		{
  			if(count($materiasPerdidas) == 1)
  			{
  				?>
  				Pendiente
  				<?	
  			}
  			else
  			{
  				?>
  				No
  				<?				
  			}

  			$materiasPerdidas=array();
  		}
*/






  		?></b>
  	</td>
  </tr>

</tr>
<td>
<!--
	<table width="100%">
		<tr><td >Recuperaci&oacute;n</td><td></td></tr>
		<?
		if(count($materiasPerdidas) == 0)
		{
			?><tr><td>N/A</td><td></td></tr><?
		}
		else
		{
			if(count($materiasPerdidas) == 1)
			{
				$llaves = array_keys($materiasPerdidas);
				$sql13 = "select materia from materia where idMateria = ".$llaves[0];

				$resultado13 = mysql_query($sql13,$MySQL);				  
				if(mysql_num_rows($resultado13) > 0)
				{
					$filas13 = mysql_fetch_array($resultado13);

					?>
					<tr>
						<td><? echo $filas13['materia']; ?></td>
						<td><? echo number_format($materiasPerdidas[$llaves[0]],$cantidadDecimales); ?></td>
					</tr>
					<?   		
				}
			}
			else
			{
				?><tr><td>N/A</td><td></td></tr><?
			}
		}
		?>	
	</table>
-->
</td>
</tr></table> 
</td>
</tr>

</table>
<p style="width:1px"></p>

<p style="width:1px"></p>
<table width="100%" border="0">
	<tr>
		<td colspan="2"><strong>OBSERVACIONES Y/O RECOMENDACIONES</strong><br />
			<br />
			<hr />
		</td>
	</tr>
	<tr  valign="top">
		<td><br />
			_________________________<br />
			<strong>FIRMA RECTORA </strong><br />
			Hna. Berta Lucia Salazar Gómez<? //$iv->nombreDirectorCurso (); ?></td>
			<td><br />
				_________________________<br />
				<strong>FIRMA DIRECTOR DE GRUPO </strong><br />
				<? $iv->nombreDirectorCurso (); ?></td></td>
			</tr>
		</table>
	</div>
</center>
<div class="page-break"></div>
<?


}while($filas9 = mysql_fetch_array($resultado9));





if($_GET['pdf'])
{
	?>
</body>
</html>
<?	
}
?>

