<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Untitled Document</title>
<style type="text/css">
<!--
.excel1 {
padding-top:1px;
padding-right:1px;
padding-left:1px;
color:black;
font-size:11.0pt;
font-weight:400;
font-style:normal;
text-decoration:none;
font-family:Calibri, sans-serif;
text-align:general;
vertical-align:bottom;
border:none;
white-space:nowrap;
}
.excel2 {
padding-top:1px;
padding-right:1px;
padding-left:18px;
color:black;
font-size:11.0pt;
font-weight:400;
font-style:normal;
text-decoration:none;
font-family:Calibri, sans-serif;
text-align:left;
vertical-align:bottom;
border:none;
white-space:nowrap;
}
-->
</style>
</head>

<body>
<table width="622" border="1" align="center">
  <tr>
    <th width="46" align="center" scope="col">Estado</th>
    <th width="560" align="center" scope="col">Tarea</th>
  </tr>
  <tr>
    <td align="center">OK</td>
    <td>Que los profesores puedan ver solo las materias y cursos que tienen asignados.</td>
  </tr>
  <tr>
    <td align="center">OK</td>
    <td>Subir desempenos y competencias por parte de los profesores.</td>
  </tr>
  <tr>
    <td align="center">OK</td>
    <td>tildes y eñes.</td>
  </tr>
  <tr>
    <td align="center">&nbsp;</td>
    <td>Dejar parametrizable los periodos</td>
  </tr>
  <tr>
    <td align="center">&nbsp;</td>
    <td>Colocar orden en el area para que puedan organizar como se va a mostrar.</td>
  </tr>
  <tr>
    <td align="center">&nbsp;</td>
    <td>Dejar periodo en vez de bimestre en todo lado.</td>
  </tr>
  <tr>
    <td align="center">&nbsp;</td>
    <td>Reporte (Resumen de desempeño), grafico:<br />
      <table cellspacing="0" cellpadding="0" class="excel1">
        <col width="80" span="5" style="width:60pt;" />
        <col width="135" style="width:101pt;" />
        <tr style="height:15.0pt;">
          <td width="80" style="height:15.0pt;width:60pt;">Filtros:</td>
          <td width="80" style="width:60pt;"></td>
          <td width="80" style="width:60pt;"></td>
          <td width="80" style="width:60pt;"></td>
          <td width="80" style="width:60pt;"></td>
          <td width="135" style="width:101pt;"></td>
        </tr>
        <tr style="height:15.0pt;">
          <td class="excel2" style="height:15.0pt;">Area</td>
          <td></td>
          <td></td>
          <td></td>
          <td>Falta el periodo</td>
          <td></td>
        </tr>
        <tr style="height:15.0pt;">
          <td class="excel2" style="height:15.0pt;">Materia</td>
          <td></td>
          <td></td>
          <td></td>
          <td></td>
          <td></td>
        </tr>
        <tr style="height:15.0pt;">
          <td class="excel2" style="height:15.0pt;">Curso</td>
          <td></td>
          <td></td>
          <td></td>
          <td></td>
          <td></td>
        </tr>
        <tr style="height:15.0pt;">
          <td class="excel2" colspan="2" style="height:15.0pt;">Desempeno</td>
          <td></td>
          <td></td>
          <td></td>
          <td></td>
        </tr>
        <tr style="height:15.0pt;">
          <td class="excel2" colspan="4" style="height:15.0pt;">El    administrador puede seleccionar el profesor</td>
          <td></td>
          <td></td>
        </tr>
        <tr style="height:15.0pt;">
          <td class="excel2" colspan="4" style="height:15.0pt;">El    profesor solo puede ver lo del profesor</td>
          <td></td>
          <td></td>
        </tr>
        <tr style="height:15.0pt;">
          <td style="height:15.0pt;"></td>
          <td></td>
          <td></td>
          <td></td>
          <td>periodo</td>
          <td></td>
        </tr>
        <tr style="height:15.0pt;">
          <td style="height:15.0pt;">Docente</td>
          <td>Area</td>
          <td>Materia</td>
          <td>Curso</td>
          <td>Desempeno</td>
          <td>Numero estudiantes</td>
        </tr>
        <tr style="height:15.0pt;">
          <td align="center" style="height:15.0pt;"></td>
          <td align="center">ss</td>
          <td align="center">j</td>
          <td align="center">1</td>
          <td align="center">C</td>
          <td align="center">22</td>
        </tr>
        <tr style="height:15.0pt;">
          <td align="center" style="height:15.0pt;"></td>
          <td align="center">ss</td>
          <td align="center">g</td>
          <td align="center">1</td>
          <td align="center">a</td>
          <td align="center">3</td>
        </tr>
        <tr style="height:15.0pt;">
          <td align="center" style="height:15.0pt;"></td>
          <td align="center">ss</td>
          <td align="center">h</td>
          <td align="center">1</td>
          <td align="center">B</td>
          <td align="center">22</td>
        </tr>
    </table></td>
  </tr>
  <tr>
    <td align="center">&nbsp;</td>
    <td>Implementar el boletin de calificaciones.</td>
  </tr>
  <tr>
    <td align="center">OK</td>
    <td>En el calculo de la nota, no se puede tener en cuenta los valores que estan en cero.</td>
  </tr>
  <tr>
    <td align="center">&nbsp;</td>
    <td>Imprimir el listado donde se colocan las notas.</td>
  </tr>
  <tr>
    <td align="center">&nbsp;</td>
    <td>El año en informe valorativo esta quemado, hay que cambiarlo para que sea dinamico.</td>
  </tr>
  <tr>
    <td align="center">&nbsp;</td>
    <td>Imprimir el Informe valorativo, ver la manera de colocarlo en pdf.</td>
  </tr>
  <tr>
    <td align="center">&nbsp;</td>
    <td>Firma del rector, ver como se puede parametrizar de la mejor manera para que sea dinamico.</td>
  </tr>
  <tr>
    <td align="center">&nbsp;</td>
    <td><h6>Generar impresion de boletines por curso, o todos.</h6></td>
  </tr>
  <tr>
    <td align="center">&nbsp;</td>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td align="center">&nbsp;</td>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td align="center">&nbsp;</td>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td align="center">&nbsp;</td>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td align="center">&nbsp;</td>
    <td>&nbsp;</td>
  </tr>
</table>
<blockquote>&nbsp;</blockquote>
<p>&nbsp;</p>
<p>&nbsp;</p>
<p>&nbsp;</p>
</body>
</html>