<?php require_once('Connections/MySQL.php'); ?>
<?php
if (!isset($_SESSION)) {
  session_start();
}
$MM_authorizedUsers = "Administrador,Profesor";
$MM_donotCheckaccess = "false";

// *** Restrict Access To Page: Grant or deny access to this page
function isAuthorized($strUsers, $strGroups, $UserName, $UserGroup) { 
  // For security, start by assuming the visitor is NOT authorized. 
  $isValid = False; 

  // When a visitor has logged into this site, the Session variable MM_Username set equal to their username. 
  // Therefore, we know that a user is NOT logged in if that Session variable is blank. 
  if (!empty($UserName)) { 
    // Besides being logged in, you may restrict access to only certain users based on an ID established when they login. 
    // Parse the strings into arrays. 
    $arrUsers = Explode(",", $strUsers); 
    $arrGroups = Explode(",", $strGroups); 
    if (in_array($UserName, $arrUsers)) { 
      $isValid = true; 
    } 
    // Or, you may restrict access to only certain users based on their username. 
    if (in_array($UserGroup, $arrGroups)) { 
      $isValid = true; 
    } 
    if (($strUsers == "") && false) { 
      $isValid = true; 
    } 
  } 
  return $isValid; 
}

$MM_restrictGoTo = "error.php";
if (!((isset($_SESSION['MM_Username'])) && (isAuthorized("",$MM_authorizedUsers, $_SESSION['MM_Username'], $_SESSION['MM_UserGroup'])))) {   
  $MM_qsChar = "?";
  $MM_referrer = $_SERVER['PHP_SELF'];
  if (strpos($MM_restrictGoTo, "?")) $MM_qsChar = "&";
  if (isset($QUERY_STRING) && strlen($QUERY_STRING) > 0) 
  $MM_referrer .= "?" . $QUERY_STRING;
  $MM_restrictGoTo = $MM_restrictGoTo. $MM_qsChar . "accesscheck=" . urlencode($MM_referrer);
  header("Location: ". $MM_restrictGoTo); 
  exit;
}
mysql_select_db($database_MySQL,$MySQL);
if ($_GET['idCurso'])
	{
	?>
	* (nota perdida) nota recuperada
	<table border="1"  >
	<?		
	////Titulo de las materias
	$sql0 = "		select distinct m.idMateria, m.materia 
					from cursomateriaprofesor as cmp 
					inner join curso as c on cmp.idCurso = c.idCurso
					inner join materia as m on m.idMateria = cmp.idMateria
					where c.idCurso = ".$_GET['idCurso']."
					order by m.materia asc
					";
	$resultado0 = mysql_query($sql0);
	if($filas0 = mysql_fetch_array($resultado0))
		{
			////Imprime la cabecera de la tabla
			?>
			<thead>
			<tr height="200px">
      	<td>Estudiante</td>
				<?
        do{
					?>
					<td class="letravertical">
					<?
					echo $filas0['materia'];
					?>
					</td>
					<?
        }while($filas0 = mysql_fetch_array($resultado0));
        ?>
				<td>Periodo</td>
			</tr>
			</thead>
			<?
		}
		else
		{
		echo mysql_error();	
		}
		
	////Alumnos pertenecientes al curso seleccionado
	////Busca los alumnos que tiene el curso
	$sql1 = "select a.apellidos, a.nombres , ac.idCurso, ac.idAlumno, c.ano
			from alumnocurso as ac 
			inner join alumno as a on a.idAlumno = ac.idAlumno 
			inner join curso as c on ac.idCurso = c.idCurso
			where ac.idCurso = ".$_GET['idCurso']."
			order by a.apellidos asc, a.nombres asc
			";

	$resultado1 = mysql_query($sql1,$MySQL);
	if($filas1 = mysql_fetch_array($resultado1))
		{
		do{
			?>
			<tr valign="middle"><td rowspan="4"><b>
			<?
			////Nombre del alumno
			echo $filas1['apellidos']." ".$filas1['nombres'];
			?>
      </b>
			</td>
			<?
			////Periodos
			for($i = 1; $i <= 4; $i++)
				{
					
				////Materias del curso
				$sql2 = "select distinct m.idMateria, m.materia 
						from cursomateriaprofesor as cmp 
						inner join curso as c on cmp.idCurso = c.idCurso
						inner join materia as m on m.idMateria = cmp.idMateria
						where c.idCurso = ".$filas1['idCurso']."
						order by m.materia asc
						";
				$resultado2 = mysql_query($sql2,$MySQL);
				if($filas2 = mysql_fetch_array($resultado2))
					{
					if (mysql_num_rows($resultado2) ==0 )
						{
						////Si no hay materias
						?>
						<td>??</td>
						<?
						}
						else
						{
						unset($materiasOrden);
						$contadorOrdenMaterias = 0;
						do {  
						$materiasOrden[$contadorOrdenMaterias] = $filas2['idMateria'];
						$contadorOrdenMaterias++;
							////Nota en la materia para el alumno en el curso seleccionado y bimestre del ciclo
							$sql3 = "select ifnull(((((nota1 +
						nota2 +
						nota3 +
						nota4 +
						nota5 +
						nota6 )/
						
						(
						case when nota1 > 0 then 1 else 0 end 	
						+
						case when nota2 > 0 then 1 else 0 end 	
						+
						case when nota3 > 0 then 1 else 0 end 	
						+
						case when nota4 > 0 then 1 else 0 end 	
						+
						case when nota5 > 0 then 1 else 0 end 	
						+
						case when nota6 > 0 then 1 else 0 end 	
						)
						) * 40 )/100),0)
						+
						ifnull(((((nota7 +
						nota8 +
						nota9 +
						nota10 +
						nota11 +
						nota12 )/
						(
						case when nota7 > 0 then 1 else 0 end 	
						+
						case when nota8 > 0 then 1 else 0 end 	
						+
						case when nota9 > 0 then 1 else 0 end 	
						+
						case when nota10 > 0 then 1 else 0 end 	
						+
						case when nota11 > 0 then 1 else 0 end 	
						+
						case when nota12 > 0 then 1 else 0 end 	
						)						
						
						) * 30 )/100),0)
						+
						ifnull(((((nota13 +
						nota14 )/
						(
						case when nota13 > 0 then 1 else 0 end 	
						+
						case when nota14 > 0 then 1 else 0 end 							
						)
						) * 10 )/100),0) 
						+
						ifnull((((nota15/
						(
						case when nota15 > 0 then 1 else 0 end 							
						)
						) * 20 )/100),0) 

						as nota ,
						recuperacion as recuperacion
									from boletin 
									where idcurso = ".$filas1['idCurso']." 
										and idmateria = ".$filas2['idMateria']."
										and bimestre = ".$i."
										and idalumno = ".$filas1['idAlumno'];	
							$resultado3 = mysql_query($sql3,$MySQL);
							if (mysql_num_rows($resultado3) == 0)
								{
								////Si no hay notas.
								?>
								<td>&nbsp;</td>
								<?											
								}
								else
								{
								if($filas3 = mysql_fetch_array($resultado3))
									{
									do {
										?>
										<td align="center">
										<?
										if($filas3['nota']>$filas3['recuperacion'])
											{
												if(number_format($filas3['nota'],1) < 3.5) { ?> <font color="red"> <? }

												echo number_format($filas3['nota'],1);
												
												if(number_format($filas3['nota'],1) < 3.5) { ?> </font> <? }
												
												$notas[$filas1['idAlumno']][$filas2['idMateria']][$i] = number_format($filas3['nota'],1); // $filas3['nota'];
											}
											else
											{
												echo '(';
												if(number_format($filas3['nota'],1) < 3.5) { ?>	<font color="red"> <? } 

												echo number_format($filas3['nota'],1);
												
												if(number_format($filas3['nota'],1) < 3.5) { ?> </font> <? }
												echo ') ';

												if(number_format($filas3['recuperacion'],1) < 3.5) { ?> <font color="red"> <? } 

												echo number_format($filas3['recuperacion'],1);
												
												if(number_format($filas3['recuperacion'],1) < 3.5) { ?> </font> <? }

												$notas[$filas1['idAlumno']][$filas2['idMateria']][$i] = number_format($filas3['recuperacion'],1); //$filas3['recuperacion'];
											}			
										?>
										</td>
										<?
										}while($filas3 = mysql_fetch_array($resultado3));
									}
									else
									{
									echo mysql_error();
									}																					
								}

							}while($filas2 = mysql_fetch_array($resultado2));								
						}
					}
					else
					{
					echo mysql_error();	
					}
					
				?>
				<td>
				<?
				switch ($i)
					{
					case 1:
					?>
					Primero
					<?
					break;
					case 2:
					?>
					Segundo
					<?
					break;
					case 3:
					?>
					Tercero
					<?
					break;
					case 4:
					?>
					Cuarto
					<?
					break;					
					}
				?>
                </td>
				</tr>
				<?					
				}
				?>
							        
		        <tr>
		        <td align="right"><i>Promedio:</i></td>
		        <?
				for ($i1 = 0 ; $i1 < count($materiasOrden ); $i1++)
					{
						?>
						<td align="center">
						<? 
						$promedioMateria = 	array_sum($notas[$filas1['idAlumno']][$materiasOrden[$i1]]) / count($notas[$filas1['idAlumno']][$materiasOrden[$i1]]); 

						if(number_format($promedioMateria,1) < 3.5) { ?> <font color="red"><b> <? }

						echo number_format($promedioMateria,1);

						if(number_format($promedioMateria,1) < 3.5) { ?></b></font><? }

						?>
						</td>
						<?
					}
				unset($notas);
		        ?>
		        <td>Ultimo</td>
		        </tr>
			<?
			}while($filas1 = mysql_fetch_array($resultado1));	
		}
		else
		{
		echo mysql_error();	
		}
	?>
	</table>
	<?
	}
?>
