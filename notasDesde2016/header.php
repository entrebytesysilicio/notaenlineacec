<? include "clases.php"; ?>
<!-- El charset debe estar sincronizado con el usado por MySQL, ver archivo Connections\MySQL.php -->
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" /> 
<!-- <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" /> -->
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link href="estilo.css" rel="stylesheet" type="text/css" />
<!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
<!-- Bootstrap -->
<link href="css/bootstrap.min.css" rel="stylesheet">
<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<!--[if lt IE 9]>
<script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
<![endif]-->
<script language="javascript" type="text/javascript" src="ajax.js"></script>
<script language="javascript" type="text/javascript" src="js/jquery-1.10.2.min.js"></script>
<script language="javascript" type="text/javascript" src="js/bootstrap.min.js"></script>

<?php 
	date_default_timezone_set('America/Bogota');
	setlocale(LC_ALL,"es_CO");
 ?>