<?php require_once('Connections/MySQL.php'); ?>
<?php require_once('clases.php'); ?>
<?php
if (!isset($_SESSION)) {
	session_start();
}
$MM_authorizedUsers = "Administrador,Profesor";
$MM_donotCheckaccess = "false";

// *** Restrict Access To Page: Grant or deny access to this page
function isAuthorized($strUsers, $strGroups, $UserName, $UserGroup) { 
	// For security, start by assuming the visitor is NOT authorized. 
	$isValid = False; 

	// When a visitor has logged into this site, the Session variable MM_Username set equal to their username. 
	// Therefore, we know that a user is NOT logged in if that Session variable is blank. 
	if (!empty($UserName)) { 
		// Besides being logged in, you may restrict access to only certain users based on an ID established when they login. 
		// Parse the strings into arrays. 
		$arrUsers = Explode(",", $strUsers); 
		$arrGroups = Explode(",", $strGroups); 
		if (in_array($UserName, $arrUsers)) { 
			$isValid = true; 
		} 
		// Or, you may restrict access to only certain users based on their username. 
		if (in_array($UserGroup, $arrGroups)) { 
			$isValid = true; 
		} 
		if (($strUsers == "") && false) { 
			$isValid = true; 
		} 
	} 
	return $isValid; 
}

$MM_restrictGoTo = "error.php";
if (!((isset($_SESSION['MM_Username'])) && (isAuthorized("",$MM_authorizedUsers, $_SESSION['MM_Username'], $_SESSION['MM_UserGroup'])))) {   
	$MM_qsChar = "?";
	$MM_referrer = $_SERVER['PHP_SELF'];
	if (strpos($MM_restrictGoTo, "?")) $MM_qsChar = "&";
	if (isset($QUERY_STRING) && strlen($QUERY_STRING) > 0) 
		$MM_referrer .= "?" . $QUERY_STRING;
	$MM_restrictGoTo = $MM_restrictGoTo. $MM_qsChar . "accesscheck=" . urlencode($MM_referrer);
	header("Location: ". $MM_restrictGoTo); 
	exit;
}
?>
<?php
if (!function_exists("GetSQLValueString")) {
	function GetSQLValueString($theValue, $theType, $theDefinedValue = "", $theNotDefinedValue = "") 
	{
		if (PHP_VERSION < 6) {
			$theValue = get_magic_quotes_gpc() ? stripslashes($theValue) : $theValue;
		}

		$theValue = function_exists("mysql_real_escape_string") ? mysql_real_escape_string($theValue) : mysql_escape_string($theValue);

		switch ($theType) {
			case "text":
				$theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
				break;    
			case "long":
			case "int":
				$theValue = ($theValue != "") ? intval($theValue) : "NULL";
				break;
			case "double":
				$theValue = ($theValue != "") ? doubleval($theValue) : "NULL";
				break;
			case "date":
				$theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
				break;
			case "defined":
				$theValue = ($theValue != "") ? $theDefinedValue : $theNotDefinedValue;
				break;
		}
		return $theValue;
	}
}

$Notas = new Notas();

$colname_rsAlumnos = "-1";
if (isset($_GET['idCurso'])) {
	$colname_rsAlumnos = $_GET['idCurso'];
}
mysql_select_db($database_MySQL, $MySQL);
$query_rsAlumnos = "SELECT	ac.idAlumnoCurso, 
a.nombres, 
a.apellidos, 
a.identificacion,
case b.nota when 0 then '' else b.nota end as nota,
case b.nota1 when 0 then '' else b.nota1 end as nota1,
case b.nota2 when 0 then '' else b.nota2 end as nota2,
case b.nota3 when 0 then '' else b.nota3 end as nota3,
case b.nota4 when 0 then '' else b.nota4 end as nota4,
case b.nota5 when 0 then '' else b.nota5 end as nota5,
case b.nota6 when 0 then '' else b.nota6 end as nota6,
case b.nota7 when 0 then '' else b.nota7 end as nota7,
case b.nota8 when 0 then '' else b.nota8 end as nota8,
case b.nota9 when 0 then '' else b.nota9 end as nota9,
case b.nota10 when 0 then '' else b.nota10 end as nota10,
case b.nota11 when 0 then '' else b.nota11 end as nota11,
case b.nota12 when 0 then '' else b.nota12 end as nota12,
case b.nota13 when 0 then '' else b.nota13 end as nota13,
case b.nota14 when 0 then '' else b.nota14 end as nota14,	
case b.nota15 when 0 then '' else b.nota15 end as nota15,
case b.fallas when 0 then '' else b.fallas end as fallas,
case b.recuperacion when 0 then '' else b.recuperacion end as recuperacion,
b.idBoletin,
a.idAlumno,
c.idCurso,
".$_GET['idMateria']." as idMateria,
".$_GET['bimestre']." as bimestre
FROM alumnocurso as ac
inner join alumno as a on a.idAlumno = ac.idAlumno
inner join curso as c on c.idCurso = ac.idCurso						   
left join boletin as b 	on b.idAlumno = a.idAlumno 
and b.idCurso = c.idCurso 
and b.idMateria = ".$_GET['idMateria']."	
and b.bimestre = ".$_GET['bimestre']."
WHERE ac.idCurso = ".$_GET['idCurso']." ORDER BY a.apellidos collate utf8_general_ci asc, a.nombres ASC";
$rsAlumnos = mysql_query($query_rsAlumnos, $MySQL) or die(mysql_error());
$row_rsAlumnos = mysql_fetch_assoc($rsAlumnos);
$totalRows_rsAlumnos = mysql_num_rows($rsAlumnos);
?>		

<!DOCTYPE html>
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Planilla de valoraci&oacute;n</title>
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->    
    <!-- Bootstrap -->
    <script language="javascript" type="text/javascript" src="js/jquery-1.10.2.min.js"></script>
    <script language="javascript" type="text/javascript" src="js/bootstrap.min.js"></script>
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
	  <style type="text/css">

		  @page{
margin-left: 30px;
margin-right: 30px;
margin-top: 30px;
margin-bottom: 30px;
size: legal, landscape;
}
	  </style>
  </head>
  <body>
 

 <table class="" width="100%" border="1" cellpadding="0" cellspacing="0">
      <tr>
        <td rowspan="4" class="text-center"><img height="96px" src="imagenes/logo.png"></td>
        <td rowspan="4" class="text-center" style="letter-spacing:2px;padding:2px;font-weight:bold;font-size:large;">Hermanas Mercedarias del Santisimo Sacramento<br />
            Colegio Eucar&iacute;stico Campestre<br />
            PLANILLA DE VALORACI&Oacute;N<br />
            <?php echo date("Y"); ?>
		  </td>
        <td class="text-center " >C&oacute;digo</td>        
      </tr>
      <tr>
        <td class="text-center ">F02 - PR - MA - 002</td>
      </tr>
      <tr>
        <td class="text-center ">Versi&oacute;n 09</td>
      </tr>
      <tr>
        <td class="text-center ">Actualizado: 18-01-2017</td>
      </tr>      
  </table> 
  <table class="table" style="margin-bottom:0px;" width="100%" border="1" cellpadding="0" cellspacing="0">
      <tr>
        <td >DOCENTE: <b>
<?php 
$base = new BD();
$sql = "select u.nombre from cursomateriaprofesor  as cmp 
inner join usuario as u on u.idUsuario = cmp.idProfesor
where cmp.idCurso = ".$_GET['idCurso']." and cmp.idMateria = ".$_GET['idMateria'];
$profesor = $base->consultarUnValor($sql);
echo $profesor;
?>
			</b>
		  </td>
        <td >ASIGNATURA: <b>
<?php 
$base = new BD();
$sql = "select concat(area,' ',materia) materia from materia where idMateria = ".$_GET['idMateria'];
$materia = $base->consultarUnValor($sql);
echo $materia;
?>
			</b>
		  </td>
        <td >GRADO: <b>
<?php 
$base = new BD();
$sql = "select curso from curso where idCurso = ".$_GET['idCurso'];
$curso = $base->consultarUnValor($sql);
echo $curso;
?>
</b>
</td>

        <td class="">PERIODO: <b><?php echo $_GET['bimestre']; ?></b>
		  </td>
        
      </tr>      
  </table>
 <table class="table table-hover table-condensed table-striped" border="1">
  <thead>
    <tr align="center">
        <td rowspan="2" style="border-right:double; border-bottom: double;"><p style="writing-mode: tb-rl;">COD</p></td>
        <td rowspan="2" style="border-right:double; border-bottom: double;">ESTUDIANTES</td>   
      <td colspan="6" style="border-right:double; ">COGNITIVO<br />40%</td>
        <td rowspan="2" style="border-bottom:double;"><p style="text-align:center; font-weight:bold; writing-mode: tb-rl; white-space:nowrap;">Final</p></td>
      <td colspan="6" style="border-right:double; border-left:double;">PROCEDIMENTAL<br />30%</td>
        <td rowspan="2" style="border-bottom:double;"><p style="text-align:center; font-weight:bold; writing-mode: tb-rl; white-space:nowrap;">Final</p></td>
      <td colspan="2" style="border-left:double; border-right:double;" >ACTITUDINAL<br />10%</td>
		<td rowspan="2"  style="border-bottom:double;border-right:double;"><p style="writing-mode: tb-rl; font-weight:bold; white-space:nowrap;">Final</p></td>
        <td style="border-left:double; border-right:double;" >VALORACI&Oacute;N FINAL 20%</td>
		<td rowspan="2" style="border-bottom:double;border-right:double;"><p style="text-align:center; font-weight:bold; writing-mode: tb-rl; white-space:nowrap;">Final</p></td> 
		<!-- <td rowspan="2"  style="border-bottom:double;border-right:double;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td> -->
        <td rowspan="2"  style="border-bottom:double;border-right:double;"><p style="writing-mode: tb-rl; font-weight:bold; white-space:nowrap;">Nota final</p></td>
		<td rowspan="2"  style="border-bottom:double;border-right:double;"><p style="writing-mode: tb-rl;">Recuperaci&oacute;n</p></td>
        <!-- <td rowspan="2"  style="border-bottom:double;border-right:double;"><p style="writing-mode: tb-rl;">Promedio Acumulado</p></td> -->
      <!--<td colspan="5"></td>-->
      <td colspan="15">ASISTENCIA</td>
    </tr>
    <tr align="center">

 
      <!--<td>Nombres</td>-->
      <!--<td>Identificacion</td>-->
      <!--<td>Estado</td>-->
	<td style="border-bottom: double;border-left: double; ">&nbsp;1&nbsp;&nbsp;</td>
	<td style="border-bottom: double;">&nbsp;2&nbsp;&nbsp;</td>
	<td style="border-bottom: double;">&nbsp;3&nbsp;&nbsp;</td>
	<td style="border-bottom: double;">&nbsp;4&nbsp;&nbsp;</td>
	<td style="border-bottom: double;">&nbsp;5&nbsp;&nbsp;</td>
	<td style="border-bottom: double;border-right:double;">&nbsp;6&nbsp;&nbsp;</td>
	<td style="border-bottom: double;border-left: double; ">&nbsp;1&nbsp;&nbsp;</td>
	<td style="border-bottom: double;">&nbsp;2&nbsp;&nbsp;</td>
	<td style="border-bottom: double;">&nbsp;3&nbsp;&nbsp;</td>
	<td style="border-bottom: double;">&nbsp;4&nbsp;&nbsp;</td>
	<td style="border-bottom: double;">&nbsp;5&nbsp;&nbsp;</td>
	<td style="border-bottom: double;border-right:double;">&nbsp;6&nbsp;&nbsp;</td>
	<td style="border-bottom: double;border-left:double;">A</td>
	<td style="border-bottom: double;border-right:double;">C</td>
    <td style="border-bottom:double; border-left:double; border-right:double;">PERIODO</td>

        
     
      

<!--       <td>Definitiva</td>      -->  
      <!--<td>Fallas</td>
      <td>Promedio proyectado</td>-->
      <td colspan="5"  style="border-bottom:double; border-right:double; border-right:double;">Mes:_________</td>
      <td colspan="5"  style="border-bottom:double;border-right:double; border-right:double;">Mes:_________</td>
      <td colspan="5"  style="border-bottom:double; border-left:double;">Mes:_________</td>
    </tr>
  </thead>
  <?php $i = 1;  do { ?>
  <tr align="center" id="tr_<? echo $row_rsAlumnos['idAlumno']; ?>">
    <td style="border-right:double;"><? echo $i++; ?></tdstyle="border-right:double;">
    <td align="left"  style="border-right:double;white-space: nowrap;"><?php echo $row_rsAlumnos['apellidos']; ?>&nbsp;<?php echo $row_rsAlumnos['nombres']; ?></td>
    <!--<td align="left"></td>-->      
    <!--<td align="left"><?php //echo $row_rsAlumnos['identificacion']; ?></td>-->
    <!--<td align="left"><div id="resultado_<? //echo $row_rsAlumnos['idAlumno']; ?>"></div></td>-->
    <td align="center" ><?php echo is_numeric($row_rsAlumnos['nota1']) ? number_format($row_rsAlumnos['nota1'],1): ''; ?></td>
    <td align="center" ><?php echo is_numeric($row_rsAlumnos['nota2']) ? number_format($row_rsAlumnos['nota2'],1): ''; ?></td>
    <td align="center" ><?php echo is_numeric($row_rsAlumnos['nota3']) ? number_format($row_rsAlumnos['nota3'],1): ''; ?></td>
    <td align="center" ><?php echo is_numeric($row_rsAlumnos['nota4']) ? number_format($row_rsAlumnos['nota4'],1): ''; ?></td>
    <td align="center" ><?php echo is_numeric($row_rsAlumnos['nota5']) ? number_format($row_rsAlumnos['nota5'],1): ''; ?></td>
    <td align="center" style="border-right:double;"><?php echo is_numeric($row_rsAlumnos['nota6']) ? number_format($row_rsAlumnos['nota6'],1): ''; ?></td>
	  <td style="border-left:double; border-right:double;">
	  	<?
	  	$cantidadNotas = 0;
	  	$sumatoriaNotas = 0;
	  	$promedioCognitivo = 0;
	  	if($row_rsAlumnos['nota1'] > 0)
	  	{
	  		$sumatoriaNotas += $row_rsAlumnos['nota1'];
	  		$cantidadNotas++;
	  	}
	  	if($row_rsAlumnos['nota2'] > 0)
	  	{
	  		$sumatoriaNotas += $row_rsAlumnos['nota2'];
	  		$cantidadNotas++;
	  	}
	  	if($row_rsAlumnos['nota3'] > 0)
	  	{
	  		$sumatoriaNotas += $row_rsAlumnos['nota3'];
	  		$cantidadNotas++;
	  	}
	  	if($row_rsAlumnos['nota4'] > 0)
	  	{
	  		$sumatoriaNotas += $row_rsAlumnos['nota4'];
	  		$cantidadNotas++;
	  	}
	  	if($row_rsAlumnos['nota5'] > 0)
	  	{
	  		$sumatoriaNotas += $row_rsAlumnos['nota5'];
	  		$cantidadNotas++;
	  	}
	  	if($row_rsAlumnos['nota6'] > 0)
	  	{
	  		$sumatoriaNotas += $row_rsAlumnos['nota6'];
	  		$cantidadNotas++;
	  	}	  	
	  	if($cantidadNotas == 0)
	  	{
	  		////avoid division by cero
	  		$cantidadNotas = 1;
	  	}
	  	$promedioCognitivo = $sumatoriaNotas/$cantidadNotas;

	  	echo $promedioCognitivo > 0 ? number_format(($promedioCognitivo*40/100),1) : '';
	  	$promedioCognitivo = $promedioCognitivo*40/100;
	  	$cantidadNotas = 0;
	  	$sumatoriaNotas = 0;
	  	?>
	  </td>
    <td align="center" style="border-left:double;" ><?php echo is_numeric($row_rsAlumnos['nota7']) ? number_format($row_rsAlumnos['nota7'],1): ''; ?></td>
    <td align="center" ><?php echo is_numeric($row_rsAlumnos['nota8']) ? number_format($row_rsAlumnos['nota8'],1): ''; ?></td>
    <td align="center" ><?php echo is_numeric($row_rsAlumnos['nota9']) ? number_format($row_rsAlumnos['nota9'],1): ''; ?></td>
    <td align="center" ><?php echo is_numeric($row_rsAlumnos['nota10']) ? number_format($row_rsAlumnos['nota10'],1): ''; ?></td>
    <td align="center" ><?php echo is_numeric($row_rsAlumnos['nota11']) ? number_format($row_rsAlumnos['nota11'],1): ''; ?></td>
    <td align="center" ><?php echo is_numeric($row_rsAlumnos['nota12']) ? number_format($row_rsAlumnos['nota12'],1): ''; ?></td>
      <td style="border-left:double; border-right:double;">
	  	<?
	  	$cantidadNotas = 0;
	  	$sumatoriaNotas = 0;
	  	$promedioProcedimental = 0;
	  	if($row_rsAlumnos['nota7'] > 0)
	  	{
	  		$sumatoriaNotas += $row_rsAlumnos['nota7'];
	  		$cantidadNotas++;
	  	}
	  	if($row_rsAlumnos['nota8'] > 0)
	  	{
	  		$sumatoriaNotas += $row_rsAlumnos['nota8'];
	  		$cantidadNotas++;
	  	}
	  	if($row_rsAlumnos['nota9'] > 0)
	  	{
	  		$sumatoriaNotas += $row_rsAlumnos['nota9'];
	  		$cantidadNotas++;
	  	}
	  	if($row_rsAlumnos['nota10'] > 0)
	  	{
	  		$sumatoriaNotas += $row_rsAlumnos['nota10'];
	  		$cantidadNotas++;
	  	}
	  	if($row_rsAlumnos['nota11'] > 0)
	  	{
	  		$sumatoriaNotas += $row_rsAlumnos['nota11'];
	  		$cantidadNotas++;
	  	}
	  	if($row_rsAlumnos['nota12'] > 0)
	  	{
	  		$sumatoriaNotas += $row_rsAlumnos['nota12'];
	  		$cantidadNotas++;
	  	}	  	
	  	if($cantidadNotas == 0)
	  	{
	  		////avoid division by cero
	  		$cantidadNotas = 1;
	  	}
	  	$promedioProcedimental = $sumatoriaNotas/$cantidadNotas;

	  	echo $promedioProcedimental > 0 ? number_format(($promedioProcedimental*30/100),1) : '';
	  	$promedioProcedimental = $promedioProcedimental*30/100;
	  	$cantidadNotas = 0;
	  	$sumatoriaNotas = 0;
	  	?>
      </td>
    <td align="center"><?php echo is_numeric($row_rsAlumnos['nota13']) ? number_format($row_rsAlumnos['nota13'],1): ''; ?></td>
    <td align="center" style="border-right:double;"><?php echo is_numeric($row_rsAlumnos['nota14']) ? number_format($row_rsAlumnos['nota14'],1): ''; ?></td>
    <td style="border-left:double; border-right:double;">
	  	<?
	  	$cantidadNotas = 0;
	  	$sumatoriaNotas = 0;
	  	$promedioActitudinal = 0;
	  	if($row_rsAlumnos['nota13'] > 0)
	  	{
	  		$sumatoriaNotas += $row_rsAlumnos['nota13'];
	  		$cantidadNotas++;
	  	}
	  	if($row_rsAlumnos['nota14'] > 0)
	  	{
	  		$sumatoriaNotas += $row_rsAlumnos['nota14'];
	  		$cantidadNotas++;
	  	}
	  	if($cantidadNotas == 0)
	  	{
	  		////avoid division by cero
	  		$cantidadNotas = 1;
	  	}
	  	$promedioActitudinal = $sumatoriaNotas/$cantidadNotas;

	  	echo $promedioActitudinal > 0 ? number_format(($promedioActitudinal*10/100),1) : '';
	  	$promedioActitudinal = $promedioActitudinal*10/100;
	  	$cantidadNotas = 0;
	  	$sumatoriaNotas = 0;
	  	?>    	
    </td> 
    <td align="center" ><?php echo is_numeric($row_rsAlumnos['nota15']) ? number_format($row_rsAlumnos['nota15'],1): ''; ?></td>
      <!-- <td style="border-left:double; border-right:double;"></td> -->
      <!-- <td style="border-left:double; border-right:double;"></td> -->
      <td style="border-left:double; border-right:double;"><? 
      $valoracionFinal = number_format(($row_rsAlumnos['nota15']*20/100),1);
      echo $valoracionFinal > 0 ? $valoracionFinal : ''; 
      ?></td>
      <td>
      	<?
      	$notaFinal = $promedioCognitivo + $promedioProcedimental + $promedioActitudinal + $valoracionFinal;
      	echo $notaFinal > 0 ? number_format($notaFinal,1) : '';
      	$promedioCognitivo = 0; 
      	$promedioProcedimental = 0;
      	$promedioActitudinal = 0;
      	$valoracionFinal = 0;
      	?>
      </td>
    <td align="center" style="border-left:double; border-right:double;"><?php echo is_numeric($row_rsAlumnos['recuperacion']) ? number_format($row_rsAlumnos['recuperacion'],1): ''; ?></td>
    
    <!--<td >-->
      <?php      
			//$paraPromediar1 = 0;
			//if ($row_rsAlumnos['nota1'] > 0) { $paraPromediar1++; }
			//if ($row_rsAlumnos['nota2'] > 0) { $paraPromediar1++; }
			//if ($row_rsAlumnos['nota3'] > 0) { $paraPromediar1++; }
			//if ($row_rsAlumnos['nota4'] > 0) { $paraPromediar1++; }
			//if ($row_rsAlumnos['nota5'] > 0) { $paraPromediar1++; }
			//if ($row_rsAlumnos['nota6'] > 0) { $paraPromediar1++; }
			//if ($paraPromediar1 == 0) {  $paraPromediar1 = 1;}
			//$paraPromediar2 = 0;
			//if ($row_rsAlumnos['nota7'] > 0) { $paraPromediar2++; }
			//if ($row_rsAlumnos['nota8'] > 0) { $paraPromediar2++; }
			//if ($row_rsAlumnos['nota9'] > 0) { $paraPromediar2++; }
			//if ($row_rsAlumnos['nota10'] > 0) { $paraPromediar2++;  }
			//if ($row_rsAlumnos['nota11'] > 0) { $paraPromediar2++;  }
			//if ($row_rsAlumnos['nota12'] > 0) { $paraPromediar2++;  }
			//if ($paraPromediar2 == 0) {  $paraPromediar2 = 1;}

			//$paraPromediar3 = 0;
			//if ($row_rsAlumnos['nota13'] > 0) { $paraPromediar3++;  }
			//if ($row_rsAlumnos['nota14'] > 0) { $paraPromediar3++;  }
			//if ($paraPromediar3 == 0) {  $paraPromediar3 = 1;}
			
			//$paraPromediar4 = 0;
			//if ($row_rsAlumnos['nota15'] > 0) { $paraPromediar4++;  }
			//if ($paraPromediar4 == 0) {  $paraPromediar4 = 1;}

			//      echo number_format(
			//        (((($row_rsAlumnos['nota1'] +
			//         $row_rsAlumnos['nota2'] +
			//         $row_rsAlumnos['nota3'] +
			//         $row_rsAlumnos['nota4'] +
			//         $row_rsAlumnos['nota5'] +
			//         $row_rsAlumnos['nota6'] )/$paraPromediar1) * 40 )/100)
			//+
			//(((($row_rsAlumnos['nota7'] +
			//  $row_rsAlumnos['nota8'] +
			//  $row_rsAlumnos['nota9'] +
			//  $row_rsAlumnos['nota10'] +
			//  $row_rsAlumnos['nota11'] +
			//  $row_rsAlumnos['nota12'] )/$paraPromediar2) * 30 )/100) 
			//+
			//(((($row_rsAlumnos['nota13'] +
			//$row_rsAlumnos['nota14'] )/$paraPromediar3) * 10 )/100)
			//          +
			//((($row_rsAlumnos['nota15'] /$paraPromediar4) * 20 )/100),1);

      ?>
<!--</td>
<td align="center" >-->
  <?php //echo $row_rsAlumnos['fallas']; ?>
<!--</td>-->
  <?
  /*
			$promedioActualAcumulado = 0;
			$promediosEncontrados = array();
			for($bimestreActual = 1 ; $bimestreActual <= $_GET['bimestre']; $bimestreActual++)
			{
			    $promediosEncontrados[$bimestreActual] = $Notas->notaFinalBimestre ($row_rsAlumnos['idAlumno'],$_GET['idCurso'],$_GET['idMateria'],$bimestreActual); 
			}
			$promedioActualAcumulado = array_sum($promediosEncontrados) / count($promediosEncontrados) ;
*/
			//$promedioProyectado = ($promedioActualAcumulado + $Notas->notaFinalBimestre ($row_rsAlumnos['idAlumno'],$_GET['idCurso'],$_GET['idMateria'],$_GET['bimestre']))/ 2;
  ?>

<!--<td>
 <? //echo number_format($promedioProyectado,1); ?>
  </td>-->
<!-- <td style="border-left:double; border-right:double;">
  <? //echo number_format($promedioActualAcumulado,1); ?>

</td> -->
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td style="border-right:double;">&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td style="border-right:double;">&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
</tr>
<?php } while ($row_rsAlumnos = mysql_fetch_assoc($rsAlumnos)); ?>
</table>

<div class="row">
  <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
  <legend>ACTIVIDADES COGNITIVAS</legend>
<?
$sql = "select  substr(fechaActividad,1,10) as Fecha, 
		##notaNombre as Nota, 
		notaDescripcion as Descripción
from cursomateriaprofesornota 
where idCursoMateriaProfesor in (select idCursoMateriaProfesor from cursomateriaprofesor where idCurso = ".$_GET['idCurso']." and idMateria = ".$_GET['idMateria'].") 
and bimestre = ".$_GET['bimestre']." and nota between 1 and 6 order by cursomateriaprofesornota.nota asc
";
$base = new BD();
$base->consultaATabla($sql);
?>    
  </div>
  <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
  <legend>ACTIVIDADES PROCEDIMENTALES</legend>
<?
$sql = "select  substr(fechaActividad,1,10) as Fecha,
		##notaNombre as Nota, 
		notaDescripcion as Descripción
from cursomateriaprofesornota 
where idCursoMateriaProfesor in (select idCursoMateriaProfesor from cursomateriaprofesor where idCurso = ".$_GET['idCurso']." and idMateria = ".$_GET['idMateria'].") 
and bimestre = ".$_GET['bimestre']." and nota between 7 and 12 order by cursomateriaprofesornota.nota asc
";
$base = new BD();
$base->consultaATabla($sql);
?>      
  </div>  
</div>



  </body>
</html>
<?php
mysql_free_result($rsAlumnos);
?>

