<?php require_once('Connections/MySQL.php'); ?>
<?php
if (!isset($_SESSION)) {
  session_start();
}
$MM_authorizedUsers = "Administrador,Profesor";
$MM_donotCheckaccess = "false";

// *** Restrict Access To Page: Grant or deny access to this page
function isAuthorized($strUsers, $strGroups, $UserName, $UserGroup) { 
  // For security, start by assuming the visitor is NOT authorized. 
  $isValid = False; 

  // When a visitor has logged into this site, the Session variable MM_Username set equal to their username. 
  // Therefore, we know that a user is NOT logged in if that Session variable is blank. 
  if (!empty($UserName)) { 
    // Besides being logged in, you may restrict access to only certain users based on an ID established when they login. 
    // Parse the strings into arrays. 
    $arrUsers = Explode(",", $strUsers); 
    $arrGroups = Explode(",", $strGroups); 
    if (in_array($UserName, $arrUsers)) { 
      $isValid = true; 
    } 
    // Or, you may restrict access to only certain users based on their username. 
    if (in_array($UserGroup, $arrGroups)) { 
      $isValid = true; 
    } 
    if (($strUsers == "") && false) { 
      $isValid = true; 
    } 
  } 
  return $isValid; 
}

$MM_restrictGoTo = "error.php";
if (!((isset($_SESSION['MM_Username'])) && (isAuthorized("",$MM_authorizedUsers, $_SESSION['MM_Username'], $_SESSION['MM_UserGroup'])))) {   
  $MM_qsChar = "?";
  $MM_referrer = $_SERVER['PHP_SELF'];
  if (strpos($MM_restrictGoTo, "?")) $MM_qsChar = "&";
  if (isset($QUERY_STRING) && strlen($QUERY_STRING) > 0) 
  $MM_referrer .= "?" . $QUERY_STRING;
  $MM_restrictGoTo = $MM_restrictGoTo. $MM_qsChar . "accesscheck=" . urlencode($MM_referrer);
  header("Location: ". $MM_restrictGoTo); 
  exit;
}
?>
<?php
if (!function_exists("GetSQLValueString")) {
function GetSQLValueString($theValue, $theType, $theDefinedValue = "", $theNotDefinedValue = "") 
{
  if (PHP_VERSION < 6) {
    $theValue = get_magic_quotes_gpc() ? stripslashes($theValue) : $theValue;
  }

  $theValue = function_exists("mysql_real_escape_string") ? mysql_real_escape_string($theValue) : mysql_escape_string($theValue);

  switch ($theType) {
    case "text":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;    
    case "long":
    case "int":
      $theValue = ($theValue != "") ? intval($theValue) : "NULL";
      break;
    case "double":
      $theValue = ($theValue != "") ? doubleval($theValue) : "NULL";
      break;
    case "date":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;
    case "defined":
      $theValue = ($theValue != "") ? $theDefinedValue : $theNotDefinedValue;
      break;
  }
  return $theValue;
}
}

mysql_select_db($database_MySQL, $MySQL);
$query_rsCurso = "	SELECT 	distinct
							c.idCurso, 
							c.ano, 
							c.curso 
					FROM 	cursomateriaprofesor as cmp
					inner 	join curso as c on c.idCurso = cmp.idCurso
					inner 	join usuario as u on u.idUsuario = cmp.idProfesor
					where 1 = 1 
					and c.ano = ".date("Y");
if ($_SESSION['MM_UserGroup'] != 'Administrador')
	{
	$query_rsCurso .= " and u.usuario = '".$_SESSION['MM_Username']."'";
	}
$query_rsCurso .= " ORDER BY c.ano desc, c.orden ASC";
$rsCurso = mysql_query($query_rsCurso, $MySQL) or die(mysql_error());
$row_rsCurso = mysql_fetch_assoc($rsCurso);
$totalRows_rsCurso = mysql_num_rows($rsCurso);


mysql_select_db($database_MySQL, $MySQL);
$query_rsBimestre = "SELECT * FROM parametro WHERE parametro = 'bimestreActual'";
$rsBimestre = mysql_query($query_rsBimestre, $MySQL) or die(mysql_error());
$row_rsBimestre = mysql_fetch_assoc($rsBimestre);
$totalRows_rsBimestre = mysql_num_rows($rsBimestre);
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>

<title>:: Boletin ::</title>
<? include "header.php"; ?>
<script language="javascript" type="text/javascript">

function ColocarNota(idAlumno,idCurso,idMateria,bimestre,id,valor)
	{
	valor = valor.replace(',','.');
	valor = valor.replace(' ','');
	valor = NuloACero(valor);
	var nombreCampo = document.getElementById(id).name; 
	
	if(!isNaN(valor))
		{
		if (nombreCampo == 'fallas')
			{
			mostrar_url('BoletinCrearFallas.php','idAlumno='+idAlumno+'&idCurso='+idCurso+'&idMateria='+idMateria+'&bimestre='+bimestre+'&valor='+valor,'resultado_'+idAlumno,'get');		
			}
			else
			{	
			if(valor <= 5 && valor >= 0)
				{	
				
				if (nombreCampo == 'recuperacion')
					{
					mostrar_url('BoletinCrearRecuperacion.php','idAlumno='+idAlumno+'&idCurso='+idCurso+'&idMateria='+idMateria+'&bimestre='+bimestre+'&valor='+valor,'resultado_'+idAlumno,'get');							
					}
					else
					{
					mostrar_url('BoletinCrear.php','idAlumno='+idAlumno+'&idCurso='+idCurso+'&idMateria='+idMateria+'&bimestre='+bimestre+'&nota='+nombreCampo+'&valor='+valor,'resultado_'+idAlumno,'get');	
					}
					////Recalcula la nota final, promedio proyectado.
					var nota1 =parseFloat(NuloACero(document.getElementById('nota1_'+idAlumno).value));
					var nota2 =parseFloat(NuloACero(document.getElementById('nota2_'+idAlumno).value));
					var nota3 =parseFloat(NuloACero(document.getElementById('nota3_'+idAlumno).value));
					var nota4 =parseFloat(NuloACero(document.getElementById('nota4_'+idAlumno).value));
					var nota5 =parseFloat(NuloACero(document.getElementById('nota5_'+idAlumno).value));
					var nota6 =parseFloat(NuloACero(document.getElementById('nota6_'+idAlumno).value));
					var nota7 =parseFloat(NuloACero(document.getElementById('nota7_'+idAlumno).value));
					var nota8 =parseFloat(NuloACero(document.getElementById('nota8_'+idAlumno).value));
					var nota9 =parseFloat(NuloACero(document.getElementById('nota9_'+idAlumno).value));
					var nota10 =parseFloat(NuloACero(document.getElementById('nota10_'+idAlumno).value));
					var nota11 =parseFloat(NuloACero(document.getElementById('nota11_'+idAlumno).value));
					var nota12 =parseFloat(NuloACero(document.getElementById('nota12_'+idAlumno).value));
					var nota13 =parseFloat(NuloACero(document.getElementById('nota13_'+idAlumno).value));
					var nota14 =parseFloat(NuloACero(document.getElementById('nota14_'+idAlumno).value));
					var nota15 =parseFloat(NuloACero(document.getElementById('nota15_'+idAlumno).value));
					
					var paraPromediar1 = 0;
					if (nota1 > 0) { paraPromediar1++;	}
					if (nota2 > 0) { paraPromediar1++;	}
					if (nota3 > 0) { paraPromediar1++;	}
					if (nota4 > 0) { paraPromediar1++;	}
					if (nota5 > 0) { paraPromediar1++;	}
					if (nota6 > 0) { paraPromediar1++;	}
					if (paraPromediar1 == 0) {  paraPromediar1 = 1;}
					var paraPromediar2 = 0;
					if (nota7 > 0) { paraPromediar2++;	}
					if (nota8 > 0) { paraPromediar2++;	}
					if (nota9 > 0) { paraPromediar2++;	}
					if (nota10 > 0) { paraPromediar2++;	}
					if (nota11 > 0) { paraPromediar2++;	}
					if (nota12 > 0) { paraPromediar2++;	}
					if (paraPromediar2 == 0) {  paraPromediar2 = 1;}
					
					var paraPromediar3 = 0;
					if (nota13 > 0) { paraPromediar3++;	}
					if (nota14 > 0) { paraPromediar3++;	}
					if (paraPromediar3 == 0) {  paraPromediar3 = 1;}

					var paraPromediar4 = 0;
					if (nota15 > 0) { paraPromediar4++;	}
					if (paraPromediar4 == 0) {  paraPromediar4 = 1;}
					
					
					var calculo = ((((nota1 + nota2 + nota3 + nota4 + nota5 + nota6)/paraPromediar1) * 40 )/100) 
					+ ((((nota7+nota8+nota9+nota10+nota11+nota12)/paraPromediar2) * 30 )/100)	
					+ ((((nota13+nota14)/paraPromediar3) * 10 )/100)
					+ ((((nota15)/paraPromediar4) * 20 )/100);
					var definitiva = parseFloat(calculo);
					var notaRecuperacion = parseFloat(document.getElementById('recuperacion_'+idAlumno).value);
					var promedioActualAcumulado = parseFloat(document.getElementById('promedioActualAcumulado_'+idAlumno).value);
					document.getElementById('definitiva_'+idAlumno).value = parseFloat(definitiva).toFixed(1);
					if (definitiva > notaRecuperacion)
					{
						var promedioProyectado = parseFloat( (definitiva + promedioActualAcumulado )/ 2 ).toFixed(1);
					}
					else
					{
						var promedioProyectado = parseFloat( (notaRecuperacion + promedioActualAcumulado )/ 2 ).toFixed(1);
					}
					document.getElementById('promedioProyectado_'+idAlumno).value = promedioProyectado;
					
				}
				else
				{
				//La nota es mayor a 5, debe ser menor o igual	
				alert('El numero minimo para la nota es 0 y el mayor es 5.');				
				}								
			}

		}
		else
		{
		//La nota no es numerica
		alert('Por favor coloque un valor n�merico para la nota. El valor actual es ('+valor+')');
		}
	}
	
var filaSeleccionada = 0;

function sombrearTr(id)
	{
	if(filaSeleccionada > 0)
		{
		document.getElementById('tr_'+filaSeleccionada).className = "";
		}
	document.getElementById('tr_'+id).className = "trseleccionado";
	filaSeleccionada = id;
	}

function fBoletinMostrarAlumnos()
	{
	var idCurso,idMateria,bimestre,divTitulos, titulo, divBoletin;
	titulo = '';
	divTitulos = document.getElementById('divBoletinTitulo');
	idCurso =document.getElementById('idCurso');
	
	if(idCurso.options[idCurso.selectedIndex].text)
		{
		titulo += '<center>Notas para el curso: <b>'+idCurso.options[idCurso.selectedIndex].text+'</b>';	
		}
	
	idCurso = idCurso.options[idCurso.selectedIndex].value;	
	idMateria =document.getElementById('idMateria');
	
	if(idMateria.options[idMateria.selectedIndex].text)
		{
		titulo += ' Materia: <b>'+idMateria.options[idMateria.selectedIndex].text+'</b>';
		}
	
	idMateria = idMateria.options[idMateria.selectedIndex].value;	
	bimestre =document.getElementById('bimestre');
	
	if(bimestre.options[bimestre.selectedIndex].text)
		{
		titulo += ' bimestre: <b>'+bimestre.options[bimestre.selectedIndex].text+'</b></center>';
		}
	
	divTitulos.innerHTML = titulo;
	bimestre = bimestre.options[bimestre.selectedIndex].value;		
	if ( idCurso > 0 && idMateria > 0 && bimestre > 0)
		{
		mostrar_url('BoletinAlumnoListar.php','idCurso='+idCurso+'&idMateria='+idMateria+'&bimestre='+bimestre,'divBoletin','get');	
		}
		else
		{
			divBoletin = divTitulos = document.getElementById('divBoletin');
			divBoletin.innerHTML = 'Por favor seleccione todos los criterios.';
		}
	}
	
function fBoletinMostrarMaterias(idCurso)
	{
		mostrar_url('Parametros.php','accion=materiacursoboletin&idCurso='+idCurso,'divIdMateria','get');
	}
			
</script>
</head>

<body>
<? include "menu.php"; ?>

<table border="1"><tr valign="top">

<td>
&nbsp;&nbsp;<br />
<table>
<thead><tr><td colspan="2">Notas</td></tr></thead>
<tr><td>Curso:</td><td><select name="idCurso" id="idCurso" onchange="javascript:fBoletinMostrarMaterias(this.value);"><option></option>
  <?php
do {  
?>
  <option value="<?php echo $row_rsCurso['idCurso']?>"><?php echo $row_rsCurso['ano']?> - <?php echo $row_rsCurso['curso']?></option>
  <?php
} while ($row_rsCurso = mysql_fetch_assoc($rsCurso));
  $rows = mysql_num_rows($rsCurso);
  if($rows > 0) {
      mysql_data_seek($rsCurso, 0);
	  $row_rsCurso = mysql_fetch_assoc($rsCurso);
  }
?>
</select></td></tr>
<tr><td>Materia:</td><td>
<div id="divIdMateria">
<select ></select>
</div>
</td>

</tr>
<tr><td>Periodo:</td><td><select name="bimestre" id="bimestre" onchange="javascript:fBoletinMostrarAlumnos();"><option></option>
  <?php
do {  
?>
  <option value="<?php echo $row_rsBimestre['valor']?>"><?php echo $row_rsBimestre['valor']?></option>
  <?php
} while ($row_rsBimestre = mysql_fetch_assoc($rsBimestre));
  $rows = mysql_num_rows($rsBimestre);
  if($rows > 0) {
      mysql_data_seek($rsBimestre, 0);
	  $row_rsBimestre = mysql_fetch_assoc($rsBimestre);
  }
?>
</select></td></tr>
<thead><tr><td colspan="2">&nbsp;</td></tr></thead>
</table>

</td>

<td width="100%"><div id="divBoletinTitulo"></div><div id="divBoletin"></div></td></tr>


</body>
</html>
<?php
mysql_free_result($rsCurso);
mysql_free_result($rsBimestre);
?>
