<?php require_once('Connections/MySQL.php'); ?>
<?php
if (!isset($_SESSION)) {
  session_start();
}
if (!function_exists("GetSQLValueString")) {
function GetSQLValueString($theValue, $theType, $theDefinedValue = "", $theNotDefinedValue = "") 
{
  if (PHP_VERSION < 6) {
    $theValue = get_magic_quotes_gpc() ? stripslashes($theValue) : $theValue;
  }

  $theValue = function_exists("mysql_real_escape_string") ? mysql_real_escape_string($theValue) : mysql_escape_string($theValue);

  switch ($theType) {
    case "text":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;    
    case "long":
    case "int":
      $theValue = ($theValue != "") ? intval($theValue) : "NULL";
      break;
    case "double":
      $theValue = ($theValue != "") ? doubleval($theValue) : "NULL";
      break;
    case "date":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;
    case "defined":
      $theValue = ($theValue != "") ? $theDefinedValue : $theNotDefinedValue;
      break;
  }
  return $theValue;
}
}

$editFormAction = $_SERVER['PHP_SELF'];
if (isset($_SERVER['QUERY_STRING'])) {
  $editFormAction .= "?" . htmlentities($_SERVER['QUERY_STRING']);
}

if ((isset($_POST["MM_update"])) && ($_POST["MM_update"] == "form1")) {
  $updateSQL = sprintf("UPDATE parametro SET parametro=%s, valor=%s WHERE idParametro=%s",
                       GetSQLValueString($_POST['parametro'], "text"),
                       GetSQLValueString($_POST['valor'], "text"),
                       GetSQLValueString($_POST['idParametro'], "int"));

  mysql_select_db($database_MySQL, $MySQL);
  $Result1 = mysql_query($updateSQL, $MySQL) or die(mysql_error());

  $updateGoTo = "Parametro.php";
  if (isset($_SERVER['QUERY_STRING'])) {
    $updateGoTo .= (strpos($updateGoTo, '?')) ? "&" : "?";
    $updateGoTo .= $_SERVER['QUERY_STRING'];
  }
  header(sprintf("Location: %s", $updateGoTo));
}

$colname_rsParametro = "-1";
if (isset($_GET['idParametro'])) {
  $colname_rsParametro = $_GET['idParametro'];
}
mysql_select_db($database_MySQL, $MySQL);
$query_rsParametro = sprintf("SELECT * FROM parametro WHERE idParametro = %s", GetSQLValueString($colname_rsParametro, "int"));
$rsParametro = mysql_query($query_rsParametro, $MySQL) or die(mysql_error());
$row_rsParametro = mysql_fetch_assoc($rsParametro);
$totalRows_rsParametro = mysql_num_rows($rsParametro);
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title>:: Informacion de la cuenta ::</title>
<? include "header.php"; ?>
</head>

<body>
<? include "menu.php"; ?>
<form action="<?php echo $editFormAction; ?>" method="post" name="form1" id="form1"><br />

  <table align="center">
  <thead>
    <tr >
      <td colspan="2">Parametro</td>
    </tr>  
    </thead>
    <tr valign="baseline">
      <td nowrap="nowrap" align="right">Parametro:</td>
      <td><input type="text" name="parametro" value="<?php echo htmlentities($row_rsParametro['parametro'], ENT_COMPAT, 'utf-8'); ?>" size="32" /></td>
    </tr>
    <tr valign="baseline">
      <td nowrap="nowrap" align="right">Valor:</td>
      <td><input type="text" name="valor" value="<?php echo htmlentities($row_rsParametro['valor'], ENT_COMPAT, 'utf-8'); ?>" size="32" /></td>
    </tr>
    <thead>
    <tr >
      <td colspan="2"><input type="submit" value="Actualizar" /></td>
    </tr>
    </thead>
  </table>
  <input type="hidden" name="MM_update" value="form1" />
  <input type="hidden" name="idParametro" value="<?php echo $row_rsParametro['idParametro']; ?>" /><br />

</form>

</body>
</html>
<?php
mysql_free_result($rsParametro);
?>
