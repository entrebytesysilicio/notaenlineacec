<?php require_once('Connections/MySQL.php'); ?>
<?php
if (!isset($_SESSION)) {
  session_start();
}
$MM_authorizedUsers = "Administrador";
$MM_donotCheckaccess = "false";

// *** Restrict Access To Page: Grant or deny access to this page
function isAuthorized($strUsers, $strGroups, $UserName, $UserGroup) { 
  // For security, start by assuming the visitor is NOT authorized. 
  $isValid = False; 

  // When a visitor has logged into this site, the Session variable MM_Username set equal to their username. 
  // Therefore, we know that a user is NOT logged in if that Session variable is blank. 
  if (!empty($UserName)) { 
    // Besides being logged in, you may restrict access to only certain users based on an ID established when they login. 
    // Parse the strings into arrays. 
    $arrUsers = Explode(",", $strUsers); 
    $arrGroups = Explode(",", $strGroups); 
    if (in_array($UserName, $arrUsers)) { 
      $isValid = true; 
    } 
    // Or, you may restrict access to only certain users based on their username. 
    if (in_array($UserGroup, $arrGroups)) { 
      $isValid = true; 
    } 
    if (($strUsers == "") && false) { 
      $isValid = true; 
    } 
  } 
  return $isValid; 
}

$MM_restrictGoTo = "error.php";
if (!((isset($_SESSION['MM_Username'])) && (isAuthorized("",$MM_authorizedUsers, $_SESSION['MM_Username'], $_SESSION['MM_UserGroup'])))) {   
  $MM_qsChar = "?";
  $MM_referrer = $_SERVER['PHP_SELF'];
  if (strpos($MM_restrictGoTo, "?")) $MM_qsChar = "&";
  if (isset($QUERY_STRING) && strlen($QUERY_STRING) > 0) 
  $MM_referrer .= "?" . $QUERY_STRING;
  $MM_restrictGoTo = $MM_restrictGoTo. $MM_qsChar . "accesscheck=" . urlencode($MM_referrer);
  header("Location: ". $MM_restrictGoTo); 
  exit;
}
?>
<?php
if (!function_exists("GetSQLValueString")) {
function GetSQLValueString($theValue, $theType, $theDefinedValue = "", $theNotDefinedValue = "") 
{
  if (PHP_VERSION < 6) {
    $theValue = get_magic_quotes_gpc() ? stripslashes($theValue) : $theValue;
  }

  $theValue = function_exists("mysql_real_escape_string") ? mysql_real_escape_string($theValue) : mysql_escape_string($theValue);

  switch ($theType) {
    case "text":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;    
    case "long":
    case "int":
      $theValue = ($theValue != "") ? intval($theValue) : "NULL";
      break;
    case "double":
      $theValue = ($theValue != "") ? doubleval($theValue) : "NULL";
      break;
    case "date":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;
    case "defined":
      $theValue = ($theValue != "") ? $theDefinedValue : $theNotDefinedValue;
      break;
  }
  return $theValue;
}
}

$editFormAction = $_SERVER['PHP_SELF'];
if (isset($_SERVER['QUERY_STRING'])) {
  $editFormAction .= "?" . htmlentities($_SERVER['QUERY_STRING']);
}

if ((isset($_POST["MM_insert"])) && ($_POST["MM_insert"] == "form1")) {
  $insertSQL = sprintf("INSERT INTO usuario (nombre, usuario, clave, idPerfil) VALUES (%s, %s, %s, %s)",
                       GetSQLValueString($_POST['nombre'], "text"),
                       GetSQLValueString($_POST['usuario'], "text"),
                       GetSQLValueString($_POST['clave'], "text"),
                       GetSQLValueString($_POST['idPerfil'], "int"));

  mysql_select_db($database_MySQL, $MySQL);
  $Result1 = mysql_query($insertSQL, $MySQL) or die(mysql_error());

  $insertGoTo = "usuario_crear.php";
  if (isset($_SERVER['QUERY_STRING'])) {
    $insertGoTo .= (strpos($insertGoTo, '?')) ? "&" : "?";
    $insertGoTo .= $_SERVER['QUERY_STRING'];
  }
  header(sprintf("Location: %s", $insertGoTo));
}

mysql_select_db($database_MySQL, $MySQL);
$query_rs_perfil = "SELECT * FROM perfil ORDER BY perfil ASC";
$rs_perfil = mysql_query($query_rs_perfil, $MySQL) or die(mysql_error());
$row_rs_perfil = mysql_fetch_assoc($rs_perfil);
$totalRows_rs_perfil = mysql_num_rows($rs_perfil);
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>

<title>:: Usuarios ::</title>
<link href="estilo.css" rel="stylesheet" type="text/css" />
</head>

<body>
<? include "menu.php"; ?>
<br />
<br />
<?
$a = new eliax();
$a->tabla = 'usuario';
$a->agregarRegistroMostrarFormulario();
?>
<br />
<?
$a->tabla = 'alumno';
$a->agregarRegistroMostrarFormulario();
?>
<br />
<?
$a->tabla = 'alumnoacudiente';
$a->agregarRegistroMostrarFormulario();
?>
<br />
<?
$a->tabla = 'alumnocurso';
$a->agregarRegistroMostrarFormulario();
?>
<br />
<?
$a->tabla = 'boletin';
$a->agregarRegistroMostrarFormulario();
?>
<br />
<?
$a->tabla = 'curso';
$a->agregarRegistroMostrarFormulario();
?>
<br />
<?
$a->tabla = 'cursomateriadesempeno';
$a->agregarRegistroMostrarFormulario();
?>
<br />
<?
$a->tabla = 'cursomateriaprofesor';
$a->agregarRegistroMostrarFormulario();
?>
<br />
<?
$a->tabla = 'desempeno';
$a->agregarRegistroMostrarFormulario();
?>
<br />
<?
$a->tabla = 'materia';
$a->agregarRegistroMostrarFormulario();
?>
<br />
<?
$a->tabla = 'perfil';
$a->agregarRegistroMostrarFormulario();
?>
<br />
<?




?>
<p>&nbsp;</p>
:: Listar usuarios:

</body>
</html>
<?php
mysql_free_result($rs_perfil);
?>
