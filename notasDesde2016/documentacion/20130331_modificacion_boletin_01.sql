ALTER TABLE `boletin`
	CHANGE COLUMN `fallas` `fallas` INT(10) NOT NULL DEFAULT '0' AFTER `nota14`;

ALTER TABLE `boletin`
	CHANGE COLUMN `recuperacion` `recuperacion` FLOAT NOT NULL DEFAULT '0' AFTER `fallas`;

ALTER TABLE `boletin`
	CHANGE COLUMN `notapadre` `notapadre` INT(11) NOT NULL DEFAULT '0' AFTER `recuperacion`;

ALTER TABLE `boletin`
	CHANGE COLUMN `nota` `nota` FLOAT NULL DEFAULT '0' COMMENT 'Nota' AFTER `bimestre`;