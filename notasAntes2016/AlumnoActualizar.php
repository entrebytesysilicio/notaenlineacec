<?php require_once('Connections/MySQL.php'); ?>
<?php
if (!isset($_SESSION)) {
  session_start();
}
$MM_authorizedUsers = "Administrador,Profesor";
$MM_donotCheckaccess = "false";

// *** Restrict Access To Page: Grant or deny access to this page
function isAuthorized($strUsers, $strGroups, $UserName, $UserGroup) { 
  // For security, start by assuming the visitor is NOT authorized. 
  $isValid = False; 

  // When a visitor has logged into this site, the Session variable MM_Username set equal to their username. 
  // Therefore, we know that a user is NOT logged in if that Session variable is blank. 
  if (!empty($UserName)) { 
    // Besides being logged in, you may restrict access to only certain users based on an ID established when they login. 
    // Parse the strings into arrays. 
    $arrUsers = Explode(",", $strUsers); 
    $arrGroups = Explode(",", $strGroups); 
    if (in_array($UserName, $arrUsers)) { 
      $isValid = true; 
    } 
    // Or, you may restrict access to only certain users based on their username. 
    if (in_array($UserGroup, $arrGroups)) { 
      $isValid = true; 
    } 
    if (($strUsers == "") && false) { 
      $isValid = true; 
    } 
  } 
  return $isValid; 
}

$MM_restrictGoTo = "error.php";
if (!((isset($_SESSION['MM_Username'])) && (isAuthorized("",$MM_authorizedUsers, $_SESSION['MM_Username'], $_SESSION['MM_UserGroup'])))) {   
  $MM_qsChar = "?";
  $MM_referrer = $_SERVER['PHP_SELF'];
  if (strpos($MM_restrictGoTo, "?")) $MM_qsChar = "&";
  if (isset($QUERY_STRING) && strlen($QUERY_STRING) > 0) 
  $MM_referrer .= "?" . $QUERY_STRING;
  $MM_restrictGoTo = $MM_restrictGoTo. $MM_qsChar . "accesscheck=" . urlencode($MM_referrer);
  header("Location: ". $MM_restrictGoTo); 
  exit;
}
?>
<?php
if (!function_exists("GetSQLValueString")) {
function GetSQLValueString($theValue, $theType, $theDefinedValue = "", $theNotDefinedValue = "") 
{
  if (PHP_VERSION < 6) {
    $theValue = get_magic_quotes_gpc() ? stripslashes($theValue) : $theValue;
  }

  $theValue = function_exists("mysql_real_escape_string") ? mysql_real_escape_string($theValue) : mysql_escape_string($theValue);

  switch ($theType) {
    case "text":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;    
    case "long":
    case "int":
      $theValue = ($theValue != "") ? intval($theValue) : "NULL";
      break;
    case "double":
      $theValue = ($theValue != "") ? doubleval($theValue) : "NULL";
      break;
    case "date":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;
    case "defined":
      $theValue = ($theValue != "") ? $theDefinedValue : $theNotDefinedValue;
      break;
  }
  return $theValue;
}
}

$editFormAction = $_SERVER['PHP_SELF'];
if (isset($_SERVER['QUERY_STRING'])) {
  $editFormAction .= "?" . htmlentities($_SERVER['QUERY_STRING']);
}

	$activar = ($_POST['activo'] == 'on') ? "1":"0";
	
if ((isset($_POST["MM_update"])) && ($_POST["MM_update"] == "form1")) {
  $updateSQL = sprintf("UPDATE alumno SET nombres=%s, apellidos=%s, identificacion=%s, activo=%s WHERE idAlumno=%s",
                       GetSQLValueString($_POST['nombres'], "text"),
                       GetSQLValueString($_POST['apellidos'], "text"),
                       GetSQLValueString($_POST['identificacion'], "text"),
					   GetSQLValueString($activar, "int"),
                       GetSQLValueString($_POST['idAlumno'], "int"));

  mysql_select_db($database_MySQL, $MySQL);
  $Result1 = mysql_query($updateSQL, $MySQL) or die(mysql_error());

  $updateGoTo = "AlumnoActualizar.php";
  if (isset($_SERVER['QUERY_STRING'])) {
    $updateGoTo .= (strpos($updateGoTo, '?')) ? "&" : "?";
    $updateGoTo .= $_SERVER['QUERY_STRING'];
  }
  header(sprintf("Location: %s", $updateGoTo));
}

$colname_rsAlumno = "-1";
if (isset($_GET['idAlumno'])) {
  $colname_rsAlumno = $_GET['idAlumno'];
}
mysql_select_db($database_MySQL, $MySQL);
$query_rsAlumno = sprintf("SELECT * FROM alumno WHERE idAlumno = %s", GetSQLValueString($colname_rsAlumno, "int"));
$rsAlumno = mysql_query($query_rsAlumno, $MySQL) or die(mysql_error());
$row_rsAlumno = mysql_fetch_assoc($rsAlumno);
$totalRows_rsAlumno = mysql_num_rows($rsAlumno);
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>

<title>:: Alumno Actualizar ::</title>
<link href="estilo.css" rel="stylesheet" type="text/css" />
<? include "header.php"; ?>
<script language="javascript" type="text/javascript">
	function fUsuarioActualizarMostrarAcudientes ()
	{
	var idUsuario;
	idUsuario = document.getElementById('idAlumno').value;
	mostrar_url('AcudienteAlumnoListar.php','idAlumno='+idUsuario,'divAlumnoActualizarListadoAcudientes','get');	
	}	
	
	function fAlumnoActualizarBuscarAcudiente (nombre)
	{
	mostrar_url('Parametros.php','accion=buscaracudiente&acudiente='+nombre,'divAlumnoActualizarAcudienteResultado','get');	
	}
	
function fAlumnoAcudienteAgregar()
		{
		var idAlumno, idAcudiente;
		idAlumno = document.getElementById('idAlumno').value;
		idAcudiente =document.getElementById('slAlumnoAcudienteEncontrados');
		idAcudiente = idAcudiente.options[idAcudiente.selectedIndex].value;
		mostrar_url('AlumnoAcudienteAgregar.php','MM_insert=form1&idAlumno='+idAlumno+"&idAcudiente="+idAcudiente,'divAlumnoActualizarListadoAcudientes','get');
		
				setTimeout('fUsuarioActualizarMostrarAcudientes ()', 2*1000); //segundos * mil, el parametro es en ms es decir 5 * 1000 = 5 segundos
			
			
			}	
			
				function fAcudienteAlumnoEliminar (idAlumnoAcudiente)
		{
			mostrar_url('AlumnoAcudienteEliminar.php','idAlumnoAcudiente='+idAlumnoAcudiente,'divAlumnoActualizarListadoAcudientes','get');
				setTimeout('fUsuarioActualizarMostrarAcudientes ()', 2*1000); //segundos * mil, el parametro es en ms es decir 5 * 1000 = 5 segundos
		}
		
	function fAlumnoCursoListar ()
	{
		idAlumno = document.getElementById('idAlumno').value;
	mostrar_url('AlumnoCursoListar.php','idAlumno='+idAlumno,'divAlumnoCursoListado','get');	
	}		
		
	
    </script>
</head>

<body onload="javascript:fUsuarioActualizarMostrarAcudientes (); javascript:fAlumnoCursoListar ();">
<? include "menu.php";

?>


<table align="center">
<tr valign="top"><td>
<form action="<?php echo $editFormAction; ?>" method="post" name="form1" id="form1">	
 <table align="center">
  <thead>
  <tr><td colspan="2">Actualizar alumno</td></tr>
  </thead>
    <tr valign="baseline">
      <td nowrap="nowrap" align="right">Nombres:</td>
      <td><input type="text" name="nombres" value="<?php echo htmlentities($row_rsAlumno['nombres'], ENT_COMPAT, 'iso-8859-1'); ?>" size="32" /></td>
    </tr>
    <tr valign="baseline">
      <td nowrap="nowrap" align="right">Apellidos:</td>
      <td><input type="text" name="apellidos" value="<?php echo htmlentities($row_rsAlumno['apellidos'], ENT_COMPAT, 'iso-8859-1'); ?>" size="32" /></td>
    </tr>
    <tr valign="baseline">
      <td nowrap="nowrap" align="right">Identificacion:</td>
      <td><input type="text" name="identificacion" value="<?php echo htmlentities($row_rsAlumno['identificacion'], ENT_COMPAT, 'iso-8859-1'); ?>" size="32" /></td>
    </tr>
    <tr valign="baseline">
      <td nowrap="nowrap" align="right">Activo:</td>
      <td><input type="checkbox" <?php if (!(strcmp($row_rsAlumno['activo'],1))) {echo "checked=\"checked\"";} ?> name="activo" /></td>
    </tr>    
    <thead>
    <tr valign="baseline">
      <td colspan="2"><input type="submit" value="Actualizar" /></td>
    </tr>
    </thead>
  </table>
  <input type="hidden" name="MM_update" value="form1" />
  <input type="hidden" id="idAlumno" name="idAlumno" value="<?php echo $row_rsAlumno['idAlumno']; ?>" />
</form>
</td><td>
<table>
<thead>
<tr><td>Acudientes</td></tr>
</thead>
<tr><td>Nombre: <input type="text" name="tbAlumnoActualizarAcudiente" id="tbAlumnoActualizarAcudiente" onkeyup="javascript:fAlumnoActualizarBuscarAcudiente(this.value);" /></td></tr>
<tr><td><div id="divAlumnoActualizarAcudienteResultado" class="divAlumnoActualizarAcudienteResultado"></div></td></tr>
<thead>
<tr><td><input type="button" name="Agregar acudiente" value="Agregar acudiente" onclick="javascript:fAlumnoAcudienteAgregar();" /></td></tr>
</thead>
</table>
</td></tr>
<tr><td colspan="2">
<table align="center" width="100%">
<thead>
<tr><td>Acudientes de <?php echo htmlentities($row_rsAlumno['nombres'], ENT_COMPAT, 'iso-8859-1'); ?> <?php echo htmlentities($row_rsAlumno['apellidos'], ENT_COMPAT, 'iso-8859-1'); ?></td></tr>
</thead>
<tr><td>

<div id="divAlumnoActualizarListadoAcudientes"></div>
</td></tr>
<thead>
<tr><td>&nbsp;</td></tr>
</thead>
</table>
</td></tr>


<tr><td colspan="2">
<table align="center" width="100%">
<thead><tr><td>Cursos en los que ha estado <?php echo htmlentities($row_rsAlumno['nombres'], ENT_COMPAT, 'iso-8859-1'); ?> <?php echo htmlentities($row_rsAlumno['apellidos'], ENT_COMPAT, 'iso-8859-1'); ?>	 </td></tr></thead>
<tr><td><div id="divAlumnoCursoListado"></div></td></tr>
<thead><tr><td>&nbsp;</td></tr></thead>
</table>

</td></tr>

</table>



 
<p>&nbsp;</p>
Cursos
Notas
Profesores
Promedio a�o/a�o





</body>
</html>
<?php
mysql_free_result($rsAlumno);
?>
