<?php require_once('Connections/MySQL.php'); ?>
<?php
if (!isset($_SESSION)) {
  session_start();
}
$MM_authorizedUsers = "Administrador";
$MM_donotCheckaccess = "false";

header('Content-Type: text/html; charset=utf-8');
// *** Restrict Access To Page: Grant or deny access to this page
function isAuthorized($strUsers, $strGroups, $UserName, $UserGroup) { 
  // For security, start by assuming the visitor is NOT authorized. 
  $isValid = False; 

  // When a visitor has logged into this site, the Session variable MM_Username set equal to their username. 
  // Therefore, we know that a user is NOT logged in if that Session variable is blank. 
  if (!empty($UserName)) { 
    // Besides being logged in, you may restrict access to only certain users based on an ID established when they login. 
    // Parse the strings into arrays. 
    $arrUsers = Explode(",", $strUsers); 
    $arrGroups = Explode(",", $strGroups); 
    if (in_array($UserName, $arrUsers)) { 
      $isValid = true; 
    } 
    // Or, you may restrict access to only certain users based on their username. 
    if (in_array($UserGroup, $arrGroups)) { 
      $isValid = true; 
    } 
    if (($strUsers == "") && false) { 
      $isValid = true; 
    } 
  } 
  return $isValid; 
}

$MM_restrictGoTo = "error.php";
if (!((isset($_SESSION['MM_Username'])) && (isAuthorized("",$MM_authorizedUsers, $_SESSION['MM_Username'], $_SESSION['MM_UserGroup'])))) {   
  $MM_qsChar = "?";
  $MM_referrer = $_SERVER['PHP_SELF'];
  if (strpos($MM_restrictGoTo, "?")) $MM_qsChar = "&";
  if (isset($QUERY_STRING) && strlen($QUERY_STRING) > 0) 
  $MM_referrer .= "?" . $QUERY_STRING;
  $MM_restrictGoTo = $MM_restrictGoTo. $MM_qsChar . "accesscheck=" . urlencode($MM_referrer);
  header("Location: ". $MM_restrictGoTo); 
  exit;
}

include "clases.php";

$base = new BD();
?>
<div class="row">
<?
if($_POST['actividad'] == 'historialAcademicoListar')
{
$sql = "select curso.ano, curso.curso, '' as institucion
		from alumnocurso 
		inner join curso 
			on alumnocurso.idCurso = curso.idCurso 
		where idAlumno = ".$_POST['idAlumno']."
		union
		select ano, curso, institucion
		from cursootrasinstituciones
		where idAlumno = ".$_POST['idAlumno']."
		order by 1 desc, 2 asc";

	if($resultado =mysql_query($sql,$MySQL))
	{
		if(mysql_num_rows($resultado) > 0)
		{

			$fila = mysql_fetch_array($resultado);
			$i = 1;
			do {

				if($i == 1 || $i == 11)
				{
					?>
					<div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
					<table class="table-hover table-striped table">
						<thead>
							<tr>
								<th>No.</th>
								<th>A&ntilde;o</th>
								<th>Curso</th>
								<th>Instituci&oacute;n</th>
							</tr>
						</thead>
						<tbody>
					<?
				}

				?>
				<tr>
					<td><?php echo $i; ?></td>
					<td><?php echo $fila['ano']; ?></td>
					<td><?php echo $fila['curso']; ?></td>					
					<td><?php echo $fila['institucion']; ?> </td>
				</tr>
				<?

				$i++;
				if($i == 11 )
				{
				?>
				</tbody>
				</table>
				</div>
				<?
				}

			}while($fila = mysql_fetch_array($resultado));
			if($i > 11)
			{
			?>
			</tbody>
			</table>
			</div>
			<?			
			}
		}
		else 
		{
		?>
		<div class="alert alert-info">
			<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
			No se encontraron datos.
		</div>
		<?	
		}
	}
	else 
	{	
		?>
		<div class="alert alert-danger">
			<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
			Error al consultar la informaci&oacute;n en la base de datos. <br><br><?php echo mysql_error(); ?>
		</div>
		<?
	}
}
?>
</div>
