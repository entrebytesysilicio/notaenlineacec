<?php require_once('Connections/MySQL.php'); ?>
<?php
if (!isset($_SESSION)) {
  session_start();
}
$MM_authorizedUsers = "Administrador";
$MM_donotCheckaccess = "false";

header('Content-Type: text/html; charset=utf-8');
// *** Restrict Access To Page: Grant or deny access to this page
function isAuthorized($strUsers, $strGroups, $UserName, $UserGroup) { 
  // For security, start by assuming the visitor is NOT authorized. 
  $isValid = False; 

  // When a visitor has logged into this site, the Session variable MM_Username set equal to their username. 
  // Therefore, we know that a user is NOT logged in if that Session variable is blank. 
  if (!empty($UserName)) { 
    // Besides being logged in, you may restrict access to only certain users based on an ID established when they login. 
    // Parse the strings into arrays. 
    $arrUsers = Explode(",", $strUsers); 
    $arrGroups = Explode(",", $strGroups); 
    if (in_array($UserName, $arrUsers)) { 
      $isValid = true; 
    } 
    // Or, you may restrict access to only certain users based on their username. 
    if (in_array($UserGroup, $arrGroups)) { 
      $isValid = true; 
    } 
    if (($strUsers == "") && false) { 
      $isValid = true; 
    } 
  } 
  return $isValid; 
}

$MM_restrictGoTo = "error.php";
if (!((isset($_SESSION['MM_Username'])) && (isAuthorized("",$MM_authorizedUsers, $_SESSION['MM_Username'], $_SESSION['MM_UserGroup'])))) {   
  $MM_qsChar = "?";
  $MM_referrer = $_SERVER['PHP_SELF'];
  if (strpos($MM_restrictGoTo, "?")) $MM_qsChar = "&";
  if (isset($QUERY_STRING) && strlen($QUERY_STRING) > 0) 
  $MM_referrer .= "?" . $QUERY_STRING;
  $MM_restrictGoTo = $MM_restrictGoTo. $MM_qsChar . "accesscheck=" . urlencode($MM_referrer);
  header("Location: ". $MM_restrictGoTo); 
  exit;
}

include "clases.php";

$base = new BD();

if($_POST['actividad'] == "listar")
{
	$sql = "select idCursoOtrasInstituciones, ano, curso, institucion, promovido from cursootrasinstituciones where idAlumno = ".$_POST['idAlumno'];
	if($resultado =mysql_query($sql,$MySQL))
	{
		if(mysql_num_rows($resultado) > 0)
		{
			?>
			<table class="table-hover table-striped table">
				<thead>
					<tr>
						<th>No.</th>
						<th>A&ntilde;o</th>
						<th>Instituci&oacute;n</th>
						<th>Curso</th>
						<th>Promovido</th>
						<th></th>
					</tr>
				</thead>
				<tbody>
			<?
			$fila = mysql_fetch_array($resultado);
			$i = 1;
			do {
				?>
				<tr>
					<td><?php echo $i; ?></td>
					<td><?php echo $fila['ano']; ?></td>
					<td><?php echo $fila['institucion']; ?> </td>
					<td><?php echo $fila['curso']; ?></td>
					<td><?php if($fila['promovido'] == 1) { ?>Si<? } else { ?>No<? } ?></td>
					<td><button type="button" name="cursoOtrasInstitucionesEliminar" id="cursoOtrasInstitucionesEliminiar" onclick="fcursoOtrasInstitucionesEliminar(<?php echo $fila['idCursoOtrasInstituciones']; ?>);" class="btn btn-danger">Eliminar</button></td>
				</tr>
				<?
				$i++;
			}while($fila = mysql_fetch_array($resultado));
			?>
			</tbody>
			</table>
			<?
		}
		else 
		{
		?>
		<div class="alert alert-info">
			<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
			No se encontraron datos.
		</div>
		<?	
		}
	}
	else 
	{	
		?>
		<div class="alert alert-danger">
			<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
			Error al consultar la informaci&oacute;n en la base de datos. <br><br><?php echo mysql_error(); ?>
		</div>
		<?
	}
}

if($_POST['actividad'] == "agregar")
{
	if($_POST['ano'] != "" && $_POST['curso'] != ""  && $_POST['institucion'] != ""  && $_POST['promovido'] != "" )
	{
		$sql = "insert into cursootrasinstituciones (idAlumno,ano, curso, institucion, promovido) 
				select ".$_POST['idAlumno'].",".$_POST['ano'].",'".$_POST['curso']."','".$_POST['institucion']."',".$_POST['promovido']." from dual
				where not exists (select 'x' from cursootrasinstituciones where idAlumno = ".$_POST['idAlumno']." and ano = ".$_POST['ano']." and curso = '".$_POST['curso']."' and institucion = '".$_POST['institucion']."' and promovido = ".$_POST['promovido'].")";	
		$resultado = $base->consultar($sql);
		if($resultado)
		{
			if(mysql_affected_rows() > 0)
			{
			?>
			<div class="alert alert-success">
				<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
				Registro almacenado correctamente.
			</div>
			<?				
			}
			else
			{
				?>
			<div class="alert alert-info">
				<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
				La informaci&oacute;n que quiere ingresar ya ex&iacute;ste.
			</div>
				<?
			}

		}
		else
		{
		?>	
		<div class="alert alert-warning">
			<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
			No se almacen&oacute; la informaci&oacute;n. 
		</div>
		<?			
		}

	}
	else
	{
		?>
		<div class="alert alert-warning">
			<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
			Por favor coloque todos los datos.
		</div>
		<?
	}
}

if($_POST['actividad'] == "eliminar")
{
if($_POST['idCursoOtrasInstituciones'])
{
		$sql = "delete from cursootrasinstituciones where idCursoOtrasInstituciones = ".$_POST['idCursoOtrasInstituciones'];
		$resultado = $base->consultar($sql);
		if($resultado)
		{
			if(mysql_affected_rows() > 0)
			{
			?>
			<div class="alert alert-success">
				<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
				Registro eliminado correctamente.
			</div>
			<?				
			}
			else
			{
				?>
			<div class="alert alert-info">
				<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
				No se econtraron registros con ese id.
			</div>
				<?
			}

		}
		else
		{
		?>	
		<div class="alert alert-warning">
			<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
			Problemas al eliminar los datos.
		</div>
		<?			
		}
}
else 
{	
?>
<div class="alert alert-warning">
	<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
	Por favor env&iacute;e el id que desea eliminar.
</div>
<?
}
}





?>