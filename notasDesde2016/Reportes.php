<?php
if (!isset($_SESSION)) {
  session_start();
}
$MM_authorizedUsers = "Administrador,Profesor";
$MM_donotCheckaccess = "false";

// *** Restrict Access To Page: Grant or deny access to this page
function isAuthorized($strUsers, $strGroups, $UserName, $UserGroup) { 
  // For security, start by assuming the visitor is NOT authorized. 
  $isValid = False; 

  // When a visitor has logged into this site, the Session variable MM_Username set equal to their username. 
  // Therefore, we know that a user is NOT logged in if that Session variable is blank. 
  if (!empty($UserName)) { 
    // Besides being logged in, you may restrict access to only certain users based on an ID established when they login. 
    // Parse the strings into arrays. 
    $arrUsers = Explode(",", $strUsers); 
    $arrGroups = Explode(",", $strGroups); 
    if (in_array($UserName, $arrUsers)) { 
      $isValid = true; 
    } 
    // Or, you may restrict access to only certain users based on their username. 
    if (in_array($UserGroup, $arrGroups)) { 
      $isValid = true; 
    } 
    if (($strUsers == "") && false) { 
      $isValid = true; 
    } 
  } 
  return $isValid; 
}

$MM_restrictGoTo = "error.php";
if (!((isset($_SESSION['MM_Username'])) && (isAuthorized("",$MM_authorizedUsers, $_SESSION['MM_Username'], $_SESSION['MM_UserGroup'])))) {   
  $MM_qsChar = "?";
  $MM_referrer = $_SERVER['PHP_SELF'];
  if (strpos($MM_restrictGoTo, "?")) $MM_qsChar = "&";
  if (isset($QUERY_STRING) && strlen($QUERY_STRING) > 0) 
  $MM_referrer .= "?" . $QUERY_STRING;
  $MM_restrictGoTo = $MM_restrictGoTo. $MM_qsChar . "accesscheck=" . urlencode($MM_referrer);
  header("Location: ". $MM_restrictGoTo); 
  exit;
}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title>:: Reportes ::</title>
<? include "header.php";?>
<script language="javascript" type="application/javascript">
function fReporteInformeValorativo()
	{
	mostrar_url('InformeValorativo.php','sinmenu=sinmenu','divReporteSeleccionado','get');
	}

function fReporteInformeValorativoCualitativo()
	{
	mostrar_url('InformeValorativoCualitativo.php','sinmenu=sinmenu','divReporteSeleccionado','get');
	}	

function fReporteInformeValorativoCualitativoCurso()
	{
	mostrar_url('InformeValorativoCualitativoCurso.php','sinmenu=sinmenu','divReporteSeleccionado','get');
	}	

	
function fReporteResumenValorativo()
	{
	mostrar_url('ResumenValorativo.php','sinmenu=sinmenu','divReporteSeleccionado','get');
	
	}	
	
function fReporteCursoAlumnoPromedio()
	{
	mostrar_url('ReporteCursoAlumnoPromedio.php','sinmenu=sinmenu','divReporteSeleccionado','get');
	}		
	
function fReporteCursoAlumnoPromedioListar()
	{
		var idCurso;
		idCurso =document.getElementById('idCurso');
		idCurso = idCurso.options[idCurso.selectedIndex].value;	
		mostrar_url('ReporteCursoAlumnoPromedioListar.php','idCurso='+idCurso,'divReporte','get');
	}
function fResumenValorativoListar()
	{
	var idCurso;
	idCurso =document.getElementById('idCurso');
	idCurso = idCurso.options[idCurso.selectedIndex].value;	
	if(NuloACero(idCurso) > 0)
		{
		mostrar_url('ResumenValorativoListar.php','idCurso='+idCurso,'divResumenValorativo','get');			
		}
	}
	

	function fInformeValorativoListarCurso()
	{
	var ano;
	ano = document.getElementById('ano').value;
	mostrar_url('Parametros.php','accion=cursos&ano='+ano,'divCurso','get');
	}
	
function fInformeValorativoListarAno()	
	{
	mostrar_url('Parametros.php','accion=anos','divAno','get');
	}	
	
function fInformeValorativoListarAlumno()
	{
	var idCurso;
	idCurso =document.getElementById('idCurso');
	idCurso = idCurso.options[idCurso.selectedIndex].value;	
	mostrar_url('Parametros.php','accion=alumnosCurso&idCurso='+idCurso,'divAlumno','get');
	}
	
function fInformeValorativoMostrarInforme()
	{
	//var idCurso,idAlumno,ano,pdf;
	var idCurso,idAlumno,ano;
	idCurso =document.getElementById('idCurso');	
	//pdf =document.getElementById('pdf');
	idCurso = idCurso.options[idCurso.selectedIndex].value;		
	idAlumno =document.getElementById('idAlumno');
	idAlumno = idAlumno.options[idAlumno.selectedIndex].value;
	periodo =document.getElementById('periodo');
	periodo = periodo.options[periodo.selectedIndex].value;	
	ano = document.getElementById('ano');
	ano = ano.options[ano.selectedIndex].text;
	if(idCurso > 0 && idAlumno > 0 && ano > 0 && periodo > 0)
		{
		//pdf.href = 'InformeValorativoListar.php?idCurso='+idCurso+'&idAlumno='+idAlumno+'&ano='+ano+'&periodo='+periodo+'&pdf=pdf';
		mostrar_url('InformeValorativoListar.php','idCurso='+idCurso+'&idAlumno='+idAlumno+'&ano='+ano+'&periodo='+periodo,'divInformeValorativo','get');			
		}
		else
		{
		alert('Debe seleccionar todos los criterios.');
		}
	}

function fInformeValorativoCualitativoMostrarInforme()
	{
	//var idCurso,idAlumno,ano,pdf;
	var idCurso,idAlumno,ano;
	idCurso =document.getElementById('idCurso');	
	//pdf =document.getElementById('pdf');
	idCurso = idCurso.options[idCurso.selectedIndex].value;		
	idAlumno =document.getElementById('idAlumno');
	idAlumno = idAlumno.options[idAlumno.selectedIndex].value;
	periodo =document.getElementById('periodo');
	periodo = periodo.options[periodo.selectedIndex].value;	
	ano = document.getElementById('ano');
	ano = ano.options[ano.selectedIndex].text;
	if(idCurso > 0 && idAlumno > 0 && ano > 0 && periodo > 0)
		{
		//pdf.href = 'InformeValorativoListar.php?idCurso='+idCurso+'&idAlumno='+idAlumno+'&ano='+ano+'&periodo='+periodo+'&pdf=pdf';
		mostrar_url('InformeValorativoCualitativoListar.php','idCurso='+idCurso+'&idAlumno='+idAlumno+'&ano='+ano+'&periodo='+periodo,'divInformeValorativo','get');			
		}
		else
		{
		alert('Debe seleccionar todos los criterios.');
		}
	}	
	
function fReporteAsignacionAcademica()
	{
		mostrar_url('ReporteAsignacionAcademica.php','','divReporteSeleccionado','get');
	}
	
function fReporteResumenDesempeno()
	{
		mostrar_url('ReporteResumenDesempeno.php','','divReporteSeleccionado','get');
	}
	

function fInformeValorativoListarAno()	
	{
	mostrar_url('Parametros.php','accion=anos','divAno','get');
	}		
	
	
</script>

<script language="javascript" type="text/javascript">

////Para el archivo ReporteAsignacionAcademica.php
function fMostrarCursos (ano)
	{
	mostrar_url('Parametros.php','accion=cursosReporteAsignacionAcademica&ano='+ano,'divCurso','get');
	}
	
function fReporteAsignacionAcademicaListar (curso)
	{
		if(NuloACero(curso) == 0)
			{
			alert('Por favor seleccione el curso.');
			}
			else
			{
			var ano;
			ano = document.getElementById('anos');
			ano = ano.options[ano.selectedIndex].text;
			mostrar_url('ReporteAsignacionAcademicaListar.php','ano='+ano+'&curso='+curso,'divReporteAsignacionAcademica','get');					
			}
	}
	
//Muestra los cursos para el reporte de resumen de desempeno
function fMostrarCursosResumenDesempeno (ano)
	{
	mostrar_url('Parametros.php','accion=cursosResumenDesempeno&ano='+ano,'divCurso','get');
	}
	
function fImpresionBoletin ()
	{
	mostrar_url('ReporteImpresionBoletin.php','','divReporteSeleccionado','get');						
	}

	function fImpresionBoletinFinal ()
	{
	mostrar_url('ReporteImpresionBoletinFinal.php','','divReporteSeleccionado','get');						
	}

	function fImpresionBoletinFinalAreas ()
	{
	mostrar_url('ReporteImpresionBoletinFinalAreas.php','','divReporteSeleccionado','get');						
	}
	
function fReporteImpresionBoletinListar()
	{
	var bimestre,idCurso;
	bimestre = document.getElementById('bimestre');
	if (NuloACero(bimestre.selectedIndex) > 0)
		{
		bimestre = bimestre.options[bimestre.selectedIndex].value;
		idCurso = document.getElementById('idCurso');
		if (NuloACero(idCurso.selectedIndex) > 0)
			{
			idCurso = idCurso.options[idCurso.selectedIndex].value;
			window.location ="ReporteImpresionBoletinListar.php?idCurso="+idCurso+"&bimestre="+bimestre+"&pdf=pdf";
			}	
			else
			{
			alert("Por favor seleccione un bimestre.");				
			}

		}	
		else
		{
		alert("Por favor seleccione un bimestre.");	
		}
	}

function fReporteImpresionBoletinCualitativoListar()
	{
	var bimestre,idCurso;
	bimestre = document.getElementById('bimestre');
	if (NuloACero(bimestre.selectedIndex) > 0)
		{
		bimestre = bimestre.options[bimestre.selectedIndex].value;
		idCurso = document.getElementById('idCurso');
		if (NuloACero(idCurso.selectedIndex) > 0)
			{
			idCurso = idCurso.options[idCurso.selectedIndex].value;
			window.location ="InformeValorativoCualitativoCursoListar.php?idCurso="+idCurso+"&bimestre="+bimestre+"&pdf=pdf";
			}	
			else
			{
			alert("Por favor seleccione un bimestre.");				
			}

		}	
		else
		{
		alert("Por favor seleccione un bimestre.");	
		}
	}

function fReporteImpresionBoletinFinalListar()
	{
	var bimestre,idCurso,libroFinal,ano;
	ano = document.getElementById('ano');
	ano = ano.options[ano.selectedIndex].value;
	idCurso = document.getElementById('idCurso');
	libroFinal = document.getElementById('libroFinal');
	if(libroFinal.checked)
		{
		libroFinal = 1;	
		}
		else
		{
		libroFinal = 0;	
		}
	
	if (NuloACero(idCurso.selectedIndex) > 0)
		{
		idCurso = idCurso.options[idCurso.selectedIndex].value;
		if(libroFinal === 0)
			{
			window.location ="ReporteImpresionBoletinFinalListar.php?idCurso="+idCurso+"&ano="+ano+"&bimestre=4&pdf=pdf";		
			}
			else
			{
			window.location ="ReporteImpresionBoletinFinalLibroFinalListar.php?idCurso="+idCurso+"&ano="+ano+"&bimestre=4&pdf=pdf";	
			}
		}	
		else
		{
		alert("Por favor seleccione un curso.");				
		}
	}	

	function fReporteImpresionBoletinFinalListarAreas()
	{
	var bimestre,idCurso,libroFinal,ano;
	ano = document.getElementById('ano');
	ano = ano.options[ano.selectedIndex].value;
	idCurso = document.getElementById('idCurso');
	libroFinal = document.getElementById('libroFinal');
	if(libroFinal.checked)
		{
		libroFinal = 1;	
		}
		else
		{
		libroFinal = 0;	
		}
	
	if (NuloACero(idCurso.selectedIndex) > 0)
		{
		idCurso = idCurso.options[idCurso.selectedIndex].value;
		if(libroFinal === 0)
			{
			window.location ="ReporteImpresionBoletinFinalListarAreas.php?idCurso="+idCurso+"&ano="+ano+"&bimestre=4&pdf=pdf";		
			}
			else
			{
			window.location ="ReporteImpresionBoletinFinalLibroFinalListarAreas.php?idCurso="+idCurso+"&ano="+ano+"&bimestre=4&pdf=pdf";	
			}
		}	
		else
		{
		alert("Por favor seleccione un curso.");				
		}
	}	
	
function fCargarAreaResumenDesempeno(idCurso)
	{
	mostrar_url('Parametros.php','accion=areasResumenDesempeno&idCurso='+idCurso,'divArea','get');
	}
	
function fMostrarReporteResumenDesempeno()
	{
	var ano,idCurso,idArea,bimestre,idMateria;
	ano = document.getElementById('anos');
	if (NuloACero(ano.selectedIndex) > 0)
		{
		ano = ano.options[ano.selectedIndex].value;	
		}
	idCurso = document.getElementById('idCurso');
	if (NuloACero(idCurso.selectedIndex) > 0)
		{
		idCurso = idCurso.options[idCurso.selectedIndex].value;
		}	
	idArea = document.getElementById('idArea');
	if (NuloACero(idArea.selectedIndex) > 0)
		{
		idArea = idArea.options[idArea.selectedIndex].value;	
		}		
	bimestre = document.getElementById('bimestre');
	if (NuloACero(bimestre.selectedIndex) > 0)
		{
		bimestre = bimestre.options[bimestre.selectedIndex].value;	
		}	
	idMateria = document.getElementById('idMateria');
	if (NuloACero(idMateria.selectedIndex) > 0)
		{
		idMateria = idMateria.options[idMateria.selectedIndex].value;	
		}		
	idProfesor = document.getElementById('idProfesor');
	if (NuloACero(idProfesor.selectedIndex) > 0)
		{
		idProfesor = idProfesor.options[idProfesor.selectedIndex].value;	
		}		
			
	mostrar_url('ReporteResumenDesempenoListar.php','ano='+ano+'&idCurso='+idCurso+'&idArea='+idArea+'&bimestre='+bimestre+'&idMateria='+idMateria+'&idProfesor='+idProfesor,'divReporteResumenDesempeno','get');	
	}
	
function fMostrarMateriasResumenDesempeno (idArea)
	{
	mostrar_url('Parametros.php','accion=materiasResumenDesempeno&idArea='+idArea,'divMateria','get');
	}	
	
function fReporteRecuperacionFinal()
	{
	mostrar_url('RecuperacionFinal.php','','divReporteSeleccionado','get');
	}	

var filaSeleccionada = 0;

function sombrearTr(id)
	{
	if(filaSeleccionada > 0)
		{
		document.getElementById('tr_'+filaSeleccionada).className = "";
		}
	document.getElementById('tr_'+id).className = "trseleccionado";
	filaSeleccionada = id;
	}

function fRecuperacionFinalVer()
	{
	var ano,idCurso,idMateria,curso,materia;	
	ano = document.getElementById('ano').value;
	idCurso = document.getElementById('idCurso');

	if (NuloACero(idCurso.selectedIndex) > 0)
		{
		curso = idCurso.options[idCurso.selectedIndex].text;
		idCurso = idCurso.options[idCurso.selectedIndex].value;
		}		
		else
		{
			idCurso = 0;
			curso = '';
		}	

	idMateria = document.getElementById('idMateria');
	if (NuloACero(idMateria.selectedIndex) > 0)
		{
		
		materia = idMateria.options[idMateria.selectedIndex].text;
		idMateria = idMateria.options[idMateria.selectedIndex].value;	
		}		
		else
		{
			idMateria = 0;
			materia = '';
		}
	mostrar_url('RecuperacionFinalListar.php','ano='+ano+'&materia='+materia+'&curso='+curso+'&idCurso='+idCurso+'&idMateria='+idMateria,'divRecuperacionFinalReporte','get');
	}		
	
	function actualizarRecuperacionFinal(idBoletin,recuperacionFinal,id)
		{
			if(recuperacionFinal >= 0 && recuperacionFinal <= 5)
			{
			var respuesta = confirm("�Desea guardar los cambios?");		
			if (respuesta == true)
				{
				mostrar_url('RecuperacionFinalActualizar.php','idBoletin='+idBoletin+'&recuperacionFinal='+recuperacionFinal+'&id='+id+'&MM_update=form1','info_'+id,'get');					
				}		
			}
			else
			{
			alert("La nota no puede ser mayor a 5 ni menor a 0.");	
			}
		
		}
		
	function fReporteImpresionBoletinFinalMostrarCursos(ano)
		{
			if(ano > 0)
				{
				mostrar_url('cursoListar.php','ano='+ano,'divListadoCursos','get');					
				}

		}


</script>
<link href="css/bootstrap.min.css" rel="stylesheet">
</head>

<body>
<? include "menu.php"; ?>
<div class="container">
<?
if($_SESSION['MM_UserGroup'] == 'Padre')
{
?>
<a href="#"  onclick="javascript:fReporteInformeValorativo();">Informe Valorativo</a>
<a href="#"  onclick="javascript:fReporteInformeValorativoCualitativo();">Informe Valorativo Cualitativo</a>
<?
}
else
{
?>
<a href="#"  onclick="javascript:fReporteInformeValorativo();">Informe Valorativo</a>
<a href="#"  onclick="javascript:fImpresionBoletin();">Impresi&oacute;n de boletines</a>
<a href="#"  onclick="javascript:fImpresionBoletinFinal();">Impresi&oacute;n de boletines finales</a>
<a href="#"  onclick="javascript:fImpresionBoletinFinalAreas();">Impresi&oacute;n de boletines finales &aacute;reas</a>
<a href="#"  onclick="javascript:fReporteAsignacionAcademica();">Asignaci&oacute;n Academica</a>
<a href="#"  onclick="javascript:fReporteResumenValorativo();">Resumen valorativo</a>
<br><br>

<a href="#"  onclick="javascript:fReporteCursoAlumnoPromedio();">Promedios por curso</a>
<a href="#"  onclick="javascript:fReporteResumenDesempeno();">Resumen de Desempe&ntilde;o</a>
<a href="#"  onclick="javascript:fReporteInformeValorativoCualitativo();">Informe Valorativo Cualitativo</a>
<a href="#"  onclick="javascript:fReporteInformeValorativoCualitativoCurso();">Informe Valorativo Cualitativo Imprimir</a>
<?
}
?>

<hr />

<div id="divReporteSeleccionado"></div>
</body>
</div>
</html>
