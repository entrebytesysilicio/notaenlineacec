<?php require_once('Connections/MySQL.php'); ?>
<?php
include "InformeValorativoClase.php";
if (!isset($_SESSION)) {
  session_start();
}
$MM_authorizedUsers = "Administrador,Profesor,Padre";
$MM_donotCheckaccess = "false";

// *** Restrict Access To Page: Grant or deny access to this page
function isAuthorized($strUsers, $strGroups, $UserName, $UserGroup) { 
  // For security, start by assuming the visitor is NOT authorized. 
  $isValid = False; 

  // When a visitor has logged into this site, the Session variable MM_Username set equal to their username. 
  // Therefore, we know that a user is NOT logged in if that Session variable is blank. 
  if (!empty($UserName)) { 
    // Besides being logged in, you may restrict access to only certain users based on an ID established when they login. 
    // Parse the strings into arrays. 
    $arrUsers = Explode(",", $strUsers); 
    $arrGroups = Explode(",", $strGroups); 
    if (in_array($UserName, $arrUsers)) { 
      $isValid = true; 
    } 
    // Or, you may restrict access to only certain users based on their username. 
    if (in_array($UserGroup, $arrGroups)) { 
      $isValid = true; 
    } 
    if (($strUsers == "") && false) { 
      $isValid = true; 
    } 
  } 
  return $isValid; 
}

$MM_restrictGoTo = "error.php";
if (!((isset($_SESSION['MM_Username'])) && (isAuthorized("",$MM_authorizedUsers, $_SESSION['MM_Username'], $_SESSION['MM_UserGroup'])))) {   
  $MM_qsChar = "?";
  $MM_referrer = $_SERVER['PHP_SELF'];
  if (strpos($MM_restrictGoTo, "?")) $MM_qsChar = "&";
  if (isset($QUERY_STRING) && strlen($QUERY_STRING) > 0) 
  $MM_referrer .= "?" . $QUERY_STRING;
  $MM_restrictGoTo = $MM_restrictGoTo. $MM_qsChar . "accesscheck=" . urlencode($MM_referrer);
  header("Location: ". $MM_restrictGoTo); 
  exit;
}
?>
<?php
if (!function_exists("GetSQLValueString")) {
function GetSQLValueString($theValue, $theType, $theDefinedValue = "", $theNotDefinedValue = "") 
{
  if (PHP_VERSION < 6) {
    $theValue = get_magic_quotes_gpc() ? stripslashes($theValue) : $theValue;
  }

  $theValue = function_exists("mysql_real_escape_string") ? mysql_real_escape_string($theValue) : mysql_escape_string($theValue);

  switch ($theType) {
    case "text":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;    
    case "long":
    case "int":
      $theValue = ($theValue != "") ? intval($theValue) : "NULL";
      break;
    case "double":
      $theValue = ($theValue != "") ? doubleval($theValue) : "NULL";
      break;
    case "date":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;
    case "defined":
      $theValue = ($theValue != "") ? $theDefinedValue : $theNotDefinedValue;
      break;
  }
  return $theValue;
}
}

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title>:: Informe Valorativo ::</title>
<? include "header.php";?>
<script language="javascript" type="text/javascript">
function fInformeValorativoListarCurso()
	{
	var ano;
	ano = document.getElementById('ano').value;
	mostrar_url('Parametros.php','accion=cursos&ano='+ano,'divCurso','get');
	}
	
function fInformeValorativoListarAno()	
	{
	mostrar_url('Parametros.php','accion=anos','divAno','get');
	}	
	
function fInformeValorativoListarAlumno()
	{
	var idCurso;
	idCurso =document.getElementById('idCurso');
	idCurso = idCurso.options[idCurso.selectedIndex].value;	
	mostrar_url('Parametros.php','accion=alumnosCurso&idCurso='+idCurso,'divAlumno','get');
	}
	
function fInformeValorativoMostrarInforme()
	{
	//var idCurso,idAlumno,ano,pdf;
	var idCurso,idAlumno,ano;
	idCurso =document.getElementById('idCurso');	
	//pdf =document.getElementById('pdf');
	idCurso = idCurso.options[idCurso.selectedIndex].value;		
	idAlumno =document.getElementById('idAlumno');
	idAlumno = idAlumno.options[idAlumno.selectedIndex].value;
	periodo =document.getElementById('periodo');
	periodo = periodo.options[periodo.selectedIndex].value;	
	ano = document.getElementById('ano');
	ano = ano.options[ano.selectedIndex].text;
	if(idCurso > 0 && idAlumno > 0 && ano > 0 && periodo > 0)
		{
	//	pdf.href = 'InformeValorativoListar.php?idCurso='+idCurso+'&idAlumno='+idAlumno+'&ano='+ano+'&periodo='+periodo+'&pdf=pdf';
		mostrar_url('InformeValorativoListar.php','idCurso='+idCurso+'&idAlumno='+idAlumno+'&ano='+ano+'&periodo='+periodo,'divInformeValorativo','get');			
		}
		else
		{
		alert('Debe seleccionar todos los criterios.');
		}
	}
</script>
</head>

<body onload="javascript:fInformeValorativoListarAno();">
<? if(!$_GET['sinmenu'])
		{
			include "menu.php"; 
		}
		?>
    <form name="formInformeValorativo" action="InformeValorativoListar.php" method="get">
<table width="100%">
<thead>
<tr><td colspan="9">Informe valorativo</td></tr>
</thead>
<tr>

	<td>A&ntilde;o:</td>
  <td><div id="divAno"><select name="ano" id="ano" onchange="javascript:fInformeValorativoListarCurso();"><option></option>
  
  
  <option value="2010" >2010</option>
    <option value="2011" >2011</option>
      <option value="2012" >2012</option>
        <option value="2013" >2013</option>
          <option value="2014" >2014</option>
            <option value="2015" >2015</option>
              <option value="2016" >2016</option>
                <option value="2017" >2017</option>
                  <option value="2018" >2018</option>
                    <option value="2019" >2019</option>
                      <option value="2020" >2020</option>
  
  
  </select></div></td>
	<td>Curso:</td>
  <td><div id="divCurso"><select name="idCurso" id="idCurso"><option></option></select></div></td>
  <td>Alumno:</td>
  <td><div id="divAlumno"><select name="idAlumno" id="idAlumno"><option></option></select></div></td>
  <td>Periodo:</td>
  <td><select name="periodo" id="periodo" onchange="javascript:fInformeValorativoMostrarInforme();"><option></option><option value="1">Primero</option><option value="2">Segundo</option><option value="3">Tercero</option><option value="4">Cuarto</option><option value="5">Ultimo</option></select></td>
  <td><input type="hidden" name="sinmenu" value="sinmenu" /><input type="hidden" name="pdf" value="pdf" /><input type="submit" name="Imprimir" value="Imprimir" /><!--<a href="" id="pdf">Imprimir</a>--></td>
 
</tr>
<thead>
<tr><td colspan="9"></td></tr>
</thead>
</table> </form>
<br />
<div id="divInformeValorativo"></div>
</body>
</html>
